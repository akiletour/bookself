![Bookself Logo](client/src/assets/images/icons/bookself-logo.svg)

[![Minimum Node Version](https://img.shields.io/badge/node-%3E%3D%2018-brightgreen)](https://nodejs.org/en/)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=micamerzeau_bookself&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=micamerzeau_bookself)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=micamerzeau_bookself&metric=coverage)](https://sonarcloud.io/summary/new_code?id=micamerzeau_bookself)

**Bookself allows you to record every piece of culture that you've seen, played, heard, or read, in one place. Never forget !**

## :computer: Stack

- `Node` : v18+
- `Express` : v4.18.2
- `Angular` : v16.2+
- `Typescript` : v5.2+
- `MongoDB` : v6.x
- `Mongoose` : v7.x
- `Handlebars` : v7.1+
- `Tailwind` : v3.3+
- `Mocha` : v10.2+

## :pencil2: Configuration

Bookself uses external APIs to retrieve works.

First of all, you need to specify the required variables for the project. Create a `.env` file at the root of the project.

- `CLIENT_URL` : Your frontend url, used in the backend (for reset password links, for exemple). Default `http://localhost:4200`
- `SERVER_IMAGES_URL` : Backend images url, used in emails
- `SERVER_PORT` : Port used to launch the app (Default/Fallback to `4200`)
- `TEST_PORT` : Port used to launch the app in unit tests. Must be different from your app `PORT` to run concurrently.
- `DB_USER` : Your MongoDB username
- `DB_PASSWORD` : Your MongoDB password
- `DB_NAME` : Your MongoDB database name
- `DB_TEST_NAME` : Your MongoDB test database name, used for unit tests
- `EMAIL_FROM` : The email sender
- `ACCESS_TOKEN_SECRET` : The secret used to generate your access JWT
- `REFRESH_TOKEN_SECRET` : The secret used to generate your refresh JWT
- `TMDB_PROVIDER_KEY` : TMDB API key.
- `IGDB_PROVIDER_ID` : Client ID of your Twitch app to connect to IGDB API.
- `IGDB_PROVIDER_SECRET` : Client Secret of your Twitch app to connect to IGDB API.
- `COMICVINE_PROVIDER_KEY` : Comic Vine API key.
- `POSTMARK_TOKEN` : POSTMARK token to send emails.

_The Atlas MongoDB url is directly pasted in the files. It will be improved at some point._

### Front

Angular uses his own environment variables in `client/src/environments` folder. Create an `environment.ts file` and fill the required variables. For production, create an `environment.prod.ts` file.

```
export const environment = {
  production: false,
  apiDomain: "http://localhost:4300",
  imagesDomain: "http://localhost:4300/images",
};
```

## :zap: Usage

Install dependencies

```
npm i
cd client
npm i
cd ../server
npm i
```

### Launch the app :rocket:

`npm start` : Launch Express server and Angular client

`npm run server` : Launch Express server

`npm run client` : Launch Angular client

### Building app :hammer_and_wrench:

In Angular /client folder :

`npm start` : Serve frontend on default port (`4200`). Equivalent to `npm run client` inside root folder.

`npm run build` : Build app with production flag

### Testing :shield:

#### Linters

**Husky** is used to prevent commit if linters fail.

`npm run lint` : Run Eslint & Stylelint

`npm run lint:js` : Run Eslint

`npm run lint:css` : Run Stylelint

#### Unit tests

`npm run test` : Run Mocha on Express backend and Karmine on Angular frontend

`npm run cover:client` : Run Karmine on Angular frontend with code coverage

`npm run cover:server` : Run Mocha on Express backend with code coverage

## :email: Emails

[Maildev](https://github.com/maildev/maildev) is used to send emails during develoment. Once you started the Express server, access MailDev webapp at `http://localhost:1080`. Port `1025` is used to send emails.

## :notebook: API Documentation

**Redocly** is used based on OpenApi yaml files located in `server/src/api/docs`.
It can be accessed on `http://localhost:{SERVER_PORT}/doc`.

Use `npm run redocly` to update the documentation if changes are made to the yaml files.

## :fire: CI/CD

Gitlab-CI is used to analyze code quality and code coverage report for **Sonarqube**. It uses `sonar` functions in package.json to generate lcov reports.
