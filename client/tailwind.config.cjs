// https://tailwindcss.com/docs/configuration
module.exports = {
  mode: "jit",
  darkMode: "class",
  content: ["./src/**/*.{html,ts}"],
  theme: {
    spacing: Object.assign(
      {},
      ...Object.entries(Array.from(Array(46).keys())).map(([k, v]) => ({
        [`${k}`]: `${v * 8}px`,
      }))
    ),
    colors: {
      transparent: "transparent",
      form: {
        error: "#A10000",
        errorLight: "#FF1A1A",
        errorDark: "#6F0000",
        successLight: "#89DA4C",
        success: "#419602",
      },
      yellow: {
        dark: "#EB7F00",
        light: "#FFE603",
      },
      works: {
        movies: "#EF9401",
        series: "#00A3FF",
        seriesanimation: "#00B65E",
        games: "#FF2E60",
        mangas: "#E5EA01",
        comics: "#2C0894",
        books: "#01EAA4",
        music: "#9F0000",
        history: "#A32DA6",
      },
      black: "#323232",
      blacker: "#121212",
      "black-dark": "#1B191F",
      blackest: "#000000",
      grey: "#797979",
      greyer: "#565656",
      borders: {
        dark: "#D6D6D6",
        darker: "#3A393D",
        light: "#E5E5E5",
      },
      whitegrey: "#F0F0F0",
      whiteshade: "#FAFAFA",
      white: "#FFFFFF",
    },
    fontSize: {
      xs: "10px",
      sm: "12px",
      base: "14px",
      m: "20px",
      lg: "22px",
      xl: "32px",
    },
    extend: {
      dropShadow: {
        base: "1px 2px 2px rgba(0, 0, 0, 0.25)",
        frame: "0px 0px 10px rgba(0, 0, 0, 0.25)",
        white: "1px 2px 2px rgba(255, 255, 255, 1)",
      },
      backgroundImage: {
        "body-pattern": "url('/assets/images/bg.jpg')",
        search: "url('/assets/images/icons/search.svg')",
        "search-white": "url('/assets/images/icons/search-white.svg')",
      },
      fontFamily: {
        base: ["Montserrat", "sans-serif"],
      },
      screens: {
        grid: "1440px",
        mobile: "1220px",
      },
      keyframes: {
        fillCircle: {
          "0%": {transform: "rotate(0deg)"},
          "100%": {transform: "rotate(var(--rotation-angle))"},
        },
      },
      animation: {
        fillCircle: "fillCircle 3s ease-in-out",
      },
    },
  },
  plugins: [
    function ({addBase, theme}) {
      function extractColorVars(colorObj, colorGroup = "") {
        return Object.keys(colorObj).reduce((vars, colorKey) => {
          const value = colorObj[colorKey];

          const newVars =
            typeof value === "string"
              ? {[`--color${colorGroup}-${colorKey}`]: value}
              : extractColorVars(value, `-${colorKey}`);

          return {...vars, ...newVars};
        }, {});
      }

      addBase({
        ":root": extractColorVars(theme("colors")),
      });
    },
  ],
};
