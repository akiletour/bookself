self.addEventListener("fetch", (event) => {
  if (event.request.method === "GET") {
    const url = new URL(event.request.url);
    if (url.origin === location.origin) {
      event.respondWith(
        caches.open("bookself").then((cache) => {
          return cache.match(event.request).then((response) => {
            const fetchPromise = fetch(event.request).then(
              (networkResponse) => {
                if (event.request.url.indexOf("http") === 0) {
                  cache.put(event.request, networkResponse.clone());
                }
                return networkResponse;
              }
            );

            return response || fetchPromise;
          });
        })
      );
    } else if (
      event.request.destination === "image" &&
      url.origin.endsWith(location.hostname)
    ) {
      event.respondWith(
        caches.open("bookself-images").then((cache) => {
          return cache.match(event.request).then((response) => {
            if (response) {
              return response;
            }

            return fetch(event.request).then((networkResponse) => {
              cache.put(event.request, networkResponse.clone());
              return networkResponse;
            });
          });
        })
      );
    }
  } else {
    event.respondWith(fetch(event.request));
  }
});
