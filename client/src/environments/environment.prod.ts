export const environment = {
  production: true,
  apiDomain: "https://api.bookself.co",
  imagesDomain: "https://api.bookself.co/images",
};
