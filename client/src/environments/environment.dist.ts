export const environment = {
  production: false,
  apiDomain: "http://localhost:4300",
  imagesDomain: "http://localhost:4300/images",
};
