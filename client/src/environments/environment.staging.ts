export const environment = {
  production: true,
  apiDomain: "https://api.bookself.inr.dev",
  imagesDomain: "https://api.bookself.inr.dev/images",
};
