import {Component, OnDestroy, OnInit} from "@angular/core";
import {AuthService} from "./modules/auth/services/auth.service";
import {User} from "./modules/auth/models/User";
import {Subscription} from "rxjs";
import {ScreenSizeService} from "./modules/shared/services/screenSize.service";
import {environment} from "src/environments/environment";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
})
export class AppComponent implements OnInit, OnDestroy {
  public logged: boolean = false;
  public user?: User;
  public isMobile: boolean = false;
  private subscriptions: Subscription[] = [];
  public isProd = environment.production;
  public online: boolean = navigator.onLine;

  constructor(
    private AuthService: AuthService,
    private screenSizeService: ScreenSizeService
  ) {
    window.addEventListener("online", this.handleOnlineEvent);
    window.addEventListener("offline", this.handleOfflineEvent);

    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.logged = true;
          this.user = user;
          const root = document.documentElement;
          root.className = `${user.theme}`;
        } else {
          this.logged = false;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const screenSizeSubscription = this.screenSizeService.isMobile$.subscribe(
      (isMobile) => {
        this.isMobile = isMobile;
      }
    );
    this.subscriptions.push(screenSizeSubscription);
  }

  private handleOnlineEvent = () => {
    this.online = true;
  };

  private handleOfflineEvent = () => {
    this.online = false;
  };

  ngOnDestroy() {
    window.removeEventListener("online", this.handleOnlineEvent);
    window.removeEventListener("offline", this.handleOfflineEvent);
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
