import {Component, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {List, User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {CacheService} from "src/app/modules/shared/services/cache.service";
import {ListsService} from "src/app/modules/shared/services/list.service";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {ScreenSizeService} from "src/app/modules/shared/services/screenSize.service";

@Component({
  selector: "app-collections-list",
  templateUrl: "./collections-list.component.html",
})
export class CollectionsListComponent implements OnInit, OnDestroy {
  public user!: User;
  public isLoading: boolean = true;
  public lists: List[] = [];
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;

  constructor(
    private AuthService: AuthService,
    private listsService: ListsService,
    private modalService: ModalService,
    private cacheService: CacheService,
    private screenSizeService: ScreenSizeService
  ) {
    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);

    this.checkCacheFirst();

    const listsUpdatedSubscription = this.listsService.listsUpdated$.subscribe(
      () => {
        const listsSubscription = this.listsService
          .getLists(this.user._id)
          .subscribe((lists: List[] | null) => {
            if (lists) {
              this.lists = lists;
            } else {
              this.lists = [];
            }
            // Reassign lists to generate onChanges in child component
            this.lists = this.lists.slice();
          });
        this.subscriptions.push(listsSubscription);
      }
    );
    this.subscriptions.push(listsUpdatedSubscription);
  }

  fetchLists() {
    const listsSubscription = this.listsService
      .getLists(this.user._id)
      .subscribe((lists: List[] | null) => {
        if (lists) {
          this.lists = lists;
        } else {
          this.lists = [];
        }
        this.isLoading = false;
      });
    this.subscriptions.push(listsSubscription);
  }

  checkCacheFirst() {
    const cachedData = this.cacheService.getFromCache("lists");
    if (cachedData) {
      this.lists = cachedData as List[];
      this.isLoading = false;
    } else {
      this.fetchLists();
    }
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
