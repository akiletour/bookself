import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Subscription, fromEvent, throttleTime} from "rxjs";
import {StrictWorksTypes, WorksLabels} from "src/app/enums/Works.enum";
import {WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {CacheService} from "src/app/modules/shared/services/cache.service";
import {ConfigService} from "src/app/modules/shared/services/config.service";
import {WorksService} from "src/app/modules/shared/services/works.service";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
})
export class SearchComponent implements OnInit, OnDestroy {
  public user!: User;
  public term!: string;
  works: WorksModel[] = [];
  workTypes = Object.values(StrictWorksTypes);
  public isLoading: boolean = true;
  public sorting: {sortBy: string; order: string} = {
    sortBy: "discoveryDate",
    order: "desc",
  };
  private page: number = 1;
  private totalPages?: number;
  private subscriptions: Subscription[] = [];
  private limit: number;

  constructor(
    private route: ActivatedRoute,
    private worksService: WorksService,
    private AuthService: AuthService,
    private configService: ConfigService,
    private cacheService: CacheService
  ) {
    this.limit = this.configService.limit;

    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const routeSubscription = this.route.params.subscribe((params) => {
      this.term = params["term"];
      console.log(this.term);

      // Reset loading, filters & sorting
      this.isLoading = true;
      this.sorting = {
        sortBy: "discoveryDate",
        order: "desc",
      };

      this.checkCacheFirst(true);
    });
    this.subscriptions.push(routeSubscription);

    const scroll$ = fromEvent(window, "scroll");
    const scrollSubscription = scroll$
      .pipe(throttleTime(100))
      .subscribe(() => this.onScroll());
    this.subscriptions.push(scrollSubscription);

    this.checkCacheFirst();

    const worksUpdatedSubscription = this.worksService.worksUpdated$.subscribe(
      () => {
        this.worksService
          .getWorks(this.user._id, {
            query: this.term,
            page: 1,
            sortBy: this.sorting.sortBy,
            order: this.sorting.order,
            limit: this.limit * this.page,
          })
          .subscribe((response) => {
            const works = response?.data as WorksModel[];
            this.works = works;
            // Reassign works to generate onChanges in child component
            this.works = this.works.slice();
          });
      }
    );
    this.subscriptions.push(worksUpdatedSubscription);
  }

  onScroll = (): void => {
    // Load more works if we approach page bottom
    if (
      window.innerHeight + window.scrollY * 1.1 >= document.body.offsetHeight &&
      this.totalPages &&
      this.page < this.totalPages
    ) {
      this.page++;
      this.checkCacheFirst();
    }
  };

  onSortingChanged(newSorting: {sortBy: string; order: string}) {
    this.sorting = newSorting;
    this.page = 1;
    this.checkCacheFirst();
  }

  fetchWorks(
    query: string = "",
    page: number = 1,
    sortBy: string = "discoveryDate",
    order: string = "desc"
  ) {
    const worksSubscription = this.worksService
      .getWorks(this.user._id, {
        query,
        page,
        sortBy,
        order,
      })
      .subscribe((response) => {
        if (response) {
          const works = response?.data as WorksModel[];
          if (response?.pagination.totalPages) {
            this.totalPages = response?.pagination.totalPages;
          }

          if (page > 1) {
            this.works = [...this.works, ...works];
          } else {
            this.works = works;
            // Reassign works to generate onChanges in child component
            this.works = this.works.slice();
          }
        }

        this.isLoading = false;
      });
    this.subscriptions.push(worksSubscription);
  }

  public getWorkLabel(key: string): string {
    return WorksLabels[key as keyof typeof WorksLabels] || key;
  }

  checkCacheFirst(init: boolean = false): void {
    const cachedData = this.cacheService.getFromCache(
      `search_${this.term}_${this.page}_${this.sorting.sortBy}_${this.sorting.order}`
    );
    if (cachedData) {
      this.works = cachedData as WorksModel[];
      this.isLoading = false;
    } else {
      if (init) {
        this.works = []; // Reset works
      }
      this.fetchWorks(
        this.term,
        this.page,
        this.sorting.sortBy,
        this.sorting.order
      );
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
