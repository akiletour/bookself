import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {UpdatedWork, WorksModel} from "src/app/models/WorksModel";
import {WorksService} from "src/app/modules/shared/services/works.service";
import {AuthService} from "../../modules/auth/services/auth.service";
import {User} from "src/app/modules/auth/models/User";
import {WorksLabels, WorksTypes} from "src/app/enums/Works.enum";
import {Subscription, fromEvent, throttleTime} from "rxjs";
import {ConfigService} from "src/app/modules/shared/services/config.service";
import {CacheService} from "src/app/modules/shared/services/cache.service";

@Component({
  selector: "app-works-list",
  templateUrl: "./works-list.component.html",
})
export class WorksListComponent implements OnInit, OnDestroy {
  public user!: User;
  public hasWorks: boolean = false;
  works: WorksModel[] = [];
  workType!: WorksTypes;
  public isLoading: boolean = true;
  public filter: string = "All";
  public sorting: {sortBy: string; order: string} = {
    sortBy: "discoveryDate",
    order: "desc",
  };
  private page: number = 1;
  private totalPages?: number;
  private subscriptions: Subscription[] = [];
  private limit: number;

  constructor(
    private route: ActivatedRoute,
    private worksService: WorksService,
    private AuthService: AuthService,
    private configService: ConfigService,
    private cacheService: CacheService
  ) {
    this.limit = this.configService.limit;

    const authSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(authSubscription);
  }

  ngOnInit() {
    const scroll$ = fromEvent(window, "scroll");
    const scrollSubscription = scroll$
      .pipe(throttleTime(100))
      .subscribe(() => this.onScroll());
    this.subscriptions.push(scrollSubscription);

    const routeSubscription = this.route.params.subscribe((params) => {
      this.workType = params["workType"];
      // Reset loading, filters & sorting
      this.isLoading = true;
      this.hasWorks = false;
      this.sorting = {
        sortBy: "discoveryDate",
        order: "desc",
      };

      if (params["filter"]) {
        this.filter =
          params["filter"].charAt(0).toUpperCase() + params["filter"].slice(1);
      } else {
        this.filter = "All";
      }

      this.checkCacheFirst(true);
    });
    this.subscriptions.push(routeSubscription);

    const worksUpdatedSubscription = this.worksService.worksUpdated$.subscribe(
      (update: UpdatedWork) => {
        if (this.workType === update.type) {
          this.worksService
            .getWorksByType(this.user._id, this.workType, {
              page: 1,
              filter: this.filter,
              sortBy: this.sorting.sortBy,
              order: this.sorting.order,
              limit: this.limit * this.page,
            })
            .subscribe((response) => {
              if (response && response.data) {
                this.hasWorks = true;

                const works = response?.data as WorksModel[];
                this.works = works;
                // Reassign works to generate onChanges in child component
                this.works = this.works.slice();
              } else {
                this.works = [];

                if (this.filter === "All") {
                  this.hasWorks = false;
                }
              }
            });
        }
      }
    );
    this.subscriptions.push(worksUpdatedSubscription);
  }

  onScroll = (): void => {
    // Load more works if we approach page bottom
    if (
      window.innerHeight + window.scrollY * 1.1 >= document.body.offsetHeight &&
      this.totalPages &&
      this.page < this.totalPages
    ) {
      this.page++;
      this.checkCacheFirst();
    }
  };

  onFilterChanged(newFilter: string) {
    this.filter = newFilter;
    this.page = 1;
    this.checkCacheFirst();
  }

  onSortingChanged(newSorting: {sortBy: string; order: string}) {
    this.sorting = newSorting;
    this.page = 1;
    this.checkCacheFirst();
  }

  fetchWorks(
    page: number = 1,
    filter: string = "All",
    sortBy: string = "discoveryDate",
    order: string = "desc"
  ) {
    const worksSubscription = this.worksService
      .getWorksByType(this.user._id, this.workType, {
        page,
        filter,
        sortBy,
        order,
      })
      .subscribe((response) => {
        if (response && response.data.length > 0) {
          const works = response?.data as WorksModel[];
          this.hasWorks = true;
          if (response?.pagination.totalPages) {
            this.totalPages = response?.pagination.totalPages;
          }

          if (page > 1) {
            this.works = [...this.works, ...works];
          } else {
            this.works = works;
            // Reassign works to generate onChanges in child component
            this.works = this.works.slice();
          }
        } else {
          this.works = [];
          this.page = 1;
          this.totalPages = 1;
        }
        this.isLoading = false;
      });
    this.subscriptions.push(worksSubscription);
  }

  public getWorkLabel(key: string): string {
    return WorksLabels[key as keyof typeof WorksLabels] || key;
  }

  checkCacheFirst(init: boolean = false): void {
    const cachedData = this.cacheService.getFromCache(
      `${this.workType}_${this.page}_${this.filter}_${this.sorting.sortBy}_${this.sorting.order}`
    );
    if (cachedData) {
      this.works = cachedData as WorksModel[];
      this.hasWorks = true;
      this.isLoading = false;
    } else {
      if (init) {
        this.works = []; // Reset works
      }
      this.fetchWorks(
        this.page,
        this.filter,
        this.sorting.sortBy,
        this.sorting.order
      );
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
