import {Component, OnDestroy, OnInit} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";
import {AuthService} from "../../modules/auth/services/auth.service";
import {WorksTypes} from "src/app/enums/Works.enum";
import {UpdatedWork, WorksModel} from "src/app/models/WorksModel";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {WorksService} from "src/app/modules/shared/services/works.service";
import {PaginatedResponse} from "src/app/modules/shared/interfaces/ApiResponse.interface";
import {Subscription} from "rxjs";
import {CacheService} from "src/app/modules/shared/services/cache.service";
import {ScreenSizeService} from "src/app/modules/shared/services/screenSize.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit, OnDestroy {
  public user!: User;
  public lists: Array<{
    type: WorksTypes;
    title: string;
    link: string;
    linkLabel: string;
    addLabel: string;
  }>;
  public worksDatas: {[key in WorksTypes]?: Array<WorksModel>} = {};
  public isLoading: boolean = true;
  private subscriptions: Subscription[] = [];
  public isLaptop: boolean = false;
  public isTablet: boolean = false;
  public isMobile: boolean = false;
  public worksToFetch: number = 4;
  public worksToDisplay: number = 4;

  constructor(
    private AuthService: AuthService,
    private worksService: WorksService,
    private modalService: ModalService,
    private cacheService: CacheService,
    private screenSizeService: ScreenSizeService
  ) {
    this.lists = [
      {
        type: WorksTypes.Movie,
        title: "My latest movies",
        link: "/works/movies",
        linkLabel: "All my movies",
        addLabel: "Add a movie",
      },
      {
        type: WorksTypes.Series,
        title: "My latest series",
        link: "/works/series",
        linkLabel: "All my series",
        addLabel: "Add a series",
      },
      {
        type: WorksTypes.Game,
        title: "My latest games",
        link: "/works/games",
        linkLabel: "All my games",
        addLabel: "Add a game",
      },
      {
        type: WorksTypes.Manga,
        title: "My latest mangas",
        link: "/works/mangas",
        linkLabel: "All my mangas",
        addLabel: "Add a manga",
      },
      {
        type: WorksTypes.Comic,
        title: "My latest comics",
        link: "/works/comics",
        linkLabel: "All my comics",
        addLabel: "Add a comic",
      },
    ];

    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (!this.user && user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);

    const worksUpdatedSubscription = this.worksService.worksUpdated$.subscribe(
      (update: UpdatedWork) => {
        const worksSubscription = this.worksService
          .getWorksByType(this.user._id, update.type, {
            limit: this.worksToFetch,
          })
          .subscribe({
            next: (response: PaginatedResponse | null) => {
              const works = response?.data as WorksModel[];

              if (works && works.length > 0) {
                // Spread operator to change object reference & trigger changes in dashboard-stats
                this.worksDatas = {...this.worksDatas};
                this.worksDatas[update.type] = works;
              }
            },
          });
        this.subscriptions.push(worksSubscription);
      }
    );
    this.subscriptions.push(worksUpdatedSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
    const screenSizeTabletSubscription =
      this.screenSizeService.isTablet$.subscribe((isTablet) => {
        this.isTablet = isTablet;
      });
    this.subscriptions.push(screenSizeTabletSubscription);
    const screenSizeLaptopSubscription =
      this.screenSizeService.isLaptop$.subscribe((isLaptop) => {
        this.isLaptop = isLaptop;
      });
    this.subscriptions.push(screenSizeLaptopSubscription);

    if (this.isMobile) {
      this.worksToDisplay = 2;

      if (this.isTablet) {
        this.worksToDisplay = 3;
      }
    }

    let i = 0;

    for (const list of this.lists) {
      i++;

      const cachedData = this.cacheService.getFromCache(
        `${list.type}_dashboard`
      );
      if (cachedData) {
        this.worksDatas[list.type] = cachedData as WorksModel[];
        this.worksDatas[list.type] = this.worksDatas[list.type]!.slice(
          0,
          this.worksToDisplay
        );
        this.isLoading = false;
      } else {
        const worksSubscription = this.worksService
          .getWorksByType(this.user._id, list.type, {
            limit: this.worksToFetch,
          })
          .subscribe({
            next: (response: PaginatedResponse | null) => {
              const works = response?.data as WorksModel[];

              if (works && works.length > 0) {
                // Spread operator to change object reference & trigger changes in dashboard-stats
                this.worksDatas = {...this.worksDatas};
                this.worksDatas[list.type] = works;
                this.worksDatas[list.type] = this.worksDatas[list.type]!.slice(
                  0,
                  this.worksToDisplay
                );
              }

              if (i === this.lists.length) {
                this.isLoading = false;
              }
            },
          });
        this.subscriptions.push(worksSubscription);
      }
    }
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  public isWorksDatasEmpty(): boolean {
    return Object.keys(this.worksDatas).every((key) => {
      const worksTypeKey = key as WorksTypes;
      return !this.worksDatas[worksTypeKey]?.length;
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
