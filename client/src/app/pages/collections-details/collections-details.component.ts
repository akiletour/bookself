import {Component, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Subscription, fromEvent, throttleTime} from "rxjs";
import {WorksModel} from "src/app/models/WorksModel";
import {List, UpdatedList, User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {CacheService} from "src/app/modules/shared/services/cache.service";
import {ConfigService} from "src/app/modules/shared/services/config.service";
import {ListsService} from "src/app/modules/shared/services/list.service";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {ScreenSizeService} from "src/app/modules/shared/services/screenSize.service";
import {WorksService} from "src/app/modules/shared/services/works.service";

@Component({
  selector: "app-collections-details",
  templateUrl: "./collections-details.component.html",
  styleUrls: ["./collections-details.component.scss"],
})
export class CollectionsDetailsComponent implements OnInit, OnDestroy {
  public user!: User;
  public isLoading: boolean = true;
  public isLoadingList: boolean = true;
  public works: WorksModel[] = [];
  private limit: number;
  private listId!: string;
  public list!: List;
  private page: number = 1;
  private totalPages?: number;
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;

  constructor(
    private worksService: WorksService,
    private AuthService: AuthService,
    private configService: ConfigService,
    private listsService: ListsService,
    private route: ActivatedRoute,
    private modalService: ModalService,
    private cacheService: CacheService,
    private screenSizeService: ScreenSizeService
  ) {
    this.limit = this.configService.limit;

    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);

    const scroll$ = fromEvent(window, "scroll");
    const scrollSubscription = scroll$
      .pipe(throttleTime(100))
      .subscribe(() => this.onScroll());
    this.subscriptions.push(scrollSubscription);

    const routeSubscription = this.route.params.subscribe((params) => {
      this.listId = params["listId"];

      this.checkCacheFirst();
    });
    this.subscriptions.push(routeSubscription);

    const listsUpdatedSubscription = this.listsService.listsUpdated$.subscribe(
      (list: UpdatedList) => {
        if (list.operator === "edit") {
          this.list = list.list;
        }
      }
    );
    this.subscriptions.push(listsUpdatedSubscription);

    const worksUpdatedSubscription = this.worksService.worksUpdated$.subscribe(
      () => {
        const worksSubscription = this.worksService
          .getWorksFromList(this.user._id, this.listId, {
            page: 1,
            limit: this.limit * this.page,
          })
          .subscribe((response) => {
            if (response?.data) {
              const works = response?.data as WorksModel[];
              this.works = works;
              // Reassign works to generate onChanges in child component
              this.works = this.works.slice();
            } else {
              this.works = [];
            }
          });
        this.subscriptions.push(worksSubscription);
      }
    );
    this.subscriptions.push(worksUpdatedSubscription);
  }

  onScroll = (): void => {
    // Load more works if we approach page bottom
    if (
      window.innerHeight + window.scrollY * 1.1 >= document.body.offsetHeight &&
      this.totalPages &&
      this.page < this.totalPages
    ) {
      this.page++;
      this.fetchWorks(this.page);
    }
  };

  fetchWorks(page: number = 1) {
    const worksSubscription = this.worksService
      .getWorksFromList(this.user._id, this.listId, {
        page,
      })
      .subscribe((response) => {
        const works = response?.data as WorksModel[];
        if (works) {
          if (response?.pagination.totalPages) {
            this.totalPages = response?.pagination.totalPages;
          }

          if (page > 1) {
            this.works = [...this.works, ...works];
          } else {
            this.works = works;
            // Reassign works to generate onChanges in child component
            this.works = this.works.slice();
          }
        }
        this.isLoading = false;
      });
    this.subscriptions.push(worksSubscription);
  }

  checkCacheFirst(): void {
    const cachedList = this.cacheService.getFromCache(
      `list_${this.listId}_metas`
    );
    if (cachedList) {
      this.list = cachedList as List;
      this.isLoadingList = false;
    } else {
      const listSubscription = this.listsService
        .getListById(this.user._id, this.listId)
        .subscribe({
          next: (list: List | null) => {
            if (list) {
              this.list = list;
              this.isLoadingList = false;
            } else {
              this.isLoadingList = true;
            }
          },
        });
      this.subscriptions.push(listSubscription);
    }

    const cachedData = this.cacheService.getFromCache(
      `list_${this.listId}_${this.page}`
    );
    if (cachedData) {
      this.works = cachedData as WorksModel[];
      this.isLoading = false;
    } else {
      this.fetchWorks(this.page);
    }
  }

  updateList(modalId: string) {
    this.modalService.openModal(modalId);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
