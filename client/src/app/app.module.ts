import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule} from "@angular/core";
import {HttpClientModule} from "@angular/common/http";
import {BrowserModule} from "@angular/platform-browser";

import {httpInterceptorProviders} from "./services/interceptors";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {NavbarComponent} from "./components/navbar/navbar.component";
import {HeaderComponent} from "./components/header/header.component";
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import {DashboardEmptyComponent} from "./components/dashboard-empty/dashboard-empty.component";
import {HistoryComponent} from "./pages/history/history.component";
import {AuthModule} from "./modules/auth/auth.module";
import {AuthService} from "./modules/auth/services/auth.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DashboardStatsComponent} from "./components/dashboard-stats/dashboard-stats.component";
import {StatsModule} from "./modules/stats/stats.module";
import {SharedModule} from "./modules/shared/shared.module";
import {WorksListComponent} from "./pages/works-list/works-list.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {NgxSkeletonLoaderModule} from "ngx-skeleton-loader";
import {CollectionsListComponent} from "./pages/collections-list/collections-list.component";
import {CollectionsDetailsComponent} from "./pages/collections-details/collections-details.component";
import {CollectionsEmptyComponent} from "./components/collections-empty/collections-empty.component";
import {CollectionsDetailsEmptyComponent} from "./components/collections-details-empty/collections-details-empty.component";
import {ServiceWorkerModule} from "@angular/service-worker";

import {environment} from "../environments/environment";
import {HeaderMobileComponent} from "./components/header-mobile/header-mobile.component";
import {ProfileMenuComponent} from "./components/profile-menu/profile-menu.component";
import {AddWorkButtonMobileComponent} from "./components/add-work-button-mobile/add-work-button-mobile.component";
import {DashboardStatsMobileComponent} from "./components/dashboard-stats-mobile/dashboard-stats-mobile.component";
import {SearchFormComponent} from "./components/search-form/search-form.component";
import {SearchComponent} from "./pages/search/search.component";
import {SearchItemComponent} from "./components/search-item/search-item.component";

export function setAuthenticatedUser(AuthService: AuthService) {
  return () => {
    const savedUser = localStorage.getItem("user");
    if (savedUser) {
      AuthService.user = JSON.parse(savedUser);
    }
  };
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HeaderComponent,
    DashboardComponent,
    DashboardEmptyComponent,
    HistoryComponent,
    WorksListComponent,
    DashboardStatsComponent,
    CollectionsListComponent,
    CollectionsDetailsComponent,
    CollectionsEmptyComponent,
    CollectionsDetailsEmptyComponent,
    HeaderMobileComponent,
    ProfileMenuComponent,
    AddWorkButtonMobileComponent,
    DashboardStatsMobileComponent,
    SearchComponent,
    SearchFormComponent,
    SearchItemComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    FormsModule,
    ReactiveFormsModule,
    StatsModule,
    SharedModule,
    NgxSkeletonLoaderModule,
    ServiceWorkerModule.register("ngsw.js", {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: "registerWhenStable:30000",
    }),
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: setAuthenticatedUser,
      multi: true,
      deps: [AuthService],
    },
    httpInterceptorProviders,
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
