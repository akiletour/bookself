import {Injectable, OnDestroy} from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from "@angular/common/http";
import {Observable, Subscription, throwError} from "rxjs";
import {catchError, switchMap} from "rxjs/operators";
import {AuthService} from "../../modules/auth/services/auth.service";
import {User} from "src/app/modules/auth/models/User";
import {Router} from "@angular/router";

@Injectable()
export class AuthInterceptor implements HttpInterceptor, OnDestroy {
  public user?: User | null;
  private subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    // subscribe to user changes
    const userSubscription = this.authService.user$.subscribe(
      (user: User | null) => {
        if (user) {
          this.user = user;
        } else {
          this.user = null;
        }
      }
    );
    this.subscriptions.push(userSubscription);
  }

  intercept(
    request: HttpRequest<User>,
    next: HttpHandler
  ): Observable<HttpEvent<User>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        const refreshToken = this.user?.tokens?.refreshToken;
        const email = this.user?.email;

        if (error.status === 401 && email && refreshToken) {
          return this.authService.refreshToken(refreshToken, email).pipe(
            switchMap((response) => {
              const clonedRequest = request.clone({
                setHeaders: {
                  Authorization: `Bearer ${this.user?.tokens?.twitchAccessToken}`,
                },
              });

              const user = {
                ...(response as User),
                tokens: response.tokens,
              };
              localStorage.setItem("user", JSON.stringify(user));
              this.authService.user = user;

              return next.handle(clonedRequest).pipe(
                catchError((refreshedError) => {
                  if (refreshedError.status === 401) {
                    this.authService.logout();
                    this.router.navigate(["/login"]);
                  }
                  return throwError(() => refreshedError);
                })
              );
            }),
            catchError((refreshError) => {
              return throwError(() => refreshError);
            })
          );
        } else if (error.status === 401) {
          this.authService.logout();
          this.router.navigate(["/login"]);
        }

        return throwError(() => error);
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
