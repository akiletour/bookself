import {CanActivateFn, Router} from "@angular/router";
import {AuthService} from "../modules/auth/services/auth.service";
import {inject} from "@angular/core";

export const loggedGuard: CanActivateFn = () => {
  const authService = inject(AuthService);
  const router = inject(Router);

  if (authService.user) {
    return true;
  }

  // Redirect to the login page
  return router.parseUrl("/login");
};
