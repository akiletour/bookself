import {CanActivateFn, Router} from "@angular/router";
import {AuthService} from "../modules/auth/services/auth.service";
import {inject} from "@angular/core";

export const authGuard: CanActivateFn = () => {
  const authService = inject(AuthService);
  const router = inject(Router);

  if (authService.user) {
    // Redirect to the dashboard
    return router.parseUrl("/");
  }

  return true;
};
