import {Movie} from "./Movie";
import {Series} from "./Series";
import {MoviesDetailsComponent} from "../modules/shared/components/movies-details/movies-details.component";
import {SeriesDetailsComponent} from "../modules/shared/components/series-details/series-details.component";
import {GamesDetailsComponent} from "../modules/shared/components/games-details/games-details.component";
import {TypeGuard} from "../modules/shared/pipes/guardType.pipe";
import {WorksTypes} from "../enums/Works.enum";
import {Game} from "./Game";
import {GamesProviders} from "../enums/Game";
import {MoviesProviders} from "../enums/Movie";
import {MangasProviders} from "../enums/Manga";
import {ComicsProviders} from "../enums/Comic";
import {Manga} from "./Manga";
import {MangasDetailsComponent} from "../modules/shared/components/mangas-details/mangas-details.component";
import {ComicsDetailsComponent} from "../modules/shared/components/comics-details/comics-details.component";
import {Comic} from "./Comic";

export type Work = {
  _id: string;
  provider: GamesProviders | MoviesProviders | MangasProviders;
  providerId: string;
  title: string;
  releaseDate: Date;
  additionDate: Date;
  discoveryDate: Date;
  cover: string;
  genres?: string[];
  lists?: {
    name: string;
    id: string;
  }[];
  type: WorksTypes;
  rating: number;
  userRating: number;
};

export type WorksModel = Movie | Series | Game | Manga | Comic;
//   | Book;

export type WorksDTO = {
  provider:
    | GamesProviders
    | MoviesProviders
    | MangasProviders
    | ComicsProviders;
  providerId: string;
  listId?: string;
};

export const isMovie: TypeGuard<WorksModel, Movie> = (
  work: WorksModel
): work is Movie => work.type === WorksTypes.Movie;

export const isSeries: TypeGuard<WorksModel, Series> = (
  work: WorksModel
): work is Series => work.type === WorksTypes.Series;

export const isGame: TypeGuard<WorksModel, Game> = (
  work: WorksModel
): work is Game => work.type === WorksTypes.Game;

export const isManga: TypeGuard<WorksModel, Manga> = (
  work: WorksModel
): work is Manga => work.type === WorksTypes.Manga;

export const isComic: TypeGuard<WorksModel, Comic> = (
  work: WorksModel
): work is Comic => work.type === WorksTypes.Comic;

// isBook

export type WorkDetailsComponent =
  | typeof MoviesDetailsComponent
  | typeof SeriesDetailsComponent
  | typeof GamesDetailsComponent
  | typeof MangasDetailsComponent
  | typeof ComicsDetailsComponent
  // | typeof BooksDetailsComponent
  | null;

export type UpdatedWork = {
  type: WorksTypes;
  operator: string;
  work: WorksModel | string;
  list?: boolean;
};
