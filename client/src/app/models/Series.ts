import {WorksTypes} from "../enums/Works.enum";
import {SeriesProgress} from "../modules/auth/models/User";
import {Cast} from "./Movie";
import {Work} from "./WorksModel";

export interface Series extends Work {
  lastAiredDate: Date;
  creators?: Creator[];
  cast?: Cast[];
  numberOfEpisodes: number;
  numberOfSeasons: number;
  seasons?: Season[];
  synopsis?: string;
  status: string;
  tagline: string;
  progress?: SeriesProgress;
  type: WorksTypes;
}

export interface Season {
  _id?: string;
  name: string;
  seasonNumber: number;
  releaseDate: Date;
  synopsis: string;
  numberOfEpisodes: number;
  cover: string;
  episodes: [
    {
      name: string;
      number: number;
      releaseDate: Date;
      synopsis: string;
      duration: string;
    },
  ];
}

export interface Creator {
  name: string;
  profile: string;
}
