import {GamesCategories, GamesStatuses} from "../enums/Game";
import {WorksTypes} from "../enums/Works.enum";
import {Pagination} from "./Pagination";
import {Work} from "./WorksModel";

export interface Game extends Work {
  artworks?: string[];
  involvedCompanies?: Company[];
  category?: GamesCategories;
  dlcs?: DLC[];
  userDlcs?: {
    _id?: string;
    providerId: number;
  }[];
  franchise?: string;
  parentGame?: string;
  platforms: string[];
  remakes?: string[];
  summary?: string;
  status: GamesStatuses;
  themes: string[];
  type: WorksTypes;
  videos?: string[];
  hltb: HLTB;
}

export interface DLC {
  providerId: number;
  name: string;
  cover: string;
  releaseDate: Date;
}

export interface Company {
  name: string;
  role: string;
  logo: string;
}

export interface HLTB {
  main?: number;
  mainExtra?: number;
  completionist?: number;
}

export interface GamesPaginated {
  data: Game[];
  pagination: Pagination;
}
