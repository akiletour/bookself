import {WorksTypes} from "../enums/Works.enum";
import {Work} from "./WorksModel";

export interface Movie extends Work {
  directors?: Director[];
  cast?: Cast[];
  duration?: string;
  synopsis?: string;
  tagline?: string;
  type: WorksTypes;
}

export interface Cast {
  name: string;
  character: string;
  profile: string;
}

export interface Director {
  name: string;
  profile: string;
}
