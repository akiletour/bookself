import {WorksTypes} from "../enums/Works.enum";
import {ComicProgress} from "../modules/auth/models/User";
import {Pagination} from "./Pagination";
import {Work} from "./WorksModel";

export interface Comic extends Work {
  publisher?: string;
  authors?: Author[];
  volumes?: Volume[];
  summary?: string;
  progress?: ComicProgress[];
  type: WorksTypes;
}

export interface Author {
  name: string;
  profile?: string;
}

export interface Volume {
  volumeNumber: number;
  volumeName: string;
  cover?: string;
}

export interface ComicsPaginated {
  data: Comic[];
  pagination: Pagination;
}
