import {WorksTypes} from "../enums/Works.enum";
import {MangaProgress} from "../modules/auth/models/User";
import {Pagination} from "./Pagination";
import {Work} from "./WorksModel";

export interface Manga extends Work {
  themes: string[];
  demographic: string;
  status: string;
  authors?: Author[];
  artists?: Author[];
  volumes?: Volume[];
  summary?: string;
  progress?: MangaProgress[];
  type: WorksTypes;
}

export interface Author {
  name: string;
  profile?: string;
}

export interface Volume {
  volumeNumber: number;
  cover?: string;
}

export interface MangasPaginated {
  data: Manga[];
  pagination: Pagination;
}
