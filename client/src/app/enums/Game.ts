export enum GamesCategories {
  "Game" = 0,
  "DLC" = 1,
  "Expansion" = 2,
  "Bundle" = 3,
  "Standalone" = 4,
  "Mod" = 5,
  "Episode" = 6,
  "Season" = 7,
  "Remake" = 8,
  "Remaster" = 9,
  "Expanded Game" = 10,
  "Port" = 11,
  "Fork" = 12,
  "Pack" = 13,
  "Update" = 14,
}

export enum GamesStatuses {
  "released" = "Released",
  "alpha" = "alpha",
  "beta" = "Beta",
  "early_access" = "Early access",
  "offline" = "Offline",
  "cancelled" = "Cancelled",
  "rumored" = "Rumored",
  "delisted" = "Delisted",
}

export enum GamesProviders {
  "IGDB" = 1,
}

export enum HowLongToBeatTimeLabels {
  "main" = "Main Game",
  "mainExtra" = "Main Game + Extra",
  "completionist" = "Completionist",
}
