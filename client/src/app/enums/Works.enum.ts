export enum WorksTypes {
  Movie = "movies",
  Series = "series",
  Animation = "series/animation",
  Game = "games",
  Manga = "mangas",
  Comic = "comics",
  // Book = "books",
}

export enum StrictWorksTypes {
  Movie = "movies",
  Series = "series",
  Game = "games",
  Manga = "mangas",
  Comic = "comics",
  // Book = "books",
}

export enum WorksLabels {
  movies = "movies",
  series = "series",
  "series/animation" = "animated series",
  games = "games",
  mangas = "mangas",
  comics = "comics",
  // books = "books",
}
