import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {loggedGuard} from "./guards/logged.guard";
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import {WorksListComponent} from "./pages/works-list/works-list.component";
import {HistoryComponent} from "./pages/history/history.component";
import {CollectionsListComponent} from "./pages/collections-list/collections-list.component";
import {CollectionsDetailsComponent} from "./pages/collections-details/collections-details.component";
import {SearchComponent} from "./pages/search/search.component";

const routes: Routes = [
  {
    path: "account",
    canActivate: [loggedGuard],
    loadChildren: () =>
      import("./modules/account/account.module").then((m) => m.AccountModule),
  },
  {
    path: "",
    component: DashboardComponent,
    canActivate: [loggedGuard],
    pathMatch: "full",
  },
  {
    path: "works/:workType",
    canActivate: [loggedGuard],
    children: [
      {
        path: "",
        component: WorksListComponent,
      },
      {
        path: ":filter",
        component: WorksListComponent,
      },
    ],
  },
  {
    path: "collections",
    canActivate: [loggedGuard],
    children: [
      {
        path: "",
        component: CollectionsListComponent,
      },
      {
        path: ":listId",
        component: CollectionsDetailsComponent,
      },
    ],
  },
  {
    path: "history",
    component: HistoryComponent,
    canActivate: [loggedGuard],
    pathMatch: "full",
  },
  {
    path: "search",
    canActivate: [loggedGuard],
    children: [
      {
        path: ":term",
        component: SearchComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
