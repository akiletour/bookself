import {Component, Input} from "@angular/core";
import {WorksTypes} from "src/app/enums/Works.enum";
import {WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";

@Component({
  selector: "app-dashboard-stats",
  templateUrl: "./dashboard-stats.component.html",
  styleUrls: ["./dashboard-stats.component.scss"],
})
export class DashboardStatsComponent {
  @Input() public user!: User;
  @Input() content!: {[key in WorksTypes]?: Array<WorksModel>};
}
