import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";
import {ModalService} from "src/app/modules/shared/services/modal.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent {
  @Input() user?: User;

  constructor(public modalService: ModalService) {}

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }
}
