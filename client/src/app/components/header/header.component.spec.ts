import {ComponentFixture, TestBed} from "@angular/core/testing";

import {HeaderComponent} from "./header.component";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {HttpClient, HttpHandler} from "@angular/common/http";

describe("HeaderComponent", () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [AuthService, HttpClient, HttpHandler],
    });
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
