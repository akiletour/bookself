import {ComponentFixture, TestBed} from "@angular/core/testing";

import {DashboardStatsMobileComponent} from "./dashboard-stats-mobile.component";

describe("DashboardStatsMobileComponent", () => {
  let component: DashboardStatsMobileComponent;
  let fixture: ComponentFixture<DashboardStatsMobileComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardStatsMobileComponent],
    });
    fixture = TestBed.createComponent(DashboardStatsMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
