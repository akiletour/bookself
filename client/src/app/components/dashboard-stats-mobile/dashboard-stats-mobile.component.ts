import {Component, Input, OnInit} from "@angular/core";
import {WorksTypes} from "src/app/enums/Works.enum";
import {WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {register} from "swiper/element/bundle";

@Component({
  selector: "app-dashboard-stats-mobile",
  templateUrl: "./dashboard-stats-mobile.component.html",
  styleUrls: ["./dashboard-stats-mobile.component.scss"],
})
export class DashboardStatsMobileComponent implements OnInit {
  @Input() public user!: User;
  @Input() content!: {[key in WorksTypes]?: Array<WorksModel>};

  ngOnInit() {
    // Register Swiper custom elements
    register();

    const swiperEl = document.querySelector("swiper-container");

    const params = {
      injectStyles:
        this.user.theme === "light"
          ? [
              `     
          .swiper-pagination {
            bottom: 0 !important;
          }
          .swiper-pagination-bullet-active {            
            background: linear-gradient(
              135deg,
              var(--color-yellow-light),
              var(--color-yellow-dark)
            );
          }
        }
        `,
            ]
          : [
              `     
          .swiper-pagination {
            bottom: 0 !important;
          }
          .swiper-pagination-bullet {            
            background: white;
          }
          .swiper-pagination-bullet-active {            
            background: linear-gradient(
              135deg,
              var(--color-yellow-light),
              var(--color-yellow-dark)
            );
          }
        }
        `,
            ],
    };

    Object.assign(swiperEl!, params);

    swiperEl!.initialize();
  }
}
