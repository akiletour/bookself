import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {WorksTypes} from "src/app/enums/Works.enum";

@Component({
  selector: "app-dashboard-empty",
  templateUrl: "./dashboard-empty.component.html",
  styleUrls: ["./dashboard-empty.component.scss"],
})
export class DashboardEmptyComponent {
  @Input() type!: WorksTypes;
  @Input() user!: User;

  constructor(public modalService: ModalService) {}

  openModal(modalId: string, type?: WorksTypes) {
    this.modalService.setWorkType(type);
    this.modalService.openModal(modalId);
  }
}
