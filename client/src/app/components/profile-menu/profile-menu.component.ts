import {Component, HostListener, Input} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {environment} from "src/environments/environment";

@Component({
  selector: "app-profile-menu",
  templateUrl: "./profile-menu.component.html",
  styleUrls: ["./profile-menu.component.scss"],
})
export class ProfileMenuComponent {
  @Input() user?: User;
  public imagesDomain: string = environment.imagesDomain;
  public isSubmenuOpen: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) {}

  toggleSubmenu() {
    this.isSubmenuOpen = !this.isSubmenuOpen;
  }

  @HostListener("document:click", ["$event"])
  onDocumentClick(event: Event) {
    const targetElement = event.target as HTMLElement;

    if (
      !targetElement.closest(".profile") ||
      targetElement.closest(".profile__submenu li")
    ) {
      this.isSubmenuOpen = false;
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(["/login"]);
  }
}
