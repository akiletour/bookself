import {ComponentFixture, TestBed} from "@angular/core/testing";

import {CollectionsEmptyComponent} from "./collections-empty.component";

describe("CollectionsEmptyComponent", () => {
  let component: CollectionsEmptyComponent;
  let fixture: ComponentFixture<CollectionsEmptyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionsEmptyComponent],
    });
    fixture = TestBed.createComponent(CollectionsEmptyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
