import {Component, OnDestroy} from "@angular/core";
import {Subscription} from "rxjs";
import {User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {ModalService} from "src/app/modules/shared/services/modal.service";

@Component({
  selector: "app-collections-empty",
  templateUrl: "./collections-empty.component.html",
  styleUrls: ["./collections-empty.component.scss"],
})
export class CollectionsEmptyComponent implements OnDestroy {
  public user!: User;
  private subscriptions: Subscription[] = [];

  constructor(
    private AuthService: AuthService,
    public modalService: ModalService
  ) {
    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
