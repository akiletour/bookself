import {ComponentFixture, TestBed} from "@angular/core/testing";

import {AddWorkButtonMobileComponent} from "./add-work-button-mobile.component";

describe("AddWorkButtonMobileComponent", () => {
  let component: AddWorkButtonMobileComponent;
  let fixture: ComponentFixture<AddWorkButtonMobileComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddWorkButtonMobileComponent],
    });
    fixture = TestBed.createComponent(AddWorkButtonMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
