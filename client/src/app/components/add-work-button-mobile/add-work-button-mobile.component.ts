import {Component} from "@angular/core";
import {ModalService} from "src/app/modules/shared/services/modal.service";

@Component({
  selector: "app-add-work-button-mobile",
  templateUrl: "./add-work-button-mobile.component.html",
})
export class AddWorkButtonMobileComponent {
  constructor(private modalService: ModalService) {}

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }
}
