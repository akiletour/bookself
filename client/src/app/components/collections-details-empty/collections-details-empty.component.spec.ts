import {ComponentFixture, TestBed} from "@angular/core/testing";

import {CollectionsDetailsEmptyComponent} from "./collections-details-empty.component";

describe("CollectionsDetailsEmptyComponent", () => {
  let component: CollectionsDetailsEmptyComponent;
  let fixture: ComponentFixture<CollectionsDetailsEmptyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionsDetailsEmptyComponent],
    });
    fixture = TestBed.createComponent(CollectionsDetailsEmptyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
