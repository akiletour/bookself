import {Component, Input} from "@angular/core";
import {List} from "src/app/modules/auth/models/User";
import {ModalService} from "src/app/modules/shared/services/modal.service";

@Component({
  selector: "app-collections-details-empty",
  templateUrl: "./collections-details-empty.component.html",
  styleUrls: ["./collections-details-empty.component.scss"],
})
export class CollectionsDetailsEmptyComponent {
  @Input() list!: List;

  constructor(private modalService: ModalService) {}

  updateList(modalId: string) {
    this.modalService.openModal(modalId);
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }
}
