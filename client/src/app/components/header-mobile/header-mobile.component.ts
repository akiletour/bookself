import {Component, HostListener, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";
import {ModalService} from "src/app/modules/shared/services/modal.service";

@Component({
  selector: "app-header-mobile",
  templateUrl: "./header-mobile.component.html",
  styleUrls: ["./header-mobile.component.scss"],
})
export class HeaderMobileComponent {
  @Input() user?: User;
  isMenuOpen = false;

  constructor(public modalService: ModalService) {}

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  toggleBurger() {
    this.isMenuOpen = !this.isMenuOpen;
  }

  @HostListener("document:click", ["$event"])
  onDocumentClick(event: Event) {
    const targetElement = event.target as HTMLElement;

    if (!targetElement.closest(".menu-icon")) {
      this.isMenuOpen = false;
    }
  }
}
