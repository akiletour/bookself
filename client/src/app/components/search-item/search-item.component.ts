import {Component, Input} from "@angular/core";
import {GamesCategories} from "src/app/enums/Game";
import {
  WorkDetailsComponent,
  WorksModel,
  isComic,
  isGame,
  isManga,
  isMovie,
  isSeries,
} from "src/app/models/WorksModel";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {WorksService} from "src/app/modules/shared/services/works.service";
import {environment} from "src/environments/environment";

@Component({
  selector: "app-search-item",
  templateUrl: "./search-item.component.html",
  styleUrls: ["./search-item.component.scss"],
})
export class SearchItemComponent {
  @Input() searchResult!: WorksModel;
  public isMovie = isMovie;
  public isSeries = isSeries;
  public isGame = isGame;
  public isManga = isManga;
  public isComic = isComic;
  public detailsComponent!: WorkDetailsComponent;
  public imagesPath!: string;
  private imagesDomain: string = environment.imagesDomain;

  constructor(
    private worksService: WorksService,
    private modalService: ModalService
  ) {
    this.imagesPath = this.imagesDomain + "/";
  }

  ngOnInit() {
    // Define card component based on type
    this.detailsComponent = this.worksService.getWorksDetailsComponentByType(
      this.searchResult.type
    );
  }

  openModal(modalId: string) {
    this.modalService.openModal("search_" + modalId);
  }

  public isMangaOrComic(work: WorksModel): boolean {
    return this.isManga(work) || this.isComic(work);
  }

  public getGameCategories(key?: GamesCategories): string | void {
    if (key !== undefined && GamesCategories[key] !== undefined) {
      return GamesCategories[key];
    } else {
      return "";
    }
  }
}
