import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {WorksLabels, WorksTypes} from "src/app/enums/Works.enum";
import {User} from "src/app/modules/auth/models/User";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {ScreenSizeService} from "src/app/modules/shared/services/screenSize.service";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit, OnDestroy {
  @Input() user?: User;
  public workTypes = Object.values(WorksTypes);
  public isMobile: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private screenSizeService: ScreenSizeService,
    private modalService: ModalService
  ) {}

  ngOnInit() {
    const screenSizeSubscription = this.screenSizeService.isMobile$.subscribe(
      (isMobile) => {
        this.isMobile = isMobile;
      }
    );
    this.subscriptions.push(screenSizeSubscription);
  }

  public getWorkLabel(key: string): string {
    return WorksLabels[key as keyof typeof WorksLabels] || key;
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
