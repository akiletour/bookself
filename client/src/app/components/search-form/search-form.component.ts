import {
  AfterViewInit,
  Component,
  HostListener,
  Input,
  OnDestroy,
} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
  BehaviorSubject,
  Observable,
  Subject,
  Subscription,
  debounceTime,
  distinctUntilChanged,
  of,
  switchMap,
} from "rxjs";
import {Pagination} from "src/app/models/Pagination";
import {WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {PaginatedResponse} from "src/app/modules/shared/interfaces/ApiResponse.interface";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {WorksService} from "src/app/modules/shared/services/works.service";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"],
})
export class SearchFormComponent implements AfterViewInit, OnDestroy {
  @Input() user!: User;
  @Input() isMobile?: boolean;
  searchForm: FormGroup;
  searchTerm$ = new Subject<string>();
  searchTerm!: string;
  private searchResultsSubject = new BehaviorSubject<WorksModel[] | []>([]);
  searchResults$ = this.searchResultsSubject.asObservable();
  pagination!: Pagination;
  public isLoading: boolean = false;
  public searchResultsVisible: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private worksService: WorksService,
    private modalService: ModalService
  ) {
    this.searchForm = this.formBuilder.group({
      search: ["", Validators.required],
    });
  }

  ngAfterViewInit() {
    const searchSubscription = this.searchTerm$
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((term: string) => {
          this.searchTerm = term;
          if (this.searchTerm) {
            return this.search(term);
          } else {
            return of(null);
          }
        })
      )
      .subscribe((res) => {
        this.isLoading = false;
        this.setSearchResults(res);
      });
    this.subscriptions.push(searchSubscription);
  }

  @HostListener("document:click", ["$event"])
  onDocumentClick(event: Event) {
    if (
      this.isSearchResultClick(event) ||
      (!this.isSearchResultClick(event) && !this.isModaleCloseClick(event))
    ) {
      this.searchResultsVisible = false;
    }
  }

  isSearchResultClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".searchDiv");
  }

  isModaleCloseClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".close");
  }

  isSeeAllClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".seeAll");
  }

  setSearchResults(results: PaginatedResponse | null) {
    if (results) {
      this.searchResultsSubject.next(results.data as WorksModel[]);
      this.pagination = results.pagination!;
    } else {
      this.searchResultsSubject.next([]);
      this.pagination = {page: 1, totalPages: 1};
    }
  }

  onInputChange(event: Event): void {
    this.isLoading = true;
    this.searchResultsVisible = true;
    const inputElement = event.target as HTMLInputElement;
    if (inputElement) {
      this.searchTerm$.next(inputElement.value);
    }
  }

  search(term: string): Observable<PaginatedResponse | null> {
    this.isLoading = true;
    this.searchResultsVisible = true;
    return this.worksService.getWorks(this.user._id, {query: term, limit: 8});
  }

  closeModals() {
    this.modalService.closeModal();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
