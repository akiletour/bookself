import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {PasswordDTO, User, UserDTO} from "../../../modules/auth/models/User";

import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {map} from "rxjs";
import {AuthService} from "../../auth/services/auth.service";

@Injectable()
export class AccountService {
  private apiDomain: string = environment.apiDomain;
  private options: {headers: {Authorization: string}};

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    this.options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}`,
      },
    };
  }

  public uploadPicture(userId: string, picture: FormData): Observable<User> {
    return this.http
      .post<User>(
        `${this.apiDomain}/users/${userId}/picture`,
        picture,
        this.options
      )
      .pipe(
        map((res) => {
          return res as User;
        })
      );
  }

  public updateUser(userId: string, user: UserDTO): Observable<User> {
    return this.http
      .patch<User>(`${this.apiDomain}/users/${userId}`, user, this.options)
      .pipe(
        map((res) => {
          return res as User;
        })
      );
  }

  public deleteUser(userId: string): Observable<boolean> {
    return this.http.delete<boolean>(
      `${this.apiDomain}/users/${userId}`,
      this.options
    );
  }

  public updatePassword(
    userId: string,
    password: PasswordDTO
  ): Observable<User> {
    return this.http.post<User>(
      `${this.apiDomain}/users/${userId}/password`,
      password,
      this.options
    );
  }
}
