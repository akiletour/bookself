import {TestBed} from "@angular/core/testing";

import {AccountService} from "./account.service";
import {HttpClient, HttpHandler} from "@angular/common/http";

describe("AccountService", () => {
  let service: AccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountService, HttpClient, HttpHandler],
    });
    service = TestBed.inject(AccountService);
  });

  it("should be created", () => {
    expect(service).toBeTruthy();
  });
});
