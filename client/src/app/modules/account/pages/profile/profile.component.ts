import {Component, ElementRef, OnDestroy, ViewChild} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {AccountService} from "../../services/account.service";
import {environment} from "src/environments/environment";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnDestroy {
  public user!: User;
  selectedImageUrl: string | ArrayBuffer | null = "";
  file: File | null = null;
  @ViewChild("fileInput", {static: false})
  fileInput!: ElementRef;
  public imagesDomain: string = environment.imagesDomain;
  private subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private accountService: AccountService,
    private modalService: ModalService,
    private router: Router
  ) {
    const userSubscription = this.authService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  uploadProfilePicture() {
    const fileInput = this.fileInput.nativeElement;
    if (fileInput) {
      fileInput.click();
    }
  }

  onFileSelected(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    if (inputElement.files && inputElement.files[0]) {
      this.file = inputElement.files[0];
      const reader = new FileReader();
      reader.onload = () => {
        this.selectedImageUrl = reader.result;
        this.uploadImage();
      };
      reader.readAsDataURL(this.file);
    }
  }

  uploadImage() {
    const formData = new FormData();
    formData.append("file", this.file as File);

    const uploadPictureSubscription = this.accountService
      .uploadPicture(this.user._id, formData)
      .subscribe({
        next: (user) => {
          this.authService.user = user;
        },
      });
    this.subscriptions.push(uploadPictureSubscription);
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  openDeleteConfirmation(event: Event, modalId: string) {
    event.stopPropagation();
    this.openModal("dlt_acc_" + modalId);
  }

  closeModal(modalId: string) {
    this.modalService.closeModal(modalId);
  }

  deleteAccount(userId: string) {
    const deleteUserSubscription = this.accountService
      .deleteUser(userId)
      .subscribe({
        next: () => {
          this.closeModal("dlt_acc_" + userId);
          this.authService.logout();
          this.router.navigate(["/login"]);
        },
      });

    this.subscriptions.push(deleteUserSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
