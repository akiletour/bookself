import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {User, UserDTO} from "src/app/modules/auth/models/User";
import {AccountService} from "../../services/account.service";
import {Subscription} from "rxjs";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: "app-edit-profile-preferences",
  templateUrl: "./edit-profile-preferences.component.html",
  animations: [
    trigger("fadeInOut", [
      transition(":enter", [
        style({opacity: 0}),
        animate("0.05s", style({opacity: 0})),
        animate("0.1s", style({opacity: 1})),
      ]),
      transition(":leave", [animate("0.1s", style({opacity: 0}))]),
    ]),
  ],
})
export class EditProfilePreferencesComponent implements OnInit, OnDestroy {
  @Input() user!: User;
  public isDarkTheme: boolean = false;
  public success?: string;
  public error?: CustomError | null;
  private subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private accountService: AccountService
  ) {}

  ngOnInit() {
    this.isDarkTheme = this.user.theme === "light" ? false : true;
  }

  toggleTheme() {
    this.user.theme = this.user.theme === "light" ? "dark" : "light";

    const userInformationsForm: UserDTO = {
      theme: this.user.theme,
    };

    const updateUserSubscription = this.accountService
      .updateUser(this.user._id, userInformationsForm)
      .subscribe({
        next: (user) => {
          this.authService.user = user;

          const root = document.documentElement;
          root.className = `${user.theme}`;

          this.success = "Theme updated";
          this.error = null;
          setTimeout(() => {
            this.success = "";
          }, 2000);
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updateUserSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
