import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User, UserDTO} from "src/app/modules/auth/models/User";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {AccountService} from "../../services/account.service";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Subscription} from "rxjs";

@Component({
  selector: "app-edit-profile-form",
  templateUrl: "./edit-profile-form.component.html",
  styleUrls: ["./edit-profile-form.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
  ],
})
export class EditProfileFormComponent implements OnInit, OnDestroy {
  @Input() user!: User;
  userInformationsForm: FormGroup;
  error!: CustomError | null;
  public success!: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private authService: AuthService
  ) {
    this.userInformationsForm = this.formBuilder.group({
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      username: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      picture: [""],
    });
  }

  ngOnInit() {
    this.userInformationsForm.patchValue({
      firstname: this.user.firstname,
      lastname: this.user.lastname,
      username: this.user.username,
      email: this.user.email,
    });
  }

  onSubmit() {
    if (this.userInformationsForm.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    const userInformationsForm: UserDTO = {
      firstname: this.userInformationsForm.value.firstname,
      lastname: this.userInformationsForm.value.lastname,
      username: this.userInformationsForm.value.username,
      picture: this.userInformationsForm.value.firstname,
    };

    const updateUserSubscription = this.accountService
      .updateUser(this.user._id, userInformationsForm)
      .subscribe({
        next: (user) => {
          this.authService.user = user;
          this.success = "Informations updated !";
          this.error = null;
          setTimeout(() => {
            this.success = "";
          }, 2000);
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updateUserSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
