import {Component, Input, OnDestroy} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PasswordDTO, User} from "src/app/modules/auth/models/User";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {AccountService} from "../../services/account.service";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {Subscription} from "rxjs";

@Component({
  selector: "app-edit-password-form",
  templateUrl: "./edit-password-form.component.html",
  styleUrls: ["./edit-password-form.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
  ],
})
export class EditPasswordFormComponent implements OnDestroy {
  @Input() user!: User;
  passwordForm: FormGroup;
  error!: CustomError | null;
  public success!: string;
  private subscriptions: Subscription[] = [];
  public showOldPassword = false;
  public showNewPassword = false;
  public showNewConfirmPassword = false;

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private authService: AuthService
  ) {
    this.passwordForm = this.formBuilder.group({
      old_password: ["", Validators.required],
      new_password: ["", Validators.required],
      confirm_new_password: ["", Validators.required],
    });
  }

  onSubmit() {
    if (this.passwordForm.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    if (
      this.passwordForm.value.password !==
      this.passwordForm.value.confirm_password
    ) {
      this.error = {
        error: {
          error: "Passwords do not match",
        },
      };
      return;
    }

    const passwordForm: PasswordDTO = {
      old_password: this.passwordForm.value.old_password,
      new_password: this.passwordForm.value.new_password,
      confirm_new_password: this.passwordForm.value.confirm_new_password,
    };

    const updatePasswordSubscription = this.accountService
      .updatePassword(this.user._id, passwordForm)
      .subscribe({
        next: (user: User) => {
          this.authService.user = user;
          this.success = "Password updated !";
          this.passwordForm.reset();
          this.error = null;
          setTimeout(() => {
            this.success = "";
          }, 2000);
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updatePasswordSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
