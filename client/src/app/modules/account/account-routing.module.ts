import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import {ProfileComponent} from "./pages/profile/profile.component";
import {FriendsComponent} from "./pages/friends/friends.component";
import {loggedGuard} from "src/app/guards/logged.guard";

export const accountRoutes: Routes = [
  {
    path: "",
    canActivate: [loggedGuard],
    children: [
      {
        path: "",
        redirectTo: "profile",
        pathMatch: "full",
      },

      {
        path: "profile",
        component: ProfileComponent,
      },
      {
        path: "friends",
        component: FriendsComponent,
      },
      {
        path: "stats",
        loadChildren: () =>
          import("src/app/modules/stats/stats.module").then(
            (m) => m.StatsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(accountRoutes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
