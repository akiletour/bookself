import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {ProfileComponent} from "./pages/profile/profile.component";
import {FriendsComponent} from "./pages/friends/friends.component";

import {AccountRoutingModule} from "./account-routing.module";
import {EditProfileFormComponent} from "./components/edit-profile-form/edit-profile-form.component";
import {EditPasswordFormComponent} from "./components/edit-password-form/edit-password-form.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AccountService} from "./services/account.service";
import {SharedModule} from "../shared/shared.module";
import {EditProfilePreferencesComponent} from "./components/edit-profile-preferences/edit-profile-preferences.component";

@NgModule({
  declarations: [
    ProfileComponent,
    FriendsComponent,
    EditProfileFormComponent,
    EditPasswordFormComponent,
    EditProfilePreferencesComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    AccountRoutingModule,
    SharedModule,
  ],
  providers: [AccountService],
})
export class AccountModule {}
