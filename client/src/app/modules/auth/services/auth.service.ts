import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {User, LoginDTO, updatePasswordDTO, SignupDTO} from "../models/User";

import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {BehaviorSubject, map} from "rxjs";
import {CacheService} from "../../shared/services/cache.service";

@Injectable()
export class AuthService {
  private apiDomain: string = environment.apiDomain;
  private userSubject: BehaviorSubject<User | null> =
    new BehaviorSubject<User | null>(null);
  public user$: Observable<User | null>;

  constructor(
    private http: HttpClient,
    private cacheService: CacheService
  ) {
    this.user$ = this.userSubject.asObservable();
  }

  public login(user: LoginDTO): Observable<void> {
    return this.http.post<User>(`${this.apiDomain}/login`, user).pipe(
      map((response) => {
        if (response?.tokens) {
          const user = {
            ...(response as User),
            tokens: response.tokens,
          };
          localStorage.setItem("user", JSON.stringify(user));
          this.userSubject.next(user);
        }
      })
    );
  }

  public refreshToken(refreshToken: string, email: string): Observable<User> {
    return this.http
      .post<User>(
        `${this.apiDomain}/refresh_token`,
        {email},
        {
          headers: {
            Authorization: `Bearer ${refreshToken}`,
          },
        }
      )
      .pipe(
        map((response) => {
          if (response?.tokens) {
            const user = {
              ...(response as User),
              tokens: response.tokens,
            };
            this.userSubject.next(user);
          }
          return response;
        })
      );
  }

  public logout(): void {
    this.cacheService.clearCache();
    this.userSubject.next(null);
    const root = document.documentElement;
    root.className = "";
  }

  public signup(user: SignupDTO): Observable<User> {
    return this.http.post<User>(`${this.apiDomain}/signup`, user);
  }

  public activateAccount(activationToken: string): Observable<void> {
    return this.http.post<void>(`${this.apiDomain}/activate_account`, {
      activationToken,
    });
  }

  public sendActivationEmail(email: string): Observable<User> {
    return this.http.post<User>(`${this.apiDomain}/send_activation_email`, {
      email,
    });
  }

  public sendResetToken(email: string): Observable<User> {
    return this.http.post<User>(`${this.apiDomain}/send_reset_password_email`, {
      email,
    });
  }

  public verifyResetToken(resetToken: string): Observable<User> {
    return this.http.post<User>(`${this.apiDomain}/verify_reset_token`, {
      resetToken,
    });
  }

  public updatePassword(user: updatePasswordDTO): Observable<User> {
    return this.http.post<User>(`${this.apiDomain}/reset_password`, user);
  }

  public get user(): User | null {
    return this.userSubject.value;
  }

  public set user(user: User) {
    const storedUserString: string | null = localStorage.getItem("user");
    const currentUser: User = storedUserString
      ? JSON.parse(storedUserString)
      : {};
    const updatedUser: User = {...currentUser, ...user};

    this.userSubject.next(updatedUser);
    localStorage.setItem("user", JSON.stringify(updatedUser));
  }

  public clearUser() {
    this.userSubject.next(null);
  }
}
