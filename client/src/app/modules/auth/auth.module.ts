import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {AuthenticationRoutingModule} from "./auth-routing.module";

/**
 * SERVICES
 */
import {AuthService} from "./services/auth.service";

/**
 * COMPONENTS
 */
import {LoginFormComponent} from "./components/login-form/login-form.component";
import {SignupFormComponent} from "./components/signup-form/signup-form.component";
import {HeaderComponent} from "./components/header/header.component";
import {LoginComponent} from "./pages/login/login.component";
import {SignupComponent} from "./pages/signup/signup.component";
import {AccountCreatedComponent} from "./pages/account-created/account-created.component";
import {AccountActivationComponent} from "./pages/account-activation/account-activation.component";
import {ResetPasswordComponent} from "./pages/reset-password/reset-password.component";
import {ForgottenPasswordComponent} from "./pages/forgotten-password/forgotten-password.component";
import {ForgottenPasswordFormComponent} from "./components/forgotten-password-form/forgotten-password-form.component";
import {ResetPasswordFormComponent} from "./components/reset-password-form/reset-password-form.component";
import {AccountActivationFormComponent} from "./components/account-activation-form/account-activation-form.component";

@NgModule({
  declarations: [
    LoginComponent,
    LoginFormComponent,
    SignupComponent,
    SignupFormComponent,
    AccountCreatedComponent,
    AccountActivationComponent,
    ResetPasswordComponent,
    HeaderComponent,
    SignupFormComponent,
    ForgottenPasswordComponent,
    ForgottenPasswordFormComponent,
    ResetPasswordFormComponent,
    AccountActivationFormComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthenticationRoutingModule,
  ],
  providers: [AuthService],
})
export class AuthModule {}
