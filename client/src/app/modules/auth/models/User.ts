export interface User {
  _id: string;
  email: string;
  firstname?: string;
  lastname?: string;
  password: string;
  confirm_password?: string;
  username: string;
  picture?: string;
  movies?: UserMovie[];
  series?: UserSeries[];
  games?: UserGame[];
  mangas?: UserMangas[];
  comics?: UserComic[];
  lists?: List[];
  activated: boolean;
  createdAt: Date;
  updatedAt: Date;
  theme: string;
  resetToken?: string;
  resetTokenExpiration?: Date;
  tokens?: {
    accessToken: string;
    refreshToken: string;
    twitchAccessToken: string;
  };
}

export interface List {
  _id: string;
  name: string;
  description?: string;
  works: {
    _id: string;
    cover: string;
    additionDate: Date;
    model: string;
  }[];
  createdAt: Date;
  updatedAt: Date;
}

export interface UserMovie {
  _id: string;
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
}

export interface UserSeries {
  _id: string;
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
  progress?: SeriesProgress;
}

export interface SeriesProgress {
  season: number;
  episode: number;
  completed: boolean;
}

export interface UserGame {
  _id: string;
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
  dlcs?: {
    providerId: number;
  }[];
}

export interface UserMangas {
  _id: string;
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
  progress?: MangaProgress[];
}

export interface MangaProgress {
  volume: number;
}

export interface UserComic {
  _id: string;
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
  progress?: ComicProgress[];
}

export interface ComicProgress {
  volume: number;
}

export interface UserActivity {
  movies?: number;
  series?: number;
  games?: number;
  // animation?: number,
  mangas?: number;
  comics?: number;
  // books?: number,
}

// DTOs

export interface UserDTO {
  firstname?: string;
  lastname?: string;
  username?: string;
  picture?: string;
  theme?: string;
}

export interface LoginDTO {
  email: string;
  password: string;
}

export interface SignupDTO {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  confirm_password: string;
}

export interface PasswordDTO {
  old_password: string;
  new_password: string;
  confirm_new_password: string;
}

export interface updatePasswordDTO {
  _id: string;
  password: string;
  confirm_password: string;
  resetToken: string;
}

export type UpdatedList = {
  operator: string;
  list: List;
};

export interface ListDTO {
  name?: string;
  description?: string;
}
