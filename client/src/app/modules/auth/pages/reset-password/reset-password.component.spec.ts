import {ComponentFixture, TestBed} from "@angular/core/testing";

import {ResetPasswordComponent} from "./reset-password.component";
import {ResetPasswordFormComponent} from "../../components/reset-password-form/reset-password-form.component";
import {HeaderComponent} from "../../components/header/header.component";
import {AuthService} from "../../services/auth.service";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterTestingModule} from "@angular/router/testing";

describe("ResetPasswordComponent", () => {
  let component: ResetPasswordComponent;
  let fixture: ComponentFixture<ResetPasswordComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule],
      declarations: [
        ResetPasswordComponent,
        ResetPasswordFormComponent,
        HeaderComponent,
      ],
      providers: [AuthService, HttpClient, HttpHandler],
    });
    fixture = TestBed.createComponent(ResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
