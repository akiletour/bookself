import {Component, OnDestroy, OnInit} from "@angular/core";
import {AuthService} from "../../services/auth.service";
import {ActivatedRoute} from "@angular/router";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {Subscription} from "rxjs";

@Component({
  selector: "app-account-activation",
  templateUrl: "./account-activation.component.html",
  styleUrls: ["./account-activation.component.scss"],
})
export class AccountActivationComponent implements OnInit, OnDestroy {
  error!: CustomError;
  loading: boolean = true;
  activationToken!: string;
  isTokenValid: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    const routeSubscription = this.route.params.subscribe((params) => {
      this.activationToken = params["token"];

      const activationSubscription = this.authService
        .activateAccount(this.activationToken)
        .subscribe({
          next: () => {
            this.loading = false;
            this.isTokenValid = true;
          },
          error: (error: CustomError) => {
            this.loading = false;
            this.error = error;
          },
        });
      this.subscriptions.push(activationSubscription);
    });
    this.subscriptions.push(routeSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
