import {ComponentFixture, TestBed} from "@angular/core/testing";

import {AccountActivationComponent} from "./account-activation.component";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {AuthService} from "../../services/auth.service";
import {RouterTestingModule} from "@angular/router/testing";
import {AccountActivationFormComponent} from "../../components/account-activation-form/account-activation-form.component";
import {HeaderComponent} from "../../components/header/header.component";

describe("AccountActivationComponent", () => {
  let component: AccountActivationComponent;
  let fixture: ComponentFixture<AccountActivationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, RouterTestingModule],
      declarations: [
        AccountActivationComponent,
        AccountActivationFormComponent,
        HeaderComponent,
      ],
      providers: [AuthService, HttpClient, HttpHandler],
    });
    fixture = TestBed.createComponent(AccountActivationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
