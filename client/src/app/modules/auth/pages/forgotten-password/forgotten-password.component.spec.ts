import {ComponentFixture, TestBed} from "@angular/core/testing";

import {ForgottenPasswordComponent} from "./forgotten-password.component";
import {ForgottenPasswordFormComponent} from "../../components/forgotten-password-form/forgotten-password-form.component";
import {HeaderComponent} from "../../components/header/header.component";
import {AuthService} from "../../services/auth.service";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";

describe("ForgottenPasswordComponent", () => {
  let component: ForgottenPasswordComponent;
  let fixture: ComponentFixture<ForgottenPasswordComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [
        ForgottenPasswordComponent,
        ForgottenPasswordFormComponent,
        HeaderComponent,
      ],
      providers: [AuthService, HttpClient, HttpHandler],
    });
    fixture = TestBed.createComponent(ForgottenPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
