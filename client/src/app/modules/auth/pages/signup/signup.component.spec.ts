import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SignupComponent} from "./signup.component";
import {HeaderComponent} from "../../components/header/header.component";
import {SignupFormComponent} from "../../components/signup-form/signup-form.component";
import {AuthService} from "../../services/auth.service";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";

describe("SignupComponent", () => {
  let component: SignupComponent;
  let fixture: ComponentFixture<SignupComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [SignupComponent, SignupFormComponent, HeaderComponent],
      providers: [AuthService, HttpClient, HttpHandler],
    });
    fixture = TestBed.createComponent(SignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
