import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import {LoginComponent} from "./pages/login/login.component";
import {authGuard} from "src/app/guards/auth.guard";
import {SignupComponent} from "./pages/signup/signup.component";
import {ResetPasswordComponent} from "./pages/reset-password/reset-password.component";
import {AccountCreatedComponent} from "./pages/account-created/account-created.component";
import {AccountActivationComponent} from "./pages/account-activation/account-activation.component";
import {ForgottenPasswordComponent} from "./pages/forgotten-password/forgotten-password.component";

export const authenticationRoutes: Routes = [
  {
    path: "login",
    component: LoginComponent,
    canActivate: [authGuard],
  },
  {
    path: "signup",
    component: SignupComponent,
    canActivate: [authGuard],
  },
  {
    path: "forgotten-password",
    component: ForgottenPasswordComponent,
    canActivate: [authGuard],
  },
  {
    path: "reset-password/:token",
    component: ResetPasswordComponent,
    canActivate: [authGuard],
  },
  {
    path: "account-created",
    component: AccountCreatedComponent,
    canActivate: [authGuard],
  },
  {
    path: "account-activation/:token",
    component: AccountActivationComponent,
    canActivate: [authGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(authenticationRoutes)],
  exports: [RouterModule],
})
export class AuthenticationRoutingModule {}
