import {Component, OnDestroy} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {AuthService} from "../../services/auth.service";
import {SignupDTO} from "../../models/User";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: "app-signup-form",
  templateUrl: "./signup-form.component.html",
  styleUrls: ["./signup-form.component.scss"],
})
export class SignupFormComponent implements OnDestroy {
  signupForm: FormGroup;
  error!: CustomError;
  private subscriptions: Subscription[] = [];
  public showPassword = false;
  public showConfirmPassword = false;
  public submitButtonText = "Sign up";

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.signupForm = this.formBuilder.group({
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required],
      confirm_password: ["", Validators.required],
    });
  }

  onSubmit() {
    if (this.signupForm.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    if (
      this.signupForm.value.password !== this.signupForm.value.confirm_password
    ) {
      this.error = {
        error: {
          error: "Passwords do not match",
        },
      };
      return;
    }

    this.submitButtonText = "Creating account...";
    const user: SignupDTO = {
      email: this.signupForm.value.email,
      firstname: this.signupForm.value.firstname,
      lastname: this.signupForm.value.lastname,
      password: this.signupForm.value.password,
      confirm_password: this.signupForm.value.confirm_password,
    };

    const signUpSubscription = this.authService.signup(user).subscribe({
      next: () => {
        this.router.navigate(["/account-created"]);
      },
      error: (error: CustomError) => {
        this.error = error;
        this.submitButtonText = "Sign up";
      },
    });
    this.subscriptions.push(signUpSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
