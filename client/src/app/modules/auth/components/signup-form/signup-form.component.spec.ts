import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SignupFormComponent} from "./signup-form.component";
import {AuthService} from "../../services/auth.service";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";

describe("SignupFormComponent", () => {
  let component: SignupFormComponent;
  let fixture: ComponentFixture<SignupFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [SignupFormComponent],
      providers: [AuthService, HttpClient, HttpHandler],
    });
    fixture = TestBed.createComponent(SignupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
