import {Component, OnDestroy} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {LoginDTO, User} from "../../models/User";
import {Subscription} from "rxjs";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"],
})
export class LoginFormComponent implements OnDestroy {
  activationEmailSent: boolean = false;
  loginForm: FormGroup;
  error!: CustomError;
  user!: User;
  public submitButtonText = "Log in";
  public showPassword = false;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required],
    });
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    this.submitButtonText = "Logging in...";
    const user: LoginDTO = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    };

    const loginSubscription = this.authService.login(user).subscribe({
      next: () => {
        this.router.navigate(["/"]);
      },
      error: (error: CustomError) => {
        this.submitButtonText = "Log in";
        this.error = error;
      },
    });
    this.subscriptions.push(loginSubscription);
    return false;
  }

  sendActivationEmail() {
    const activationEmailSubscription = this.authService
      .sendActivationEmail(this.loginForm.value.email)
      .subscribe({
        next: () => {
          this.activationEmailSent = true;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(activationEmailSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
