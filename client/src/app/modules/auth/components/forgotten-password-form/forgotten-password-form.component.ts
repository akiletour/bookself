import {Component, OnDestroy} from "@angular/core";
import {AuthService} from "../../services/auth.service";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";

@Component({
  selector: "app-forgotten-password-form",
  templateUrl: "./forgotten-password-form.component.html",
  styleUrls: ["./forgotten-password-form.component.scss"],
})
export class ForgottenPasswordFormComponent implements OnDestroy {
  sendTokenForm: FormGroup;
  tokenSent: boolean = false;
  error!: CustomError;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    this.sendTokenForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
    });
  }

  onSubmit() {
    if (this.sendTokenForm.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    const sendResetTokenSubscription = this.authService
      .sendResetToken(this.sendTokenForm.value.email)
      .subscribe({
        next: () => {
          this.tokenSent = true;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(sendResetTokenSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
