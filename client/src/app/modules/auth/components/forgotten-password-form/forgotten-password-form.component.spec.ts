import {ComponentFixture, TestBed} from "@angular/core/testing";

import {ForgottenPasswordFormComponent} from "./forgotten-password-form.component";
import {AuthService} from "../../services/auth.service";
import {HttpClient, HttpHandler} from "@angular/common/http";
import {ReactiveFormsModule} from "@angular/forms";

describe("ForgottenPasswordFormComponent", () => {
  let component: ForgottenPasswordFormComponent;
  let fixture: ComponentFixture<ForgottenPasswordFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ForgottenPasswordFormComponent],
      providers: [AuthService, HttpClient, HttpHandler],
    });
    fixture = TestBed.createComponent(ForgottenPasswordFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
