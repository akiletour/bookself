import {Component, Renderer2} from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"],
})
export class HeaderComponent {
  constructor(private renderer: Renderer2) {}

  ngAfterViewInit() {
    this.renderer.addClass(document.body, "arts-bg");
  }

  ngOnDestroy() {
    this.renderer.removeClass(document.body, "arts-bg");
  }
}
