import {Component, OnDestroy, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {ActivatedRoute} from "@angular/router";
import {User, updatePasswordDTO} from "../../models/User";
import {Subscription} from "rxjs";

@Component({
  selector: "app-reset-password-form",
  templateUrl: "./reset-password-form.component.html",
  styleUrls: ["./reset-password-form.component.scss"],
})
export class ResetPasswordFormComponent implements OnInit, OnDestroy {
  loading: boolean = true;
  user!: User;
  resetToken!: string;
  isTokenValid: boolean = false;
  resetPasswordForm: FormGroup;
  passwordChanged: boolean = false;
  public showNewPassword = false;
  public showNewConfirmPassword = false;
  error!: CustomError;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private route: ActivatedRoute
  ) {
    this.resetPasswordForm = this.formBuilder.group({
      password: ["", Validators.required],
      confirm_password: ["", Validators.required],
    });
  }

  ngOnInit() {
    const routeSubscription = this.route.params.subscribe((params) => {
      this.resetToken = params["token"];

      const verifyTokenSubscription = this.authService
        .verifyResetToken(this.resetToken)
        .subscribe({
          next: (res: User) => {
            this.loading = false;
            this.isTokenValid = true;
            this.user = res!;
          },
          error: (error: CustomError) => {
            this.loading = false;
            this.error = error;
          },
        });
      this.subscriptions.push(verifyTokenSubscription);
    });
    this.subscriptions.push(routeSubscription);
  }

  onSubmit() {
    if (this.resetPasswordForm.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    if (
      this.resetPasswordForm.value.password !==
      this.resetPasswordForm.value.confirm_password
    ) {
      this.error = {
        error: {
          error: "Passwords do not match",
        },
      };
      return;
    }

    const userPasswordDTO: updatePasswordDTO = {
      _id: this.user._id,
      password: this.resetPasswordForm.value.password,
      confirm_password: this.resetPasswordForm.value.confirm_password,
      resetToken: this.resetToken,
    };

    const updatePasswordSubscription = this.authService
      .updatePassword(userPasswordDTO)
      .subscribe({
        next: () => {
          this.passwordChanged = true;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updatePasswordSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
