import {ComponentFixture, TestBed} from "@angular/core/testing";

import {AccountActivationFormComponent} from "./account-activation-form.component";
import {ReactiveFormsModule} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {HttpClient, HttpHandler} from "@angular/common/http";

describe("AccountActivationFormComponent", () => {
  let component: AccountActivationFormComponent;
  let fixture: ComponentFixture<AccountActivationFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [AccountActivationFormComponent],
      providers: [AuthService, HttpClient, HttpHandler],
    });
    fixture = TestBed.createComponent(AccountActivationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
