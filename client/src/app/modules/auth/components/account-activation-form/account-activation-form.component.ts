import {Component, OnDestroy} from "@angular/core";
import {AuthService} from "../../services/auth.service";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";

@Component({
  selector: "app-account-activation-form",
  templateUrl: "./account-activation-form.component.html",
  styleUrls: ["./account-activation-form.component.scss"],
})
export class AccountActivationFormComponent implements OnDestroy {
  activationForm: FormGroup;
  activationSent: boolean = false;
  error!: CustomError;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {
    this.activationForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
    });
  }

  onSubmit() {
    if (this.activationForm.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    const sendActivationEmailSubscription = this.authService
      .sendActivationEmail(this.activationForm.value.email)
      .subscribe({
        next: () => {
          this.activationSent = true;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(sendActivationEmailSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
