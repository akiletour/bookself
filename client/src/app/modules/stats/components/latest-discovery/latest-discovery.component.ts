import {Component, Input, OnChanges, SimpleChanges} from "@angular/core";
import {WorksTypes} from "src/app/enums/Works.enum";
import {Comic} from "src/app/models/Comic";
import {Manga} from "src/app/models/Manga";
import {
  WorkDetailsComponent,
  WorksModel,
  isComic,
  isGame,
  isManga,
  isMovie,
  isSeries,
} from "src/app/models/WorksModel";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {WorksService} from "src/app/modules/shared/services/works.service";
import {environment} from "src/environments/environment";

@Component({
  selector: "app-latest-discovery",
  templateUrl: "./latest-discovery.component.html",
  styleUrls: ["./latest-discovery.component.scss"],
})
export class LatestDiscoveryComponent implements OnChanges {
  @Input() content!: {[key in WorksTypes]?: Array<WorksModel>};
  @Input() isMobile: boolean = false;
  public latestDiscovery!: WorksModel;
  public type!: WorksTypes;
  public isMovie = isMovie;
  public isSeries = isSeries;
  public isGame = isGame;
  public isManga = isManga;
  public isComic = isComic;
  public imagesPath!: string;
  public detailsComponent!: WorkDetailsComponent;
  private imagesDomain: string = environment.imagesDomain;

  constructor(
    public modalService: ModalService,
    private worksService: WorksService
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["content"] && this.content) {
      this.latestDiscovery = this.getLastAddedWork(this.content);
      this.defineImagesPath(this.type);

      // Define card component based on type
      this.detailsComponent = this.worksService.getWorksDetailsComponentByType(
        this.type
      );
    }
  }

  public isMangaOrComic(work: WorksModel): work is Manga | Comic {
    return this.isManga(work) || this.isComic(work);
  }

  get mangaOrComicItem(): Manga | Comic | null {
    return this.isMangaOrComic(this.latestDiscovery)
      ? this.latestDiscovery
      : null;
  }

  defineImagesPath(type: WorksTypes) {
    this.imagesPath = this.imagesDomain + "/" + type;
  }

  getLastAddedWork(content: {
    [key in WorksTypes]?: Array<WorksModel>;
  }): WorksModel {
    let latestDiscovery: WorksModel;

    Object.entries(content).forEach(([key, worksArray]) => {
      if (worksArray && worksArray.length > 0) {
        const sortedWorksArray = worksArray.slice().sort((a, b) => {
          const dateA = new Date(a.discoveryDate).getTime();
          const dateB = new Date(b.discoveryDate).getTime();
          return dateB - dateA;
        });

        if (
          !latestDiscovery ||
          new Date(sortedWorksArray[0].discoveryDate) >
            new Date(latestDiscovery.discoveryDate)
        ) {
          latestDiscovery = sortedWorksArray[0];
          this.type = key as WorksTypes;
        }
      }
    });

    return latestDiscovery!;
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }
}
