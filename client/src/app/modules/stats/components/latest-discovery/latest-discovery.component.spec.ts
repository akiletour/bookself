import {ComponentFixture, TestBed} from "@angular/core/testing";

import {LatestDiscoveryComponent} from "./latest-discovery.component";

describe("LatestDiscoveryComponent", () => {
  let component: LatestDiscoveryComponent;
  let fixture: ComponentFixture<LatestDiscoveryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LatestDiscoveryComponent],
    });
    fixture = TestBed.createComponent(LatestDiscoveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
