import {Component, Input, OnDestroy} from "@angular/core";
import {Subscription} from "rxjs";
import {WorksTypes, WorksLabels} from "src/app/enums/Works.enum";
import {UpdatedWork} from "src/app/models/WorksModel";
import {User, UserActivity} from "src/app/modules/auth/models/User";
import {UserService} from "src/app/modules/shared/services/user.service";
import {WorksService} from "src/app/modules/shared/services/works.service";

@Component({
  selector: "app-month-activity",
  templateUrl: "./month-activity.component.html",
  styleUrls: ["./month-activity.component.scss"],
})
export class MonthActivityComponent implements OnDestroy {
  @Input() user!: User;
  @Input() isMobile: boolean = false;
  public worksActivities: {[key in WorksTypes]?: number} = {};
  currentDate = new Date();
  public currentMonth = this.currentDate.toLocaleString("en", {month: "long"});
  private subscriptions: Subscription[] = [];

  constructor(
    private userService: UserService,
    private worksService: WorksService
  ) {
    const worksUpdatedSubscription = this.worksService.worksUpdated$.subscribe(
      (update: UpdatedWork) => {
        if (!update.list) {
          if (update.operator === "add") {
            this.worksActivities![update.type]
              ? (this.worksActivities as {[key: string]: number})[update.type]++
              : (this.worksActivities![update.type] = 1);
          } else if (update.operator === "remove") {
            if (this.worksActivities![update.type]) {
              (this.worksActivities as {[key: string]: number})[update.type]--;
            }
          }
        }
      }
    );
    this.subscriptions.push(worksUpdatedSubscription);
  }

  ngOnInit() {
    this.getUserActivity(this.user);
  }

  getUserActivity(user: User) {
    const userActivitySubscription = this.userService
      .getUserActivity(user, "month")
      .subscribe({
        next: (activity: UserActivity) => {
          this.worksActivities = activity;
        },
      });
    this.subscriptions.push(userActivitySubscription);
  }

  getSortedWorksActivities(obj: {[key in WorksTypes]?: number}): {
    key: string;
    value: number;
  }[] {
    const entries = Object.entries(obj);
    return entries
      .sort((a, b) => b[1] - a[1])
      .map(([key, value]) => ({key, value}));
  }

  public getWorkLabel(key: string): string {
    return WorksLabels[key as keyof typeof WorksLabels] || key;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
