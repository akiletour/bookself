import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";

import {loggedGuard} from "src/app/guards/logged.guard";
import {DashboardComponent} from "./pages/dashboard/dashboard.component";

export const statsRoutes: Routes = [
  {
    path: "",
    canActivate: [loggedGuard],
    children: [
      {
        path: "",
        component: DashboardComponent,
        pathMatch: "full",
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(statsRoutes)],
  exports: [RouterModule],
})
export class StatsRoutingModule {}
