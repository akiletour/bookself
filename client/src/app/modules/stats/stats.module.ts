import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LatestDiscoveryComponent} from "./components/latest-discovery/latest-discovery.component";
import {MonthActivityComponent} from "./components/month-activity/month-activity.component";
import {SharedModule} from "../shared/shared.module";
import {RouterModule} from "@angular/router";
import {DashboardComponent} from "./pages/dashboard/dashboard.component";
import {StatsRoutingModule} from "./stats-routing.module";

@NgModule({
  declarations: [
    LatestDiscoveryComponent,
    MonthActivityComponent,
    DashboardComponent,
  ],
  imports: [CommonModule, RouterModule, SharedModule, StatsRoutingModule],
  exports: [LatestDiscoveryComponent, MonthActivityComponent],
})
export class StatsModule {}
