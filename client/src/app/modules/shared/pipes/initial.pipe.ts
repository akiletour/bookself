import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "initial",
})
export class InitialPipe implements PipeTransform {
  transform(value: string): string {
    if (value) {
      return value.slice(0, 3) + ".";
    }
    return "";
  }
}
