import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "duration",
})
export class DurationPipe implements PipeTransform {
  transform(minutes: string | undefined): string {
    if (minutes) {
      const hours = Math.floor(parseInt(minutes) / 60);
      const remainingMinutes = parseInt(minutes) % 60;

      if (hours > 0 && remainingMinutes > 0) {
        return `${hours}h ${remainingMinutes}m`;
      } else if (hours > 0) {
        return `${hours}h`;
      } else {
        return `${remainingMinutes}m`;
      }
    } else {
      return "";
    }
  }
}
