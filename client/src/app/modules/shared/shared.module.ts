import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ModalComponent} from "./components/modal/modal.component";
import {WorksService} from "./services/works.service";
import {AddWorkComponent} from "./components/add-work/add-work.component";
import {ListHeaderComponent} from "./components/list-header/list-header.component";
import {ListContentComponent} from "./components/list-content/list-content.component";
import {MoviesDetailsComponent} from "./components/movies-details/movies-details.component";
import {ListContentByDateComponent} from "./components/list-content-by-date/list-content-by-date.component";
import {ModalService} from "./services/modal.service";
import {RouterModule} from "@angular/router";
import {DurationPipe} from "src/app/modules/shared/pipes/duration.pipe";
import {InitialPipe} from "src/app/modules/shared/pipes/initial.pipe";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SeriesDetailsComponent} from "./components/series-details/series-details.component";
import {UserService} from "./services/user.service";
import {GuardTypePipe} from "./pipes/guardType.pipe";
import {WorkCardComponent} from "./components/work-card/work-card.component";
import {SeriesService} from "./services/series.service";
import {WorkDateComponent} from "./components/work-date/work-date.component";
import {EditWorkDateComponent} from "./components/edit-work-date/edit-work-date.component";

// Material
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MAT_DATE_LOCALE, MatNativeDateModule} from "@angular/material/core";
import {GamesDetailsComponent} from "./components/games-details/games-details.component";
import {GameService} from "./services/game.service";
import {NgxSkeletonLoaderModule} from "ngx-skeleton-loader";
import {SkeletonDashboardComponent} from "./components/skeleton-dashboard/skeleton-dashboard.component";
import {SkeletonWorksListComponent} from "./components/skeleton-works-list/skeleton-works-list.component";
import {SkeletonSearchWorkComponent} from "./components/skeleton-search-work/skeleton-search-work.component";
import {ConfigService} from "./services/config.service";
import {EditWorkRatingComponent} from "./components/edit-work-rating/edit-work-rating.component";
import {WorkDetailsImageComponent} from "./components/work-details-image/work-details-image.component";
import {AddToListComponent} from "./components/add-to-list/add-to-list.component";
import {CreateListComponent} from "./components/create-list/create-list.component";
import {ListsService} from "./services/list.service";
import {ListCollectionsComponent} from "./components/list-collections/list-collections.component";
import {CollectionCardComponent} from "./components/collection-card/collection-card.component";
import {CollectionCardDataComponent} from "./components/collection-card-data/collection-card-data.component";
import {SkeletonCollectionsListComponent} from "./components/skeleton-collections-list/skeleton-collections-list.component";
import {SkeletonCollectionDetailsComponent} from "./components/skeleton-collection-details/skeleton-collection-details.component";
import {DetailsAddToListComponent} from "./components/details-add-to-list/details-add-to-list.component";
import {ToastComponent} from "./components/toast/toast.component";
import {EditListComponent} from "./components/edit-list/edit-list.component";
import {NewsModalComponent} from "./components/news-modal/news-modal.component";
import {WorkDeleteComponent} from "./components/work-delete/work-delete.component";
import {ScreenSizeService} from "./services/screenSize.service";
import {FiltersMobileComponent} from "./components/filters-mobile/filters-mobile.component";
import {FiltersComponent} from "./components/filters/filters.component";
import {SkeletonHistoryComponent} from "./components/skeleton-history/skeleton-history.component";
import {BaseFiltersComponent} from "./components/base-filters/base-filters.component";
import {MangasDetailsComponent} from "./components/mangas-details/mangas-details.component";
import {MangaService} from "./services/manga.service";
import {MangaVolumeComponent} from "./components/manga-volume/manga-volume.component";
import {ComicsDetailsComponent} from "./components/comics-details/comics-details.component";
import {ComicIssueComponent} from "./components/comic-issue/comic-issue.component";
import {ComicService} from "./services/comic.service";
import {SkeletonSearchItemComponent} from "./components/skeleton-search-item/skeleton-search-item.component";
import {WorkListsComponent} from "./components/work-lists/work-lists.component";
@NgModule({
  declarations: [
    ModalComponent,
    AddWorkComponent,
    ListHeaderComponent,
    ListContentComponent,
    ListContentByDateComponent,
    MoviesDetailsComponent,
    DurationPipe,
    InitialPipe,
    GuardTypePipe,
    SeriesDetailsComponent,
    WorkCardComponent,
    WorkDateComponent,
    EditWorkDateComponent,
    GamesDetailsComponent,
    SkeletonDashboardComponent,
    SkeletonWorksListComponent,
    SkeletonSearchWorkComponent,
    EditWorkRatingComponent,
    WorkDetailsImageComponent,
    AddToListComponent,
    CreateListComponent,
    ListCollectionsComponent,
    CollectionCardComponent,
    CollectionCardDataComponent,
    SkeletonCollectionsListComponent,
    SkeletonCollectionDetailsComponent,
    DetailsAddToListComponent,
    ToastComponent,
    EditListComponent,
    NewsModalComponent,
    WorkDeleteComponent,
    FiltersMobileComponent,
    FiltersComponent,
    SkeletonHistoryComponent,
    BaseFiltersComponent,
    MangasDetailsComponent,
    MangaVolumeComponent,
    ComicsDetailsComponent,
    ComicIssueComponent,
    SkeletonSearchItemComponent,
    WorkListsComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxSkeletonLoaderModule,
  ],
  providers: [
    WorksService,
    ModalService,
    UserService,
    SeriesService,
    GameService,
    MangaService,
    ComicService,
    ListsService,
    ConfigService,
    ScreenSizeService,
    {provide: MAT_DATE_LOCALE, useValue: "fr-FR"},
  ],
  exports: [
    ModalComponent,
    AddWorkComponent,
    ListHeaderComponent,
    ListContentComponent,
    ListContentByDateComponent,
    MoviesDetailsComponent,
    SkeletonDashboardComponent,
    SkeletonWorksListComponent,
    SkeletonSearchWorkComponent,
    SkeletonSearchItemComponent,
    DurationPipe,
    InitialPipe,
    GuardTypePipe,
    CreateListComponent,
    ListCollectionsComponent,
    SkeletonCollectionsListComponent,
    SkeletonCollectionDetailsComponent,
    SkeletonHistoryComponent,
    EditListComponent,
    NewsModalComponent,
    ToastComponent,
  ],
})
export class SharedModule {}
