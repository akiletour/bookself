import {
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {WorksService} from "../../services/works.service";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {
  Subject,
  Subscription,
  catchError,
  debounceTime,
  of,
  switchMap,
} from "rxjs";

@Component({
  selector: "app-edit-work-rating",
  templateUrl: "./edit-work-rating.component.html",
  styleUrls: ["./edit-work-rating.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
  ],
})
export class EditWorkRatingComponent implements OnInit, OnDestroy {
  @Input() item!: WorksModel;
  @ViewChild("rating") rating!: ElementRef;
  ratingForm: FormGroup;
  private inputChangeSubject = new Subject<number>();
  user!: User;
  public currentRating?: number;
  public success?: string;
  public error?: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private worksService: WorksService,
    private AuthService: AuthService,
    private fb: FormBuilder
  ) {
    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);

    this.ratingForm = this.fb.group({
      rating: ["", Validators.required],
    });
  }

  ngOnInit() {
    if (this.item.userRating) {
      this.currentRating = this.item.userRating;

      this.ratingForm.setValue({
        rating: this.item.userRating,
      });
    }

    const inputSubscription = this.inputChangeSubject
      .pipe(
        debounceTime(1000),
        switchMap((userRating) => {
          this.item.userRating = this.ratingForm.get("rating")?.value;
          return this.worksService
            .updateWorkRatingByType(
              this.user._id,
              this.item._id,
              this.item.type,
              userRating
            )
            .pipe(
              catchError(() => {
                return of(null);
              })
            );
        })
      )
      .subscribe({
        next: (user: User | null) => {
          if (user) {
            this.success = "Rating updated !";
            setTimeout(() => {
              this.success = "";
            }, 3000);
          }
        },
        error: () => {
          this.error = "Error during update. Please try again";
        },
      });
    this.subscriptions.push(inputSubscription);
  }

  focusRatingForm() {
    this.rating.nativeElement.focus();
  }

  onInputChange(event: Event): void {
    this.error = "";

    const inputElement = event.target as HTMLInputElement;

    if (inputElement && inputElement.value !== "") {
      inputElement.value = inputElement.value.replace(/,/g, ".");
      const userRating: number = parseFloat(inputElement.value);
      let roundedRating =
        userRating % 1 >= 0.5 ? Math.ceil(userRating) : Math.floor(userRating);
      inputElement.value = roundedRating.toString();
      this.currentRating = roundedRating;

      if (roundedRating > 100) {
        roundedRating = 100;
        this.ratingForm.get("rating")?.setValue(100);
      }

      this.inputChangeSubject.next(roundedRating);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
