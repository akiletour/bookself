import {ComponentFixture, TestBed} from "@angular/core/testing";

import {EditWorkRatingComponent} from "./edit-work-rating.component";

describe("EditWorkRatingComponent", () => {
  let component: EditWorkRatingComponent;
  let fixture: ComponentFixture<EditWorkRatingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditWorkRatingComponent],
    });
    fixture = TestBed.createComponent(EditWorkRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
