import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Season, Series} from "src/app/models/Series";
import {CustomError} from "../../interfaces/CustomError.interface";
import {SeriesService} from "../../services/series.service";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {User} from "src/app/modules/auth/models/User";
import {
  Subject,
  Subscription,
  debounceTime,
  distinctUntilChanged,
  filter,
  merge,
  switchMap,
} from "rxjs";
import {WorksTypes} from "src/app/enums/Works.enum";
import {state, style, transition, trigger, animate} from "@angular/animations";
import {environment} from "src/environments/environment";
import {MoviesProviders} from "src/app/enums/Movie";
import {WorksDTO, WorksModel} from "src/app/models/WorksModel";
import {WorksService} from "../../services/works.service";
import {CacheService} from "../../services/cache.service";
import {ScreenSizeService} from "../../services/screenSize.service";

@Component({
  selector: "app-series-details",
  templateUrl: "./series-details.component.html",
  styleUrls: ["./series-details.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
  ],
})
export class SeriesDetailsComponent
  implements OnInit, OnChanges, AfterViewInit, OnDestroy
{
  @Input() item!: Series;
  @Input() list?: string;
  @Input() type!: WorksTypes;
  public user!: User;
  season$ = new Subject<number>();
  episode$ = new Subject<number>();
  progressForm: FormGroup;
  public error?: CustomError;
  public success!: string;
  public imagesDomain: string = environment.imagesDomain;
  public seriesProviders = MoviesProviders;
  addError: CustomError | undefined;
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;
  public isTablet: boolean = false;
  @ViewChild("seriesDetailsElement", {static: false})
  gameDetailsElement!: ElementRef;

  constructor(
    private formBuilder: FormBuilder,
    private seriesService: SeriesService,
    private AuthService: AuthService,
    private elRef: ElementRef,
    private worksService: WorksService,
    private cacheService: CacheService,
    private screenSizeService: ScreenSizeService
  ) {
    this.progressForm = this.formBuilder.group({
      season: [1, Validators.min(1)],
      episode: [1, Validators.min(1)],
    });

    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
    const screenSizeTabletSubscription =
      this.screenSizeService.isTablet$.subscribe((isTablet) => {
        this.isTablet = isTablet;
      });
    this.subscriptions.push(screenSizeTabletSubscription);

    this.setSeasons(this.item);
    if (this.item.seasons && this.item.seasons.length > 0) {
      const episodeSubscription = this.episode$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((episode: number) => {
          const progress = {
            season: parseInt(this.season!.value),
            episode,
            completed: false,
          };

          if (
            this.season!.value === this.totalSeasons &&
            this.episode!.value == this.totalEpisodes
          ) {
            progress.completed = true;
          }

          this.item.progress = progress;

          return this.seriesService.updateUserSeriesProgress(
            this.item,
            this.user,
            progress
          );
        })
      );

      const seasonSubscription = this.season$.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((season: number) => {
          const progress = {
            season,
            episode: 1,
            completed: false,
          };

          if (
            this.season!.value === this.totalSeasons &&
            this.episode!.value == this.totalEpisodes
          ) {
            progress.completed = true;
          }

          this.item.progress = progress;

          return this.seriesService.updateUserSeriesProgress(
            this.item,
            this.user,
            progress
          );
        })
      );

      const progressSubscription = merge(
        episodeSubscription,
        seasonSubscription
      )
        .pipe(filter((value) => value !== null))
        .subscribe({
          next: () => {
            this.seriesService.notifySeriesUpdated(this.item);
            this.cacheService.updateInCacheByPrefix(
              ["series_", "history_", "list_"],
              this.item
            );
            this.error = undefined;
            this.success = "Progression updated !";
            setTimeout(() => {
              this.success = "";
            }, 3000);
          },
          error: (error: CustomError) => {
            this.error = error;
          },
        });
      this.subscriptions.push(progressSubscription);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["item"] && this.item) {
      this.setSeasons(this.item);
    }
  }

  ngAfterViewInit() {
    const rangeInputs = this.elRef.nativeElement.querySelectorAll(
      'input[type="range"'
    ) as NodeListOf<HTMLInputElement>;
    rangeInputs.forEach((input: HTMLInputElement) => {
      this.updateRange(input);
    });

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          // Check for series updates once component is rendered in the DOM
          const seriesUpdatedSubscription = this.seriesService
            .updateSeries(this.item._id, this.item.provider)
            .subscribe({
              next: (series) => {
                if (series.seasons) {
                  const filteredSeasons = series.seasons.filter(
                    (season) => season.name !== "Specials"
                  );
                  this.item.seasons =
                    filteredSeasons.length > 0
                      ? (filteredSeasons as Season[])
                      : undefined;

                  this.item.numberOfEpisodes = series.numberOfEpisodes;
                  this.item.numberOfSeasons = series.numberOfSeasons;
                  this.item.status = series.status;

                  // Update slider
                  this.progressForm.setValue({
                    season: this.item.progress?.season ?? 1,
                    episode: this.item.progress?.episode ?? 1,
                  });

                  this.seriesService.notifySeriesUpdated(this.item);

                  this.cacheService.updateInCacheByPrefix(
                    ["series_", "history_", "list_"],
                    this.item
                  );
                }
                // Stop observing once series has been updated
                observer.disconnect();
              },
              error: (error: CustomError) => {
                this.error = error;
              },
            });
          this.subscriptions.push(seriesUpdatedSubscription);
        }
      });
    });

    if (this.item && this.gameDetailsElement) {
      observer.observe(this.gameDetailsElement.nativeElement);
    }
  }

  setSeasons(item: Series) {
    if (item.progress) {
      this.progressForm.setValue({
        season: item.progress.season ?? 1,
        episode: item.progress.episode ?? 1,
      });
    }

    if (item.seasons && item.seasons.length > 0) {
      const filteredSeasons = item.seasons.filter(
        (season) => season.name !== "Specials"
      );
      this.item.seasons =
        filteredSeasons.length > 0 ? (filteredSeasons as Season[]) : undefined;
    }
  }

  onInputChange(event: Event): void {
    const inputElement = event.target as HTMLInputElement;
    if (inputElement) {
      const tempSliderValue: number = parseInt(inputElement.value);

      // Pass next values
      if (inputElement.getAttribute("formcontrolname") == "season") {
        this.updateRange(inputElement);

        // Change season : reset episode range style before passing value
        this.progressForm.setValue({
          season: tempSliderValue,
          episode: 1,
        });
        const inputEpisode = document.querySelector(
          'input[type="range"][formcontrolname="episode"'
        ) as HTMLInputElement;
        this.updateRange(inputEpisode);

        this.season$.next(tempSliderValue);
      } else if (inputElement.getAttribute("formcontrolname") == "episode") {
        this.updateRange(inputElement);

        this.episode$.next(tempSliderValue);
      }
    }
  }

  updateRange(input: HTMLInputElement) {
    // Range style
    const tempSliderValue: number = parseInt(input.value);
    let progress: number;

    if (input.getAttribute("formcontrolname") == "season") {
      progress = ((tempSliderValue - 1) / (this.totalSeasons - 1)) * 100;
    } else if (input.getAttribute("formcontrolname") == "episode") {
      progress = ((tempSliderValue - 1) / (this.totalEpisodes - 1)) * 100;
    }

    input.style.cssText = `
    border-radius: 8px;
    background: linear-gradient(to right, rgb(255 230 3 / 100%), rgb(235 127 0 / 100%) ${progress!}%, #E5E5E5 ${progress!}%)`;
  }

  get season() {
    return this.progressForm.get("season");
  }

  get episode() {
    return this.progressForm.get("episode");
  }

  get totalSeasons() {
    return this.item.seasons!.length;
  }

  get totalEpisodes() {
    const selectedSeason = this.item.seasons![this.season!.value - 1];
    return selectedSeason ? selectedSeason.numberOfEpisodes : 0;
  }

  addFromList(work: WorksModel, user: User, event: Event): void {
    const WorksDTO: WorksDTO = {
      provider: work.provider,
      providerId: work.providerId,
    };
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      this.addError = undefined;
      linkElement.innerHTML = "Adding...";

      const addWorkSubscription = this.worksService
        .addWorkByType(WorksDTO, WorksTypes.Series, user)
        .subscribe({
          next: (work: WorksModel | null) => {
            if (work) {
              this.item = work as Series;
            }
          },
          error: (error: CustomError) => {
            linkElement.innerHTML = "Add";
            this.addError = error;
            setTimeout(() => {
              this.addError = undefined;
            }, 3000);
          },
        });
      this.subscriptions.push(addWorkSubscription);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
