import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";

@Component({
  selector: "app-skeleton-works-list",
  templateUrl: "./skeleton-works-list.component.html",
})
export class SkeletonWorksListComponent {
  @Input() user!: User;
}
