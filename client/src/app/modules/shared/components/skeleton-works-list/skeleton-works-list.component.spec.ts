import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SkeletonWorksListComponent} from "./skeleton-works-list.component";

describe("SkeletonWorksListComponent", () => {
  let component: SkeletonWorksListComponent;
  let fixture: ComponentFixture<SkeletonWorksListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SkeletonWorksListComponent],
    });
    fixture = TestBed.createComponent(SkeletonWorksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
