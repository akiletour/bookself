import {trigger, transition, style, animate} from "@angular/animations";
import {Component, Input} from "@angular/core";

@Component({
  selector: "app-toast",
  templateUrl: "./toast.component.html",
  animations: [
    trigger("fadeInOut", [
      transition(":enter", [
        style({opacity: 0}),
        animate("0.05s", style({opacity: 0})),
        animate("0.1s", style({opacity: 1})),
      ]),
      transition(":leave", [animate("0.1s", style({opacity: 0}))]),
    ]),
  ],
})
export class ToastComponent {
  @Input() message?: string;
  @Input() classes?: string;
  @Input() offline: boolean = false;

  public getClasses() {
    const classObject: {[key: string]: boolean} = {
      "!bottom-10 sm:!bottom-4 !right-auto !left-2 sm:!text-sm": this.offline,
      [this.classes as string]: this.classes !== undefined,
    };

    return classObject;
  }
}
