import {Component, Input} from "@angular/core";
import {List, User} from "src/app/modules/auth/models/User";
import {ModalService} from "../../services/modal.service";

@Component({
  selector: "app-list-collections",
  templateUrl: "./list-collections.component.html",
  styleUrls: ["./list-collections.component.scss"],
})
export class ListCollectionsComponent {
  @Input() content!: List[];
  @Input() add: boolean = false;
  @Input() addLabel?: string;
  @Input() user!: User;

  constructor(public modalService: ModalService) {}

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }
}
