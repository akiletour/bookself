import {Component, EventEmitter, Input, OnDestroy, Output} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomError} from "../../interfaces/CustomError.interface";
import {ListsService} from "../../services/list.service";
import {User} from "src/app/modules/auth/models/User";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {ModalService} from "../../services/modal.service";
import {Subscription} from "rxjs";

@Component({
  selector: "app-create-list",
  templateUrl: "./create-list.component.html",
  styleUrls: ["./create-list.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
  ],
})
export class CreateListComponent implements OnDestroy {
  @Input() user!: User;
  @Output() listCreated = new EventEmitter<string>();
  createList: FormGroup;
  public error?: CustomError;
  public success?: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private listsService: ListsService,
    private modalService: ModalService
  ) {
    this.createList = this.formBuilder.group({
      name: ["", Validators.required],
      description: [""],
    });
  }

  onSubmit() {
    this.error = undefined;
    this.success = undefined;

    if (this.createList.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    const list = {
      name: this.createList.value.name,
      description: this.createList.value.description,
    };

    const addListSubscription = this.listsService
      .addList(this.user, list)
      .subscribe({
        next: () => {
          this.success = "List created !";
          this.listCreated.emit(list.name);
          this.createList.reset();
          setTimeout(() => {
            this.success = "";
            this.modalService.closeModal("addList");
          }, 2000);
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(addListSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
