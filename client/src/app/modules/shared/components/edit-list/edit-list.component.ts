import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {List, User} from "src/app/modules/auth/models/User";
import {CustomError} from "../../interfaces/CustomError.interface";
import {ListsService} from "../../services/list.service";
import {ModalService} from "../../services/modal.service";
import {Subscription} from "rxjs";
import {trigger, state, style, transition, animate} from "@angular/animations";

@Component({
  selector: "app-edit-list",
  templateUrl: "./edit-list.component.html",
  styleUrls: ["./edit-list.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
  ],
})
export class EditListComponent implements OnInit, OnDestroy {
  @Input() user!: User;
  @Input() list!: List;
  @Output() listCreated = new EventEmitter<string>();
  updateList: FormGroup;
  public error?: CustomError;
  public success?: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private listsService: ListsService,
    private modalService: ModalService
  ) {
    this.updateList = this.formBuilder.group({
      name: ["", Validators.required],
      description: [""],
    });
  }

  ngOnInit() {
    this.updateList.setValue({
      name: this.list.name,
      description: this.list.description,
    });
  }

  onSubmit() {
    this.error = undefined;
    this.success = undefined;

    if (this.updateList.invalid) {
      this.error = {
        error: {
          error: "Please fill all required fields",
        },
      };
      return;
    }

    const list = {
      name: this.updateList.value.name,
      description: this.updateList.value.description,
    };

    const listUpdateSubscription = this.listsService
      .updateList(this.user, this.list._id, list)
      .subscribe({
        next: () => {
          this.success = "List updated !";
          this.listCreated.emit(list.name);
          setTimeout(() => {
            this.success = "";
            this.modalService.closeModal("editList" + this.list._id);
          }, 2000);
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(listUpdateSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
