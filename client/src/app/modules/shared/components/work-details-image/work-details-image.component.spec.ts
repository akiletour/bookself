import {ComponentFixture, TestBed} from "@angular/core/testing";

import {WorkDetailsImageComponent} from "./work-details-image.component";

describe("WorkDetailsImageComponent", () => {
  let component: WorkDetailsImageComponent;
  let fixture: ComponentFixture<WorkDetailsImageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WorkDetailsImageComponent],
    });
    fixture = TestBed.createComponent(WorkDetailsImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
