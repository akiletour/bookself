import {Component, Input} from "@angular/core";
import {WorksModel} from "src/app/models/WorksModel";
import {environment} from "src/environments/environment";

@Component({
  selector: "app-work-details-image",
  templateUrl: "./work-details-image.component.html",
  styleUrls: ["./work-details-image.component.scss"],
})
export class WorkDetailsImageComponent {
  @Input() item!: WorksModel;
  public imagesDomain: string = environment.imagesDomain;
}
