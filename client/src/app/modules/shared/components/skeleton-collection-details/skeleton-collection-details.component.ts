import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";

@Component({
  selector: "app-skeleton-collection-details",
  templateUrl: "./skeleton-collection-details.component.html",
})
export class SkeletonCollectionDetailsComponent {
  @Input() user!: User;
}
