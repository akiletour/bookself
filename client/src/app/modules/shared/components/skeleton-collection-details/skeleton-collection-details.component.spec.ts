import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SkeletonCollectionDetailsComponent} from "./skeleton-collection-details.component";

describe("SkeletonCollectionDetailsComponent", () => {
  let component: SkeletonCollectionDetailsComponent;
  let fixture: ComponentFixture<SkeletonCollectionDetailsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SkeletonCollectionDetailsComponent],
    });
    fixture = TestBed.createComponent(SkeletonCollectionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
