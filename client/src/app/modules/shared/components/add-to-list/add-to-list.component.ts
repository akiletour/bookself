import {Component, EventEmitter, Input, OnDestroy, Output} from "@angular/core";
import {WorksDTO, WorksModel} from "src/app/models/WorksModel";
import {List, User} from "src/app/modules/auth/models/User";
import {ListsService} from "../../services/list.service";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {Subscription} from "rxjs";
import {ModalService} from "../../services/modal.service";
import {WorksService} from "../../services/works.service";
import {CustomError} from "../../interfaces/CustomError.interface";

@Component({
  selector: "app-add-to-list",
  templateUrl: "./add-to-list.component.html",
  styleUrls: ["./add-to-list.component.scss"],
})
export class AddToListComponent implements OnDestroy {
  @Input() item!: WorksModel;
  @Input() user!: User;
  @Input() inModal?: boolean = false;
  @Output() addedToList = new EventEmitter<string>();
  @Output() addToListError = new EventEmitter<CustomError>();
  @Output() creatingList = new EventEmitter<boolean>();
  @Output() close = new EventEmitter<{event: Event; itemProviderId: string}>();
  searchTerm: string = "";
  private subscriptions: Subscription[] = [];

  constructor(
    private listsService: ListsService,
    private authService: AuthService,
    private modalService: ModalService,
    private worksService: WorksService
  ) {
    const listsUpdatedSubscription = this.listsService.listsUpdated$.subscribe(
      () => {
        // Update user
        this.authService.user = this.user;
      }
    );

    this.subscriptions.push(listsUpdatedSubscription);
  }

  get filteredLists() {
    return this.user.lists?.filter((list) =>
      list.name.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
  }

  closeAddToList(event: Event) {
    this.close.emit({event, itemProviderId: this.item.providerId});
  }

  addToList(event: Event, work: WorksModel, user: User, list: List) {
    (event.target as HTMLElement).classList.add("loading");
    const workDTO: WorksDTO = {
      provider: work.provider,
      providerId: work.providerId,
    };

    const addWorkSubscription = this.worksService
      .addWorkByType(workDTO, work.type, user, list._id)
      .subscribe({
        next: (work: WorksModel | null) => {
          if (work) {
            this.addedToList.emit(`${list.name}`);
            (event.target as HTMLElement).classList.remove("loading");
            (event.target as HTMLElement).classList.add("success");
            setTimeout(() => {
              (event.target as HTMLElement).classList.remove("success");
            }, 5000);
          }
        },
        error: (error: CustomError) => {
          (event.target as HTMLElement).classList.remove("loading");
          this.addToListError.emit(error);
        },
      });
    this.subscriptions.push(addWorkSubscription);
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
    this.creatingList.emit(true);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
