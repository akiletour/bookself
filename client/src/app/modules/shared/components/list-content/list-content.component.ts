import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {WorksTypes} from "src/app/enums/Works.enum";
import {WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {ModalService} from "src/app/modules/shared/services/modal.service";
import {ScreenSizeService} from "../../services/screenSize.service";

@Component({
  selector: "app-list-content",
  templateUrl: "./list-content.component.html",
  styleUrls: ["./list-content.component.scss"],
})
export class ListContentComponent implements OnInit, OnDestroy {
  @Input() type!: WorksTypes;
  @Input() content!: Array<WorksModel>;
  @Input() add: boolean = false;
  @Input() addLabel?: string;
  @Input() user!: User;
  @Input() isList: boolean = false;
  @Input() list?: string;
  public isMobile: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor(
    public modalService: ModalService,
    private screenSizeService: ScreenSizeService
  ) {}

  openModal(modalId: string, type?: WorksTypes) {
    this.modalService.setWorkType(type);
    this.modalService.openModal(modalId);
  }

  trackByFn(_index: number, item: WorksModel) {
    return item._id;
  }

  ngOnInit() {
    const screenSizeSubscription = this.screenSizeService.isMobile$.subscribe(
      (isMobile) => {
        this.isMobile = isMobile;
      }
    );
    this.subscriptions.push(screenSizeSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
