import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";

@Component({
  selector: "app-skeleton-search-work",
  templateUrl: "./skeleton-search-work.component.html",
})
export class SkeletonSearchWorkComponent {
  @Input() user!: User;
}
