import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SkeletonSearchWorkComponent} from "./skeleton-search-work.component";

describe("SkeletonSearchWorkComponent", () => {
  let component: SkeletonSearchWorkComponent;
  let fixture: ComponentFixture<SkeletonSearchWorkComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SkeletonSearchWorkComponent],
    });
    fixture = TestBed.createComponent(SkeletonSearchWorkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
