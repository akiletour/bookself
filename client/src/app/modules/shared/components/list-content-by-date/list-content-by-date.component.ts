import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges,
  ViewChild,
} from "@angular/core";
import {WorksTypes} from "src/app/enums/Works.enum";
import {WorksModel} from "src/app/models/WorksModel";
import {Subscription, fromEvent, interval} from "rxjs";
import {debounce, throttleTime} from "rxjs/operators";
import {ActivatedRoute} from "@angular/router";
import {User} from "src/app/modules/auth/models/User";
import {ScreenSizeService} from "../../services/screenSize.service";

@Component({
  selector: "app-list-content-by-date",
  templateUrl: "./list-content-by-date.component.html",
  styleUrls: ["./list-content-by-date.component.scss"],
})
export class ListContentByDateComponent
  implements OnChanges, OnInit, OnDestroy
{
  @Input() type?: WorksTypes;
  @Input() content!: Array<WorksModel>;
  @Input() add: boolean = false;
  @Input() user!: User;
  @Input() isHistory: boolean = false;
  @Output() filterChanged = new EventEmitter<string>();
  @Output() sortingChanged = new EventEmitter<{
    sortBy: string;
    order: string;
  }>();
  public isMobile: boolean = false;
  private subscriptions: Subscription[] = [];
  public sortedContent: {
    year: number;
    months: {
      month: string;
      data: WorksModel[];
    }[];
  }[] = [];
  public activeElement: Element | null = null;
  public activeFilter: string = "All";
  public activeSorting: {sortBy: string; order: string} = {
    sortBy: "discoveryDate",
    order: "desc",
  };
  public sortLabel: string = "Sort by";
  public sortingLoading: boolean = false;
  public filters: Array<string | never> = ["All"];
  public sortOptions = [
    {
      label: "Discovery date, descending",
      values: {sortBy: "discoveryDate", order: "desc"},
    },
    {
      label: "Discovery date, ascending",
      values: {sortBy: "discoveryDate", order: "asc"},
    },
    {label: "Rating, descending", values: {sortBy: "rating", order: "desc"}},
    {label: "Rating, ascending", values: {sortBy: "rating", order: "asc"}},
  ];
  @ViewChild("timelineElement", {static: false}) timelineElement!: ElementRef;

  constructor(
    private renderer: Renderer2,
    private route: ActivatedRoute,
    private screenSizeService: ScreenSizeService
  ) {
    const routeSubscription = this.route.params.subscribe((params) => {
      if (params["filter"]) {
        this.activeFilter =
          params["filter"].charAt(0).toUpperCase() + params["filter"].slice(1);
      }
    });
    this.subscriptions.push(routeSubscription);
  }

  // Manage timeline on scroll
  onScroll = (): void => {
    // Make timeline visible after we scrolled past page header
    if (this.renderer && this.timelineElement) {
      this.renderer.addClass(
        this.timelineElement.nativeElement,
        "!opacity-100"
      );
    }

    // Years are h2 and months are h3 in template
    const timeStamps = document.querySelectorAll("h2:not(.filters-title), h3");

    let divOnTop: Element;

    // Check all divs to figure out which is in top of the screen
    timeStamps.forEach((div) => {
      const rect = div.getBoundingClientRect();
      if (rect.top >= 0 && rect.top <= window.innerHeight) {
        if (!divOnTop || rect.top < divOnTop.getBoundingClientRect().top) {
          divOnTop = div;
        }
      }
    });

    if (divOnTop!) {
      // Assign div on top to activeElement to manage dynamic classes in template
      this.activeElement = divOnTop;

      // Scroll timeline based on divOnTop
      const divOnTopClass = divOnTop.classList[0];

      const timelineToFocusElement = document.querySelector(
        `.timeline .${divOnTopClass}`
      ) as HTMLElement;

      // Scroll to timeline element synced with divOnTop, substracting half the screen height and some so that current timeline element is in the center of the screen
      const heightToScroll =
        timelineToFocusElement?.offsetTop - window.innerHeight / 2 + 95;

      if (this.timelineElement) {
        this.timelineElement.nativeElement.scrollTo(0, heightToScroll);
      }
    }
  };

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);

    const scroll$ = fromEvent(window, "scroll");
    const scrollSubscription = scroll$
      .pipe(throttleTime(200))
      .subscribe(() => this.onScroll());
    this.subscriptions.push(scrollSubscription);

    const scrollStopped$ = scroll$.pipe(debounce(() => interval(2000)));
    const scrollStoppedSubscription = scrollStopped$.subscribe(() =>
      this.onScrollStopped()
    );
    this.subscriptions.push(scrollStoppedSubscription);

    if (this.type) {
      this.defineFilters(this.type);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["content"] && this.content) {
      this.sortContent(this.content);

      if (this.type) {
        this.defineFilters(this.type);
      }
    }
  }

  defineFilters(type: WorksTypes) {
    if (type === WorksTypes.Movie) {
      this.filters = [
        "All",
        "Action",
        "Animation",
        "Comedy",
        "Fantasy",
        "Thriller",
      ];
    } else if (type === WorksTypes.Series) {
      this.filters = [
        "All",
        "Action & Adventure",
        "Animation",
        "Comedy",
        "Sci-Fi & Fantasy",
      ];
    } else if (type === WorksTypes.Game) {
      this.filters = [
        "All",
        "Indie",
        "Role-playing (RPG)",
        "Shooter",
        "Strategy",
      ];
    } else if (type === WorksTypes.Manga) {
      this.filters = [
        "All",
        "Adventure",
        "Fantasy",
        "Horror",
        "Comedy",
        "Romance",
        "Slice of Life",
      ];
    } else {
      this.filters = ["All"];
    }
  }

  private sortContent(content: Array<WorksModel>): void {
    if (this.activeSorting.sortBy === "discoveryDate") {
      this.sortedContent = [];

      content.forEach((work) => {
        const discoveryDate = new Date(work.discoveryDate);
        const year = discoveryDate.getFullYear();
        const monthName = this.getMonthName(discoveryDate.getMonth());

        let yearEntry = this.sortedContent.find((entry) => entry.year === year);
        if (!yearEntry) {
          yearEntry = {year, months: []};
          this.sortedContent.push(yearEntry);
        }

        let monthEntry = yearEntry.months.find(
          (entry) => entry.month === monthName
        );
        if (!monthEntry) {
          monthEntry = {month: monthName, data: []};
          yearEntry.months.push(monthEntry);
        }

        monthEntry.data.push(work);
      });

      setTimeout(() => {
        this.sortingLoading = false;
      }, 100);
    }
    this.sortingLoading = false;
  }

  public filterByGenre(genre: string): void {
    this.activeFilter = genre;
    this.filterChanged.emit(this.activeFilter);
  }

  public sort(sortOptions: {sortBy: string; order: string}): void {
    this.sortingLoading = true;
    this.activeSorting = sortOptions;
    this.sortingChanged.emit(this.activeSorting);
  }

  public getMonthName(monthNumber: number): string {
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    return months[monthNumber];
  }

  public getKeys(
    obj:
      | {[year: string]: {[month: string]: WorksModel[]}}
      | {[month: string]: WorksModel[]}
  ) {
    return Object.keys(obj);
  }

  onScrollStopped(): void {
    // Hide timeline after 2s
    if (this.renderer && this.timelineElement) {
      this.renderer.removeClass(
        this.timelineElement.nativeElement,
        "!opacity-100"
      );
    }
  }

  scrollTo(year: number, month: string = ""): void {
    // Scroll to h2 / h3 element based on year and month if it exists
    const element =
      month === ""
        ? document.querySelector(`.y${year}`)
        : document.querySelector(`.y${year}m${month}`);

    if (element) {
      window.scrollTo({
        top: element.getBoundingClientRect().top + window.scrollY - 120,
        behavior: "smooth",
      });
    }
  }

  trackByYear(_index: number, year: {year: number}) {
    return year.year;
  }

  trackByMonth(_index: number, month: {month: string}) {
    return month.month;
  }

  trackByFn(_index: number, item: WorksModel) {
    return item._id;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
