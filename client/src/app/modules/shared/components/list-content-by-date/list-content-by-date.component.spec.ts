import {ComponentFixture, TestBed} from "@angular/core/testing";

import {ListContentByDateComponent} from "./list-content-by-date.component";

describe("ListContentByDateComponent", () => {
  let component: ListContentByDateComponent;
  let fixture: ComponentFixture<ListContentByDateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListContentByDateComponent],
    });
    fixture = TestBed.createComponent(ListContentByDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
