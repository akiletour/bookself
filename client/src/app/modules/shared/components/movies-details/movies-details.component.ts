import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {MoviesProviders} from "src/app/enums/Movie";
import {Movie} from "src/app/models/Movie";
import {environment} from "src/environments/environment";
import {WorksService} from "../../services/works.service";
import {CustomError} from "../../interfaces/CustomError.interface";
import {WorksDTO, WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {WorksTypes} from "src/app/enums/Works.enum";
import {Subscription} from "rxjs";
import {ScreenSizeService} from "../../services/screenSize.service";

@Component({
  selector: "app-movies-details",
  templateUrl: "./movies-details.component.html",
})
export class MoviesDetailsComponent implements OnInit, OnDestroy {
  @Input() item!: Movie;
  @Input() list?: string;
  public user!: User;
  public imagesDomain: string = environment.imagesDomain;
  public moviesProviders = MoviesProviders;
  addError: CustomError | undefined;
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;
  public isTablet: boolean = false;

  constructor(
    private worksService: WorksService,
    private AuthService: AuthService,
    private screenSizeService: ScreenSizeService
  ) {
    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
    const screenSizeTabletSubscription =
      this.screenSizeService.isTablet$.subscribe((isTablet) => {
        this.isTablet = isTablet;
      });
    this.subscriptions.push(screenSizeTabletSubscription);
  }

  addFromList(work: WorksModel, user: User, event: Event): void {
    const WorksDTO: WorksDTO = {
      provider: work.provider,
      providerId: work.providerId,
    };
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      this.addError = undefined;
      linkElement.innerHTML = "Adding...";

      const addWorkSubscription = this.worksService
        .addWorkByType(WorksDTO, WorksTypes.Movie, user)
        .subscribe({
          next: (work: WorksModel | null) => {
            if (work) {
              this.item = work as Movie;
            }
          },
          error: (error: CustomError) => {
            linkElement.innerHTML = "Add";
            this.addError = error;
            setTimeout(() => {
              this.addError = undefined;
            }, 3000);
          },
        });
      this.subscriptions.push(addWorkSubscription);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
