import {ComponentFixture, TestBed} from "@angular/core/testing";

import {MangaVolumeComponent} from "./manga-volume.component";

describe("MangaVolumeComponent", () => {
  let component: MangaVolumeComponent;
  let fixture: ComponentFixture<MangaVolumeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MangaVolumeComponent],
    });
    fixture = TestBed.createComponent(MangaVolumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
