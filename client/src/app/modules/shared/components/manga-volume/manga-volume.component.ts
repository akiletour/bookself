import {Component, Input, OnDestroy} from "@angular/core";
import {WorksTypes} from "src/app/enums/Works.enum";
import {Manga, Volume} from "src/app/models/Manga";
import {User} from "src/app/modules/auth/models/User";
import {CustomError} from "../../interfaces/CustomError.interface";
import {CacheService} from "../../services/cache.service";
import {MangaService} from "../../services/manga.service";
import {Subscription} from "rxjs";
import {environment} from "src/environments/environment";

@Component({
  selector: "app-manga-volume",
  templateUrl: "./manga-volume.component.html",
  styleUrls: ["./manga-volume.component.scss"],
})
export class MangaVolumeComponent implements OnDestroy {
  @Input() item!: Manga;
  @Input() volume!: Volume;
  @Input() type!: WorksTypes;
  @Input() user!: User;
  public imagesDomain: string = environment.imagesDomain;
  public error?: CustomError;
  private subscriptions: Subscription[] = [];

  constructor(
    private mangaService: MangaService,
    private cacheService: CacheService
  ) {}

  public isVolumeOwned(mangaVolume: {volume: number}): boolean {
    if (this.item.progress) {
      return this.item.progress.some(
        (volume) => volume.volume === mangaVolume.volume
      );
    }

    return false;
  }

  shouldShowCover(cover?: string): boolean {
    return cover !== undefined && !cover.includes("undefined");
  }

  updateVolumes(manga: Manga, user: User, volumes: number[], method: string) {
    const updateVolumeSubscription = this.mangaService
      .updateUserMangaProgress(manga, user, {volumes}, method)
      .subscribe({
        next: (user) => {
          const matchingManga = user.mangas!.find(
            (manga) => String(manga._id) === String(this.item._id)
          );

          if (matchingManga) {
            this.item.progress = matchingManga.progress;
          }

          this.cacheService.updateInCacheByPrefix(
            ["mangas_", "history_", "list_"],
            this.item
          );

          this.error = undefined;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updateVolumeSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
