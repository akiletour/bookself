import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {Subscription} from "rxjs";
import {ComicsProviders} from "src/app/enums/Comic";
import {WorksTypes} from "src/app/enums/Works.enum";
import {Comic, Volume} from "src/app/models/Comic";
import {WorksDTO, WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {environment} from "src/environments/environment";
import {CustomError} from "../../interfaces/CustomError.interface";
import {CacheService} from "../../services/cache.service";
import {ScreenSizeService} from "../../services/screenSize.service";
import {WorksService} from "../../services/works.service";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {ComicService} from "../../services/comic.service";

@Component({
  selector: "app-comics-details",
  templateUrl: "./comics-details.component.html",
  styleUrls: ["./comics-details.component.scss"],
})
export class ComicsDetailsComponent
  implements OnInit, OnChanges, AfterViewInit, OnDestroy
{
  @Input() item!: Comic;
  @Input() list?: string;
  @Input() type!: WorksTypes;
  public user!: User;
  public error?: CustomError;
  public success!: string;
  public imagesDomain: string = environment.imagesDomain;
  public comicsProviders = ComicsProviders;
  addError: CustomError | undefined;
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;
  public isTablet: boolean = false;
  @ViewChild("comicDetailsElement", {static: false})
  gameDetailsElement!: ElementRef;

  constructor(
    private comicService: ComicService,
    private AuthService: AuthService,
    private worksService: WorksService,
    private cacheService: CacheService,
    private screenSizeService: ScreenSizeService
  ) {
    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
    const screenSizeTabletSubscription =
      this.screenSizeService.isTablet$.subscribe((isTablet) => {
        this.isTablet = isTablet;
      });
    this.subscriptions.push(screenSizeTabletSubscription);
  }

  ngOnChanges() {}

  ngAfterViewInit() {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          // Check for comics updates once component is rendered in the DOM
          const comicUpdatedSubscription = this.comicService
            .updateComic(this.item._id, this.item.provider)
            .subscribe({
              next: (comic) => {
                if (comic.volumes) {
                  this.item.volumes = comic.volumes;
                  this.item.summary = comic.summary;

                  this.cacheService.updateInCacheByPrefix(
                    ["comics_", "history_", "list_"],
                    this.item
                  );
                }
                // Stop observing once comic has been updated
                observer.disconnect();
              },
              error: (error: CustomError) => {
                this.error = error;
              },
            });
          this.subscriptions.push(comicUpdatedSubscription);
        }
      });
    });

    if (this.item && this.gameDetailsElement) {
      observer.observe(this.gameDetailsElement.nativeElement);
    }
  }

  getTrimmedSummary(summary: string): string {
    if (!summary) {
      return "";
    }

    const parser = new DOMParser();
    const tempDoc = parser.parseFromString(summary, "text/html");

    const links = tempDoc.querySelectorAll("a");

    links.forEach((link) => {
      link.setAttribute("target", "_blank");

      const originalHref = link.getAttribute("href");

      if (
        originalHref &&
        !originalHref.startsWith("http://") &&
        !originalHref.startsWith("https://")
      ) {
        link.setAttribute(
          "href",
          "https://comicvine.gamespot.com" + originalHref
        );
      }
    });

    const modifiedHtml = new XMLSerializer().serializeToString(tempDoc);

    return modifiedHtml;
  }

  sortVolumesByNumber(volumes: Volume[], sort: string = "asc") {
    return volumes.sort((a, b) => {
      const dateA = a.volumeNumber;
      const dateB = b.volumeNumber;
      return sort === "asc" ? dateA - dateB : dateB - dateA;
    });
  }

  public allVolumesOwned(): boolean {
    if (this.item.progress?.length === this.item.volumes?.length) {
      return true;
    }

    return false;
  }

  updateAllVolumes(comic: Comic, user: User, method: string) {
    let volumes: {volumes: number[]} = {volumes: []};
    if (method === "post") {
      volumes = {
        volumes: this.item.volumes?.map((volume) => volume.volumeNumber) || [],
      };
    }
    const updateVolumeSubscription = this.comicService
      .updateUserComicProgress(comic, user, volumes, method)
      .subscribe({
        next: (user) => {
          const matchingComic = user.comics!.find(
            (comic) => String(comic._id) === String(this.item._id)
          );

          if (matchingComic) {
            this.item.progress = matchingComic.progress;
          }

          this.cacheService.updateInCacheByPrefix(
            ["comics_", "history_", "list_"],
            this.item
          );

          this.error = undefined;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updateVolumeSubscription);
  }

  addFromList(work: WorksModel, user: User, event: Event): void {
    const WorksDTO: WorksDTO = {
      provider: work.provider,
      providerId: work.providerId,
    };
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      this.addError = undefined;
      linkElement.innerHTML = "Adding...";

      const addWorkSubscription = this.worksService
        .addWorkByType(WorksDTO, WorksTypes.Manga, user)
        .subscribe({
          next: (work: WorksModel | null) => {
            if (work) {
              this.item = work as Comic;
            }
          },
          error: (error: CustomError) => {
            linkElement.innerHTML = "Add";
            this.addError = error;
            setTimeout(() => {
              this.addError = undefined;
            }, 3000);
          },
        });
      this.subscriptions.push(addWorkSubscription);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
