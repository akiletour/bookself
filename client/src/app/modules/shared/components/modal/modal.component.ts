import {Component, Input} from "@angular/core";
import {ModalService} from "../../services/modal.service";
// import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.scss"],
})
export class ModalComponent {
  @Input() modalId!: string;
  @Input() classes?: string;

  constructor(public modalService: ModalService) {}

  get modalClasses() {
    let classes = "modal__content";
    if (this.classes) {
      classes += " " + this.classes;
    }
    return classes;
  }

  closeModal() {
    this.modalService.closeModal(this.modalId);
  }

  clickOverlay(event: MouseEvent) {
    const clickedSection = event.target as HTMLElement;
    const frame = document.querySelector(".modal__content");

    if (frame && !frame.contains(clickedSection)) {
      this.modalService.closeModal();
    }
  }
}
