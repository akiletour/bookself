import {Component, Input} from "@angular/core";
import {WorksModel} from "src/app/models/WorksModel";
import {ModalService} from "../../services/modal.service";

@Component({
  selector: "app-work-lists",
  templateUrl: "./work-lists.component.html",
})
export class WorkListsComponent {
  @Input() item!: WorksModel;

  constructor(private modalService: ModalService) {}

  closeModals() {
    this.modalService.closeModal();
  }
}
