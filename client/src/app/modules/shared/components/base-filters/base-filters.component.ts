import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output,
} from "@angular/core";

@Component({
  selector: "app-base-filters",
  templateUrl: "./base-filters.component.html",
})
export class BaseFiltersComponent implements OnInit {
  @Input() filters: Array<string | never> = ["All"];
  @Input() sortOptions: {
    label: string;
    values: {sortBy: string; order: string};
  }[] = [];
  @Input() defaultFilter: string = "All";
  @Output() filterChanged = new EventEmitter<string>();
  @Output() sortingChanged = new EventEmitter<{
    sortBy: string;
    order: string;
  }>();
  public showDropdown = false;
  public sortLabel: string = "Sort by";
  public activeFilter: string = "All";
  public activeSorting: {sortBy: string; order: string} = {
    sortBy: "discoveryDate",
    order: "desc",
  };

  ngOnInit() {
    this.activeFilter = this.defaultFilter;
  }

  public filterByGenre(genre: string): void {
    this.activeFilter = genre;
    this.filterChanged.emit(this.activeFilter);
  }

  public sort(sortOption: {
    label: string;
    values: {sortBy: string; order: string};
  }): void {
    this.sortLabel = `${sortOption.label}`;
    this.activeSorting = sortOption.values;
    this.sortingChanged.emit(this.activeSorting);
  }

  toggleDropdown() {
    this.showDropdown = !this.showDropdown;
  }

  private isDropdownClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".dropdown");
  }

  @HostListener("document:click", ["$event"])
  onDocumentClick(event: Event) {
    if (!this.isDropdownClick(event)) {
      this.showDropdown = false;
    }
  }
}
