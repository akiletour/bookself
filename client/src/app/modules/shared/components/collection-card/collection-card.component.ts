import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {List, User} from "src/app/modules/auth/models/User";
import {ModalService} from "../../services/modal.service";
import {ListsService} from "../../services/list.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: "app-collection-card",
  templateUrl: "./collection-card.component.html",
  styleUrls: ["./collection-card.component.scss"],
})
export class CollectionCardComponent implements OnInit, OnDestroy {
  @Input() user!: User;
  @Input() item!: List;
  public covers: string[] = [];
  public imagesPath!: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private modalService: ModalService,
    private listsService: ListsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getCovers(this.item);
  }

  getCovers(item: List) {
    for (let index = 0; index < 4; index++) {
      if (item.works[index]) {
        this.covers.push(item.works[index].cover);
      }
    }
  }

  goToCollectionDetails() {
    this.router.navigate(["/collections", this.item._id]);
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  closeModal(modalId: string) {
    this.modalService.closeModal(modalId);
  }

  remove(item: List) {
    const listRemoveSubscription = this.listsService
      .removeList(this.user, item)
      .subscribe({
        next: () => {
          this.closeModal("dlt_" + item._id);
        },
      });
    this.subscriptions.push(listRemoveSubscription);
  }

  openDeleteConfirmation(event: Event, modalId: string) {
    event.stopPropagation();
    this.openModal("dlt_" + modalId);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
