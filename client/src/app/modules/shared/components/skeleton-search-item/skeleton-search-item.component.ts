import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";

@Component({
  selector: "app-skeleton-search-item",
  templateUrl: "./skeleton-search-item.component.html",
})
export class SkeletonSearchItemComponent {
  @Input() user!: User;
}
