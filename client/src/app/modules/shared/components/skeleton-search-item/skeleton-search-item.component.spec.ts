import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SkeletonSearchItemComponent} from "./skeleton-search-item.component";

describe("SkeletonSearchItemComponent", () => {
  let component: SkeletonSearchItemComponent;
  let fixture: ComponentFixture<SkeletonSearchItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SkeletonSearchItemComponent],
    });
    fixture = TestBed.createComponent(SkeletonSearchItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
