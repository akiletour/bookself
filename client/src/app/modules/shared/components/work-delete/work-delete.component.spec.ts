import {ComponentFixture, TestBed} from "@angular/core/testing";

import {WorkDeleteComponent} from "./work-delete.component";

describe("WorkDeleteComponent", () => {
  let component: WorkDeleteComponent;
  let fixture: ComponentFixture<WorkDeleteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WorkDeleteComponent],
    });
    fixture = TestBed.createComponent(WorkDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
