import {Component, Input, OnInit} from "@angular/core";
import {WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {ModalService} from "../../services/modal.service";
import {WorksService} from "../../services/works.service";
import {environment} from "src/environments/environment";
import {WorksTypes} from "src/app/enums/Works.enum";
import {Subscription} from "rxjs";
import {CustomError} from "../../interfaces/CustomError.interface";

@Component({
  selector: "app-work-delete",
  templateUrl: "./work-delete.component.html",
})
export class WorkDeleteComponent implements OnInit {
  @Input() type!: WorksTypes;
  @Input() item!: WorksModel;
  @Input() user!: User;
  @Input() list?: string;
  public imagesPath!: string;
  private imagesDomain: string = environment.imagesDomain;
  addError: CustomError | undefined;
  private subscriptions: Subscription[] = [];

  constructor(
    private modalService: ModalService,
    private worksService: WorksService
  ) {}

  ngOnInit() {
    this.imagesPath = this.imagesDomain + "/" + this.item.type;
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  openDeleteConfirmation(event: Event, modalId: string) {
    event.stopPropagation();
    this.openModal("dlt_" + modalId);
  }

  closeModal(modalId: string) {
    this.modalService.closeModal(modalId);
  }

  remove(item: WorksModel, event: Event, listId?: string) {
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      linkElement.innerHTML = "Deleting...";
      const removeWorkSubscription = this.worksService
        .removeWorkById(this.user._id, item._id, item.type, listId)
        .subscribe({
          next: () => {
            this.closeModal("dlt_" + item._id);
            this.closeModal(item._id);
          },
          error: (error: CustomError) => {
            linkElement.innerHTML = "Yes please";
            this.addError = error;
            setTimeout(() => {
              this.addError = undefined;
            }, 3000);
          },
        });
      this.subscriptions.push(removeWorkSubscription);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
