import {
  Component,
  HostListener,
  Input,
  OnChanges,
  OnDestroy,
  Renderer2,
  SimpleChanges,
} from "@angular/core";
import {WorksTypes} from "src/app/enums/Works.enum";
import {
  WorkDetailsComponent,
  WorksDTO,
  WorksModel,
  isGame,
  isMovie,
  isSeries,
  isManga,
  isComic,
} from "src/app/models/WorksModel";
import {ModalService} from "../../services/modal.service";
import {WorksService} from "../../services/works.service";
import {Series} from "src/app/models/Series";
import {SeriesService} from "../../services/series.service";
import {User} from "src/app/modules/auth/models/User";
import {environment} from "src/environments/environment";
import {trigger, transition, style, animate} from "@angular/animations";
import {CustomError} from "../../interfaces/CustomError.interface";
import {Subscription} from "rxjs";
import {Manga} from "src/app/models/Manga";
import {Comic} from "src/app/models/Comic";

@Component({
  selector: "app-work-card",
  templateUrl: "./work-card.component.html",
  styleUrls: ["./work-card.component.scss"],
  animations: [
    trigger("fadeInOut", [
      transition(":enter", [
        style({opacity: 0}),
        animate("0.05s", style({opacity: 0})),
        animate("0.1s", style({opacity: 1})),
      ]),
      transition(":leave", [animate("0.1s", style({opacity: 0}))]),
    ]),
  ],
})
export class WorkCardComponent implements OnChanges, OnDestroy {
  @Input() item!: WorksModel;
  @Input() type!: WorksTypes;
  @Input() user!: User;
  @Input() isMobile: boolean = false;
  @Input() hasMultipleTypes!: boolean;
  @Input() list?: string;
  public isMovie = isMovie;
  public isSeries = isSeries;
  public isGame = isGame;
  public isManga = isManga;
  public isComic = isComic;
  public detailsComponent!: WorkDetailsComponent;
  public completion?: number;
  public imagesPath!: string;
  private imagesDomain: string = environment.imagesDomain;
  public WorksTypes = WorksTypes;
  public addedToListSuccess?: string;
  private showAddToListState: boolean = false;
  private isCreatingList: boolean = false;
  addError: CustomError | undefined;
  private subscriptions: Subscription[] = [];

  constructor(
    public modalService: ModalService,
    private worksService: WorksService,
    private seriesService: SeriesService,
    private renderer: Renderer2
  ) {
    const seriesUpdatedSubscription =
      this.seriesService.seriesUpdated$.subscribe((series: Series) => {
        if (series._id === this.item._id) {
          const seriesItem = this.item as Series;
          seriesItem.progress = series.progress;
          this.calculateCompletion(seriesItem);
        }
      });
    this.subscriptions.push(seriesUpdatedSubscription);

    const modalOpenedSubscription = this.modalService.modalOpened.subscribe(
      (event) => {
        if (!event || (event && event.modalId === "addList" && !event.opened)) {
          setTimeout(() => {
            this.isCreatingList = false;
          }, 150);
        }
      }
    );
    this.subscriptions.push(modalOpenedSubscription);
  }

  public isMangaOrComic(work: WorksModel): work is Manga | Comic {
    return this.isManga(work) || this.isComic(work);
  }

  get mangaOrComicItem(): Manga | Comic | null {
    return this.isMangaOrComic(this.item) ? this.item : null;
  }

  openModal(modalId: string, event?: Event) {
    if (event) {
      if (!this.isAddToListVisible() && !this.isAddToListButtonClick(event)) {
        this.modalService.openModal(modalId);
      }
    } else {
      this.modalService.openModal(modalId);
    }
  }

  openDeleteConfirmation(event: Event, modalId: string) {
    event.stopPropagation();
    this.openModal("dlt_" + modalId);
  }

  closeModal(modalId: string) {
    this.modalService.closeModal(modalId);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes["item"] && this.item) {
      // Define card component based on type
      this.detailsComponent = this.worksService.getWorksDetailsComponentByType(
        this.type
      );

      this.imagesPath = this.imagesDomain + "/" + this.item.type;

      if (this.item.type === WorksTypes.Series) {
        const seriesItem = this.item as Series;
        this.calculateCompletion(seriesItem);
      }
    }
  }

  calculateCompletion(series: Series) {
    if (series.progress && !series.progress.completed) {
      if (series.seasons && series.seasons.length > 0) {
        const filteredSeasons = series.seasons.filter(
          (season) => season.name !== "Specials"
        );

        if (filteredSeasons.length > 0) {
          const totalEpisodes = filteredSeasons.reduce(
            (total, season) => total + season.numberOfEpisodes,
            0
          );

          const seenEpisodes = filteredSeasons.reduce((total, season) => {
            if (season.seasonNumber < series.progress!.season) {
              return total + season.numberOfEpisodes;
            } else if (season.seasonNumber === series.progress?.season) {
              return total + series.progress!.episode;
            } else {
              return total;
            }
          }, 0);

          this.completion = Math.round((seenEpisodes / totalEpisodes) * 100);
        }
      }
    }
  }

  remove(item: WorksModel, event: Event, listId?: string) {
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      linkElement.innerHTML = "Deleting...";
      const removeWorkSubscription = this.worksService
        .removeWorkById(this.user._id, item._id, item.type, listId)
        .subscribe({
          next: () => {
            this.closeModal("dlt_" + item._id);
            this.closeModal(item._id);
          },
          error: (error: CustomError) => {
            linkElement.innerHTML = "Yes please";
            this.addError = error;
            setTimeout(() => {
              this.addError = undefined;
            }, 3000);
          },
        });
      this.subscriptions.push(removeWorkSubscription);
    }
  }

  determinePosition(el: HTMLElement) {
    let listElement = el.parentElement;
    let cardElement = el.parentElement;

    while (listElement && !listElement.classList.contains("cards-list")) {
      listElement = listElement.parentElement;
    }
    while (cardElement && !cardElement.classList.contains("card")) {
      cardElement = cardElement.parentElement;
    }

    const parentWidth = listElement!.offsetWidth;
    const elementLeft = Math.abs(cardElement!.offsetLeft);
    const relativeXPosition = parentWidth - elementLeft;
    const percentageXPosition = (relativeXPosition / parentWidth) * 100;

    const parentTop = listElement!.scrollTop;
    const parentHeight = listElement!.offsetHeight;
    const elementTop = Math.abs(cardElement!.offsetTop);
    const relativeYPosition = elementTop - parentTop;
    const percentageYPosition = (relativeYPosition / parentHeight) * 100;

    if (percentageXPosition < 50) {
      if (cardElement?.querySelector(".addToList")) {
        this.renderer.setStyle(
          cardElement?.querySelector(".addToList"),
          "left",
          "auto"
        );
        this.renderer.setStyle(
          cardElement?.querySelector(".addToList"),
          "right",
          "140px"
        );
      }
    }
    if (percentageYPosition > 50 && parentHeight > 500) {
      if (cardElement?.querySelector(".addToList")) {
        this.renderer.setStyle(
          cardElement?.querySelector(".addToList"),
          "top",
          "-185px"
        );
        if (percentageXPosition < 50) {
          this.renderer.setStyle(
            cardElement?.querySelector(".addToList"),
            "left",
            "auto"
          );
          this.renderer.setStyle(
            cardElement?.querySelector(".addToList"),
            "right",
            "155px"
          );
        } else {
          this.renderer.setStyle(
            cardElement?.querySelector(".addToList"),
            "left",
            "45px"
          );
        }
      }
    }
  }

  stopPropagation(event: Event) {
    event.stopPropagation();
  }

  toggleAddToList(event: Event) {
    this.showAddToListState = !this.showAddToListState;

    if (!this.showAddToListState) {
      (event.target as HTMLElement).parentElement?.classList.remove(
        "listActive"
      );
    } else {
      (event.target as HTMLElement).parentElement?.classList.add("listActive");
    }

    if (this.showAddToListState) {
      setTimeout(() => {
        this.determinePosition(event.target as HTMLElement);
      }, 50);
    }
  }

  @HostListener("document:click", ["$event"])
  onDocumentClick(event: Event) {
    if (
      !this.isAddToListClick(event) &&
      !this.isAddToListButtonClick(event) &&
      !this.isCreatingList
    ) {
      this.resetAddToListState();
    }
  }

  resetAddToListState() {
    this.showAddToListState = false;
    document
      .querySelector(".addToListLink" + this.item.providerId)
      ?.classList.remove("listActive");
  }

  isAddToListVisible(): boolean {
    return this.showAddToListState || false;
  }

  isAddToListClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".addToList" + this.item.providerId);
  }

  isAddToListButtonClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".addToListLink" + this.item.providerId);
  }

  addToList(work: WorksModel, user: User, event: Event, list?: string): void {
    const WorksDTO: WorksDTO = {
      provider: work.provider,
      providerId: work.providerId,
    };
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      linkElement.innerHTML = "Adding...";

      const addWorkSubscription = this.worksService
        .addWorkByType(WorksDTO, work.type, user, list)
        .subscribe();
      this.subscriptions.push(addWorkSubscription);
    }
  }

  addToListError(error: CustomError) {
    this.addedToListSuccess = "";
    if (error) {
      this.addError = error;
      setTimeout(() => {
        this.addError = undefined;
      }, 3000);
    }
  }

  addedToList(list: string) {
    this.addError = undefined;
    if (list) {
      this.addedToListSuccess = `Added to ${list}`;
      setTimeout(() => {
        this.addedToListSuccess = "";
      }, 3000);
    }
  }

  creatingList(active: boolean) {
    this.isCreatingList = active;
  }

  closeAddToList(close: {event: Event; itemProviderId: string}) {
    if (this.item.providerId === close.itemProviderId) {
      this.stopPropagation(close.event);
      this.resetAddToListState();
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
