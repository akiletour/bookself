import {Component, Input, OnDestroy} from "@angular/core";
import {Subscription} from "rxjs";
import {WorksTypes} from "src/app/enums/Works.enum";
import {User} from "src/app/modules/auth/models/User";
import {environment} from "src/environments/environment";
import {CustomError} from "../../interfaces/CustomError.interface";
import {CacheService} from "../../services/cache.service";
import {ComicService} from "../../services/comic.service";
import {Comic, Volume} from "src/app/models/Comic";

@Component({
  selector: "app-comic-issue",
  templateUrl: "./comic-issue.component.html",
  styleUrls: ["./comic-issue.component.scss"],
})
export class ComicIssueComponent implements OnDestroy {
  @Input() item!: Comic;
  @Input() volume!: Volume;
  @Input() type!: WorksTypes;
  @Input() user!: User;
  public imagesDomain: string = environment.imagesDomain;
  public error?: CustomError;
  private subscriptions: Subscription[] = [];

  constructor(
    private comicService: ComicService,
    private cacheService: CacheService
  ) {}

  public isVolumeOwned(comicVolume: {volume: number}): boolean {
    if (this.item.progress) {
      return this.item.progress.some(
        (volume) => volume.volume === comicVolume.volume
      );
    }

    return false;
  }

  shouldShowCover(cover?: string): boolean {
    return cover !== undefined && !cover.includes("undefined");
  }

  updateVolumes(comic: Comic, user: User, volumes: number[], method: string) {
    const updateVolumeSubscription = this.comicService
      .updateUserComicProgress(comic, user, {volumes}, method)
      .subscribe({
        next: (user) => {
          const matchingComic = user.comics!.find(
            (comic) => String(comic._id) === String(this.item._id)
          );

          if (matchingComic) {
            this.item.progress = matchingComic.progress;
          }

          this.cacheService.updateInCacheByPrefix(
            ["comics_", "history_", "list_"],
            this.item
          );

          this.error = undefined;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updateVolumeSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
