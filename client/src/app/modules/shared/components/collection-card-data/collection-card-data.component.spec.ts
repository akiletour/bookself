import {ComponentFixture, TestBed} from "@angular/core/testing";

import {CollectionCardDataComponent} from "./collection-card-data.component";

describe("CollectionCardDataComponent", () => {
  let component: CollectionCardDataComponent;
  let fixture: ComponentFixture<CollectionCardDataComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CollectionCardDataComponent],
    });
    fixture = TestBed.createComponent(CollectionCardDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
