import {Component, Input} from "@angular/core";
import {List} from "src/app/modules/auth/models/User";
import {environment} from "src/environments/environment";

@Component({
  selector: "app-collection-card-data",
  templateUrl: "./collection-card-data.component.html",
  styleUrls: ["./collection-card-data.component.scss"],
})
export class CollectionCardDataComponent {
  @Input() item!: List;
  public covers: string[] = [];
  public imagesPath!: string;
  private imagesDomain: string = environment.imagesDomain;

  constructor() {}

  ngOnInit() {
    this.getCovers(this.item);
  }

  public defineImagesPath(cover: string) {
    return this.imagesDomain + "/" + cover;
  }

  getCovers(item: List) {
    for (let index = 0; index < 4; index++) {
      if (item.works[index]) {
        this.covers.push(item.works[index].cover);
      }
    }
  }
}
