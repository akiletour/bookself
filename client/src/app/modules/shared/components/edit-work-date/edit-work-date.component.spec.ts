import {ComponentFixture, TestBed} from "@angular/core/testing";

import {EditWorkDateComponent} from "./edit-work-date.component";

describe("EditWorkDateComponent", () => {
  let component: EditWorkDateComponent;
  let fixture: ComponentFixture<EditWorkDateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditWorkDateComponent],
    });
    fixture = TestBed.createComponent(EditWorkDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
