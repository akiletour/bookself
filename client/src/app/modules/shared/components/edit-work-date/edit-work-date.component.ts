import {Component, Input, OnDestroy} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {WorksService} from "../../services/works.service";
import {WorksModel} from "src/app/models/WorksModel";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {User} from "src/app/modules/auth/models/User";
import {ModalService} from "../../services/modal.service";
import {trigger, state, style, transition, animate} from "@angular/animations";
import {Subscription} from "rxjs";

@Component({
  selector: "app-edit-work-date",
  templateUrl: "./edit-work-date.component.html",
  styleUrls: ["./edit-work-date.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
  ],
})
export class EditWorkDateComponent implements OnDestroy {
  @Input() item!: WorksModel;
  dateForm: FormGroup;
  user!: User;
  public success?: string;
  public error?: string;
  private subscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private worksService: WorksService,
    private AuthService: AuthService,
    private modalService: ModalService
  ) {
    this.dateForm = this.fb.group({
      selectedDate: ["", Validators.required],
    });

    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  onSubmit() {
    const selectedDate = this.dateForm.get("selectedDate")?.value;
    const updateWorkSubscription = this.worksService
      .updateWorkDateByType(
        this.user._id,
        this.item._id,
        this.item.type,
        selectedDate
      )
      .subscribe({
        next: (user: User | null) => {
          if (user) {
            this.success = "Date updated !";
            setTimeout(() => {
              this.success = "";
            }, 3000);
            this.item.discoveryDate = selectedDate;
            setTimeout(() => {
              this.modalService.closeModal("ewd_" + this.item._id);
            }, 3500);
          }
        },
        error: () => {
          this.error = "Error during update. Please try again";
        },
      });
    this.subscriptions.push(updateWorkSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
