import {Component, Input, OnDestroy, OnInit} from "@angular/core";
import {WorksLabels, WorksTypes} from "src/app/enums/Works.enum";
import {WorksModel} from "src/app/models/WorksModel";
import {ModalService} from "../../services/modal.service";
import {User} from "src/app/modules/auth/models/User";
import {ScreenSizeService} from "../../services/screenSize.service";
import {Subscription} from "rxjs";

@Component({
  selector: "app-work-date",
  templateUrl: "./work-date.component.html",
  styleUrls: ["./work-date.component.scss"],
})
export class WorkDateComponent implements OnInit, OnDestroy {
  @Input() item!: WorksModel;
  @Input() user!: User;
  @Input() list?: string;
  @Input() onlyForm = false;
  public workTypes = Object.values(WorksTypes);
  public isMobile: boolean = false;
  private subscriptions: Subscription[] = [];

  constructor(
    public modalService: ModalService,
    private screenSizeService: ScreenSizeService
  ) {}

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
  }

  public getWorkLabel(key: string): string {
    return WorksLabels[key as keyof typeof WorksLabels] || key;
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
