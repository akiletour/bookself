import {ComponentFixture, TestBed} from "@angular/core/testing";

import {WorkDateComponent} from "./work-date.component";

describe("WorkDateComponent", () => {
  let component: WorkDateComponent;
  let fixture: ComponentFixture<WorkDateComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WorkDateComponent],
    });
    fixture = TestBed.createComponent(WorkDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
