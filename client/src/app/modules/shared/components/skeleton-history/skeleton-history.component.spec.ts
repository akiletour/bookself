import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SkeletonHistoryComponent} from "./skeleton-history.component";

describe("SkeletonHistoryComponent", () => {
  let component: SkeletonHistoryComponent;
  let fixture: ComponentFixture<SkeletonHistoryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SkeletonHistoryComponent],
    });
    fixture = TestBed.createComponent(SkeletonHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
