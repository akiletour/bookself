import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";

@Component({
  selector: "app-skeleton-history",
  templateUrl: "./skeleton-history.component.html",
})
export class SkeletonHistoryComponent {
  @Input() user!: User;
}
