import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {DLC, Game} from "src/app/models/Game";
import {GameService} from "../../services/game.service";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {User} from "src/app/modules/auth/models/User";
import {CustomError} from "../../interfaces/CustomError.interface";
import {environment} from "src/environments/environment";
import {GamesProviders, HowLongToBeatTimeLabels} from "src/app/enums/Game";
import {WorksDTO, WorksModel} from "src/app/models/WorksModel";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {WorksService} from "../../services/works.service";
import {WorksTypes} from "src/app/enums/Works.enum";
import {Subscription} from "rxjs";
import {CacheService} from "../../services/cache.service";
import {ScreenSizeService} from "../../services/screenSize.service";

@Component({
  selector: "app-games-details",
  templateUrl: "./games-details.component.html",
  styleUrls: ["./games-details.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
    trigger("fadeInOut", [
      transition(":enter", [
        style({opacity: 0}),
        animate("0.05s", style({opacity: 0})),
        animate("0.1s", style({opacity: 1})),
      ]),
      transition(":leave", [animate("0.1s", style({opacity: 0}))]),
    ]),
  ],
})
export class GamesDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() item!: Game;
  @Input() list?: string;
  public user!: User;
  public error?: CustomError;
  addError: CustomError | undefined;
  public imagesDomain: string = environment.imagesDomain;
  public gamesProviders = GamesProviders;
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;
  public isTablet: boolean = false;
  @ViewChild("gameDetailsElement", {static: false})
  gameDetailsElement!: ElementRef;

  constructor(
    private AuthService: AuthService,
    private gameService: GameService,
    private worksService: WorksService,
    private cacheService: CacheService,
    private screenSizeService: ScreenSizeService
  ) {
    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
    const screenSizeTabletSubscription =
      this.screenSizeService.isTablet$.subscribe((isTablet) => {
        this.isTablet = isTablet;
      });
    this.subscriptions.push(screenSizeTabletSubscription);
  }

  ngAfterViewInit() {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          // Check for game updates once component is rendered in the DOM
          const updateGameSubscription = this.gameService
            .updateGame(this.item._id, this.item.provider)
            .subscribe({
              next: (game) => {
                if (game.dlcs || game.hltb) {
                  this.item.dlcs = game.dlcs;
                  this.item.hltb = game.hltb;

                  this.cacheService.updateInCacheByPrefix(
                    ["games_", "history_", "list_"],
                    this.item
                  );
                }

                // Stop observing once game has been updated
                observer.disconnect();
              },
              error: (error: CustomError) => {
                this.error = error;
              },
            });
          this.subscriptions.push(updateGameSubscription);
        }
      });
    });

    if (this.item && this.gameDetailsElement) {
      observer.observe(this.gameDetailsElement.nativeElement);
    }
  }

  public isGameDLCOwned(gameDLC: DLC): boolean {
    if (this.item.userDlcs) {
      return this.item.userDlcs.some(
        (dlc) => dlc.providerId === gameDLC.providerId
      );
    }

    return false;
  }

  updateDLCs(game: Game, user: User, dlcId: number, method: string) {
    const updateGameDLCSubscription = this.gameService
      .updateUserGameDLCs(game, user, {dlcId}, method)
      .subscribe({
        next: (user) => {
          const matchingGame = user.games!.find(
            (game) => String(game._id) === String(this.item._id)
          );

          if (matchingGame) {
            this.item.userDlcs = matchingGame.dlcs;
          }

          this.cacheService.updateInCacheByPrefix(
            ["games_", "history_", "list_"],
            this.item
          );

          this.error = undefined;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updateGameDLCSubscription);
  }

  sortDLCByReleaseDateDescending(dlcs: DLC[]) {
    return dlcs.sort((a, b) => {
      const dateA = new Date(a.releaseDate).getTime();
      const dateB = new Date(b.releaseDate).getTime();
      return dateB - dateA;
    });
  }

  getHowLongToBeatLabelsAndValues(): {
    label: HowLongToBeatTimeLabels;
    value?: number;
  }[] {
    return Object.keys(HowLongToBeatTimeLabels).map((key) => ({
      label:
        HowLongToBeatTimeLabels[key as keyof typeof HowLongToBeatTimeLabels],
      value:
        this.item.hltb[
          key.toLowerCase() as keyof typeof HowLongToBeatTimeLabels
        ] || undefined,
    }));
  }

  addFromList(work: WorksModel, user: User, event: Event): void {
    const WorksDTO: WorksDTO = {
      provider: work.provider,
      providerId: work.providerId,
    };
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      this.addError = undefined;
      linkElement.innerHTML = "Adding...";

      const addWorkSubscription = this.worksService
        .addWorkByType(WorksDTO, WorksTypes.Game, user)
        .subscribe({
          next: (work: WorksModel | null) => {
            if (work) {
              this.item = work as Game;
            }
          },
          error: (error: CustomError) => {
            linkElement.innerHTML = "Add";
            this.addError = error;
            setTimeout(() => {
              this.addError = undefined;
            }, 3000);
          },
        });
      this.subscriptions.push(addWorkSubscription);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
