import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";

@Component({
  selector: "app-skeleton-collections-list",
  templateUrl: "./skeleton-collections-list.component.html",
})
export class SkeletonCollectionsListComponent {
  @Input() user!: User;
}
