import {ComponentFixture, TestBed} from "@angular/core/testing";

import {SkeletonCollectionsListComponent} from "./skeleton-collections-list.component";

describe("SkeletonCollectionsListComponent", () => {
  let component: SkeletonCollectionsListComponent;
  let fixture: ComponentFixture<SkeletonCollectionsListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SkeletonCollectionsListComponent],
    });
    fixture = TestBed.createComponent(SkeletonCollectionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
