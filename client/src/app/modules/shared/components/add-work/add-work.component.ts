import {
  Component,
  HostListener,
  AfterViewInit,
  Renderer2,
  OnDestroy,
  OnInit,
} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomError} from "src/app/modules/shared/interfaces/CustomError.interface";
import {BehaviorSubject, Observable, Subject, Subscription, of} from "rxjs";
import {debounceTime, distinctUntilChanged, switchMap} from "rxjs/operators";
import {User} from "src/app/modules/auth/models/User";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {Pagination} from "src/app/models/Pagination";
import {PaginatedResponse} from "src/app/modules/shared/interfaces/ApiResponse.interface";
import {WorksTypes} from "src/app/enums/Works.enum";
import {WorksService} from "../../services/works.service";
import {
  WorksDTO,
  WorksModel,
  isGame,
  isManga,
  isComic,
} from "src/app/models/WorksModel";
import {ModalService} from "../../services/modal.service";
import {GamesCategories} from "src/app/enums/Game";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {ScreenSizeService} from "../../services/screenSize.service";

@Component({
  selector: "app-add-work",
  templateUrl: "./add-work.component.html",
  styleUrls: ["./add-work.component.scss"],
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
    trigger("fadeInOut", [
      transition(":enter", [
        style({opacity: 0}),
        animate("0.05s", style({opacity: 0})),
        animate("0.1s", style({opacity: 1})),
      ]),
      transition(":leave", [animate("0.1s", style({opacity: 0}))]),
    ]),
  ],
})
export class AddWorkComponent implements OnInit, AfterViewInit, OnDestroy {
  searchForm: FormGroup;
  searchTerm$ = new Subject<string>();
  searchTerm!: string;
  searchType$ = new Subject<WorksTypes>();
  public searchType: WorksTypes = WorksTypes.Movie;
  addError: CustomError | undefined;
  private searchResultsSubject = new BehaviorSubject<WorksModel[] | []>([]);
  searchResults$ = this.searchResultsSubject.asObservable();
  pagination!: Pagination;
  user!: User;
  public worksTypes = WorksTypes;
  public worksTypesArray: Array<WorksTypes>;
  public lastAddedItem!: WorksModel;
  public imagesPath!: string;
  public isGame = isGame;
  public isManga = isManga;
  public isComic = isComic;
  public isLoading: boolean = false;
  public addedToListSuccess?: string;
  private showAddToListState: {[key: string]: boolean} = {};
  private isCreatingList: boolean = false;
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private AuthService: AuthService,
    private worksService: WorksService,
    private modalService: ModalService,
    private renderer: Renderer2,
    private screenSizeService: ScreenSizeService
  ) {
    this.worksTypesArray = Object.values(WorksTypes);

    this.searchForm = this.formBuilder.group({
      search: ["", Validators.required],
    });

    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);

    const modalOpenedSubscription = this.modalService.modalOpened.subscribe(
      (event) => {
        if (!event || (event && event.modalId === "addList" && !event.opened)) {
          setTimeout(() => {
            this.isCreatingList = false;
          }, 150);
        }
      }
    );
    this.subscriptions.push(modalOpenedSubscription);
  }

  public isMangaOrComic(work: WorksModel): boolean {
    return this.isManga(work) || this.isComic(work);
  }

  onScroll = (): void => {
    this.resetAddToListState();
  };

  determinePosition(el: HTMLElement) {
    let searchElement = el.parentElement;
    let lineElement = el.parentElement;

    while (searchElement && !searchElement.classList.contains("search")) {
      searchElement = searchElement.parentElement;
    }
    while (lineElement && !lineElement.classList.contains("line")) {
      lineElement = lineElement.parentElement;
    }

    const parentTop = searchElement!.scrollTop;
    const parentHeight = searchElement!.offsetHeight;
    const elementTop = Math.abs(lineElement!.offsetTop);

    // 320px is .search div offset from modal top
    const relativePosition = elementTop - parentTop - 320;
    const percentagePosition = (relativePosition / parentHeight) * 100;

    // 430px is .search div height
    if (lineElement?.querySelector(".addToList")) {
      this.renderer.setStyle(
        lineElement?.querySelector(".addToList"),
        "top",
        `-${Math.abs((percentagePosition * 430) / 100 + 25)}px`
      );
    }
  }

  toggleAddToList(event: Event, id: string) {
    if (!this.isMobile) {
      const currentAddToListState = this.showAddToListState[id];
      let currentAddToListButtonIsActive: boolean;
      if ((event.target as HTMLElement).classList.contains("listActive")) {
        currentAddToListButtonIsActive = true;
      } else {
        currentAddToListButtonIsActive = false;
      }
      this.resetAddToListState();
      this.showAddToListState[id] = !currentAddToListState;

      if (!currentAddToListButtonIsActive) {
        (event.target as HTMLElement).classList.add("listActive");
      }

      if (this.showAddToListState[id] === true) {
        setTimeout(() => {
          this.determinePosition(event.target as HTMLElement);
        }, 50);
      }
    } else {
      this.openModal(`addList_${id}`);
    }
  }

  @HostListener("document:click", ["$event"])
  onDocumentClick(event: Event) {
    if (
      !this.isAddToListClick(event) &&
      !this.isAddToListButtonClick(event) &&
      !this.isCreatingList
    ) {
      this.resetAddToListState();
    }
  }

  resetAddToListState() {
    this.showAddToListState = {};

    const elements = document.querySelectorAll(".listActive");
    elements.forEach((element) => {
      element.classList.remove("listActive");
    });
  }

  isAddToListVisible(id: string): boolean {
    return this.showAddToListState[id] || false;
  }

  isAddToListClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".addToList");
  }

  isAddToListButtonClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".addToListLink");
  }

  public getGameCategories(key?: GamesCategories): string | void {
    if (key !== undefined && GamesCategories[key] !== undefined) {
      return GamesCategories[key];
    } else {
      return "";
    }
  }

  public getEnumKeyByEnumValue(enumValue: string): string {
    const keys = Object.keys(WorksTypes).filter(
      (key) => WorksTypes[key as keyof typeof WorksTypes] === enumValue
    );
    return keys.length > 0 ? keys[0] : "";
  }

  defineType(type: WorksTypes) {
    this.searchType$.next(type);
    this.defineImagesPath(type);
  }

  defineImagesPath(type: WorksTypes) {
    if (type === WorksTypes.Movie || type === WorksTypes.Series) {
      this.imagesPath = "https://image.tmdb.org/t/p/original";
    } else if (type === WorksTypes.Game) {
      this.imagesPath = "https://images.igdb.com/igdb/image/upload/t_cover_big";
    } else if (type === WorksTypes.Manga) {
      this.imagesPath = "https://uploads.mangadex.org/covers";
    } else if (type === WorksTypes.Comic) {
      this.imagesPath = "";
    }
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
  }

  ngAfterViewInit() {
    this.defineImagesPath(this.searchType);

    const modalOpenedSubscription = this.modalService.modalOpened.subscribe(
      (event) => {
        const type: WorksTypes | undefined = this.modalService.getWorkType();
        if (event && type) {
          this.defineType(type);
        }
      }
    );
    this.subscriptions.push(modalOpenedSubscription);

    const searchSubscription = this.searchTerm$
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((term: string) => {
          this.searchTerm = term;
          if (this.searchTerm) {
            return this.search(term, this.searchType);
          } else {
            return of(null);
          }
        })
      )
      .subscribe((res) => {
        this.isLoading = false;
        this.setSearchResults(res);
      });
    this.subscriptions.push(searchSubscription);

    const searchTypeSubscription = this.searchType$
      .pipe(
        distinctUntilChanged(),
        switchMap((type: WorksTypes) => {
          this.searchType = type;
          if (this.searchTerm) {
            return this.search(this.searchTerm, type);
          } else {
            return of(null);
          }
        })
      )
      .subscribe((res) => {
        this.isLoading = false;
        this.setSearchResults(res);
      });
    this.subscriptions.push(searchTypeSubscription);
  }

  setSearchResults(results: PaginatedResponse | null) {
    if (results) {
      this.searchResultsSubject.next(results.data as WorksModel[]);
      this.pagination = results.pagination!;
    } else {
      this.searchResultsSubject.next([]);
      this.pagination = {page: 1, totalPages: 1};
    }
  }

  onInputChange(event: Event): void {
    this.isLoading = true;
    const inputElement = event.target as HTMLInputElement;
    if (inputElement) {
      this.searchTerm$.next(inputElement.value);
    }
  }

  search(
    term: string,
    searchType: WorksTypes
  ): Observable<PaginatedResponse | null> {
    this.isLoading = true;
    return this.worksService.searchWorksByType(term, searchType);
  }

  loadMore(page: number): void {
    const searchWorksSubscription = this.worksService
      .searchWorksByType(this.searchTerm, this.searchType, page)
      .subscribe((res) => {
        if (res?.data) {
          this.searchResultsSubject.next([
            ...this.searchResultsSubject.value,
            ...(res.data as WorksModel[]),
          ]);
          this.pagination = res.pagination!;
        }
      });
    this.subscriptions.push(searchWorksSubscription);
  }

  add(work: WorksModel, user: User, event: Event, list?: string): void {
    const WorksDTO: WorksDTO = {
      provider: work.provider,
      providerId: work.providerId,
    };
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      this.addError = undefined;
      linkElement.innerHTML = "Adding...";

      const addWorkSubscription = this.worksService
        .addWorkByType(WorksDTO, this.searchType, user, list)
        .subscribe({
          next: (work: WorksModel | null) => {
            if (work) {
              this.lastAddedItem = work;
              linkElement.innerHTML = "Added";
              linkElement.setAttribute("disabled", "");
            }
          },
          error: (error: CustomError) => {
            linkElement.innerHTML = "Add";
            this.addError = error;
            setTimeout(() => {
              this.addError = undefined;
            }, 3000);
          },
        });
      this.subscriptions.push(addWorkSubscription);
    }
  }

  addToListError(error: CustomError) {
    this.addedToListSuccess = "";
    if (error) {
      this.addError = error;
      setTimeout(() => {
        this.addError = undefined;
      }, 3000);
    }
  }

  addedToList(list: string) {
    this.addError = undefined;
    if (list) {
      this.addedToListSuccess = `Added to ${list}`;
      setTimeout(() => {
        this.addedToListSuccess = "";
      }, 3000);
    }
  }

  creatingList(active: boolean) {
    this.isCreatingList = active;
  }

  closeAddToList(close: {event: Event; itemProviderId: string}) {
    this.showAddToListState[close.itemProviderId] = false;
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
