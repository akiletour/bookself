import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import {Subscription} from "rxjs";
import {WorksTypes} from "src/app/enums/Works.enum";
import {WorksDTO, WorksModel} from "src/app/models/WorksModel";
import {User} from "src/app/modules/auth/models/User";
import {environment} from "src/environments/environment";
import {CustomError} from "../../interfaces/CustomError.interface";
import {CacheService} from "../../services/cache.service";
import {ScreenSizeService} from "../../services/screenSize.service";
import {WorksService} from "../../services/works.service";
import {AuthService} from "src/app/modules/auth/services/auth.service";
import {MangasProviders} from "src/app/enums/Manga";
import {MangaService} from "../../services/manga.service";
import {Manga, Volume} from "src/app/models/Manga";

@Component({
  selector: "app-mangas-details",
  templateUrl: "./mangas-details.component.html",
})
export class MangasDetailsComponent
  implements OnInit, OnChanges, AfterViewInit, OnDestroy
{
  @Input() item!: Manga;
  @Input() list?: string;
  @Input() type!: WorksTypes;
  public user!: User;
  public error?: CustomError;
  public success!: string;
  public imagesDomain: string = environment.imagesDomain;
  public mangasProviders = MangasProviders;
  addError: CustomError | undefined;
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;
  public isTablet: boolean = false;
  @ViewChild("mangaDetailsElement", {static: false})
  gameDetailsElement!: ElementRef;

  constructor(
    private mangaService: MangaService,
    private AuthService: AuthService,
    private worksService: WorksService,
    private cacheService: CacheService,
    private screenSizeService: ScreenSizeService
  ) {
    const userSubscription = this.AuthService.user$.subscribe({
      next: (user: User | null) => {
        if (user) {
          this.user = user;
        }
      },
    });
    this.subscriptions.push(userSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
    const screenSizeTabletSubscription =
      this.screenSizeService.isTablet$.subscribe((isTablet) => {
        this.isTablet = isTablet;
      });
    this.subscriptions.push(screenSizeTabletSubscription);
  }

  ngOnChanges() {}

  ngAfterViewInit() {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          // Check for mangas updates once component is rendered in the DOM
          const mangaUpdatedSubscription = this.mangaService
            .updateManga(this.item._id, this.item.provider)
            .subscribe({
              next: (manga) => {
                if (manga.volumes) {
                  this.item.volumes = manga.volumes;
                  this.item.status = manga.status;

                  this.cacheService.updateInCacheByPrefix(
                    ["mangas_", "history_", "list_"],
                    this.item
                  );
                }
                // Stop observing once manga has been updated
                observer.disconnect();
              },
              error: (error: CustomError) => {
                this.error = error;
              },
            });
          this.subscriptions.push(mangaUpdatedSubscription);
        }
      });
    });

    if (this.item && this.gameDetailsElement) {
      observer.observe(this.gameDetailsElement.nativeElement);
    }
  }

  getTrimmedSummary(summary: string): string {
    if (summary) {
      return summary.split("---")[0].trim();
    }
    return "";
  }

  sortVolumesByNumber(volumes: Volume[], sort: string = "asc") {
    return volumes.sort((a, b) => {
      const dateA = a.volumeNumber;
      const dateB = b.volumeNumber;
      return sort === "asc" ? dateA - dateB : dateB - dateA;
    });
  }

  public allVolumesOwned(): boolean {
    if (this.item.progress?.length === this.item.volumes?.length) {
      return true;
    }

    return false;
  }

  updateAllVolumes(manga: Manga, user: User, method: string) {
    let volumes: {volumes: number[]} = {volumes: []};
    if (method === "post") {
      volumes = {
        volumes: this.item.volumes?.map((volume) => volume.volumeNumber) || [],
      };
    }
    const updateVolumeSubscription = this.mangaService
      .updateUserMangaProgress(manga, user, volumes, method)
      .subscribe({
        next: (user) => {
          const matchingManga = user.mangas!.find(
            (manga) => String(manga._id) === String(this.item._id)
          );

          if (matchingManga) {
            this.item.progress = matchingManga.progress;
          }

          this.cacheService.updateInCacheByPrefix(
            ["mangas_", "history_", "list_"],
            this.item
          );

          this.error = undefined;
        },
        error: (error: CustomError) => {
          this.error = error;
        },
      });
    this.subscriptions.push(updateVolumeSubscription);
  }

  addFromList(work: WorksModel, user: User, event: Event): void {
    const WorksDTO: WorksDTO = {
      provider: work.provider,
      providerId: work.providerId,
    };
    const linkElement = event.target as HTMLLinkElement;

    if (linkElement.getAttribute("disabled") === null) {
      this.addError = undefined;
      linkElement.innerHTML = "Adding...";

      const addWorkSubscription = this.worksService
        .addWorkByType(WorksDTO, WorksTypes.Manga, user)
        .subscribe({
          next: (work: WorksModel | null) => {
            if (work) {
              this.item = work as Manga;
            }
          },
          error: (error: CustomError) => {
            linkElement.innerHTML = "Add";
            this.addError = error;
            setTimeout(() => {
              this.addError = undefined;
            }, 3000);
          },
        });
      this.subscriptions.push(addWorkSubscription);
    }
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
