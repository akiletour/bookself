import {Component, Input} from "@angular/core";
import {User} from "src/app/modules/auth/models/User";

@Component({
  selector: "app-skeleton-dashboard",
  templateUrl: "./skeleton-dashboard.component.html",
})
export class SkeletonDashboardComponent {
  @Input() user!: User;
}
