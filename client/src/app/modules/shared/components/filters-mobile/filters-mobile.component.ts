import {Component} from "@angular/core";
import {BaseFiltersComponent} from "../base-filters/base-filters.component";

@Component({
  selector: "app-filters-mobile",
  templateUrl: "./filters-mobile.component.html",
  styleUrls: ["./filters-mobile.component.scss"],
})
export class FiltersMobileComponent extends BaseFiltersComponent {
  public isFiltersPanelOpen = false;

  toggleFilters() {
    this.isFiltersPanelOpen = !this.isFiltersPanelOpen;
  }
}
