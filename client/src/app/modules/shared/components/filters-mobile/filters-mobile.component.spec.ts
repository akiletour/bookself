import {ComponentFixture, TestBed} from "@angular/core/testing";

import {FiltersMobileComponent} from "./filters-mobile.component";

describe("FiltersMobileComponent", () => {
  let component: FiltersMobileComponent;
  let fixture: ComponentFixture<FiltersMobileComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FiltersMobileComponent],
    });
    fixture = TestBed.createComponent(FiltersMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
