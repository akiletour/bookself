import {Component} from "@angular/core";
import {ModalService} from "../../services/modal.service";

@Component({
  selector: "app-news-modal",
  templateUrl: "./news-modal.component.html",
})
export class NewsModalComponent {
  version: number = 1.0;
  seen: boolean = true;

  constructor(private modalService: ModalService) {
    const modalSeen = this.getCookie(`modalSeen_${this.version}`);

    if (!modalSeen) {
      this.seen = false;
      this.setCookie(`modalSeen_${this.version}`, "true");
      this.modalService.openModal(`news_${this.version}`);
    }
  }

  private getCookie(name: string): string {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop()?.split(";").shift() || "";
    return "";
  }

  private setCookie(name: string, value: string) {
    document.cookie = `${name}=${value}; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/`;
  }
}
