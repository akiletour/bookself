import {Component, HostListener, Input, OnDestroy, OnInit} from "@angular/core";
import {CustomError} from "../../interfaces/CustomError.interface";
import {ModalService} from "../../services/modal.service";
import {User} from "src/app/modules/auth/models/User";
import {WorksModel} from "src/app/models/WorksModel";
import {trigger, state, style, transition, animate} from "@angular/animations";
import {Subscription} from "rxjs";
import {ScreenSizeService} from "../../services/screenSize.service";

@Component({
  selector: "app-details-add-to-list",
  templateUrl: "./details-add-to-list.component.html",
  animations: [
    trigger("fadeOut", [
      state("void", style({opacity: 1})),
      transition(":leave", [animate("0.3s", style({opacity: 0}))]),
    ]),
    trigger("fadeInOut", [
      transition(":enter", [
        style({opacity: 0}),
        animate("0.05s", style({opacity: 0})),
        animate("0.1s", style({opacity: 1})),
      ]),
      transition(":leave", [animate("0.1s", style({opacity: 0}))]),
    ]),
  ],
})
export class DetailsAddToListComponent implements OnInit, OnDestroy {
  @Input() item!: WorksModel;
  @Input() user!: User;
  addError: CustomError | undefined;
  public addedToListSuccess?: string;
  private showAddToListState: boolean = false;
  private isCreatingList: boolean = false;
  private subscriptions: Subscription[] = [];
  public isMobile: boolean = false;

  constructor(
    private modalService: ModalService,
    private screenSizeService: ScreenSizeService
  ) {
    const modalOpenedSubscription = this.modalService.modalOpened.subscribe(
      (event) => {
        if (!event || (event && event.modalId === "addList" && !event.opened)) {
          setTimeout(() => {
            this.isCreatingList = false;
          }, 150);
        }
      }
    );
    this.subscriptions.push(modalOpenedSubscription);
  }

  ngOnInit() {
    const screenSizeMobileSubscription =
      this.screenSizeService.isMobile$.subscribe((isMobile) => {
        this.isMobile = isMobile;
      });
    this.subscriptions.push(screenSizeMobileSubscription);
  }

  toggleAddToList(event: Event) {
    if (!this.isMobile) {
      this.showAddToListState = !this.showAddToListState;

      (event.target as HTMLElement).parentElement?.classList.toggle(
        "listActive"
      );
    } else {
      this.openModal(`addList_${this.item.providerId}`);
    }
  }

  resetAddToListState() {
    this.showAddToListState = false;

    const elements = document.querySelectorAll(".listActive");
    elements.forEach((element) => {
      element.classList.remove("listActive");
    });
  }

  @HostListener("document:click", ["$event"])
  onDocumentClick(event: Event) {
    if (
      !this.isAddToListClick(event) &&
      !this.isAddToListButtonClick(event) &&
      !this.isCreatingList
    ) {
      this.resetAddToListState();
    }
  }

  isAddToListClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".addToList");
  }

  isAddToListButtonClick(event: Event): boolean {
    const target = event.target as HTMLElement;
    return !!target.closest(".addToListLink");
  }

  isAddToListVisible(): boolean {
    return this.showAddToListState || false;
  }

  addToListError(error: CustomError) {
    this.addedToListSuccess = "";
    if (error) {
      this.addError = error;
      setTimeout(() => {
        this.addError = undefined;
      }, 3000);
    }
  }

  addedToList(list: string) {
    this.addError = undefined;
    if (list) {
      this.addedToListSuccess = `Added to ${list}`;
      setTimeout(() => {
        this.addedToListSuccess = "";
      }, 3000);
    }
  }

  creatingList(active: boolean) {
    this.isCreatingList = active;
  }

  closeAddToList(close: {event: Event; itemProviderId: string}) {
    if (this.item.providerId === close.itemProviderId) {
      this.resetAddToListState();
    }
  }

  openModal(modalId: string) {
    this.modalService.openModal(modalId);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
