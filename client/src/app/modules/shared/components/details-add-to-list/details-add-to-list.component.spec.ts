import {ComponentFixture, TestBed} from "@angular/core/testing";

import {DetailsAddToListComponent} from "./details-add-to-list.component";

describe("DetailsAddToListComponent", () => {
  let component: DetailsAddToListComponent;
  let fixture: ComponentFixture<DetailsAddToListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsAddToListComponent],
    });
    fixture = TestBed.createComponent(DetailsAddToListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
