import {Component} from "@angular/core";
import {BaseFiltersComponent} from "../base-filters/base-filters.component";

@Component({
  selector: "app-filters",
  templateUrl: "./filters.component.html",
  styleUrls: ["./filters.component.scss"],
})
export class FiltersComponent extends BaseFiltersComponent {}
