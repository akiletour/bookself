export interface CustomError {
  message?: string;
  status?: number;
  statusText?: string;
  url?: string;
  ok?: boolean;
  error: {
    error: string;
  };
}
