import {User} from "../../auth/models/User";
import {Pagination} from "../../../models/Pagination";
import {WorksModel} from "src/app/models/WorksModel";

export interface PaginatedResponse {
  data: T[];
  pagination: Pagination;
}

export type T = WorksModel | User;
