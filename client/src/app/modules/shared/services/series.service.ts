import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {Subject, map} from "rxjs";
import {User} from "../../../modules/auth/models/User";
import {Series} from "src/app/models/Series";
import {AuthService} from "../../auth/services/auth.service";
import {CacheService} from "./cache.service";

@Injectable()
export class SeriesService {
  private apiDomain: string = environment.apiDomain;
  private seriesUpdatedSubject = new Subject<Series>();
  public seriesUpdated$ = this.seriesUpdatedSubject.asObservable();
  private options: {headers: {Authorization: string}};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private cacheService: CacheService
  ) {
    this.options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}`,
      },
    };
  }

  public updateSeries(seriesId: string, provider: number): Observable<Series> {
    return this.http
      .patch<Series>(`${this.apiDomain}/series/${seriesId}/`, {provider})
      .pipe(
        map((res) => {
          this.cacheService.updateInCacheByPrefix(
            ["series_", "history_"],
            res,
            "seasons"
          );
          return res as Series;
        })
      );
  }

  public updateUserSeriesProgress(
    series: Series,
    user: User,
    progress: {season: number; episode: number}
  ): Observable<User> {
    return this.http
      .patch<User>(
        `${this.apiDomain}/series/${series._id}/user/${user._id}`,
        progress,
        this.options
      )
      .pipe(
        map((res) => {
          return res as User;
        })
      );
  }

  public notifySeriesUpdated(series: Series) {
    this.seriesUpdatedSubject.next(series);
  }
}
