import {Injectable} from "@angular/core";
import {WorksModel} from "src/app/models/WorksModel";
import {List} from "../../auth/models/User";
import {Series} from "src/app/models/Series";
import {Game} from "src/app/models/Game";
import {Manga} from "src/app/models/Manga";
import {Comic} from "src/app/models/Comic";

@Injectable({
  providedIn: "root",
})
export class CacheService {
  private cacheKeyPrefix = "cache_";

  private getKey(type: string): string {
    return this.cacheKeyPrefix + type;
  }

  addToCache(type: string, value: WorksModel[] | List[] | List) {
    const key = this.getKey(type);
    localStorage.setItem(key, JSON.stringify(value));
  }

  getFromCache(type: string): WorksModel[] | List[] | List {
    const key = this.getKey(type);
    const cachedData = localStorage.getItem(key);

    return cachedData ? JSON.parse(cachedData) : null;
  }

  updateInCacheByPrefix(
    prefixes: string[],
    updatedWork: WorksModel,
    props?: string
  ) {
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);

      if (key) {
        for (const prefix of prefixes) {
          if (key.startsWith(this.getKey(prefix)) && !key.includes("_metas")) {
            const cachedData = localStorage.getItem(key);
            if (cachedData) {
              const cachedArray: WorksModel[] = JSON.parse(cachedData);
              const updatedCacheArray = cachedArray.map((item) => {
                if (item._id === updatedWork._id) {
                  if (props !== undefined) {
                    if (props === "seasons") {
                      (item as Series).numberOfEpisodes = (
                        updatedWork as Series
                      ).numberOfEpisodes;
                      (item as Series).numberOfSeasons = (
                        updatedWork as Series
                      ).numberOfSeasons;
                      (item as Series).seasons = (
                        updatedWork as Series
                      ).seasons;
                      return item;
                    } else if (props === "dlcs") {
                      (item as Game).dlcs = (updatedWork as Game).dlcs;
                      return item;
                    } else if (props === "volumes") {
                      (item as Manga).status = (updatedWork as Manga).status;
                      (item as Manga).volumes = (updatedWork as Manga).volumes;
                      return item;
                    } else if (props === "issues") {
                      (item as Comic).summary = (updatedWork as Comic).summary;
                      (item as Comic).volumes = (updatedWork as Comic).volumes;
                      return item;
                    } else {
                      return item;
                    }
                  } else {
                    return updatedWork;
                  }
                } else {
                  return item;
                }
              });

              localStorage.removeItem(key);
              localStorage.setItem(key, JSON.stringify(updatedCacheArray));
            }
          }
        }
      }
    }
  }

  removeFromCache(type: string) {
    const key = this.getKey(type);
    localStorage.removeItem(key);
  }

  clearCacheByPrefix(prefixes: string[]) {
    const keysToRemove = [];

    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i);

      if (key) {
        for (const prefix of prefixes) {
          if (key.startsWith(this.getKey(prefix))) {
            keysToRemove.push(key);
          }
        }
      }
    }

    keysToRemove.forEach((key) => {
      localStorage.removeItem(key);
    });
  }

  clearCache() {
    localStorage.clear();
  }
}
