// screen-size.service.ts

import {Injectable, OnDestroy} from "@angular/core";
import {
  BehaviorSubject,
  Subscription,
  debounceTime,
  fromEvent,
  map,
} from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ScreenSizeService implements OnDestroy {
  private isMobileSubject = new BehaviorSubject<boolean>(false);
  isMobile$ = this.isMobileSubject.asObservable();
  private isTabletSubject = new BehaviorSubject<boolean>(false);
  isTablet$ = this.isTabletSubject.asObservable();
  private isLaptopSubject = new BehaviorSubject<boolean>(false);
  isLaptop$ = this.isLaptopSubject.asObservable();
  private subscriptions: Subscription[] = [];

  constructor() {
    this.checkScreenWidth();

    const scroll$ = fromEvent(window, "resize");
    const scrollSubscription = scroll$
      .pipe(
        debounceTime(200),
        map(() => this.checkScreenWidth())
      )
      .subscribe();
    this.subscriptions.push(scrollSubscription);
  }

  private checkScreenWidth() {
    const isLaptop = window.innerWidth < 1220 && window.innerWidth > 980;
    const isTablet = window.innerWidth < 1220 && window.innerWidth > 767;
    const isMobile = window.innerWidth < 1220;

    this.isLaptopSubject.next(isLaptop);
    this.isTabletSubject.next(isTablet);
    this.isMobileSubject.next(isMobile);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
