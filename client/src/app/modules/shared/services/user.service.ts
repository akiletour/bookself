import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {map} from "rxjs";
import {User, UserActivity} from "../../auth/models/User";
import {AuthService} from "../../auth/services/auth.service";

@Injectable()
export class UserService {
  private apiDomain: string = environment.apiDomain;
  private options: {headers: {Authorization: string}};

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) {
    this.options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}`,
      },
    };
  }

  public getUserActivity(user: User, filter: string): Observable<UserActivity> {
    let filterActivity: string = "";

    if (filter) {
      filterActivity = `?filter=${filter}`;
    }

    return this.http
      .get<UserActivity>(
        `${this.apiDomain}/users/${user._id}/activity${filterActivity}`,
        this.options
      )
      .pipe(
        map((res) => {
          if (res) {
            return res as UserActivity;
          }
          return {};
        })
      );
  }
}
