import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {Subject, map} from "rxjs";
import {User} from "../../../modules/auth/models/User";
import {AuthService} from "../../auth/services/auth.service";
import {CacheService} from "./cache.service";
import {Comic} from "src/app/models/Comic";

@Injectable()
export class ComicService {
  private apiDomain: string = environment.apiDomain;
  private comicsUpdatedSubject = new Subject<Comic>();
  public comicsUpdated$ = this.comicsUpdatedSubject.asObservable();
  private options: {headers: {Authorization: string}};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private cacheService: CacheService
  ) {
    this.options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}`,
      },
    };
  }

  public updateComic(comicId: string, provider: number): Observable<Comic> {
    return this.http
      .patch<Comic>(`${this.apiDomain}/comics/${comicId}/`, {provider})
      .pipe(
        map((res) => {
          this.cacheService.updateInCacheByPrefix(
            ["comics_", "history_", "list_"],
            res,
            "issues"
          );
          return res as Comic;
        })
      );
  }

  public updateUserComicProgress(
    comic: Comic,
    user: User,
    volumes: {volumes: number[]},
    method: string
  ): Observable<User> {
    return this.http
      .request<User>(
        method.toLowerCase(),
        `${this.apiDomain}/comics/${comic._id}/user/${user._id}`,
        {body: volumes, headers: this.options.headers}
      )
      .pipe(
        map((res) => {
          return res as User;
        })
      );
  }

  public notifyComicUpdated(comics: Comic) {
    this.comicsUpdatedSubject.next(comics);
  }
}
