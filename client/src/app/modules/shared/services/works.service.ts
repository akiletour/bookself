import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {PaginatedResponse} from "../interfaces/ApiResponse.interface";
import {Subject, map} from "rxjs";
import {
  UpdatedWork,
  WorkDetailsComponent,
  WorksDTO,
  WorksModel,
} from "../../../models/WorksModel";
import {WorksTypes} from "../../../enums/Works.enum";
import {MoviesDetailsComponent} from "../components/movies-details/movies-details.component";
import {SeriesDetailsComponent} from "../components/series-details/series-details.component";
import {GamesDetailsComponent} from "../components/games-details/games-details.component";
import {AuthService} from "../../auth/services/auth.service";
import {User} from "../../auth/models/User";
import {ConfigService} from "./config.service";
import {CacheService} from "./cache.service";
import {MangasDetailsComponent} from "../components/mangas-details/mangas-details.component";
import {ComicsDetailsComponent} from "../components/comics-details/comics-details.component";

@Injectable()
export class WorksService {
  private apiDomain: string = environment.apiDomain;
  private worksUpdatedSubject = new Subject<UpdatedWork>();
  public worksUpdated$ = this.worksUpdatedSubject.asObservable();
  private options: {headers: {Authorization: string}};
  private limit: number;

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    public configService: ConfigService,
    private cacheService: CacheService
  ) {
    this.limit = this.configService.limit;

    this.options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}`,
      },
    };
  }

  public searchWorksByType(
    term: string,
    type: string,
    page: number = 1
  ): Observable<PaginatedResponse | null> {
    const options =
      type === WorksTypes.Game
        ? {
            headers: {
              Authorization: `Bearer ${this.authService.user?.tokens?.twitchAccessToken}`,
            },
          }
        : undefined;

    return this.http
      .get<PaginatedResponse>(
        `${this.apiDomain}/search/${type}/${term}?page=${page}`,
        options
      )
      .pipe(
        map((res) => {
          if (res) {
            return {
              pagination: res.pagination,
              data: res.data as WorksModel[],
            };
          }
          return null;
        })
      );
  }

  public addWorkByType(
    work: WorksDTO,
    type: string,
    user: User,
    listId?: string
  ): Observable<WorksModel> {
    let routeSuffix: string = "";
    if (listId !== undefined) {
      work.listId = listId;
      routeSuffix = `/list/${listId}`;
    }

    const options =
      type === WorksTypes.Game
        ? {
            headers: {
              Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}, Bearer ${this.authService.user?.tokens?.twitchAccessToken}`,
            },
          }
        : this.options;

    return this.http
      .post<WorksModel>(
        `${this.apiDomain}/${type}/user/${user._id}${routeSuffix}`,
        work,
        options
      )
      .pipe(
        map((res) => {
          const data = res as WorksModel;
          if (listId !== undefined) {
            this.cacheService.clearCacheByPrefix([`list_${listId}_`, "lists"]);
            this.notifyWorksUpdated(type as WorksTypes, "add", data!, true);
          } else {
            this.cacheService.clearCacheByPrefix([
              `${type}_`,
              "history_",
              "search_",
            ]);
            this.notifyWorksUpdated(type as WorksTypes, "add", data!);
          }
          return data;
        })
      );
  }

  public getWorks(
    userId: string,
    options: {
      query?: string;
      limit?: number;
      page?: number;
      sortBy?: string;
      order?: string;
    } = {}
  ): Observable<PaginatedResponse | null> {
    const {
      query = "",
      page = 1,
      sortBy = "discoveryDate",
      order = "desc",
      limit = this.limit,
    } = {...options};

    const params = new HttpParams()
      .set("query", query.toString())
      .set("page", page.toString())
      .set("sortBy", sortBy.toString())
      .set("order", order.toString())
      .set("limit", limit.toString());

    return this.http
      .get<PaginatedResponse | null>(`${this.apiDomain}/works/user/${userId}`, {
        params,
        headers: this.options.headers,
      })
      .pipe(
        map((res) => {
          if (res) {
            const works = res.data as WorksModel[];

            if (query === "") {
              this.cacheService.addToCache(
                `history_${page}_${sortBy}_${order}`,
                works
              );
            } else {
              this.cacheService.addToCache(
                `search_${query}_${page}_${sortBy}_${order}`,
                works
              );
            }

            return {
              pagination: res.pagination,
              data: works,
            };
          }
          return null;
        })
      );
  }

  public getWorksFromList(
    userId: string,
    listId: string,
    options: {
      limit?: number;
      page?: number;
    } = {}
  ): Observable<PaginatedResponse | null> {
    const {page = 1, limit = this.limit} = {...options};

    const params = new HttpParams()
      .set("page", page.toString())
      .set("limit", limit.toString());

    return this.http
      .get<PaginatedResponse | null>(
        `${this.apiDomain}/works/user/${userId}/list/${listId}`,
        {
          params,
          headers: this.options.headers,
        }
      )
      .pipe(
        map((res) => {
          if (res) {
            const works = res.data as WorksModel[];
            this.cacheService.addToCache(`list_${listId}_${page}`, works);
            return {
              pagination: res.pagination,
              data: works,
            };
          }
          return null;
        })
      );
  }

  public getWorksByType(
    userId: string,
    type: string,
    options: {
      limit?: number;
      page?: number;
      filter?: string;
      sortBy?: string;
      order?: string;
    } = {}
  ): Observable<PaginatedResponse | null> {
    const {
      page = 1,
      filter = "All",
      sortBy = "discoveryDate",
      order = "desc",
      limit = this.limit,
    } = {...options};

    const params = new HttpParams()
      .set("page", page.toString())
      .set("filter", filter)
      .set("sortBy", sortBy.toString())
      .set("order", order.toString())
      .set("limit", limit.toString());

    return this.http
      .get<PaginatedResponse | null>(
        `${this.apiDomain}/${type}/user/${userId}`,
        {
          params,
          headers: this.options.headers,
        }
      )
      .pipe(
        map((res) => {
          if (res) {
            const works = res.data as WorksModel[];
            if (limit <= 4) {
              // Dashboard cache
              this.cacheService.addToCache(`${type}_dashboard`, works);
            } else {
              // Works list cache
              this.cacheService.addToCache(
                `${type}_${page}_${filter}_${sortBy}_${order}`,
                works
              );
            }

            return {
              pagination: res.pagination,
              data: works,
            };
          }
          return null;
        })
      );
  }

  public updateWorkDateByType(
    userId: string,
    workId: string,
    type: string,
    date: string
  ): Observable<User> {
    return this.http
      .patch<User>(
        `${this.apiDomain}/works/${workId}/user/${userId}`,
        {
          type,
          date,
        },
        this.options
      )
      .pipe(
        map((res) => {
          this.cacheService.clearCacheByPrefix([
            `${type}_`,
            "history_",
            "list_",
          ]);
          this.notifyWorksUpdated(type as WorksTypes, "edit", workId);
          return res as User;
        })
      );
  }

  public updateWorkRatingByType(
    userId: string,
    workId: string,
    type: string,
    userRating: number
  ): Observable<User> {
    return this.http
      .patch<User>(
        `${this.apiDomain}/works/${workId}/user/${userId}`,
        {
          type,
          userRating,
        },
        this.options
      )
      .pipe(
        map((res) => {
          this.cacheService.clearCacheByPrefix([
            `${type}_`,
            "history_",
            "list_",
          ]);
          this.notifyWorksUpdated(type as WorksTypes, "edit", workId);
          return res as User;
        })
      );
  }

  public removeWorkById(
    userId: string,
    workId: string,
    type: WorksTypes,
    listId?: string
  ): Observable<boolean> {
    if (listId) {
      return this.http
        .delete<User>(
          `${this.apiDomain}/works/${workId}/user/${userId}/list/${listId}`,
          this.options
        )
        .pipe(
          map(() => {
            this.cacheService.clearCacheByPrefix([`list_${listId}_`, "lists"]);
            this.notifyWorksUpdated(
              type as WorksTypes,
              "removeFromList",
              workId
            );
            return true;
          })
        );
    } else {
      return this.http
        .delete<User>(
          `${this.apiDomain}/works/${type}/${workId}/user/${userId}`,
          this.options
        )
        .pipe(
          map(() => {
            this.cacheService.clearCacheByPrefix([
              `${type}_`,
              "history_",
              "list_",
            ]);
            this.notifyWorksUpdated(type as WorksTypes, "remove", workId);
            return true;
          })
        );
    }
  }

  public getWorksDetailsComponentByType(type: string): WorkDetailsComponent {
    switch (type) {
      case WorksTypes.Movie:
        return MoviesDetailsComponent;
      case WorksTypes.Series:
        return SeriesDetailsComponent;
      case WorksTypes.Game:
        return GamesDetailsComponent;
      case WorksTypes.Manga:
        return MangasDetailsComponent;
      case WorksTypes.Comic:
        return ComicsDetailsComponent;
      // case WorksTypes.Book:
      //   return BookDetailsComponent;
      // case WorksTypes.Anime:
      //   return AnimeDetailsComponent;
      default:
        return null;
    }
  }

  public notifyWorksUpdated(
    type: WorksTypes,
    operator: string,
    work: WorksModel | string,
    list?: boolean
  ) {
    this.worksUpdatedSubject.next({type, operator, work, list});
  }
}
