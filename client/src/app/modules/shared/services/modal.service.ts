import {EventEmitter, Injectable} from "@angular/core";
import {WorksTypes} from "src/app/enums/Works.enum";

@Injectable({
  providedIn: "root",
})
export class ModalService {
  private type?: WorksTypes | undefined;
  public modals: {[modalId: string]: boolean} = {};
  public modalOpened: EventEmitter<{modalId: string; opened: boolean} | null> =
    new EventEmitter<{modalId: string; opened: boolean} | null>();

  constructor() {}

  openModal(modalId: string) {
    // Open specific modal
    this.modals[modalId] = true;

    this.modalOpened.emit({modalId, opened: true});
  }

  closeModal(modalId?: string) {
    if (!modalId) {
      // Close all modals
      Object.keys(this.modals).forEach((id) => (this.modals[id] = false));
      this.modalOpened.emit(null);
    } else {
      this.modals[modalId] = false;
      this.modalOpened.emit({modalId, opened: false});
    }
  }

  setWorkType(type?: WorksTypes) {
    this.type = type;
  }

  getWorkType() {
    return this.type;
  }
}
