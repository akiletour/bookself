import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {Subject, map} from "rxjs";
import {User} from "../../../modules/auth/models/User";
import {AuthService} from "../../auth/services/auth.service";
import {CacheService} from "./cache.service";
import {Manga} from "src/app/models/Manga";

@Injectable()
export class MangaService {
  private apiDomain: string = environment.apiDomain;
  private mangasUpdatedSubject = new Subject<Manga>();
  public mangasUpdated$ = this.mangasUpdatedSubject.asObservable();
  private options: {headers: {Authorization: string}};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private cacheService: CacheService
  ) {
    this.options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}`,
      },
    };
  }

  public updateManga(mangaId: string, provider: number): Observable<Manga> {
    return this.http
      .patch<Manga>(`${this.apiDomain}/mangas/${mangaId}/`, {provider})
      .pipe(
        map((res) => {
          this.cacheService.updateInCacheByPrefix(
            ["mangas_", "history_", "list_"],
            res,
            "volumes"
          );
          return res as Manga;
        })
      );
  }

  public updateUserMangaProgress(
    manga: Manga,
    user: User,
    volumes: {volumes: number[]},
    method: string
  ): Observable<User> {
    return this.http
      .request<User>(
        method.toLowerCase(),
        `${this.apiDomain}/mangas/${manga._id}/user/${user._id}`,
        {body: volumes, headers: this.options.headers}
      )
      .pipe(
        map((res) => {
          return res as User;
        })
      );
  }

  public notifyMangasUpdated(mangas: Manga) {
    this.mangasUpdatedSubject.next(mangas);
  }
}
