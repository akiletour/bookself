import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {Subject, map} from "rxjs";
import {User} from "../../../modules/auth/models/User";
import {Game} from "src/app/models/Game";
import {AuthService} from "../../auth/services/auth.service";
import {CacheService} from "./cache.service";

@Injectable()
export class GameService {
  private apiDomain: string = environment.apiDomain;
  private gamesUpdatedSubject = new Subject<Game>();
  public gamesUpdated$ = this.gamesUpdatedSubject.asObservable();
  private options: {headers: {Authorization: string}};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    private cacheService: CacheService
  ) {
    this.options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}`,
      },
    };
  }

  public updateGame(gameId: string, provider: number): Observable<Game> {
    const options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.twitchAccessToken}`,
      },
    };

    return this.http
      .patch<Game>(`${this.apiDomain}/games/${gameId}/`, {provider}, options)
      .pipe(
        map((res) => {
          this.cacheService.updateInCacheByPrefix(
            ["games_", "history_", "list_"],
            res,
            "dlcs"
          );
          return res as Game;
        })
      );
  }

  public updateUserGameDLCs(
    game: Game,
    user: User,
    dlc: {dlcId: number},
    method: string
  ): Observable<User> {
    return this.http
      .request<User>(
        method.toLowerCase(),
        `${this.apiDomain}/games/${game._id}/user/${user._id}`,
        {body: dlc, headers: this.options.headers}
      )
      .pipe(
        map((res) => {
          return res as User;
        })
      );
  }

  public notifyGamesUpdated(games: Game) {
    this.gamesUpdatedSubject.next(games);
  }
}
