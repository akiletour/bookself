import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "src/environments/environment";
import {Observable} from "rxjs/internal/Observable";
import {Subject, map} from "rxjs";
import {AuthService} from "../../auth/services/auth.service";
import {List, ListDTO, UpdatedList, User} from "../../auth/models/User";
import {ConfigService} from "./config.service";
import {CacheService} from "./cache.service";

@Injectable()
export class ListsService {
  private apiDomain: string = environment.apiDomain;
  private listsUpdatedSubject = new Subject<UpdatedList>();
  public listsUpdated$ = this.listsUpdatedSubject.asObservable();
  private options: {headers: {Authorization: string}};

  constructor(
    private http: HttpClient,
    private authService: AuthService,
    public configService: ConfigService,
    private cacheService: CacheService
  ) {
    this.options = {
      headers: {
        Authorization: `Bearer ${this.authService.user?.tokens?.accessToken}`,
      },
    };
  }

  public addList(user: User, list: ListDTO): Observable<List> {
    return this.http
      .post<List>(
        `${this.apiDomain}/lists/user/${user._id}`,
        list,
        this.options
      )
      .pipe(
        map((res) => {
          const data = res as List;
          user.lists?.unshift(data);
          this.authService.user = user;
          this.cacheService.clearCacheByPrefix(["lists"]);
          this.notifyListsUpdated("add", data!);
          return data;
        })
      );
  }

  public getLists(userId: string): Observable<List[] | null> {
    return this.http
      .get<List[] | null>(
        `${this.apiDomain}/lists/user/${userId}`,
        this.options
      )
      .pipe(
        map((res) => {
          if (res) {
            const lists = res as List[];
            this.cacheService.addToCache("lists", lists);
            return lists;
          }
          return null;
        })
      );
  }

  public getListById(userId: string, listId: string): Observable<List | null> {
    return this.http
      .get<List | null>(
        `${this.apiDomain}/lists/${listId}/user/${userId}`,
        this.options
      )
      .pipe(
        map((res) => {
          if (res) {
            const list = res as List;
            this.cacheService.addToCache(`list_${listId}_metas`, list);
            return list;
          }
          return null;
        })
      );
  }

  public updateList(
    user: User,
    listId: string,
    list: ListDTO
  ): Observable<List> {
    return this.http
      .patch<List>(
        `${this.apiDomain}/lists/${listId}/user/${user._id}`,
        list,
        this.options
      )
      .pipe(
        map((res) => {
          const data = res as List;
          const listIndex = user.lists?.findIndex(
            (list) => list._id === data._id
          );

          if (listIndex !== -1) {
            user.lists![listIndex!] = data;
          }
          this.authService.user = user;
          this.cacheService.clearCacheByPrefix([`list_${listId}_metas`]);
          this.cacheService.clearCacheByPrefix([`lists`]);
          this.notifyListsUpdated("edit", data);
          return data;
        })
      );
  }

  public removeList(user: User, list: List): Observable<boolean> {
    return this.http
      .delete<User>(
        `${this.apiDomain}/lists/${list._id}/user/${user._id}`,
        this.options
      )
      .pipe(
        map(() => {
          const listIndex = user.lists?.findIndex(
            (userList) => userList._id === list._id
          );

          if (listIndex !== -1) {
            user.lists!.splice(listIndex!, 1);
          }
          this.authService.user = user;
          this.cacheService.clearCacheByPrefix(["lists", `list_${list._id}`]);
          this.notifyListsUpdated("remove", list);
          return true;
        })
      );
  }

  public notifyListsUpdated(operator: string, list: List) {
    this.listsUpdatedSubject.next({operator, list});
  }
}
