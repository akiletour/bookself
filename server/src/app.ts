import express, {Express} from "express";
import compression from "compression";
import rateLimit from "express-rate-limit";
import helmet from "helmet";
import cors from "cors";
import * as utils from "./utils/helpers.util.ts";
import {errorHandler} from "./middleware/errorHandler.middleware.ts";
import path from "path";

import dotenv from "dotenv";

if (process.env.NODE_ENV !== "production") {
  dotenv.config({path: `${utils.__dirname}../../.env`});
}

const app: Express = express();

// Configure Helmet for security purposes
app.use(
  helmet({
    crossOriginResourcePolicy: {policy: "same-site"},
  })
);
app.use(
  cors({
    origin: process.env.CLIENT_URL,
    optionsSuccessStatus: 200,
  })
);

if (process.env.NODE_ENV !== "production") {
  app.use(
    helmet({
      crossOriginResourcePolicy: {policy: "cross-origin"},
      contentSecurityPolicy: {
        directives: {
          defaultSrc: ["'self'"],
          imgSrc: ["https://cdn.redoc.ly", "data:"],
          workerSrc: ["blob:"],
          scriptSrc: [
            "'self'",
            "blob:",
            "'unsafe-inline'",
            "https://cdn.redoc.ly",
          ],
        },
      },
    })
  );
}

// Compress API Responses
app.use(compression());

// API rate limits
let maxRequest: number;
if (process.env.NODE_ENV !== "production") {
  maxRequest = 2000;
} else {
  maxRequest = 300;
}

const limiter = rateLimit({
  windowMs: 60 * 1000, // 1 minute
  max: maxRequest, // Max requests by IP by windowMs
  message: "Too many requests from this IP, please retry later.",
});
app.set("trust proxy", 1 /* number of proxies between user and server */);

// Access Request Body
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Trim all body values
app.use(utils.trimRequestBody);

const imagesPath = path.join(utils.__dirname, "..", "images");
app.use("/images", express.static(imagesPath));

// Redoc API doc
app.use(
  "/doc",
  express.static(path.join(utils.__dirname, "/api/docs/redocly.html"))
);

/**
 * Routes
 */

// Auth
import authApiRoutes from "./api/routes/Auth.route.ts";
app.use("/", limiter, authApiRoutes);
// User
import userApiRoutes from "./api/routes/Users.route.ts";
app.use("/", limiter, userApiRoutes);
// List
import listApiRoutes from "./api/routes/Lists.route.ts";
app.use("/", limiter, listApiRoutes);
// Works
import worksApiRoutes from "./api/routes/Works.route.ts";
app.use("/", limiter, worksApiRoutes);
// Movie
import movieApiRoutes from "./api/routes/Movies.route.ts";
app.use("/", limiter, movieApiRoutes);
// Series
import seriesApiRoutes from "./api/routes/Series.route.ts";
app.use("/", limiter, seriesApiRoutes);
// Game
import gameApiRoutes from "./api/routes/Games.route.ts";
app.use("/", limiter, gameApiRoutes);
// Mangas
import mangaApiRoutes from "./api/routes/Mangas.route.ts";
app.use("/", limiter, mangaApiRoutes);
// Comics
import comicApiRoutes from "./api/routes/Comics.route.ts";
app.use("/", limiter, comicApiRoutes);
// Search
import searchApiRoutes from "./api/routes/Search.route.ts";
app.use("/", limiter, searchApiRoutes);

// Errors
app.use(errorHandler);

export default app;
