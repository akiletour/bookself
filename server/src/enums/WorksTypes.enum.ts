export enum WorksTypes {
  Movie = "movies",
  Series = "series",
  // Animation = "series/animation",
  Game = "games",
  Manga = "mangas",
  Comic = "comics",
  // Book = "books",
}
