export enum GamesCategories {
  "main_game" = 0,
  "dlc_addon" = 1,
  "expansion" = 2,
  "bundle" = 3,
  "standalone_expansion" = 4,
  "mod" = 5,
  "episode" = 6,
  "season" = 7,
  "remake" = 8,
  "remaster" = 9,
  "expanded_game" = 10,
  "port" = 11,
  "fork" = 12,
  "pack" = 13,
  "update" = 14,
}

export enum GamesStatuses {
  "released" = 0,
  "alpha" = 2,
  "beta" = 3,
  "early_access" = 4,
  "offline" = 5,
  "cancelled" = 6,
  "rumored" = 7,
  "delisted" = 8,
}

export enum GamesProviders {
  "igdb" = 1,
}
