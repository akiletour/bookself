import Express, {Request} from "express";
import Movie from "../models/Movie.model.ts";
import User from "../models/User.model.ts";
import {
  MovieDocument,
  MovieDocumentsPaginated,
} from "../../interfaces/Movie.interface.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";
import {Provider} from "../../interfaces/Provider.interface.ts";
import {
  BaseError,
  NoContentError,
  ServerError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import {toRegex} from "diacritic-regex";
import {List} from "../../interfaces/User.interface.ts";

export class MovieController {
  private movieProvider?: Provider;

  constructor(movieProvider?: Provider) {
    if (movieProvider) {
      this.movieProvider = movieProvider;
    }
  }

  async search(req: Request): Promise<MovieDocumentsPaginated | BaseError> {
    if (!this.movieProvider) {
      throw new ServerError("No movie provider provided");
    } else {
      try {
        const paginatedMovies = await this.movieProvider.search(
          req,
          WorksTypes.Movie
        );
        return paginatedMovies as MovieDocumentsPaginated;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async add(req: Request): Promise<MovieDocument | BaseError> {
    if (!this.movieProvider) {
      throw new ServerError("No movie provider provided");
    } else {
      try {
        const movie = await this.movieProvider.add(req, WorksTypes.Movie);
        return movie as MovieDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async addToList(req: Request): Promise<MovieDocument | BaseError> {
    if (!this.movieProvider) {
      throw new ServerError("No movie provider provided");
    } else {
      try {
        const listId = req.params.listId;
        const movie = await this.movieProvider.add(
          req,
          WorksTypes.Movie,
          listId
        );
        return movie as MovieDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  // Get user movies by ID
  async get(req: Express.Request) {
    const id = req.params.userId;
    const user = await User.findById(id);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const page = parseInt(req.query.page as string, 10) || 1;
    const limit = parseInt(req.query.limit as string, 10) || 100;
    const sortBy = (req.query.sortBy as string) || "discoveryDate";
    const order = (req.query.order as string) || "desc";
    const query = (req.query.query as string) || null;

    const offset = (page - 1) * limit;

    if (user.movies.length < 1) {
      throw new NoContentError("No movie found for the given query");
    }

    const movieIds = user.movies.map((movieId) => movieId._id);

    const moviesQuery: Record<string, unknown> = {_id: {$in: movieIds}};

    if (req.query.filter && req.query.filter !== "All") {
      moviesQuery.genres = req.query.filter as string;
    }

    if (query) {
      const titleRegex = toRegex()(query);
      moviesQuery.title = {$regex: new RegExp(titleRegex, "i")};
    }

    const movies = await Movie.find(moviesQuery);

    let sortedMovies: MovieDocument[];

    // Sort by rating
    if (sortBy && sortBy === "rating") {
      sortedMovies = movies.sort((a, b) => {
        const aUserData = user.movies.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.movies.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (bUserData?.rating || 0) - (aUserData?.rating || 0);
      });
    } else {
      // Sort by discoveryDate
      sortedMovies = movies.sort((a, b) => {
        const aUserData = user.movies.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.movies.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (
          (bUserData?.discoveryDate as Date).getTime() -
          (aUserData?.discoveryDate as Date).getTime()
        );
      });
    }

    if (order && order === "asc") {
      sortedMovies.reverse();
    }

    const paginatedMovies = sortedMovies.slice(offset, offset + limit);

    const formattedMovies = await Promise.all(
      paginatedMovies.map(async (movie) => {
        const userData = user.movies.find(
          (m) => m._id.toString() === movie._id.toString()
        );

        const formattedMovie = new Movie(movie);
        const formattedMovieObject = formattedMovie.toObject();
        formattedMovieObject.additionDate = userData!.additionDate as Date;
        formattedMovieObject.discoveryDate = userData!.discoveryDate as Date;

        if (userData!.rating) {
          formattedMovieObject.userRating = userData!.rating;
        }

        const lists = await Promise.all(
          user!.lists.map(async (list: List) => {
            const workInList = list.works?.find(
              (work) => work._id.toString() === movie._id.toString()
            );

            if (workInList) {
              return {name: list.name, id: list._id?.toString()};
            }

            return {name: "", id: ""};
          })
        );

        formattedMovieObject.lists = lists.filter((list) => list.name !== "");

        return formattedMovieObject;
      })
    );

    const totalFilteredMovies = movies.length;
    const totalPages = Math.ceil(totalFilteredMovies / limit);

    const pagination = {
      page,
      totalPages,
    };

    return {data: formattedMovies, pagination};
  }
}
