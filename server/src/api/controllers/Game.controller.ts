import {Request} from "express";
import {Provider} from "../../interfaces/Provider.interface.ts";
import {
  BadRequestError,
  BaseError,
  ConflictError,
  NoContentError,
  ServerError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import {
  GameDocument,
  GameDocumentsPaginated,
} from "../../interfaces/Game.interface.ts";
import User from "../models/User.model.ts";
import Game from "../models/Game.model.ts";
import {List, UserDocument} from "../../interfaces/User.interface.ts";
import {toRegex} from "diacritic-regex";

export class GameController {
  private gameProvider?: Provider;

  constructor(gameProvider?: Provider) {
    if (gameProvider) {
      this.gameProvider = gameProvider;
    }
  }

  async search(req: Request): Promise<GameDocumentsPaginated | BaseError> {
    if (!this.gameProvider) {
      throw new ServerError("No game provider provided");
    } else {
      try {
        const paginatedGames = await this.gameProvider.search(req);
        return paginatedGames as GameDocumentsPaginated;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async add(req: Request): Promise<GameDocument | BaseError> {
    if (!this.gameProvider) {
      throw new ServerError("No game provider provided");
    } else {
      try {
        const game = await this.gameProvider.add(req);
        return game as GameDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async addToList(req: Request): Promise<GameDocument | BaseError> {
    if (!this.gameProvider) {
      throw new ServerError("No game provider provided");
    } else {
      try {
        const listId = req.params.listId;
        const game = await this.gameProvider.add(req, listId);
        return game as GameDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async update(req: Request): Promise<GameDocument | BaseError> {
    if (!this.gameProvider) {
      throw new ServerError("No game provider provided");
    } else {
      try {
        const game = await this.gameProvider.update(req);
        return game as GameDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async get(req: Request): Promise<GameDocumentsPaginated | BaseError> {
    const userId = req.params.userId;
    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const page = parseInt(req.query.page as string, 10) || 1;
    const limit = parseInt(req.query.limit as string, 10) || 100;
    const sortBy = (req.query.sortBy as string) || "discoveryDate";
    const order = (req.query.order as string) || "desc";
    const query = (req.query.query as string) || null;

    const offset = (page - 1) * limit;

    if (user.games.length < 1) {
      throw new NoContentError("No game found for the given query");
    }

    const gameIds = user.games.map((gameId) => gameId._id);

    const gamesQuery: Record<string, unknown> = {_id: {$in: gameIds}};

    if (req.query.filter && req.query.filter !== "All") {
      gamesQuery.genres = req.query.filter as string;
    }

    if (query) {
      const titleRegex = toRegex()(query);
      gamesQuery.title = {$regex: new RegExp(titleRegex, "i")};
    }

    const games = await Game.find(gamesQuery);

    const totalFilteredGames = games.length;

    let sortedGames: GameDocument[];

    // Sort by rating
    if (sortBy && sortBy === "rating") {
      sortedGames = games.sort((a, b) => {
        const aUserData = user.games.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.games.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (bUserData?.rating || 0) - (aUserData?.rating || 0);
      });
    } else {
      // Sort by discoveryDate
      sortedGames = games.sort((a, b) => {
        const aUserData = user.games.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.games.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (
          (bUserData?.discoveryDate as Date).getTime() -
          (aUserData?.discoveryDate as Date).getTime()
        );
      });
    }

    if (order && order === "asc") {
      sortedGames.reverse();
    }

    const paginatedGames = sortedGames.slice(offset, offset + limit);

    const formattedGames = await Promise.all(
      paginatedGames.map(async (game) => {
        const userData = user.games.find(
          (m) => m._id.toString() === game._id.toString()
        );

        const formattedGame = new Game(game);
        const formattedGameObject = formattedGame.toObject();
        formattedGameObject.additionDate = userData!.additionDate as Date;
        formattedGameObject.discoveryDate = userData!.discoveryDate as Date;
        formattedGameObject.userDlcs = userData!.dlcs;

        if (userData!.rating) {
          formattedGameObject.userRating = userData!.rating;
        }

        const lists = await Promise.all(
          user!.lists.map(async (list: List) => {
            const workInList = list.works?.find(
              (work) => work._id.toString() === game._id.toString()
            );

            if (workInList) {
              return {name: list.name, id: list._id?.toString()};
            }

            return {name: "", id: ""};
          })
        );

        formattedGameObject.lists = lists.filter((list) => list.name !== "");

        return formattedGameObject;
      })
    );

    const totalPages = Math.ceil(totalFilteredGames / limit);

    const pagination = {
      page,
      totalPages,
    };

    return {data: formattedGames, pagination};
  }

  async updateUserGameDLCs(req: Request): Promise<UserDocument | BaseError> {
    const {userId, gameId} = req.params;

    const {dlcId} = req.body;

    if (!userId || !gameId || !dlcId) {
      throw new BadRequestError("Missing required fields");
    }

    const user = await User.findById(userId);

    if (user) {
      if (user.games && user.games.length > 0) {
        const game = user.games.find(
          (games) => games._id.toString() === gameId
        );

        if (game) {
          if (req.method === "POST") {
            if (!game.dlcs) {
              game.dlcs = [];
            }

            const dlcExists = game.dlcs.some((dlc) => dlc.providerId === dlcId);

            if (!dlcExists) {
              game.dlcs.push({providerId: dlcId});
            } else {
              throw new ConflictError(
                "The specified game DLC has already been added"
              );
            }
          } else if (req.method === "DELETE") {
            if (game.dlcs) {
              game.dlcs = game.dlcs.filter((dlc) => dlc.providerId !== dlcId);
            } else {
              game.dlcs = [];
            }
          }

          const updatedUser = await user.save();

          return updatedUser;
        } else {
          throw new NoContentError("No game found for the given query");
        }
      } else {
        throw new NoContentError("No game found for the given query");
      }
    } else {
      throw new UnprocessableEntityError("No user found for the given query");
    }
  }
}
