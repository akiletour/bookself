import {Server} from "http";
import chai from "chai";
chai.should();
import chaiHttp from "chai-http";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";

import {app} from "../../server.ts";
import mongoose from "mongoose";
import {ObjectId} from "mongodb";
import {MangasProviders} from "../../enums/Manga.enum.ts";
import {getMangaDetails} from "../../utils/providers/mangadex.util.ts";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Mangas API", () => {
  let server: Server;
  let createdUserId: mongoose.ObjectId;
  let createdMangaId: mongoose.ObjectId;
  let createdUserAccessToken: string;
  let createdListId: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["mangas", "users"]);
  });

  describe("Create user tokens", () => {
    before((done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe-duplicate@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
          activated: "true",
        })
        .end((_err, res) => {
          createdUserId = res.body._id;
          done();
        });
    });

    it("should login and generate an accessToken", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe-duplicate@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.tokens.accessToken).to.be.a.string;
          expect(res.body.tokens.twitchAccessToken).to.be.a.string;
          createdUserAccessToken = res.body.tokens.accessToken;
          done();
        });
    });
  });

  describe("Helpers MangaDex functions", () => {
    describe("Get manga details", () => {
      it("should return true if manga does exist", async () => {
        // id 6b958848-c885-4735-9201-12ee77abcb3c : Spy x Family
        const mangaDetails = await getMangaDetails(
          "6b958848-c885-4735-9201-12ee77abcb3c"
        );
        expect(mangaDetails).to.not.equal(false);
      });
    });
  });

  describe("GET /search/mangas/:term?page", () => {
    it("should return a 200 if results", (done) => {
      chai
        .request(app)
        .get("/api/search/mangas/spy?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 204 if no result", (done) => {
      chai
        .request(app)
        .get("/api/search/mangas/thiscantbeanymangaexistingever?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("GET /mangas/user/:id", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .get("/api/mangas/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 204 if no mangas", (done) => {
      chai
        .request(app)
        .get("/api/mangas/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("POST /mangas/user/:id", () => {
    it("should fail adding a manga to user if no data", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdMangaId = res.body._id;
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId)
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("POST /mangas/user/:userId/list/:listId", () => {
    before("should return a newly created list", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .send({
          name: "Test list",
          description: "Description",
        })
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdListId = res.body._id;
          done();
        });
    });

    it("should fail adding a manga to user's list if no data", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + new ObjectId() + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId + "/list/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdMangaId = res.body._id;
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId + "/list/" + createdListId)
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/mangas/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MangasProviders.mangadex,
          providerId: "6b958848-c885-4735-9201-12ee77abcb3c",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("GET /mangas/user/:id", () => {
    it("should return mangas with a 200 if mangas exist", (done) => {
      chai
        .request(app)
        .get("/api/mangas/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/mangas/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("POST /mangas/:mangasId/user/:userId", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .post("/mangas/" + createdMangaId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .post("/mangas/" + createdMangaId + "/user/" + createdUserId)
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .post("/mangas/" + createdMangaId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 204 if no manga", (done) => {
      chai
        .request(app)
        .post("/mangas/" + new ObjectId() + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .post("/mangas/" + createdMangaId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/mangas/" + createdMangaId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("PATCH /mangas", () => {
    it("should return a 422 if manga does not exist", (done) => {
      chai
        .request(app)
        .patch("/api/mangas/" + "111111111111111111111111")
        .send({
          provider: MangasProviders.mangadex,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 200 updating an existing manga", (done) => {
      chai
        .request(app)
        .patch("/api/mangas/" + createdMangaId)
        .send({
          provider: MangasProviders.mangadex,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("DELETE /mangas/:mangasId/user/:userId", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .delete("/api/mangas/" + createdMangaId + "/user/" + new ObjectId())
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .delete("/api/mangas/" + createdMangaId + "/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .delete("/api/mangas/" + createdMangaId + "/user/" + new ObjectId())
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 204 if no mangas", (done) => {
      chai
        .request(app)
        .delete("/api/mangas/" + new ObjectId() + "/user/" + createdUserId)
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .delete("/api/mangas/" + createdMangaId + "/user/" + createdUserId)
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
