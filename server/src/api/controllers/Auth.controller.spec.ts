import {Server} from "http";
import chai from "chai";
import chaiHttp from "chai-http";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";

import {app} from "../../server.ts";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Auth API", () => {
  let server: Server;
  let id: string;
  let token: string;
  let invalidActivationToken: string;
  let activationToken: string;
  let refreshToken: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["users"]);
  });

  describe("POST /signup", () => {
    it("should return a newly created user", (done) => {
      chai
        .request(app)
        .post("/signup")
        .send({
          email: "johndoe@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          invalidActivationToken = res.body.activationToken;
          done();
        });
    });

    it("should fail creating user without data", (done) => {
      chai
        .request(app)
        .post("/signup")
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail creating user with existing email", (done) => {
      chai
        .request(app)
        .post("/signup")
        .send({
          email: "johndoe@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });

    it("should fail creating user with invalid email", (done) => {
      chai
        .request(app)
        .post("/signup")
        .send({
          email: "johndoe",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("POST /login", () => {
    it("should not login a non activated user", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(403);
          done();
        });
    });
  });

  describe("POST /send_activation_email", () => {
    it("should send an account activation email", (done) => {
      chai
        .request(app)
        .post("/send_activation_email")
        .send({
          email: "johndoe@email.com",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          activationToken = res.body.activationToken;
          done();
        });
    });
  });

  describe("POST /activate_account", () => {
    it("should fail activating a user account if no token provided", (done) => {
      chai
        .request(app)
        .post("/activate_account")
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail activating a user account if token is invalid", (done) => {
      chai
        .request(app)
        .post("/activate_account")
        .send({
          activationToken: invalidActivationToken,
        })
        .end((_err, res) => {
          expect(res).to.have.status(498);
          done();
        });
    });

    it("should activate a user account", (done) => {
      chai
        .request(app)
        .post("/activate_account")
        .send({
          activationToken,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("POST /login", () => {
    it("should fail login user without data", (done) => {
      chai
        .request(app)
        .post("/login")
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail login user with incorrect email", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "janedoe@email.com",
          password: "wordpass",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should fail login user with incorrect password", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe@email.com",
          password: "wordpass",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should login an activated user", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res.statusCode).to.be.equal(200);
          refreshToken = res.body.tokens.refreshToken;
          done();
        });
    });
  });

  describe("POST /refresh_token", () => {
    it("should silently login an activated user using his refresh token", (done) => {
      chai
        .request(app)
        .post("/refresh_token")
        .set({Authorization: `Bearer ${refreshToken}`})
        .send({
          email: "johndoe@email.com",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should fail on invalid token", (done) => {
      chai
        .request(app)
        .post("/refresh_token")
        .set({Authorization: `Bearer invalid`})
        .send({
          email: "johndoe@email.com",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("POST /send_reset_password_email", () => {
    it("should send reset password link if user exist", (done) => {
      chai
        .request(app)
        .post("/send_reset_password_email")
        .send({
          email: "johndoe@email.com",
        })
        .end((_err, res) => {
          token = res.body.resetToken;
          id = res.body._id;
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should fail sending reset password link without data", (done) => {
      chai
        .request(app)
        .post("/send_reset_password_email")
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should fail sending reset password link with incorrect email", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "janedoe@email.com",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("POST /verify_reset_token", () => {
    it("should check user token", (done) => {
      chai
        .request(app)
        .post("/verify_reset_token")
        .send({
          resetToken: token,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should fail if no data", (done) => {
      chai
        .request(app)
        .post("/verify_reset_token")
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail if token is invalid", (done) => {
      chai
        .request(app)
        .post("/verify_reset_token")
        .send({
          resetToken: "invalid",
        })
        .end((_err, res) => {
          expect(res).to.have.status(498);
          done();
        });
    });
  });

  describe("POST /reset_password", () => {
    it("should fail if no data", (done) => {
      chai
        .request(app)
        .post("/reset_password")
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail if password do not match", (done) => {
      chai
        .request(app)
        .post("/reset_password")
        .send({
          _id: id,
          resetToken: token,
          password: "any",
          confirm_password: "wrong",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail if token is invalid", (done) => {
      chai
        .request(app)
        .post("/reset_password")
        .send({
          _id: id,
          resetToken: "invalid",
          password: "any",
          confirm_password: "any",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should fail if user is invalid", (done) => {
      chai
        .request(app)
        .post("/reset_password")
        .send({
          _id: "wrong",
          resetToken: token,
          password: "any",
          confirm_password: "any",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should update user password", (done) => {
      chai
        .request(app)
        .post("/reset_password")
        .send({
          _id: id,
          resetToken: token,
          password: "any",
          confirm_password: "any",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
