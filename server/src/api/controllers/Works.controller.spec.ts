import {Server} from "http";
import chai from "chai";
chai.should();
import chaiHttp from "chai-http";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";

import {app} from "../../server.ts";
import mongoose from "mongoose";
import {ObjectId} from "mongodb";
import {MoviesProviders} from "../../enums/Movie.enum.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Works API", () => {
  let server: Server;
  let createdUserId: mongoose.ObjectId;
  let createdWorkId: mongoose.ObjectId;
  let createdUserAccessToken: string;
  let createdUserTwitchAccessToken: string;
  let createdListId: string;
  let createdWorkListId: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["series", "users"]);
  });

  describe("Create and log user to generate tokens", () => {
    before((done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe-duplicate@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
          activated: true,
        })
        .end((_err, res) => {
          createdUserId = res.body._id;
          done();
        });
    });

    it("should login and generate an accessToken", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe-duplicate@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.tokens.accessToken).to.be.a.string;
          expect(res.body.tokens.twitchAccessToken).to.be.a.string;
          createdUserAccessToken = res.body.tokens.accessToken;
          createdUserTwitchAccessToken = res.body.tokens.twitchAccessToken;
          done();
        });
    });
  });

  describe("GET /works/user/:id", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 204 if no works", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("GET /works/user/:id", () => {
    before("should create a test work", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          title: "Series",
          provider: MoviesProviders.tmdb,
          providerId: 1,
          type: WorksTypes.Series,
          releaseDate: "1989-06-21T00:00:00.000Z",
          poster: "path/to/poster.jpg",
          tmdb_genres: [],
          synopsis: "Synospsis",
        })
        .end((_err, res) => {
          createdWorkId = res.body._id;

          expect(res).to.have.status(201);
          done();
        });
    });

    it("should return works with a 200 if works exist", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("GET /works/user/:userId/list/:listId", () => {
    before("should return a newly created list", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .send({
          name: "Test list",
          description: "Description",
        })
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdListId = res.body._id;
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + new ObjectId() + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + createdUserId + "/list/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 204 if no works", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("GET /works/user/:userId/list/:listId", () => {
    before("should add work to list", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId + "/list/" + createdListId)
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdWorkListId = res.body._id;
          done();
        });
    });

    it("should return works with a 200 if works exist", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/works/user/" + createdUserId + "/list/" + createdListId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("PATCH /works/:workId/user/:userId", () => {
    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .patch("/api/works/" + createdWorkId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 400 if no userRating or discoveryDate", (done) => {
      chai
        .request(app)
        .patch("/api/works/" + createdWorkId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          type: "series",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .patch("/api/works/" + createdWorkId + "/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .patch("/api/works/" + createdWorkId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          type: "series",
          date: "Thu Oct 12 2023 00:00:00 GMT+0200 (heure d’été d’Europe centrale)",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no work", (done) => {
      chai
        .request(app)
        .patch("/api/works/" + new ObjectId() + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          type: "series",
          date: "Thu Oct 12 2023 00:00:00 GMT+0200 (heure d’été d’Europe centrale)",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .patch("/api/works/" + createdWorkId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          type: "series",
          date: "Thu Oct 12 2023 00:00:00 GMT+0200 (heure d’été d’Europe centrale)",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("DELETE /works/:workId/:workId/user/:userId/list/:listId", () => {
    it("should return a 422 if no work", (done) => {
      chai
        .request(app)
        .delete(
          "/api/works/" +
            new ObjectId() +
            "/user/" +
            createdUserId +
            "/list/" +
            createdListId
        )
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .delete(
          "/api/works/" +
            createdWorkListId +
            "/user/" +
            new ObjectId() +
            "/list/" +
            createdListId
        )
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .delete(
          "/api/works/" +
            createdWorkListId +
            "/user/" +
            createdUserId +
            "/list/" +
            new ObjectId()
        )
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .delete(
          "/api/works/" +
            createdWorkListId +
            "/user/" +
            createdUserId +
            "/list/" +
            createdListId
        )
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 200 on deletion", (done) => {
      chai
        .request(app)
        .delete(
          "/api/works/" +
            createdWorkListId +
            "/user/" +
            createdUserId +
            "/list/" +
            createdListId
        )
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("DELETE /works/:workType/:workId/user/:userId", () => {
    it("should return a 400 if if workType is not valid", (done) => {
      chai
        .request(app)
        .delete(
          "/api/works/unknown/" + createdWorkId + "/user/" + createdUserId
        )
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 422 if no work", (done) => {
      chai
        .request(app)
        .delete(
          "/api/works/series/" + new ObjectId() + "/user/" + createdUserId
        )
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .delete(
          "/api/works/series/" + createdWorkId + "/user/" + new ObjectId()
        )
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .delete("/api/works/series/" + createdWorkId + "/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 200 on deletion", (done) => {
      chai
        .request(app)
        .delete("/api/works/series/" + createdWorkId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
