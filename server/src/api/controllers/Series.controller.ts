import Express, {Request} from "express";
import Series from "../models/Series.model.ts";
import User from "../models/User.model.ts";
import {
  SeriesDocument,
  SeriesDocumentsPaginated,
} from "../../interfaces/Series.interface.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";
import {Provider} from "../../interfaces/Provider.interface.ts";
import {
  BadRequestError,
  BaseError,
  NoContentError,
  ServerError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import {toRegex} from "diacritic-regex";
import {List} from "../../interfaces/User.interface.ts";
export class SeriesController {
  private seriesProvider?: Provider;

  constructor(seriesProvider?: Provider) {
    if (seriesProvider) {
      this.seriesProvider = seriesProvider;
    }
  }

  async search(req: Request): Promise<SeriesDocumentsPaginated | BaseError> {
    if (!this.seriesProvider) {
      throw new ServerError("No series provider provided");
    } else {
      try {
        const paginatedSeries = await this.seriesProvider.search(
          req,
          WorksTypes.Series
        );
        return paginatedSeries as SeriesDocumentsPaginated;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async add(req: Express.Request) {
    if (!this.seriesProvider) {
      throw new ServerError("No series provider provided");
    } else {
      try {
        const series = await this.seriesProvider.add(req, WorksTypes.Series);
        return series as SeriesDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async addToList(req: Request): Promise<SeriesDocument | BaseError> {
    if (!this.seriesProvider) {
      throw new ServerError("No series provider provided");
    } else {
      try {
        const listId = req.params.listId;
        const series = await this.seriesProvider.add(
          req,
          WorksTypes.Series,
          listId
        );
        return series as SeriesDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async update(req: Express.Request) {
    if (!this.seriesProvider) {
      throw new ServerError("No series provider provided");
    } else {
      try {
        const series = await this.seriesProvider.update(req);
        return series as SeriesDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  // Get user Series by ID
  async get(req: Express.Request) {
    const id = req.params.userId;
    const user = await User.findById(id);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const page = parseInt(req.query.page as string, 10) || 1;
    const limit = parseInt(req.query.limit as string, 10) || 100;
    const sortBy = (req.query.sortBy as string) || "discoveryDate";
    const order = (req.query.order as string) || "desc";
    const query = (req.query.query as string) || null;

    const offset = (page - 1) * limit;

    if (user.series.length < 1) {
      throw new NoContentError("No series found for the given query");
    }

    const seriesIds = user.series.map((seriesId) => seriesId._id);

    const seriesQuery: Record<string, unknown> = {_id: {$in: seriesIds}};

    if (req.query.filter && req.query.filter !== "All") {
      seriesQuery.genres = req.query.filter as string;
    }

    if (query) {
      const titleRegex = toRegex()(query);
      seriesQuery.title = {$regex: new RegExp(titleRegex, "i")};
    }

    const series = await Series.find(seriesQuery);

    let sortedSeries: SeriesDocument[];

    // Sort by rating
    if (sortBy && sortBy === "rating") {
      sortedSeries = series.sort((a, b) => {
        const aUserData = user.series.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.series.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (bUserData?.rating || 0) - (aUserData?.rating || 0);
      });
    } else {
      // Sort by discoveryDate
      sortedSeries = series.sort((a, b) => {
        const aUserData = user.series.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.series.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (
          (bUserData?.discoveryDate as Date).getTime() -
          (aUserData?.discoveryDate as Date).getTime()
        );
      });
    }

    if (order && order === "asc") {
      sortedSeries.reverse();
    }

    const paginatedSeries = sortedSeries.slice(offset, offset + limit);

    const allFormattedSeries = await Promise.all(
      paginatedSeries.map(async (series) => {
        const userData = user.series.find(
          (m) => m._id.toString() === series._id.toString()
        );

        const formattedSeries = new Series(series);
        const formattedSeriesObject = formattedSeries.toObject();
        formattedSeriesObject.additionDate = userData!.additionDate as Date;
        formattedSeriesObject.discoveryDate = userData!.discoveryDate as Date;
        formattedSeriesObject.progress = userData!.progress;

        if (userData!.rating) {
          formattedSeriesObject.userRating = userData!.rating;
        }

        const lists = await Promise.all(
          user!.lists.map(async (list: List) => {
            const workInList = list.works?.find(
              (work) => work._id.toString() === series._id.toString()
            );

            if (workInList) {
              return {name: list.name, id: list._id?.toString()};
            }

            return {name: "", id: ""};
          })
        );

        formattedSeriesObject.lists = lists.filter((list) => list.name !== "");

        return formattedSeriesObject;
      })
    );

    const totalFilteredSeries = series.length;
    const totalPages = Math.ceil(totalFilteredSeries / limit);

    const pagination = {
      page,
      totalPages,
    };

    return {data: allFormattedSeries, pagination};
  }

  // Update user Series progress by ID
  async updateUserSeries(req: Express.Request) {
    const {userId, seriesId} = req.params;

    const {season, episode, completed} = req.body;

    if (!userId || !seriesId || !season || !episode) {
      throw new BadRequestError("Missing required fields");
    }

    const user = await User.findById(userId);

    if (user) {
      if (user.series && user.series.length > 0) {
        const series = user.series.find(
          (series) => series._id.toString() === seriesId
        );

        if (series) {
          series.progress = {
            season,
            episode,
            completed,
          };

          const updatedUser = await user.save();

          return updatedUser;
        } else {
          throw new UnprocessableEntityError(
            "No series found for the given query"
          );
        }
      } else {
        throw new UnprocessableEntityError(
          "No series found for the given query"
        );
      }
    } else {
      throw new UnprocessableEntityError("No user found for the given query");
    }
  }
}
