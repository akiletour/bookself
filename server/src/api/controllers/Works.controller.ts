import Express from "express";
import User from "../models/User.model.ts";
import Game from "../models/Game.model.ts";
import Movie from "../models/Movie.model.ts";
import Series from "../models/Series.model.ts";
import Manga from "../models/Manga.model.ts";
import Comic from "../models/Comic.model.ts";
import {List, UserDocument} from "../../interfaces/User.interface.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";
import {
  BadRequestError,
  NoContentError,
  ServerError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import {WorksModel} from "../../types/WorksModel.type.ts";
import {GameDocument} from "../../interfaces/Game.interface.ts";
import {SeriesDocument} from "../../interfaces/Series.interface.ts";
import {MangaDocument} from "../../interfaces/Manga.interface.ts";
import {ComicDocument} from "../../interfaces/Comic.interface.ts";
import {toRegex} from "diacritic-regex";

export class WorkController {
  async get(req: Express.Request) {
    const id = req.params.userId;
    const user = await User.findById(id);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const page = parseInt(req.query.page as string, 10) || 1;
    const limit = parseInt(req.query.limit as string, 10) || 100;
    const sortBy = (req.query.sortBy as string) || "discoveryDate";
    const order = (req.query.order as string) || "desc";
    const query = (req.query.query as string) || null;

    const offset = (page - 1) * limit;

    const allWorks = [];

    for (const workType of Object.values(WorksTypes)) {
      const workIds = user[workType].map((workId) => workId._id);

      const workQuery: Record<string, unknown> = {_id: {$in: workIds}};

      if (req.query.filter && req.query.filter !== "All") {
        workQuery.genres = req.query.filter as string;
      }

      if (query) {
        const titleRegex = toRegex()(query);
        workQuery.title = {$regex: new RegExp(titleRegex, "i")};
      }

      let works: WorksModel[];

      switch (workType) {
        case WorksTypes.Movie:
          works = await Movie.find(workQuery);
          break;
        case WorksTypes.Series:
          works = await Series.find(workQuery);
          break;
        case WorksTypes.Game:
          works = await Game.find(workQuery);
          break;
        case WorksTypes.Manga:
          works = await Manga.find(workQuery);
          break;
        case WorksTypes.Comic:
          works = await Comic.find(workQuery);
          break;
        default:
          throw new Error(`Invalid workType: ${workType}`);
      }

      allWorks.push(...works);
    }

    if (allWorks.length < 1) {
      throw new NoContentError("No works found for the given query");
    }

    const formattedWorks = await Promise.all(
      allWorks.map(async (work) => {
        let formattedWork: WorksModel;
        let formattedWorkObject;
        let lists;

        const userData = user[work.type].find(
          (m) => m._id.toString() === work._id.toString()
        );

        switch (work.type) {
          case WorksTypes.Movie:
            formattedWork = new Movie(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = userData!.additionDate as Date;
            formattedWorkObject.discoveryDate = userData!.discoveryDate as Date;

            if (userData!.rating) {
              formattedWorkObject.userRating = userData!.rating;
            }

            lists = await this.getWorkLists(user, work);
            formattedWorkObject.lists = lists;

            return formattedWorkObject;

          case WorksTypes.Game:
            formattedWork = new Game(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = userData!.additionDate as Date;
            formattedWorkObject.discoveryDate = userData!.discoveryDate as Date;

            if (userData!.rating) {
              formattedWorkObject.userRating = userData!.rating;
            }
            if ((userData as GameDocument).dlcs) {
              formattedWorkObject.userDlcs = (userData as GameDocument)!.dlcs;
            }

            lists = await this.getWorkLists(user, work);
            formattedWorkObject.lists = lists;

            return formattedWorkObject;

          case WorksTypes.Manga:
            formattedWork = new Manga(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = userData!.additionDate as Date;
            formattedWorkObject.discoveryDate = userData!.discoveryDate as Date;

            if (userData!.rating) {
              formattedWorkObject.userRating = userData!.rating;
            }
            if ((userData as MangaDocument).progress) {
              formattedWorkObject.progress =
                (userData as MangaDocument)!.progress;
            }

            lists = await this.getWorkLists(user, work);
            formattedWorkObject.lists = lists;

            return formattedWorkObject;

          case WorksTypes.Comic:
            formattedWork = new Comic(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = userData!.additionDate as Date;
            formattedWorkObject.discoveryDate = userData!.discoveryDate as Date;

            if (userData!.rating) {
              formattedWorkObject.userRating = userData!.rating;
            }
            if ((userData as ComicDocument).progress) {
              formattedWorkObject.progress =
                (userData as ComicDocument)!.progress;
            }

            lists = await this.getWorkLists(user, work);
            formattedWorkObject.lists = lists;

            return formattedWorkObject;

          case WorksTypes.Series:
            formattedWork = new Series(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = userData!.additionDate as Date;
            formattedWorkObject.discoveryDate = userData!.discoveryDate as Date;

            if (userData!.rating) {
              formattedWorkObject.userRating = userData!.rating;
            }
            if ((userData as SeriesDocument).progress) {
              formattedWorkObject.progress =
                (userData as SeriesDocument)!.progress;
            }

            lists = await this.getWorkLists(user, work);
            formattedWorkObject.lists = lists;

            return formattedWorkObject;

          default:
            throw new ServerError("Missing required fields");
        }
      })
    );

    let sortedWorks: WorksModel[];

    // Sort by rating
    if (sortBy && sortBy === "rating") {
      sortedWorks = formattedWorks.sort((a, b) => {
        return (b?.userRating || 0) - (a?.userRating || 0);
      });
    } else {
      // Sort by discoveryDate
      sortedWorks = formattedWorks.sort((a, b) => {
        return (
          (b?.discoveryDate as Date).getTime() -
          (a?.discoveryDate as Date).getTime()
        );
      });
    }

    if (order === "asc") {
      sortedWorks.reverse();
    }

    const paginatedWorks = sortedWorks.slice(offset, offset + limit);

    const totalFilteredWorks = allWorks.length;
    const totalPages = Math.ceil(totalFilteredWorks / limit);

    const pagination = {
      page,
      totalPages,
    };

    return {data: paginatedWorks, pagination};
  }

  async getWorkLists(user: UserDocument, userWork: WorksModel) {
    const lists = await Promise.all(
      user!.lists.map(async (list: List) => {
        const workInList = list.works?.find(
          (work) => work._id.toString() === userWork._id.toString()
        );

        if (workInList) {
          return {name: list.name, id: list._id?.toString()};
        }

        return {name: "", id: ""};
      })
    );

    return lists.filter((list) => list.name !== "");
  }

  async getFromList(req: Express.Request) {
    const id = req.params.userId;
    const user = await User.findById(id);
    const listId = req.params.listId;

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const list: List | undefined = user.lists?.find(
      (item) => item?._id!.toString() === listId
    );
    if (!(user.lists && user.lists.length) || !list) {
      throw new UnprocessableEntityError("No list found for the given query");
    }

    const page = parseInt(req.query.page as string, 10) || 1;
    const limit = parseInt(req.query.limit as string, 10) || 100;

    const offset = (page - 1) * limit;

    const allWorks = [];

    for (const workType of Object.values(WorksTypes)) {
      const workIds = list.works?.map((workId) => workId._id);

      const workQuery: Record<string, unknown> = {_id: {$in: workIds}};

      let works: WorksModel[];

      switch (workType) {
        case WorksTypes.Movie:
          works = await Movie.find(workQuery);
          break;
        case WorksTypes.Series:
          works = await Series.find(workQuery);
          break;
        case WorksTypes.Game:
          works = await Game.find(workQuery);
          break;
        case WorksTypes.Manga:
          works = await Manga.find(workQuery);
          break;
        case WorksTypes.Comic:
          works = await Comic.find(workQuery);
          break;
        default:
          throw new Error(`Invalid workType: ${workType}`);
      }

      allWorks.push(...works);
    }

    if (allWorks.length < 1) {
      throw new NoContentError("No works found for the given query");
    }

    const formattedWorks = await Promise.all(
      allWorks.map(async (work) => {
        let formattedWork: WorksModel;
        let formattedWorkObject;
        let lists;

        const userData = user[work.type].find(
          (m) => m._id.toString() === work._id.toString()
        );
        const workData = list.works?.find(
          (m) => m._id.toString() === work._id.toString()
        );

        switch (work.type) {
          case WorksTypes.Movie:
            formattedWork = new Movie(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = workData!.additionDate as Date;

            if (userData) {
              formattedWorkObject.discoveryDate = userData!
                .discoveryDate as Date;
              if (userData!.rating) {
                formattedWorkObject.userRating = userData!.rating;
              }

              lists = await this.getWorkLists(user, work);
              formattedWorkObject.lists = lists;
            }
            return formattedWorkObject;

          case WorksTypes.Game:
            formattedWork = new Game(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = workData!.additionDate as Date;

            if (userData) {
              formattedWorkObject.discoveryDate = userData!
                .discoveryDate as Date;
              if (userData!.rating) {
                formattedWorkObject.userRating = userData!.rating;
              }
              if ((userData as GameDocument).userDlcs) {
                (formattedWorkObject as GameDocument).userDlcs = (
                  userData as GameDocument
                ).userDlcs;
              }

              lists = await this.getWorkLists(user, work);
              formattedWorkObject.lists = lists;
            }
            return formattedWorkObject;

          case WorksTypes.Manga:
            formattedWork = new Manga(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = workData!.additionDate as Date;

            if (userData) {
              formattedWorkObject.discoveryDate = userData!
                .discoveryDate as Date;
              if (userData!.rating) {
                formattedWorkObject.userRating = userData!.rating;
              }
              if ((userData as MangaDocument).progress) {
                (formattedWorkObject as MangaDocument).progress = (
                  userData as MangaDocument
                ).progress;
              }

              lists = await this.getWorkLists(user, work);
              formattedWorkObject.lists = lists;
            }
            return formattedWorkObject;

          case WorksTypes.Comic:
            formattedWork = new Comic(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = workData!.additionDate as Date;

            if (userData) {
              formattedWorkObject.discoveryDate = userData!
                .discoveryDate as Date;
              if (userData!.rating) {
                formattedWorkObject.userRating = userData!.rating;
              }
              if ((userData as ComicDocument).progress) {
                (formattedWorkObject as ComicDocument).progress = (
                  userData as ComicDocument
                ).progress;
              }

              lists = await this.getWorkLists(user, work);
              formattedWorkObject.lists = lists;
            }
            return formattedWorkObject;

          case WorksTypes.Series:
            formattedWork = new Series(work);
            formattedWorkObject = formattedWork.toObject();
            formattedWorkObject.additionDate = workData!.additionDate as Date;

            if (userData) {
              formattedWorkObject.discoveryDate = userData!
                .discoveryDate as Date;
              if (userData!.rating) {
                formattedWorkObject.userRating = userData!.rating;
              }
              if ((userData as SeriesDocument).progress) {
                (formattedWorkObject as SeriesDocument).progress = (
                  userData as SeriesDocument
                ).progress;
              }

              lists = await this.getWorkLists(user, work);
              formattedWorkObject.lists = lists;
            }
            return formattedWorkObject;

          default:
            throw new ServerError("Missing required fields");
        }
      })
    );

    const sortedWorks: WorksModel[] = formattedWorks.sort((a, b) => {
      return (
        (b?.additionDate as Date).getTime() -
        (a?.additionDate as Date).getTime()
      );
    });

    const paginatedWorks = sortedWorks.slice(offset, offset + limit);

    const totalFilteredWorks = allWorks.length;
    const totalPages = Math.ceil(totalFilteredWorks / limit);

    const pagination = {
      page,
      totalPages,
    };

    return {data: paginatedWorks, pagination};
  }

  async updateUserWorkDate(req: Express.Request) {
    const {userId, workId} = req.params;
    const workType: WorksTypes = req.body.type;
    const date: string = req.body.date;
    const userRating: number = req.body.userRating;

    if (!userId || !workId || !workType || (!date && !userRating)) {
      throw new BadRequestError("Missing required fields");
    }

    const user: UserDocument | null = await User.findById(userId);

    if (user) {
      if (user[workType] && user[workType].length > 0) {
        const work = user[workType].find(
          (item) => item._id.toString() === workId
        );

        if (work) {
          if (date) {
            work.discoveryDate = new Date(date);
          }

          if (userRating) {
            work.rating = userRating;
          }

          const updatedUser = await user.save();
          return updatedUser;
        } else {
          throw new UnprocessableEntityError(
            "No work found for the given query"
          );
        }
      } else {
        throw new UnprocessableEntityError("No work found for the given query");
      }
    } else {
      throw new UnprocessableEntityError("No user found for the given query");
    }
  }

  async deleteUserWorkById(req: Express.Request) {
    const {userId, workId, workType} = req.params;

    if (!Object.values(WorksTypes).includes(workType as WorksTypes)) {
      throw new BadRequestError("Invalid type");
    }

    const type: WorksTypes = workType as WorksTypes;
    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const work = user[type]?.find((item) => item._id.toString() === workId);

    if (!work) {
      throw new UnprocessableEntityError("No work found for the given query");
    }

    const index = user[type]?.findIndex(
      (item) => item._id.toString() === workId
    );
    if (index !== undefined && index !== -1) {
      user[type]?.splice(index, 1);
      const updatedUser = await user.save();
      return updatedUser;
    }
    return user;
  }

  async deleteUserWorkFromList(req: Express.Request) {
    const {userId, listId, workId} = req.params;
    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const list: List | undefined = user.lists?.find(
      (item) => item?._id!.toString() === listId
    );
    if (!(user.lists && user.lists.length) || !list) {
      throw new UnprocessableEntityError("No list found for the given query");
    }

    const work = list.works?.find((item) => item._id.toString() === workId);

    if (!work) {
      throw new UnprocessableEntityError("No work found for the given query");
    }

    const index = list.works?.findIndex(
      (item) => item._id.toString() === workId
    );

    if (index !== undefined && index !== -1) {
      list.works?.splice(index, 1);
      const updatedUser = await user.save();
      return updatedUser;
    }
    return user;
  }
}
