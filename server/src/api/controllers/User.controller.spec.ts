import {Server} from "http";
import chai from "chai";
chai.should();
import chaiHttp from "chai-http";
import mongoose from "mongoose";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";
import fs from "fs";
import * as utils from "../../utils/helpers.util.ts";

import {app} from "../../server.ts";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Users API", () => {
  let server: Server;
  let createdUserId: mongoose.ObjectId;
  let file: Buffer;
  let createdUserAccessToken: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["users"]);
  });

  describe("POST /users", () => {
    it("should return a newly created user", (done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe@email.com",
          firstname: "john",
          lastname: "doe",
          username: "jdoe",
          password: "password",
          confirm_password: "password",
          activated: true,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdUserId = res.body._id;
          done();
        });
    });

    it("should return a newly created user by generating a username if not provided", (done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe2@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          done();
        });
    });

    it("should fail creating user without password", (done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe@email.com",
          firstname: "john",
          lastname: "doe",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail if passwords don't match", (done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "wordpass",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail creating user without data", (done) => {
      chai
        .request(app)
        .post("/users")
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail creating user with invalid email", (done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail creating user with existing email", (done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("Login user to generate tokens", () => {
    it("should login and generate an accessToken", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.tokens.accessToken).to.be.a.string;
          createdUserAccessToken = res.body.tokens.accessToken;
          done();
        });
    });
  });

  describe("GET /users", () => {
    it("should return an array of users", (done) => {
      chai
        .request(app)
        .get("/api/users")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/users")
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("GET /users/:userId", () => {
    it("should return a specific user", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .get("/api/users/" + userId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 422 if user does not exist", (done) => {
      const userId = "111111111111111111111111";
      chai
        .request(app)
        .get("/api/users/" + userId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .get("/api/users/" + userId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("GET /users/:userId/activity", () => {
    it("should return a specific user activity", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .get("/api/users/" + userId + "/activity")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 422 if user does not exist", (done) => {
      const userId = "111111111111111111111111";
      chai
        .request(app)
        .get("/api/users/" + userId + "/activity")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .get("/api/users/" + userId + "/activity")
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("PATCH /users", () => {
    before((done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe-duplicate@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
        })
        .end(() => {
          done();
        });
    });

    it("should update an existing user", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .patch("/api/users/" + userId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          email: "johndoe-updated@email.com",
          username: "johndoe-updated",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should fail updating user if no token provided", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .patch("/api/users/" + userId)
        .send({
          email: "johndoe-updated@email.com",
          username: "johndoe-updated",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should fail updating user it it does not exist", (done) => {
      const userId = "111111111111111111111111";
      chai
        .request(app)
        .patch("/api/users/" + userId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          email: "johndoe-updated@email.com",
          username: "johndoe-updated",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should fail updating user with an already existing email", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .patch("/api/users/" + userId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          email: "johndoe-duplicate@email.com",
          username: "johndoe-updated",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("POST /users/:userId/password", () => {
    it("should update an existing user password", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .post("/users/" + userId + "/password")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          old_password: "password",
          new_password: "updated_password",
          confirm_new_password: "updated_password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should fail updating user password if no token provided", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .post("/users/" + userId + "/password")
        .send({
          old_password: "password",
          new_password: "updated_password",
          confirm_new_password: "updated_password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should fail updating an existing user password without datas", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .post("/users/" + userId + "/password")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail updating user password if user does not exist", (done) => {
      const userId = "111111111111111111111111";
      chai
        .request(app)
        .post("/users/" + userId + "/password")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          old_password: "password",
          new_password: "updated_password",
          confirm_new_password: "updated_password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should fail updating user password if actual password is invalid", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .post("/users/" + userId + "/password")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          old_password: "wrong_password",
          new_password: "updated_password",
          confirm_new_password: "updated_password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should fail updating user password if new passwords does not match", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .post("/users/" + userId + "/password")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          old_password: "password",
          new_password: "updated_password",
          confirm_new_password: "updated_wrong_password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("POST /api/users/:userId/picture", () => {
    before((done) => {
      file = fs.readFileSync(utils.__dirname + "../images/testImage.png");
      done();
    });

    it("should update user profile picture", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .post("/users/" + userId + "/picture")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .attach("file", file, "test")
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should fail updating user profile picture if no token provided", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .post("/users/" + userId + "/picture")
        .attach("file", file, "test")
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should fail if user does not exist", (done) => {
      const userId = "111111111111111111111111";
      chai
        .request(app)
        .post("/users/" + userId + "/picture")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .attach("file", file, "test")
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should fail if no image provided", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .post("/users/" + userId + "/picture")
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("DELETE /users", () => {
    it("should fail deleting an existing user if no token provided", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .delete("/api/users/" + userId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should delete an existing user", (done) => {
      const userId = createdUserId;
      chai
        .request(app)
        .delete("/api/users/" + userId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should fail deleting user if it does not exist", (done) => {
      const userId = "111111111111111111111111";
      chai
        .request(app)
        .delete("/api/users/" + userId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });
  });
});
