import {Server} from "http";
import chai from "chai";
chai.should();
import chaiHttp from "chai-http";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";

import {app} from "../../server.ts";
import mongoose from "mongoose";
import {ObjectId} from "mongodb";
import {getSeriesCredits, getDetails} from "../../utils/providers/tmdb.util.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";
import {MoviesProviders} from "../../enums/Movie.enum.ts";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Series API", () => {
  let server: Server;
  let createdUserId: mongoose.ObjectId;
  let createdSeriesId: mongoose.ObjectId;
  let createdUserAccessToken: string;
  let createdListId: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["series", "users"]);
  });

  describe("Helpers TMDB functions", () => {
    describe("Get Series details", () => {
      it("should return false if Series does not exist", async () => {
        // id 46648 : True Detective
        const SeriesDetails = await getDetails("268", WorksTypes.Series);
        expect(SeriesDetails).to.not.equal(false);
      });

      it("should return false if Series does not exist", async () => {
        const SeriesDetails = await getDetails(
          "0000000000000000000",
          WorksTypes.Series
        );
        expect(SeriesDetails).to.equal(false);
      });
    });

    describe("Get Series credits", () => {
      it("should return false if Series does not exist", async () => {
        // id 46648 : True Detective
        const SeriesCredits = await getSeriesCredits("268");
        expect(SeriesCredits).to.not.equal(false);
      });

      it("should return false if Series does not exist", async () => {
        const SeriesCredits = await getSeriesCredits("0000000000000000000");
        expect(SeriesCredits).to.equal(false);
      });
    });
  });

  describe("Create user to generate tokens", () => {
    before((done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe-duplicate@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
          activated: "true",
        })
        .end((_err, res) => {
          createdUserId = res.body._id;
          done();
        });
    });

    it("should login and generate an accessToken", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe-duplicate@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.tokens.accessToken).to.be.a.string;
          createdUserAccessToken = res.body.tokens.accessToken;
          done();
        });
    });
  });

  describe("GET /search/series/:term?page", () => {
    it("should return a 200 if results", (done) => {
      chai
        .request(app)
        .get("/api/search/series/batman?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 204 if no result", (done) => {
      chai
        .request(app)
        .get("/api/search/series/thiscantbeanyseriesexistingever?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("GET /series/user/:id", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .get("/api/series/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 204 if no series", (done) => {
      chai
        .request(app)
        .get("/api/series/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("POST /series/user/:id", () => {
    it("should fail adding a Series to user if no data", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/series/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId)
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          createdSeriesId = res.body._id;

          expect(res).to.have.status(201);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("POST /series/user/:userId/list/:listId", () => {
    before("should return a newly created list", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .send({
          name: "Test list",
          description: "Description",
        })
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdListId = res.body._id;
          done();
        });
    });

    it("should fail adding a series to user's list if no data", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/series/user/" + new ObjectId() + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId + "/list/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId + "/list/" + createdListId)
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/series/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "1621",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("GET /series/user/:id", () => {
    it("should return Series with a 200 if Series exist", (done) => {
      chai
        .request(app)
        .get("/api/series/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/series/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("PATCH /series/:seriesId/user/:userId", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .patch("/api/series/" + createdSeriesId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          season: 1,
          episode: 1,
          completed: false,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .patch("/api/series/" + createdSeriesId + "/user/" + createdUserId)
        .send({
          season: 1,
          episode: 1,
          completed: false,
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .patch("/api/series/" + createdSeriesId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 422 if no series", (done) => {
      chai
        .request(app)
        .patch("/api/series/" + new ObjectId() + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          season: 1,
          episode: 1,
          completed: false,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .patch("/api/series/" + createdSeriesId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          season: 1,
          episode: 1,
          completed: false,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("PATCH /series", () => {
    it("should return a 422 if series does not exist", (done) => {
      chai
        .request(app)
        .patch("/api/series/" + "111111111111111111111111")
        .send({
          provider: MoviesProviders.tmdb,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 200 updating an existing series", (done) => {
      chai
        .request(app)
        .patch("/api/series/" + createdSeriesId)
        .send({
          provider: MoviesProviders.tmdb,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
