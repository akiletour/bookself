import Express, {Request} from "express";
import Manga from "../models/Manga.model.ts";
import User from "../models/User.model.ts";
import {
  MangaDocument,
  MangaDocumentsPaginated,
} from "../../interfaces/Manga.interface.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";
import {Provider} from "../../interfaces/Provider.interface.ts";
import {
  BadRequestError,
  BaseError,
  ConflictError,
  NoContentError,
  ServerError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import {List, UserDocument} from "../../interfaces/User.interface.ts";
import {toRegex} from "diacritic-regex";

export class MangaController {
  private mangaProvider?;

  constructor(mangaProvider?: Provider) {
    if (mangaProvider) {
      this.mangaProvider = mangaProvider;
    }
  }

  async search(req: Request): Promise<MangaDocumentsPaginated | BaseError> {
    if (!this.mangaProvider) {
      throw new ServerError("No manga provider provided");
    } else {
      try {
        const paginatedMangas = await this.mangaProvider.search(
          req,
          WorksTypes.Manga
        );
        return paginatedMangas as MangaDocumentsPaginated;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async add(req: Request): Promise<MangaDocument | BaseError> {
    if (!this.mangaProvider) {
      throw new ServerError("No manga provider provided");
    } else {
      try {
        const manga = await this.mangaProvider.add(req);
        return manga as MangaDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async addToList(req: Request): Promise<MangaDocument | BaseError> {
    if (!this.mangaProvider) {
      throw new ServerError("No manga provider provided");
    } else {
      try {
        const listId = req.params.listId;
        const manga = await this.mangaProvider.add(req, listId);
        return manga as MangaDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async update(req: Request): Promise<MangaDocument | BaseError> {
    if (!this.mangaProvider) {
      throw new ServerError("No manga provider provided");
    } else {
      try {
        const manga = await this.mangaProvider.update(req);
        return manga as MangaDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  // Get user mangas by ID
  async get(req: Express.Request) {
    const id = req.params.userId;
    const user = await User.findById(id);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const page = parseInt(req.query.page as string, 10) || 1;
    const limit = parseInt(req.query.limit as string, 10) || 100;
    const sortBy = (req.query.sortBy as string) || "discoveryDate";
    const order = (req.query.order as string) || "desc";
    const query = (req.query.query as string) || null;

    const offset = (page - 1) * limit;

    if (user.mangas.length < 1) {
      throw new NoContentError("No manga found for the given query");
    }

    const mangaIds = user.mangas.map((mangaId) => mangaId._id);

    const mangasQuery: Record<string, unknown> = {_id: {$in: mangaIds}};

    if (req.query.filter && req.query.filter !== "All") {
      mangasQuery.genres = req.query.filter as string;
    }

    if (query) {
      const titleRegex = toRegex()(query);
      mangasQuery.title = {$regex: new RegExp(titleRegex, "i")};
    }

    const mangas = await Manga.find(mangasQuery);

    let sortedMangas: MangaDocument[];

    // Sort by rating
    if (sortBy && sortBy === "rating") {
      sortedMangas = mangas.sort((a, b) => {
        const aUserData = user.mangas.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.mangas.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (bUserData?.rating || 0) - (aUserData?.rating || 0);
      });
    } else {
      // Sort by discoveryDate
      sortedMangas = mangas.sort((a, b) => {
        const aUserData = user.mangas.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.mangas.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (
          (bUserData?.discoveryDate as Date).getTime() -
          (aUserData?.discoveryDate as Date).getTime()
        );
      });
    }

    if (order && order === "asc") {
      sortedMangas.reverse();
    }

    const paginatedMangas = sortedMangas.slice(offset, offset + limit);

    const formattedMangas = await Promise.all(
      paginatedMangas.map(async (manga) => {
        const userData = user.mangas.find(
          (m) => m._id.toString() === manga._id.toString()
        );

        const formattedManga = new Manga(manga);
        const formattedMangaObject = formattedManga.toObject();
        formattedMangaObject.additionDate = userData!.additionDate as Date;
        formattedMangaObject.discoveryDate = userData!.discoveryDate as Date;
        formattedMangaObject.progress = userData!.progress;

        if (userData!.rating) {
          formattedMangaObject.userRating = userData!.rating;
        }

        const lists = await Promise.all(
          user!.lists.map(async (list: List) => {
            const workInList = list.works?.find(
              (work) => work._id.toString() === manga._id.toString()
            );

            if (workInList) {
              return {name: list.name, id: list._id?.toString()};
            }

            return {name: "", id: ""};
          })
        );

        formattedMangaObject.lists = lists.filter((list) => list.name !== "");

        return formattedMangaObject;
      })
    );

    const totalFilteredMangas = mangas.length;
    const totalPages = Math.ceil(totalFilteredMangas / limit);

    const pagination = {
      page,
      totalPages,
    };

    return {data: formattedMangas, pagination};
  }

  async updateUserMangaProgress(
    req: Request
  ): Promise<UserDocument | BaseError> {
    const {userId, mangaId} = req.params;

    const {volumes} = req.body;

    if (!userId || !mangaId || !volumes) {
      throw new BadRequestError("Missing required fields");
    }

    const user = await User.findById(userId);

    if (user) {
      if (user.mangas && user.mangas.length > 0) {
        const manga = user.mangas.find(
          (mangas) => mangas._id.toString() === mangaId
        );
        const mangaDetails = await Manga.findById(mangaId);

        if (manga) {
          if (req.method === "POST") {
            if (!manga.progress) {
              manga.progress = [];
            }

            if (volumes.length === mangaDetails?.volumes?.length) {
              manga.progress = [];
              volumes.forEach((volumeNumber: number) => {
                manga.progress?.push({volume: volumeNumber});
              });
            } else {
              volumes.forEach((volumeNumber: number) => {
                const volumeExists = manga.progress?.some(
                  (progress) => progress.volume === volumeNumber
                );

                if (!volumeExists) {
                  manga.progress?.push({volume: volumeNumber});
                } else {
                  throw new ConflictError(
                    `The specified manga volume ${volumeNumber} has already been added`
                  );
                }
              });
            }
          } else if (req.method === "DELETE") {
            if (manga.progress) {
              if (volumes.length === 0) {
                manga.progress = [];
              } else {
                manga.progress = manga.progress.filter(
                  (progress) => !volumes.includes(progress.volume)
                );
              }
            } else {
              manga.progress = [];
            }
          }

          const updatedUser = await user.save();

          return updatedUser;
        } else {
          throw new NoContentError("No manga found for the given query");
        }
      } else {
        throw new NoContentError("No manga found for the given query");
      }
    } else {
      throw new UnprocessableEntityError("No user found for the given query");
    }
  }
}
