import {Server} from "http";
import chai from "chai";
chai.should();
import chaiHttp from "chai-http";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";

import {app} from "../../server.ts";
import mongoose from "mongoose";
import {ObjectId} from "mongodb";
import {
  getGameCompanyLogo,
  getGameDetails,
} from "../../utils/providers/igdb.util.ts";
import {GamesProviders} from "../../enums/Game.enum.ts";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Games API", () => {
  let server: Server;
  let createdUserId: mongoose.ObjectId;
  let createdGameId: mongoose.ObjectId;
  let createdUserAccessToken: string;
  let createdUserTwitchAccessToken: string;
  let createdListId: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["games", "users"]);
  });

  describe("Create user to generate twitchAccessToken", () => {
    before((done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe-duplicate@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
          activated: "true",
        })
        .end((_err, res) => {
          createdUserId = res.body._id;
          done();
        });
    });

    it("should login and generate a twitchAccessToken", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe-duplicate@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.tokens.accessToken).to.be.a.string;
          expect(res.body.tokens.twitchAccessToken).to.be.a.string;
          createdUserAccessToken = res.body.tokens.accessToken;
          createdUserTwitchAccessToken = res.body.tokens.twitchAccessToken;
          done();
        });
    });
  });

  describe("Helpers IGDB functions", () => {
    describe("Get game details", () => {
      it("should return true if game does exist", async () => {
        // id 114009 : db kakarot (for both dlcs & expansions)
        const gameDetails = await getGameDetails(
          "114009",
          `Bearer ${createdUserTwitchAccessToken}`
        );
        expect(gameDetails).to.not.equal(false);
      });

      it("should return empty array if game does not exist", async () => {
        const gameDetails = await getGameDetails(
          "0",
          `Bearer ${createdUserTwitchAccessToken}`
        );
        expect(gameDetails).to.be.an("array").that.is.empty;
      });
    });

    describe("Get game company logo", () => {
      it("should return true if company does exist", async () => {
        // id '21' :  irrational games
        const gameCompanyLogo = await getGameCompanyLogo(
          "21",
          `Bearer ${createdUserTwitchAccessToken}`
        );
        expect(gameCompanyLogo).to.not.equal(false);
      });

      it("should return empty array if company does not exist", async () => {
        const gameCompanyLogo = await getGameCompanyLogo(
          "0",
          `Bearer ${createdUserTwitchAccessToken}`
        );
        expect(gameCompanyLogo).to.be.an("array").that.is.empty;
      });
    });
  });

  describe("GET /search/games/:term?page", () => {
    it("should return a 401 if no authorization", (done) => {
      chai
        .request(app)
        .get("/api/search/games/batman?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 200 if results", (done) => {
      chai
        .request(app)
        .get("/api/search/games/batman?page=1")
        .set({Authorization: `Bearer ${createdUserTwitchAccessToken}`})
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 204 if no result", (done) => {
      chai
        .request(app)
        .get("/api/search/games/thiscantbeanygameexistingever?page=1")
        .set({Authorization: `Bearer ${createdUserTwitchAccessToken}`})
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("GET /games/user/:id", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .get("/api/games/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 204 if no games", (done) => {
      chai
        .request(app)
        .get("/api/games/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("POST /games/user/:id", () => {
    it("should fail adding a game to user if no data", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/games/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdGameId = res.body._id;
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId)
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("POST /games/user/:userId/list/:listId", () => {
    before("should return a newly created list", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .send({
          name: "Test list",
          description: "Description",
        })
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdListId = res.body._id;
          done();
        });
    });

    it("should fail adding a game to user's list if no data", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/games/user/" + new ObjectId() + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId + "/list/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdGameId = res.body._id;
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId + "/list/" + createdListId)
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/games/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          provider: GamesProviders.igdb,
          providerId: "114009",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("GET /games/user/:id", () => {
    it("should return games with a 200 if games exist", (done) => {
      chai
        .request(app)
        .get("/api/games/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/games/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("POST /games/:gamesId/user/:userId", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .post("/games/" + createdGameId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          dlcId: 20115,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .post("/games/" + createdGameId + "/user/" + createdUserId)
        .send({
          dlcId: 20115,
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .post("/games/" + createdGameId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 204 if no games", (done) => {
      chai
        .request(app)
        .post("/games/" + new ObjectId() + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          dlcId: 20115,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .post("/games/" + createdGameId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          dlcId: 20115,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/games/" + createdGameId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          dlcId: 20115,
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("PATCH /games", () => {
    it("should return a 422 if game does not exist", (done) => {
      chai
        .request(app)
        .patch("/api/games/" + "111111111111111111111111")
        .set({Authorization: `Bearer ${createdUserTwitchAccessToken}`})
        .send({
          provider: GamesProviders.igdb,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .patch("/api/games/" + createdGameId)
        .send({
          provider: GamesProviders.igdb,
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 200 updating an existing game", (done) => {
      chai
        .request(app)
        .patch("/api/games/" + createdGameId)
        .set({Authorization: `Bearer ${createdUserTwitchAccessToken}`})
        .send({
          provider: GamesProviders.igdb,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("DELETE /games/:gamesId/user/:userId", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .delete("/api/games/" + createdGameId + "/user/" + new ObjectId())
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          dlcId: 20115,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .delete("/api/games/" + createdGameId + "/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .delete("/api/games/" + createdGameId + "/user/" + new ObjectId())
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 204 if no games", (done) => {
      chai
        .request(app)
        .delete("/api/games/" + new ObjectId() + "/user/" + createdUserId)
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          dlcId: 20115,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .delete("/api/games/" + createdGameId + "/user/" + createdUserId)
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          dlcId: 20115,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
