import Express, {Request} from "express";
import Comic from "../models/Comic.model.ts";
import User from "../models/User.model.ts";
import {
  ComicDocument,
  ComicDocumentsPaginated,
} from "../../interfaces/Comic.interface.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";
import {Provider} from "../../interfaces/Provider.interface.ts";
import {
  BadRequestError,
  BaseError,
  ConflictError,
  NoContentError,
  ServerError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import {List, UserDocument} from "../../interfaces/User.interface.ts";
import {toRegex} from "diacritic-regex";

export class ComicController {
  private ComicProvider?;

  constructor(comicProvider?: Provider) {
    if (comicProvider) {
      this.ComicProvider = comicProvider;
    }
  }

  async search(req: Request): Promise<ComicDocumentsPaginated | BaseError> {
    if (!this.ComicProvider) {
      throw new ServerError("No comic provider provided");
    } else {
      try {
        const paginatedComics = await this.ComicProvider.search(
          req,
          WorksTypes.Comic
        );
        return paginatedComics as ComicDocumentsPaginated;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async add(req: Request): Promise<ComicDocument | BaseError> {
    if (!this.ComicProvider) {
      throw new ServerError("No comic provider provided");
    } else {
      try {
        const comic = await this.ComicProvider.add(req);
        return comic as ComicDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async addToList(req: Request): Promise<ComicDocument | BaseError> {
    if (!this.ComicProvider) {
      throw new ServerError("No comic provider provided");
    } else {
      try {
        const listId = req.params.listId;
        const comic = await this.ComicProvider.add(req, listId);
        return comic as ComicDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  async update(req: Request): Promise<ComicDocument | BaseError> {
    if (!this.ComicProvider) {
      throw new ServerError("No comic provider provided");
    } else {
      try {
        const comic = await this.ComicProvider.update(req);
        return comic as ComicDocument;
      } catch (error) {
        if (error instanceof BaseError) {
          throw error;
        } else {
          throw new ServerError("Internal Server Error");
        }
      }
    }
  }

  // Get user comics by ID
  async get(req: Express.Request) {
    const id = req.params.userId;
    const user = await User.findById(id);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const page = parseInt(req.query.page as string, 10) || 1;
    const limit = parseInt(req.query.limit as string, 10) || 100;
    const sortBy = (req.query.sortBy as string) || "discoveryDate";
    const order = (req.query.order as string) || "desc";
    const query = (req.query.query as string) || null;

    const offset = (page - 1) * limit;

    if (user.comics.length < 1) {
      throw new NoContentError("No comic found for the given query");
    }

    const comicIds = user.comics.map((comicId) => comicId._id);

    const comicsQuery: Record<string, unknown> = {_id: {$in: comicIds}};

    if (req.query.filter && req.query.filter !== "All") {
      comicsQuery.genres = req.query.filter as string;
    }

    if (query) {
      const titleRegex = toRegex()(query);
      comicsQuery.title = {$regex: new RegExp(titleRegex, "i")};
    }

    const comics = await Comic.find(comicsQuery);

    let sortedComics: ComicDocument[];

    // Sort by rating
    if (sortBy && sortBy === "rating") {
      sortedComics = comics.sort((a, b) => {
        const aUserData = user.comics.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.comics.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (bUserData?.rating || 0) - (aUserData?.rating || 0);
      });
    } else {
      // Sort by discoveryDate
      sortedComics = comics.sort((a, b) => {
        const aUserData = user.comics.find(
          (m) => m._id.toString() === a._id.toString()
        );
        const bUserData = user.comics.find(
          (m) => m._id.toString() === b._id.toString()
        );

        return (
          (bUserData?.discoveryDate as Date).getTime() -
          (aUserData?.discoveryDate as Date).getTime()
        );
      });
    }

    if (order && order === "asc") {
      sortedComics.reverse();
    }

    const paginatedComics = sortedComics.slice(offset, offset + limit);

    const formattedComics = await Promise.all(
      paginatedComics.map(async (comic) => {
        const userData = user.comics.find(
          (m) => m._id.toString() === comic._id.toString()
        );

        const formattedComic = new Comic(comic);
        const formattedComicObject = formattedComic.toObject();
        formattedComicObject.additionDate = userData!.additionDate as Date;
        formattedComicObject.discoveryDate = userData!.discoveryDate as Date;
        formattedComicObject.progress = userData!.progress;
        if (userData!.rating) {
          formattedComicObject.userRating = userData!.rating;
        }

        const lists = await Promise.all(
          user!.lists.map(async (list: List) => {
            const workInList = list.works?.find(
              (work) => work._id.toString() === comic._id.toString()
            );

            if (workInList) {
              return {name: list.name, id: list._id?.toString()};
            }

            return {name: "", id: ""};
          })
        );

        formattedComicObject.lists = lists.filter((list) => list.name !== "");

        return formattedComicObject;
      })
    );

    const totalFilteredcomics = comics.length;
    const totalPages = Math.ceil(totalFilteredcomics / limit);

    const pagination = {
      page,
      totalPages,
    };

    return {data: formattedComics, pagination};
  }

  async updateUserComicProgress(
    req: Request
  ): Promise<UserDocument | BaseError> {
    const {userId, comicId} = req.params;

    const {volumes} = req.body;

    if (!userId || !comicId || !volumes) {
      throw new BadRequestError("Missing required fields");
    }

    const user = await User.findById(userId);

    if (user) {
      if (user.comics && user.comics.length > 0) {
        const comic = user.comics.find(
          (comics) => comics._id.toString() === comicId
        );
        const comicDetails = await Comic.findById(comicId);

        if (comic) {
          if (req.method === "POST") {
            if (!comic.progress) {
              comic.progress = [];
            }

            if (volumes.length === comicDetails?.volumes?.length) {
              comic.progress = [];
              volumes.forEach((volumeNumber: number) => {
                comic.progress?.push({volume: volumeNumber});
              });
            } else {
              volumes.forEach((volumeNumber: number) => {
                const volumeExists = comic.progress?.some(
                  (progress) => progress.volume === volumeNumber
                );

                if (!volumeExists) {
                  comic.progress?.push({volume: volumeNumber});
                } else {
                  throw new ConflictError(
                    `The specified comic volume ${volumeNumber} has already been added`
                  );
                }
              });
            }
          } else if (req.method === "DELETE") {
            if (comic.progress) {
              if (volumes.length === 0) {
                comic.progress = [];
              } else {
                comic.progress = comic.progress.filter(
                  (progress) => !volumes.includes(progress.volume)
                );
              }
            } else {
              comic.progress = [];
            }
          }

          const updatedUser = await user.save();

          return updatedUser;
        } else {
          throw new NoContentError("No comic found for the given query");
        }
      } else {
        throw new NoContentError("No comic found for the given query");
      }
    } else {
      throw new UnprocessableEntityError("No user found for the given query");
    }
  }
}
