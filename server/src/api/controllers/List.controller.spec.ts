import {Server} from "http";
import chai from "chai";
chai.should();
import chaiHttp from "chai-http";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";

import {app} from "../../server.ts";
import mongoose from "mongoose";
import {ObjectId} from "mongodb";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Lists API", () => {
  let server: Server;
  let createdUserId: mongoose.ObjectId;
  let createdListId: mongoose.ObjectId;
  let createdUserAccessToken: string;
  let createdUserTwitchAccessToken: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["series", "users"]);
  });

  describe("Create and log user to generate tokens", () => {
    before((done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe-duplicate@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
          activated: true,
        })
        .end((_err, res) => {
          createdUserId = res.body._id;
          done();
        });
    });

    it("should login and generate an accessToken", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe-duplicate@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.tokens.accessToken).to.be.a.string;
          expect(res.body.tokens.twitchAccessToken).to.be.a.string;
          createdUserAccessToken = res.body.tokens.accessToken;
          createdUserTwitchAccessToken = res.body.tokens.twitchAccessToken;
          done();
        });
    });
  });

  describe("GET /lists/user/:userId", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .get("/api/lists/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 204 if no list", (done) => {
      chai
        .request(app)
        .get("/api/lists/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("POST /lists/user/:userId", () => {
    it("should return a newly created list", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .send({
          name: "Test list",
          description: "Description",
        })
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdListId = res.body._id;
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .send({
          name: "Test list",
          description: "Description",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });
  });

  describe("GET /lists/user/:id", () => {
    it("should return lists with a 200 if lists exist", (done) => {
      chai
        .request(app)
        .get("/api/lists/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/lists/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("GET /lists/:listId/user/:userId", () => {
    it("should return list with a 200 if list exist", (done) => {
      chai
        .request(app)
        .get("/api/lists/" + createdListId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return list with a 422 if no list", (done) => {
      chai
        .request(app)
        .get("/api/lists/" + new ObjectId() + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return list with a 422 if no user", (done) => {
      chai
        .request(app)
        .get("/api/lists/" + createdListId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/lists/" + createdListId + "/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("PATCH /lists/:listId/user/:userId", () => {
    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .patch("/api/lists/" + createdListId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 400 if no name or description", (done) => {
      chai
        .request(app)
        .patch("/api/lists/" + createdListId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          test: "not the required body field",
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .patch("/api/lists/" + createdListId + "/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .patch("/api/lists/" + createdListId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          name: "Updated list's name",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .patch("/api/lists/" + new ObjectId() + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          name: "Updated list's name",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .patch("/api/lists/" + createdListId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .send({
          name: "Updated list's name",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("DELETE /lists/:listId/user/:userId", () => {
    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .delete("/api/lists/" + new ObjectId() + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .delete("/api/lists/" + createdListId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .delete("/api/lists/" + createdListId + "/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 200 on deletion", (done) => {
      chai
        .request(app)
        .delete("/api/lists/" + createdListId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}, Bearer ${createdUserTwitchAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
