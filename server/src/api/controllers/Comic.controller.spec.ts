import {Server} from "http";
import chai from "chai";
chai.should();
import chaiHttp from "chai-http";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";

import {app} from "../../server.ts";
import mongoose from "mongoose";
import {ObjectId} from "mongodb";
import {ComicsProviders} from "../../enums/Comic.enum.ts";
import {getComicDetails} from "../../utils/providers/comicvine.util.ts";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Comics API", () => {
  let server: Server;
  let createdUserId: mongoose.ObjectId;
  let createdComicId: mongoose.ObjectId;
  let createdUserAccessToken: string;
  let createdListId: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["comics", "users"]);
  });

  describe("Create user tokens", () => {
    before((done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe-duplicate@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
          activated: "true",
        })
        .end((_err, res) => {
          createdUserId = res.body._id;
          done();
        });
    });

    it("should login and generate an accessToken", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe-duplicate@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.tokens.accessToken).to.be.a.string;
          expect(res.body.tokens.twitchAccessToken).to.be.a.string;
          createdUserAccessToken = res.body.tokens.accessToken;
          done();
        });
    });
  });

  describe("Helpers ComicVine functions", () => {
    describe("Get comic details", () => {
      it("should return true if comic does exist", async () => {
        // id 51404 : Aldebaran
        const mangaDetails = await getComicDetails("51404");
        expect(mangaDetails).to.not.equal(false);
      });
    });
  });

  describe("GET /search/comics/:term?page", () => {
    it("should return a 200 if results", (done) => {
      chai
        .request(app)
        .get("/api/search/comics/aldebaran?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 204 if no result", (done) => {
      chai
        .request(app)
        .get("/api/search/comics/thiscantbeanycomicexistingever?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("GET /comics/user/:id", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .get("/api/comics/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 204 if no comics", (done) => {
      chai
        .request(app)
        .get("/api/comics/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("POST /comics/user/:id", () => {
    it("should fail adding a comic to user if no data", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdComicId = res.body._id;
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId)
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("POST /comics/user/:userId/list/:listId", () => {
    before("should return a newly created list", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .send({
          name: "Test list",
          description: "Description",
        })
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdListId = res.body._id;
          done();
        });
    });

    it("should fail adding a comic to user's list if no data", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + new ObjectId() + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId + "/list/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdComicId = res.body._id;
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId + "/list/" + createdListId)
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/comics/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: ComicsProviders.comicvine,
          providerId: "51404",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("GET /comics/user/:id", () => {
    it("should return comics with a 200 if comics exist", (done) => {
      chai
        .request(app)
        .get("/api/comics/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .get("/api/comics/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });
  });

  describe("POST /comics/:mangasId/user/:userId", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .post("/comics/" + createdComicId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .post("/comics/" + createdComicId + "/user/" + createdUserId)
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .post("/comics/" + createdComicId + "/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 204 if no comic", (done) => {
      chai
        .request(app)
        .post("/comics/" + new ObjectId() + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .post("/comics/" + createdComicId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/comics/" + createdComicId + "/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("PATCH /comics", () => {
    it("should return a 422 if comic does not exist", (done) => {
      chai
        .request(app)
        .patch("/api/comics/" + "111111111111111111111111")
        .send({
          provider: ComicsProviders.comicvine,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 200 updating an existing comic", (done) => {
      chai
        .request(app)
        .patch("/api/comics/" + createdComicId)
        .send({
          provider: ComicsProviders.comicvine,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  describe("DELETE /comics/:mangasId/user/:userId", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .delete("/api/comics/" + createdComicId + "/user/" + new ObjectId())
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .delete("/api/comics/" + createdComicId + "/user/" + createdUserId)
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 400 if no body", (done) => {
      chai
        .request(app)
        .delete("/api/comics/" + createdComicId + "/user/" + new ObjectId())
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .end((_err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it("should return a 204 if no comics", (done) => {
      chai
        .request(app)
        .delete("/api/comics/" + new ObjectId() + "/user/" + createdUserId)
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });

    it("should return a 200 on update", (done) => {
      chai
        .request(app)
        .delete("/api/comics/" + createdComicId + "/user/" + createdUserId)
        .set({Authorization: `Bearer ${createdUserAccessToken}`})
        .send({
          volumes: [1],
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
