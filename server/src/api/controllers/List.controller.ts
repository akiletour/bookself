import {Request} from "express";
import {
  BadRequestError,
  BaseError,
  NoContentError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import User from "../models/User.model.ts";
import Game from "../models/Game.model.ts";
import Movie from "../models/Movie.model.ts";
import Series from "../models/Series.model.ts";
import Manga from "../models/Manga.model.ts";
import Comic from "../models/Comic.model.ts";
import {List} from "../../interfaces/User.interface.ts";
import {handleErrors} from "../../utils/helpers.util.ts";

export class ListController {
  async add(req: Request): Promise<List | BaseError> {
    const id = req.params.userId;
    try {
      const user = await User.findById(id);

      if (!user) {
        throw new UnprocessableEntityError("No user found for the given query");
      }

      const {name, description} = req.body;

      if (!name) {
        throw new BadRequestError("Missing required fields");
      }

      user.lists.push({
        name,
        description,
      });

      const updatedUser = await user.save();

      return updatedUser.lists[updatedUser.lists.length - 1];
    } catch (err) {
      throw handleErrors(err);
    }
  }

  async update(req: Request): Promise<List | BaseError> {
    const userId = req.params.userId;
    const listId = req.params.listId;
    const {name, description} = req.body;

    if (!name && !description) {
      throw new BadRequestError("Missing required fields");
    }

    try {
      const user = await User.findById(userId);

      if (!user) {
        throw new UnprocessableEntityError("No user found for the given query");
      }

      const listToUpdate = user.lists.find(
        (list) => list._id!.toString() === listId
      );

      if (!listToUpdate) {
        throw new UnprocessableEntityError("No list found for the given query");
      }

      const {name, description} = req.body;

      listToUpdate.name = name;
      listToUpdate.description = description;
      listToUpdate.updatedAt = new Date(Date.now());

      await user.save();

      return listToUpdate;
    } catch (err) {
      throw handleErrors(err);
    }
  }

  async get(req: Request): Promise<List[] | BaseError> {
    const userId = req.params.userId;
    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    if (!(user.lists && user.lists.length)) {
      throw new NoContentError("No list found for the given query");
    } else {
      const sortedLists = user.lists.sort((a, b) => {
        return b.updatedAt!.getTime() - a.updatedAt!.getTime();
      });

      const sortedListsWithCovers = await Promise.all(
        sortedLists.map(async (list) => {
          await Promise.all(
            (list.works || []).slice(0, 4).map(async (work) => {
              const model = work.model;
              const id = work._id;

              let movie, series, game, manga, comic;

              switch (model) {
                case "Movie":
                  movie = await Movie.findById(id);
                  work.cover = "/movies" + movie?.cover;
                  break;
                case "Series":
                  series = await Series.findById(id);
                  work.cover = "/series" + series?.cover;
                  break;
                case "Game":
                  game = await Game.findById(id);
                  work.cover = "/games" + game?.cover;
                  break;
                case "Manga":
                  manga = await Manga.findById(id);
                  work.cover = "/mangas" + manga?.cover;
                  break;
                case "Comic":
                  comic = await Comic.findById(id);
                  work.cover = "/comics" + comic?.cover;
                  break;
                default:
                  break;
              }

              return work;
            })
          );

          return list;
        })
      );

      return sortedListsWithCovers;
    }
  }

  async getById(req: Request): Promise<List | BaseError> {
    const userId = req.params.userId;
    const user = await User.findById(userId);
    const listId = req.params.listId;

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const list: List | undefined = user.lists?.find(
      (item: List) => item?._id!.toString() === listId
    );
    if (!(user.lists && user.lists.length) || !list) {
      throw new UnprocessableEntityError("No list found for the given query");
    } else {
      return list;
    }
  }

  async delete(req: Request): Promise<boolean | BaseError> {
    const {userId, listId} = req.params;

    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    const index = user.lists.findIndex(
      (item) => item._id!.toString() === listId
    );

    if (index !== undefined && index !== -1) {
      user.lists.splice(index, 1);
      const updatedUser = await user.save();
      if (updatedUser instanceof User) {
        return true;
      } else {
        return false;
      }
    } else {
      throw new UnprocessableEntityError("No list found for the given query");
    }
  }
}
