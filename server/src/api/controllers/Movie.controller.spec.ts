import {Server} from "http";
import chai from "chai";
chai.should();
import chaiHttp from "chai-http";
const {expect} = chai;
import {setup, teardown} from "../../utils/testsSetup.util.ts";

import {app} from "../../server.ts";
import mongoose from "mongoose";
import {ObjectId} from "mongodb";
import {getMovieCredits, getDetails} from "../../utils/providers/tmdb.util.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";
import {MoviesProviders} from "../../enums/Movie.enum.ts";

const PORT =
  process.env.NODE_ENV && process.env.NODE_ENV === "test"
    ? +process.env.TEST_PORT!
    : 5000;

chai.use(chaiHttp);

describe("Movies API", () => {
  let server: Server;
  let createdUserId: mongoose.ObjectId;
  let createdUserAccessToken: string;
  let createdListId: string;

  before(async () => {
    server = await setup(app, PORT);
  });

  after(async () => {
    await teardown(server, ["movies", "users"]);
  });

  describe("Helpers TMDB functions", () => {
    describe("Get movie details", () => {
      it("should return true if movie does exist", async () => {
        // id 268 : 1989 Batman movie
        const movieDetails = await getDetails("268", WorksTypes.Movie);
        expect(movieDetails).to.not.equal(false);
      });

      it("should return false if movie does not exist", async () => {
        const movieDetails = await getDetails(
          "0000000000000000000",
          WorksTypes.Movie
        );
        expect(movieDetails).to.equal(false);
      });
    });

    describe("Get movie credits", () => {
      it("should return true if movie does exist", async () => {
        // id 268 : 1989 Batman movie
        const movieCredits = await getMovieCredits("268");
        expect(movieCredits).to.not.equal(false);
      });

      it("should return false if movie does not exist", async () => {
        const movieCredits = await getMovieCredits("0000000000000000000");
        expect(movieCredits).to.equal(false);
      });
    });
  });

  describe("Create user to generate tokens", () => {
    before((done) => {
      chai
        .request(app)
        .post("/users")
        .send({
          email: "johndoe-duplicate@email.com",
          firstname: "john",
          lastname: "doe",
          password: "password",
          confirm_password: "password",
          activated: true,
        })
        .end((_err, res) => {
          createdUserId = res.body._id;
          done();
        });
    });

    it("should login and generate an accessToken", (done) => {
      chai
        .request(app)
        .post("/login")
        .send({
          email: "johndoe-duplicate@email.com",
          password: "password",
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.tokens.accessToken).to.be.a.string;
          createdUserAccessToken = res.body.tokens.accessToken;
          done();
        });
    });
  });

  describe("GET /search/movies/:term?page", () => {
    it("should return a 200 if results", (done) => {
      chai
        .request(app)
        .get("/api/search/movies/batman?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it("should return a 204 if no result", (done) => {
      chai
        .request(app)
        .get("/api/search/movies/thiscantbeanymovieexistingever?page=1")
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("GET /movies/user/:id", () => {
    it("should return a 422 if no users", (done) => {
      chai
        .request(app)
        .get("/api/movies/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 204 if no movies", (done) => {
      chai
        .request(app)
        .get("/api/movies/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(204);
          done();
        });
    });
  });

  describe("POST /movies/user/:id", () => {
    it("should fail adding a movie to user if no data", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          // 268 : Batman
          provider: MoviesProviders.tmdb,
          providerId: 268,
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 401 if no token provided", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId)
        .send({
          provider: MoviesProviders.tmdb,
          providerId: 268,
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: 268,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: 268,
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("POST /movies/user/:userId/list/:listId", () => {
    before("should return a newly created list", (done) => {
      chai
        .request(app)
        .post("/lists/user/" + createdUserId)
        .send({
          name: "Test list",
          description: "Description",
        })
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          createdListId = res.body._id;
          done();
        });
    });

    it("should fail adding a movie to user's list if no data", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });

    it("should return a 422 if no user", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + new ObjectId() + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "268",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 422 if no list", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId + "/list/" + new ObjectId())
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "268",
        })
        .end((_err, res) => {
          expect(res).to.have.status(422);
          done();
        });
    });

    it("should return a 201 if added", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "268",
        })
        .end((_err, res) => {
          expect(res).to.have.status(201);
          done();
        });
    });

    it("should return a 401 if unauthorized", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId + "/list/" + createdListId)
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "268",
        })
        .end((_err, res) => {
          expect(res).to.have.status(401);
          done();
        });
    });

    it("should return a 409 if already added", (done) => {
      chai
        .request(app)
        .post("/movies/user/" + createdUserId + "/list/" + createdListId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .send({
          provider: MoviesProviders.tmdb,
          providerId: "268",
        })
        .end((_err, res) => {
          expect(res).to.have.status(409);
          done();
        });
    });
  });

  describe("GET /movies/user/:id", () => {
    it("should return movies with a 200 if movies exist", (done) => {
      chai
        .request(app)
        .get("/api/movies/user/" + createdUserId)
        .set({
          Authorization: `Bearer ${createdUserAccessToken}`,
        })
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

  it("should return a 401 if no token provided", (done) => {
    chai
      .request(app)
      .get("/api/movies/user/" + createdUserId)
      .end((_err, res) => {
        expect(res).to.have.status(401);
        done();
      });
  });
});
