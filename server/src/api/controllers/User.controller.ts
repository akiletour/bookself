import Express from "express";
import {
  ActivityItem,
  UserActivity,
  UserDocument,
  UserDocumentsPaginated,
} from "../../interfaces/User.interface.ts";
import User from "../models/User.model.ts";
import bcrypt from "bcryptjs";
import {comparePassword, handleErrors} from "../../utils/helpers.util.ts";
import {
  BadRequestError,
  BaseError,
  ConflictError,
  NoContentError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import mongoose from "mongoose";

export class UserController {
  async add(req: Express.Request): Promise<UserDocument | BaseError> {
    const {password, confirm_password, email, firstname, lastname} = req.body;

    if (!password || !confirm_password || !email || !firstname || !lastname) {
      throw new BadRequestError("Missing required fields");
    }

    if (password !== confirm_password) {
      throw new BadRequestError("Passwords do not match");
    }

    // Generate a username if not provided
    if (!req.body.username) {
      req.body.username =
        req.body.firstname.charAt(0).toLowerCase() +
        req.body.lastname.trim().replace(/\s+/g, "-").toLowerCase();
    }

    req.body.email = req.body.email.toLowerCase();

    const user = new User(req.body);
    return bcrypt.hash(password, 12).then(async (hashedPassword) => {
      user.password = hashedPassword;
      return user
        .save()
        .then((user: UserDocument) => {
          return user;
        })
        .catch((err) => {
          const error =
            err.errors.email.properties.message ===
            "The specified email address is already in use"
              ? new ConflictError(err.errors.email.properties.message)
              : new BadRequestError(err.errors.email.properties.message);

          throw handleErrors(error);
        });
    });
  }

  async get(req: Express.Request): Promise<UserDocumentsPaginated | BaseError> {
    return User.find()
      .then((users) => {
        if (users.length === 0) {
          throw new NoContentError("No user found for the given query");
        }

        const page = req.query.page ? parseInt(req.query.page as string) : 1;

        const limitParam = req.query.limit
          ? parseInt(req.query.limit as string)
          : undefined;
        const limit =
          limitParam && limitParam > 0 && limitParam <= users.length
            ? limitParam
            : 100;

        const offset = (page - 1) * limit;

        const usersPaginated: UserDocument[] = users.slice(offset, limit);

        const pagination = {
          page,
          totalPages: Math.ceil(users.length / limit),
        };

        return {data: usersPaginated, pagination};
      })
      .catch((err) => handleErrors(err));
  }

  async getById(req: Express.Request): Promise<UserDocument | BaseError> {
    const id = req.params.userId;
    return User.findById(id)
      .then((user) => {
        if (!user) {
          throw new UnprocessableEntityError(
            "No user found for the given query"
          );
        } else {
          return user;
        }
      })
      .catch((err) => {
        throw handleErrors(err);
      });
  }

  async update(req: Express.Request): Promise<UserDocument | BaseError> {
    const id = req.params.userId;
    try {
      const user = await User.findById(id);
      if (!user) {
        throw new UnprocessableEntityError("No user found for the given query");
      }

      user.email = req.body.email ?? user.email;
      user.firstname = req.body.firstname ?? user.firstname;
      user.lastname = req.body.lastname ?? user.lastname;
      user.username = req.body.username ?? user.username;
      user.theme = req.body.theme ?? user.theme;
      user.updatedAt = new Date();

      const updatedUser = await user.save();

      return updatedUser;
    } catch (err) {
      if (err instanceof mongoose.Error.ValidationError) {
        const error =
          err.errors?.email.message ===
          "The specified email address is already in use"
            ? new ConflictError(err.errors.email.message)
            : new BadRequestError(err.errors.email.message);

        throw handleErrors(error);
      }
      throw handleErrors(err);
    }
  }

  async uploadProfilePicture(
    req: Express.Request
  ): Promise<UserDocument | BaseError> {
    const userId = req.params.userId;

    if (req.file) {
      try {
        const profilePicturePath: string | undefined = req.file?.path.replace(
          "images",
          ""
        );

        const user: UserDocument | null = await User.findByIdAndUpdate(
          userId,
          {
            picture: profilePicturePath,
          },
          {new: true}
        );

        if (user) {
          return user;
        } else {
          throw new UnprocessableEntityError(
            "No user found for the given query"
          );
        }
      } catch (err: unknown) {
        throw handleErrors(err);
      }
    } else {
      throw new BadRequestError("Missing required image");
    }
  }

  async updatePassword(
    req: Express.Request
  ): Promise<UserDocument | BaseError> {
    const id = req.params.userId;

    const {old_password, new_password, confirm_new_password} = req.body;

    if (!old_password || !new_password || !confirm_new_password) {
      throw new BadRequestError("Missing required fields");
    }

    if (new_password !== confirm_new_password) {
      throw new BadRequestError("Passwords do not match");
    }

    const user = await User.findById(id);

    if (user) {
      const doMatch = await comparePassword(old_password, user.password);

      if (doMatch) {
        return bcrypt
          .hash(new_password.toString(), 12)
          .then(async (hashedPassword) => {
            user.password = hashedPassword;
            return user
              .save()
              .then((user: UserDocument) => {
                return user;
              })
              .catch((err) => {
                throw handleErrors(err);
              });
          });
      } else {
        throw new BadRequestError("Your actual password is invalid");
      }
    } else {
      throw new UnprocessableEntityError("No user found for the given query");
    }
  }

  async delete(req: Express.Request): Promise<boolean | BaseError> {
    const id = req.params.userId;
    return User.findById(id)
      .then((user: UserDocument | null) => {
        if (!user) {
          throw new UnprocessableEntityError(
            "No user found for the given query"
          );
        }
        return User.findByIdAndRemove(id);
      })
      .then((result) => {
        if (result instanceof User) {
          return true;
        } else {
          return false;
        }
      })
      .catch((err) => {
        throw handleErrors(err);
      });
  }

  async getUserActivity(
    req: Express.Request
  ): Promise<UserActivity | BaseError> {
    const id = req.params.userId;

    try {
      const user = await User.findById(id);

      if (!user) {
        throw new UnprocessableEntityError("No user found for the given query");
      }

      const filter = req.query.filter?.toString();

      const activity: UserActivity = {};

      if (filter) {
        const propertiesToCount = [
          "movies",
          "series",
          "games",
          "animation",
          "mangas",
          "comics",
          "books",
        ];
        const currentDate = new Date();
        const currentMonth = currentDate.getMonth();
        const currentYear = currentDate.getFullYear();

        propertiesToCount.forEach((property) => {
          const propertyKey = property as keyof UserDocument;
          const propertyActivityKey = property as keyof UserActivity;

          if (user[propertyKey]) {
            if (filter === "year") {
              activity[propertyActivityKey] = user[propertyKey].filter(
                (item: ActivityItem) => {
                  const itemDate = new Date(item.additionDate);
                  return itemDate.getFullYear() === currentYear;
                }
              ).length;
            } else if (filter === "month") {
              activity[propertyActivityKey] = user[propertyKey].filter(
                (item: ActivityItem) => {
                  const itemDate = new Date(item.additionDate);
                  return itemDate.getMonth() === currentMonth;
                }
              ).length;
            } else {
              activity[propertyActivityKey] = user[propertyKey].length;
            }
          }
        });
      } else {
        activity.movies = user.movies?.length;
        activity.series = user.series?.length;
        activity.games = user.games?.length;
        activity.mangas = user.mangas?.length;
        activity.comics = user.comics?.length;
        // Add other properties here
      }

      return activity;
    } catch (err) {
      throw handleErrors(err);
    }
  }
}
