import Express from "express";
import User from "../models/User.model.ts";
import {
  UserDocument,
  UserLogged,
  UserLoginPayload,
} from "../../interfaces/User.interface.ts";
import {randomBytes} from "crypto";
import nodemailer, {Transporter} from "nodemailer";
import postmarkTransport from "nodemailer-postmark-transport";
import SMTPTransport from "nodemailer/lib/smtp-transport/index";
import {promisify} from "util";

import path from "path";
import fs from "fs";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import * as utils from "../../utils/helpers.util.ts";
import {comparePassword, handleErrors} from "../../utils/helpers.util.ts";
import axios from "axios";
import {
  ActivatedError,
  BadRequestError,
  BaseError,
  ForbiddenError,
  InvalidTokenError,
  ServerError,
  UnauthorizedError,
  UnprocessableEntityError,
} from "../../types/CustomErrors.type.ts";
import {UserController} from "./User.controller.ts";

export class AuthController {
  private randomBytesAsync = promisify(randomBytes);
  private transporter!: Transporter<SMTPTransport.SentMessageInfo>;
  private clientUrl = process.env.CLIENT_URL!;
  private imagesUrl = process.env.SERVER_IMAGES_URL!;

  constructor() {
    if (process.env.NODE_ENV === "production") {
      this.transporter = nodemailer.createTransport(
        postmarkTransport({
          auth: {
            apiKey: process.env.POSTMARK_TOKEN!,
          },
        })
      );
    } else {
      this.transporter = nodemailer.createTransport({
        host: "0.0.0.0",
        port: 1025,
        secure: false,
        tls: {
          // do not fail on invalid certs
          rejectUnauthorized: false,
        },
      });
    }
  }

  async logIn(req: Express.Request): Promise<UserLogged | BaseError> {
    const {email, password} = req.body;

    if (!email || !password) {
      throw new BadRequestError("Missing required fields");
    }

    const user = await User.findOne({email: (email as string).toLowerCase()});

    if (!user) {
      throw new UnauthorizedError("Sorry, no account match those informations");
    }

    const doMatch = await comparePassword(password, user.password);

    if (!doMatch) {
      throw new UnauthorizedError("Sorry, no account match those informations");
    }

    if (!user.activated) {
      throw new ForbiddenError("Sorry, this account is not activated");
    }

    const payload = {
      id: user.id,
      email: user.email,
      username: user.username,
    };

    const accessToken = this.generateAccessToken(payload);
    const refreshToken = this.generateRefreshToken(payload);

    try {
      const response = await axios.post(
        `https://id.twitch.tv/oauth2/token`,
        null,
        {
          params: {
            client_id: process.env.IGDB_PROVIDER_ID,
            client_secret: process.env.IGDB_PROVIDER_SECRET,
            grant_type: "client_credentials",
          },
        }
      );

      const twitchAccessToken = response.data?.access_token;

      if (!twitchAccessToken) {
        throw new ServerError("Error during tokens generation");
      }

      const userWithTokens = {
        ...user.toObject(),
        tokens: {
          accessToken,
          refreshToken,
          twitchAccessToken,
        },
      };

      return userWithTokens;
    } catch (error) {
      throw handleErrors(error);
    }
  }

  async refreshToken(req: Express.Request): Promise<UserLogged | BaseError> {
    const {email} = req.body;

    if (!email) {
      throw new BadRequestError("Missing required fields");
    }

    const user = await User.findOne({email});

    if (!user) {
      throw new UnauthorizedError("Sorry, no account match those informations");
    }

    if (!user.activated) {
      throw new ForbiddenError("Sorry, this account is not activated");
    }

    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];

    if (!token) {
      throw new UnauthorizedError("Sorry, no token provided");
    }

    const payload = {
      id: user.id,
      email: user.email,
      username: user.username,
    };

    try {
      const response = await axios.post(
        `https://id.twitch.tv/oauth2/token`,
        null,
        {
          params: {
            client_id: process.env.IGDB_PROVIDER_ID,
            client_secret: process.env.IGDB_PROVIDER_SECRET,
            grant_type: "client_credentials",
          },
        }
      );

      const twitchAccessToken = response.data.access_token;

      return new Promise((resolve, reject) => {
        jwt.verify(token, process.env.REFRESH_TOKEN_SECRET!, (err) => {
          if (err) {
            reject(
              new UnauthorizedError(
                "Sorry, refresh token is invalid or expired. Please log in again"
              )
            );
          } else {
            const refreshedToken = this.generateAccessToken(payload);

            const userWithTokens = {
              ...user.toObject(),
              tokens: {
                accessToken: refreshedToken,
                refreshToken: token,
                twitchAccessToken,
              },
            };

            resolve(userWithTokens);
          }
        });
      });
    } catch (error) {
      throw handleErrors(error);
    }
  }

  async signUp(req: Express.Request): Promise<UserDocument | BaseError> {
    return this.randomBytesAsync(32)
      .then(async (buffer) => {
        const token = buffer.toString("hex");

        req.body.activationToken = token;
        req.body.activationTokenExpiration = new Date(
          new Date().getTime() + 86400000
        );

        const userController = new UserController();

        const newUser = await userController.add(req);

        return newUser;
      })
      .then(async (user) => {
        if (user instanceof User) {
          const content = `<p style="font-size: 32px; font-weight: 700;">Hello ${user.firstname} !</p>
          <p>Your account on Bookself has been created.<br/><br/>
          Please click the link below to activate your account. <br/><br/><br/><a style="display: inline-block; padding: 17px; background: #333; border-radius: 100%, font-family: 'Arial'; font-weight: 700; font-size:20px; text-align: center; color: white; overflow: hidden; cursor:pointer; text-decoration: none; border-radius: 7px;" href="${process.env.CLIENT_URL}/account-activation/${user.activationToken}">Activate my account</a></p>`;
          const subject = "Bookself - Please activate your account";

          await this.sendEmailWithTemplate(
            user,
            this.clientUrl,
            this.imagesUrl,
            content,
            subject
          );

          return user;
        } else {
          throw new ServerError("Unknown error");
        }
      })
      .catch((err) => {
        throw handleErrors(err);
      });
  }

  async activateAccount(
    req: Express.Request
  ): Promise<UserDocument | BaseError> {
    const token = req.body.activationToken
      ? req.body.activationToken.toString()
      : undefined;

    if (!token) {
      throw new BadRequestError("No token provided");
    }

    if (!/^[a-zA-Z0-9]+$/.test(token)) {
      throw new InvalidTokenError("Invalid token");
    }

    return User.findOne({
      activationToken: token,
      activationTokenExpiration: {$gt: Date.now()},
    })
      .then((user) => {
        if (!user) {
          throw new InvalidTokenError("Invalid token");
        }
        user.activated = true;
        user.activationToken = undefined;
        user.activationTokenExpiration = undefined;
        return user.save();
      })
      .then(async (user) => {
        if (!user) {
          throw new UnprocessableEntityError(
            "No user found for the given query"
          );
        }
        const content = `<p style="font-size: 32px; font-weight: 700;">Hello ${user.firstname} !</p>
            <p>Your account on Bookself has been activated.<br/><br/>
            You can now login and use Bookself !<br/><br/><br/><a style="display: inline-block; padding: 17px; background: #333; border-radius: 100%, font-family: 'Arial'; font-weight: 700; font-size:20px; text-align: center; color: white; overflow: hidden; cursor:pointer; text-decoration: none; border-radius: 7px;" href="${process.env.CLIENT_URL}">Login</a></p>`;
        const subject = "Bookself - Account activated";

        await this.sendEmailWithTemplate(
          user,
          this.clientUrl,
          this.imagesUrl,
          content,
          subject
        );

        return user;
      })
      .catch((err) => {
        throw handleErrors(err);
      });
  }

  async sendActivationEmail(
    req: Express.Request
  ): Promise<UserDocument | BaseError> {
    return new Promise((resolve, reject) => {
      randomBytes(32, (err, buffer) => {
        if (err) {
          reject(handleErrors(err));
        }

        const token = buffer.toString("hex");
        User.findOne({email: req.body.email})
          .then((user) => {
            if (!user) {
              throw new UnauthorizedError(
                "Sorry, no account match those informations"
              );
            }

            if (user && user.activated === true) {
              throw new ActivatedError("This account is already activated");
            }

            user.activationToken = token;
            // Token expires after 1h
            user.activationTokenExpiration = new Date(
              new Date().getTime() + 3600000
            );
            return user.save();
          })
          .then(async (user) => {
            if (!user) {
              throw new UnprocessableEntityError(
                "No user found for the given query"
              );
            }

            const content = `<p style="font-size: 32px; font-weight: 700;">Hello ${user.firstname} !</p>
          <p>Your account on Bookself has been created.<br/><br/>
          Please click the link below to activate your account. <br/><br/><br/><a style="display: inline-block; padding: 17px; background: #333; border-radius: 100%, font-family: 'Arial'; font-weight: 700; font-size:20px; text-align: center; color: white; overflow: hidden; cursor:pointer; text-decoration: none; border-radius: 7px;" href="${process.env.CLIENT_URL}/account-activation/${user.activationToken}">Activate my account</a></p>`;
            const subject = "Bookself - Please activate your account";

            await this.sendEmailWithTemplate(
              user,
              this.clientUrl,
              this.imagesUrl,
              content,
              subject
            );

            resolve(user);
          })
          .catch((err) => {
            reject(handleErrors(err));
          });
      });
    });
  }

  async sendResetPasswordEmail(
    req: Express.Request
  ): Promise<UserDocument | BaseError> {
    return new Promise((resolve, reject) => {
      randomBytes(32, (err, buffer) => {
        if (err) {
          reject(handleErrors(err));
        }

        const token = buffer.toString("hex");
        User.findOne({email: req.body.email})
          .then((user) => {
            if (!user) {
              throw new UnprocessableEntityError(
                "No user found for the given query"
              );
            }

            user.resetToken = token;
            // Token expires after 1h
            user.resetTokenExpiration = new Date(
              new Date().getTime() + 3600000
            );
            return user.save();
          })
          .then(async (user) => {
            if (!user) {
              throw new UnprocessableEntityError(
                "No user found for the given query"
              );
            }

            const content = `<p style="font-size: 32px; font-weight: 700;">Hello ${user.firstname} !</p>
          <p>Click the link below to reset your password. If this request didn't come from you, please ignore this mail.<br/><br/><br/>
          <a style="display: inline-block; padding: 17px; background: #333; border-radius: 100%, font-family: 'Arial'; font-weight: 700; font-size:20px; text-align: center; color: white; overflow: hidden; cursor:pointer; text-decoration: none; border-radius: 7px;" href="${process.env.CLIENT_URL}/reset-password/${token}">Reset my password</a></p>`;
            const subject = "Bookself - Reset your password";

            await this.sendEmailWithTemplate(
              user,
              this.clientUrl,
              this.imagesUrl,
              content,
              subject
            );

            resolve(user);
          })
          .catch((err) => {
            reject(handleErrors(err));
          });
      });
    });
  }

  async verifyResetToken(
    req: Express.Request
  ): Promise<UserDocument | BaseError> {
    const token = req.body.resetToken
      ? req.body.resetToken.toString()
      : undefined;

    if (!token) {
      throw new BadRequestError("No token provided");
    }

    if (!/^[a-zA-Z0-9]+$/.test(token)) {
      throw new InvalidTokenError("Invalid token");
    }

    return User.findOne({
      resetToken: token,
      resetTokenExpiration: {$gt: Date.now()},
    })
      .then((user) => {
        if (!user) {
          throw new InvalidTokenError("Invalid token");
        } else {
          return user;
        }
      })
      .catch((err) => {
        throw handleErrors(err);
      });
  }

  async resetPassword(req: Express.Request): Promise<UserDocument | BaseError> {
    const {password, confirm_password, resetToken, _id} = req.body;

    const passwordString = password?.toString();
    const confirmPasswordString = confirm_password?.toString();
    const resetTokenString = resetToken?.toString();
    const idString = _id?.toString();

    if (!passwordString || !confirmPasswordString) {
      throw new BadRequestError("Missing required fields");
    }

    if (passwordString !== confirmPasswordString) {
      throw new BadRequestError("Passwords do not match");
    }

    if (!/^[a-zA-Z0-9]+$/.test(resetTokenString)) {
      throw new InvalidTokenError("Invalid refresh token");
    }

    if (!/^[0-9a-fA-F]{24}$/.test(idString)) {
      throw new BadRequestError("Id is invalid");
    }

    return User.findOne({
      _id: idString,
      resetToken: resetTokenString,
      resetTokenExpiration: {$gt: Date.now()},
    })
      .then(async (user) => {
        if (!user) {
          throw new UnauthorizedError(
            "Sorry, no account match those informations"
          );
        }

        const hashedPassword = await bcrypt.hash(passwordString, 12);
        user.password = hashedPassword;
        user.resetToken = undefined;
        user.resetTokenExpiration = undefined;
        return user.save();
      })
      .then((user) => {
        if (user instanceof User) {
          return user;
        } else {
          throw new ServerError("Unknown error");
        }
      })
      .catch((err) => {
        throw handleErrors(err);
      });
  }

  generateAccessToken(user: UserLoginPayload) {
    return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET!, {expiresIn: "7d"});
  }

  generateRefreshToken(user: UserLoginPayload) {
    return jwt.sign(user, process.env.REFRESH_TOKEN_SECRET!, {expiresIn: "1y"});
  }

  async sendEmailWithTemplate(
    user: UserDocument,
    clientUrl: string,
    imagesUrl: string,
    content: string,
    subject: string
  ) {
    const templatePath = path.join(utils.__dirname, "templates", "email.html");

    const readTemplate = () => {
      return new Promise<string>((resolve, reject) => {
        fs.readFile(templatePath, "utf8", (err, data) => {
          if (err) {
            reject(handleErrors(err));
          }
          resolve(data);
        });
      });
    };

    const data = await readTemplate();

    const generatedHTML = data
      .replace("{{ clientUrl }}", clientUrl!)
      .replace("{{ imagesUrl }}", imagesUrl!)
      .replace("{{ content }}", content);

    this.transporter.sendMail({
      to: user.email,
      from: process.env.EMAIL_FROM,
      subject,
      html: generatedHTML,
    });
  }
}
