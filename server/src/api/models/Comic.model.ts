import mongoose from "mongoose";
import {ComicDocument} from "../../interfaces/Comic.interface";

const Schema = mongoose.Schema;

const comicSchema = new Schema<ComicDocument>({
  provider: Number,
  providerId: String,
  title: String,
  publisher: String,
  releaseDate: Number,
  cover: String,
  genres: [String],
  authors: [
    {
      name: String,
      profile: String,
    },
  ],
  volumes: [
    {
      volumeNumber: Number,
      volumeName: String,
      cover: String,
    },
  ],
  rating: Number,
  summary: String,
  type: String,
});

export default mongoose.model("Comic", comicSchema);
