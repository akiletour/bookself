import mongoose from "mongoose";
import {UserDocument, UserModel} from "../../interfaces/User.interface.ts";

const Schema = mongoose.Schema;

const customValidators = {
  uniqueEmail: {
    validator: async function (
      this: UserDocument,
      email: string
    ): Promise<boolean> {
      const user = await (this.constructor as UserModel).findOne({email});
      return !user || this.id === user.id;
    },
    message: "The specified email address is already in use",
  },
  validEmail: {
    validator: function (email: string) {
      const emailRegex = /^\w+(?:[.-]\w+)*@\w+(?:[.-]\w+)*(?:\.\w{2,3})+$/;
      return emailRegex.test(email);
    },
    message: "Please enter a valid email address",
  },
};

const userSchema = new Schema<UserDocument>({
  email: {
    type: String,
    unique: true,
    validate: [customValidators.uniqueEmail, customValidators.validEmail],
    required: true,
  },
  firstname: String,
  lastname: String,
  password: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  picture: String,
  movies: [
    {
      _id: {type: mongoose.Schema.Types.ObjectId, ref: "Movie"},
      additionDate: Date,
      discoveryDate: Date,
      rating: {
        type: Number,
        required: false,
        default: null,
      },
    },
  ],
  series: [
    {
      _id: {type: mongoose.Schema.Types.ObjectId, ref: "Series"},
      additionDate: Date,
      discoveryDate: Date,
      rating: {
        type: Number,
        required: false,
        default: null,
      },
      progress: {
        season: Number,
        episode: Number,
        completed: Boolean,
      },
    },
  ],
  games: [
    {
      _id: {type: mongoose.Schema.Types.ObjectId, ref: "Game"},
      additionDate: Date,
      discoveryDate: Date,
      rating: {
        type: Number,
        required: false,
        default: null,
      },
      dlcs: [
        {
          providerId: Number,
        },
      ],
    },
  ],
  mangas: [
    {
      _id: {type: mongoose.Schema.Types.ObjectId, ref: "Manga"},
      additionDate: Date,
      discoveryDate: Date,
      rating: {
        type: Number,
        required: false,
        default: null,
      },
      progress: [
        {
          volume: Number,
        },
      ],
    },
  ],
  comics: [
    {
      _id: {type: mongoose.Schema.Types.ObjectId, ref: "Comic"},
      additionDate: Date,
      discoveryDate: Date,
      rating: {
        type: Number,
        required: false,
        default: null,
      },
      progress: [
        {
          volume: Number,
        },
      ],
    },
  ],
  // books: [
  //   {
  //     _id: {type: mongoose.Schema.Types.ObjectId, ref: "Books"},
  // additionDate: Date,
  // discoveryDate: Date,
  //   },
  // ],
  lists: [
    {
      name: String,
      description: String,
      works: [
        {
          model: {
            type: String,
            required: true,
            enum: ["Movie", "Series", "Game", "Manga", "Comic"],
          },
          _id: {type: mongoose.Schema.Types.ObjectId, refPath: "type"},
          additionDate: Date,
          cover: String,
        },
      ],
      createdAt: {
        type: Date,
        default: Date.now,
      },
      updatedAt: {
        type: Date,
        default: Date.now,
      },
    },
  ],
  activated: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
  theme: {
    type: String,
    default: "light",
  },
  activationToken: String,
  activationTokenExpiration: Date,
  resetToken: String,
  resetTokenExpiration: Date,
});

export default mongoose.model("User", userSchema);
