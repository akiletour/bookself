import mongoose from "mongoose";
import {SeriesDocument} from "../../interfaces/Series.interface";

const Schema = mongoose.Schema;

const seriesSchema = new Schema<SeriesDocument>({
  provider: Number,
  providerId: String,
  title: String,
  releaseDate: Date,
  lastAiredDate: Date,
  cover: String,
  genres: [String],
  creators: [
    {
      name: String,
      profile: String,
    },
  ],
  cast: [
    {
      name: String,
      character: String,
      profile: String,
    },
  ],
  numberOfEpisodes: Number,
  numberOfSeasons: Number,
  rating: Number,
  seasons: [
    {
      name: String,
      seasonNumber: Number,
      releaseDate: Date,
      synopsis: String,
      numberOfEpisodes: Number,
      cover: String,
      episodes: [
        {
          name: String,
          number: Number,
          releaseDate: Date,
          synopsis: String,
          duration: Number,
        },
      ],
    },
  ],
  synopsis: String,
  status: String,
  tagline: String,
  type: String,
});

export default mongoose.model("Series", seriesSchema);
