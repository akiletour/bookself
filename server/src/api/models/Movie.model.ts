import mongoose from "mongoose";
import {MovieDocument} from "../../interfaces/Movie.interface.ts";

const Schema = mongoose.Schema;

const movieSchema = new Schema<MovieDocument>({
  provider: Number,
  providerId: String,
  title: String,
  releaseDate: Date,
  cover: String,
  genres: [String],
  directors: [
    {
      name: String,
      profile: String,
    },
  ],
  cast: [
    {
      name: String,
      character: String,
      profile: String,
    },
  ],
  duration: String,
  rating: Number,
  synopsis: String,
  tagline: String,
  type: String,
});

export default mongoose.model("Movie", movieSchema);
