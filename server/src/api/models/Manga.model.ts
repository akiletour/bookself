import mongoose from "mongoose";
import {MangaDocument} from "../../interfaces/Manga.interface";

const Schema = mongoose.Schema;

const mangaSchema = new Schema<MangaDocument>({
  provider: Number,
  providerId: String,
  status: String,
  title: String,
  releaseDate: Number,
  cover: String,
  genres: [String],
  themes: [String],
  demographic: String,
  authors: [
    {
      name: String,
      profile: String,
    },
  ],
  artists: [
    {
      name: String,
      profile: String,
    },
  ],
  volumes: [
    {
      volumeNumber: Number,
      cover: String,
    },
  ],
  rating: Number,
  summary: String,
  type: String,
});

export default mongoose.model("Manga", mangaSchema);
