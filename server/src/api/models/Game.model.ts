import mongoose from "mongoose";
import {GameDocument} from "../../interfaces/Game.interface.ts";

const Schema = mongoose.Schema;

const gameSchema = new Schema<GameDocument>({
  provider: Number,
  providerId: String,
  title: String,
  releaseDate: Date,
  cover: String,
  genres: [String],
  artworks: [String],
  involvedCompanies: [
    {
      name: String,
      role: String,
      logo: String,
    },
  ],
  category: String,
  dlcs: [
    {
      providerId: Number,
      name: String,
      cover: String,
      releaseDate: Date,
    },
  ],
  franchise: String,
  platforms: [String],
  rating: Number,
  remakes: [String],
  summary: String,
  status: Number,
  themes: [String],
  videos: [String],
  hltb: {
    main: {
      type: Number,
      required: false,
      default: null,
    },
    mainExtra: {
      type: Number,
      required: false,
      default: null,
    },
    completionist: {
      type: Number,
      required: false,
      default: null,
    },
  },
  type: String,
});

export default mongoose.model("Game", gameSchema);
