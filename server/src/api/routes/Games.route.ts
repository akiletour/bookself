import {Request, Response, NextFunction, Router} from "express";
import {authenticateToken, findProvider} from "../../utils/helpers.util.ts";
import {IGDBProvider} from "../../services/IGDBProvider.service.ts";
import {GameController} from "../controllers/Game.controller.ts";
import {ServerError} from "../../types/CustomErrors.type.ts";
import {GameDocumentsPaginated} from "../../interfaces/Game.interface.ts";
import asyncHandler from "express-async-handler";

const router: Router = Router();

const providers = [IGDBProvider];

// GET /api/games/user/:userId
router.get(
  "/games/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const gameController = new GameController();

    try {
      const result = (await gameController.get(req)) as GameDocumentsPaginated;

      if (result.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/games/user/:userId
router.post(
  "/games/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported game provider"));
    } else {
      const gameController = new GameController(provider!);

      try {
        const result = await gameController.add(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// POST /api/games/user/:userId/list/:listId
router.post(
  "/games/user/:userId/list/:listId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported game provider"));
    } else {
      const gameController = new GameController(provider!);

      try {
        const result = await gameController.addToList(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// POST /api/games/:gameId/user/:userId
router.post(
  "/games/:gameId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const gameController = new GameController();

    try {
      const result = await gameController.updateUserGameDLCs(req);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// DELETE /api/games/:gameId/user/:userId
router.delete(
  "/games/:gameId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const gameController = new GameController();

    try {
      const result = await gameController.updateUserGameDLCs(req);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// PATCH /api/games/:gameId
router.patch(
  "/games/:gameId",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported game provider"));
    } else {
      const gameController = new GameController(provider!);

      try {
        const result = await gameController.update(req);
        res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

export default router;
