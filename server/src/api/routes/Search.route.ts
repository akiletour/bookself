import {Request, Response, NextFunction, Router} from "express";
import {IGDBProvider} from "../../services/IGDBProvider.service.ts";
import {GameController} from "../controllers/Game.controller.ts";
import {GameDocumentsPaginated} from "../../interfaces/Game.interface.ts";
import {MovieDocumentsPaginated} from "../../interfaces/Movie.interface.ts";
import {SeriesController} from "../controllers/Series.controller.ts";
import {MovieController} from "../controllers/Movie.controller.ts";
import {TMDBProvider} from "../../services/TMDBProvider.service.ts";
import asyncHandler from "express-async-handler";
import {MangaController} from "../controllers/Manga.controller.ts";
import {MangaDexProvider} from "../../services/MangaDexProvider.service.ts";
import {MangaDocumentsPaginated} from "../../interfaces/Manga.interface.ts";
import {ComicVineProvider} from "../../services/ComicVineProvider.service.ts";
import {ComicController} from "../controllers/Comic.controller.ts";
import {ComicDocumentsPaginated} from "../../interfaces/Comic.interface.ts";

const router: Router = Router();

// GET /api/movies/:searchTerm
router.get(
  "/search/movies/:searchTerm",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const moviesProvider = new TMDBProvider();
    const movieController = new MovieController(moviesProvider);

    try {
      const result = await movieController.search(req);
      const searchResult = result as MovieDocumentsPaginated;

      if (searchResult.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// GET /api/series/:searchTerm
router.get(
  "/search/series/:searchTerm",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const seriesProvider = new TMDBProvider();
    const seriesController = new SeriesController(seriesProvider);

    try {
      const result = await seriesController.search(req);
      const searchResult = result as MovieDocumentsPaginated;

      if (searchResult.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// GET /api/games/:searchTerm
router.get(
  "/search/games/:searchTerm",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const gamesProvider = new IGDBProvider();
    const gameController = new GameController(gamesProvider);

    try {
      const result = await gameController.search(req);
      const searchResult = result as GameDocumentsPaginated;

      if (searchResult.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// GET /api/mangas/:searchTerm
router.get(
  "/search/mangas/:searchTerm",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const mangasProvider = new MangaDexProvider();
    const mangaController = new MangaController(mangasProvider);

    try {
      const result = await mangaController.search(req);
      const searchResult = result as MangaDocumentsPaginated;

      if (searchResult.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// GET /api/comics/:searchTerm
router.get(
  "/search/comics/:searchTerm",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const comicsProvider = new ComicVineProvider();
    const comicController = new ComicController(comicsProvider);

    try {
      const result = await comicController.search(req);
      const searchResult = result as ComicDocumentsPaginated;

      if (searchResult.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

export default router;
