import {Request, Response, NextFunction, Router} from "express";
import {authenticateToken, findProvider} from "../../utils/helpers.util.ts";
import {ServerError} from "../../types/CustomErrors.type.ts";
import asyncHandler from "express-async-handler";
import {ComicDocumentsPaginated} from "../../interfaces/Comic.interface.ts";
import {ComicController} from "../controllers/Comic.controller.ts";
import {ComicVineProvider} from "../../services/ComicVineProvider.service.ts";

const router: Router = Router();

const providers = [ComicVineProvider];

// GET /api/comics/user/:userId
router.get(
  "/comics/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const comicController = new ComicController();

    try {
      const result = (await comicController.get(
        req
      )) as ComicDocumentsPaginated;

      if (result.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/comics/user/:userId
router.post(
  "/comics/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported comic provider"));
    } else {
      const comicController = new ComicController(provider!);

      try {
        const result = await comicController.add(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// POST /api/comics/user/:userId/list/:listId
router.post(
  "/comics/user/:userId/list/:listId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported comic provider"));
    } else {
      const comicController = new ComicController(provider!);

      try {
        const result = await comicController.addToList(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// POST /api/comics/:comicId/user/:userId
router.post(
  "/comics/:comicId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const comicController = new ComicController();

    try {
      const result = await comicController.updateUserComicProgress(req);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// DELETE /api/comics/:comicId/user/:userId
router.delete(
  "/comics/:comicId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const comicController = new ComicController();

    try {
      const result = await comicController.updateUserComicProgress(req);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// PATCH /api/comics/:comicId
router.patch(
  "/comics/:comicId",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported comic provider"));
    } else {
      const comicController = new ComicController(provider!);

      try {
        const result = await comicController.update(req);
        res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

export default router;
