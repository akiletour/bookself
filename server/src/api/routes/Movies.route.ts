import {Request, Response, NextFunction, Router} from "express";
import {authenticateToken, findProvider} from "../../utils/helpers.util.ts";
import {TMDBProvider} from "../../services/TMDBProvider.service.ts";
import {ServerError} from "../../types/CustomErrors.type.ts";
import {MovieController} from "../controllers/Movie.controller.ts";
import {MovieDocumentsPaginated} from "../../interfaces/Movie.interface.ts";
import asyncHandler from "express-async-handler";

const router: Router = Router();

const providers = [TMDBProvider];

// GET /api/movies/user/:userId
router.get(
  "/movies/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const movieController = new MovieController();

    try {
      const result = (await movieController.get(
        req
      )) as MovieDocumentsPaginated;

      if (result.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/movies/user/:userId
router.post(
  "/movies/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported movie provider"));
    } else {
      const movieController = new MovieController(provider!);

      try {
        const result = await movieController.add(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// POST /api/movies/user/:userId/list/:listId
router.post(
  "/movies/user/:userId/list/:listId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported movie provider"));
    } else {
      const movieController = new MovieController(provider!);

      try {
        const result = await movieController.addToList(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

export default router;
