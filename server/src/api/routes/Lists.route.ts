import {Request, Response, Router, NextFunction} from "express";
import {authenticateToken} from "../../utils/helpers.util.ts";
import {List} from "../../interfaces/User.interface.ts";
import {ServerError} from "../../types/CustomErrors.type.ts";
import asyncHandler from "express-async-handler";
import {ListController} from "../controllers/List.controller.ts";

const router: Router = Router();

// GET /api/lists/user/:userId
router.get(
  "/lists/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const listController = new ListController();

    try {
      const result = (await listController.get(req)) as List[];
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// GET /api/lists/:listId/user/:userId/
router.get(
  "/lists/:listId/user/:userId/",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const listController = new ListController();

    try {
      const result = (await listController.getById(req)) as List;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/lists/user/:userId
router.post(
  "/lists/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const listController = new ListController();

    try {
      const result = (await listController.add(req)) as List;
      res.status(201).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// PATCH /api/lists/:listId/user/:userId
router.patch(
  "/lists/:listId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const listController = new ListController();

    try {
      const result = (await listController.update(req)) as List;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// DELETE /api/lists/:listId/user/:userId
router.delete(
  "/lists/:listId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const listController = new ListController();

    try {
      const result = (await listController.delete(req)) as boolean;
      if (result) {
        res.status(200).json(result);
      } else {
        throw new ServerError("Unknown error");
      }
    } catch (error) {
      next(error);
    }
  })
);

export default router;
