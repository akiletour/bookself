import {Request, Response, Router, NextFunction} from "express";
import {authenticateToken, findProvider} from "../../utils/helpers.util.ts";
import {SeriesDocumentsPaginated} from "../../interfaces/Series.interface.ts";
import {SeriesController} from "../controllers/Series.controller.ts";
import {ServerError} from "../../types/CustomErrors.type.ts";
import {TMDBProvider} from "../../services/TMDBProvider.service.ts";
import asyncHandler from "express-async-handler";

const router: Router = Router();

const providers = [TMDBProvider];

// GET /api/series/user/:userId
router.get(
  "/series/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const seriesController = new SeriesController();

    try {
      const result = (await seriesController.get(
        req
      )) as SeriesDocumentsPaginated;

      if (result.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/series/user/:userId
router.post(
  "/series/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported series provider"));
    } else {
      const seriesController = new SeriesController(provider!);

      try {
        const result = await seriesController.add(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// POST /api/series/user/:userId/list/:listId
router.post(
  "/series/user/:userId/list/:listId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported series provider"));
    } else {
      const seriesController = new SeriesController(provider!);

      try {
        const result = await seriesController.addToList(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// PATCH /api/series/:seriesId
router.patch(
  "/series/:seriesId",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported game provider"));
    } else {
      const seriesController = new SeriesController(provider!);

      try {
        const result = await seriesController.update(req);
        res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// PATCH /api/series/:seriesId/user/:userId
router.patch(
  "/series/:seriesId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const seriesController = new SeriesController();

    try {
      const result = await seriesController.updateUserSeries(req);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

export default router;
