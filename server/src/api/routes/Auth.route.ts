import {Request, Response, Router, NextFunction} from "express";
import {AuthController} from "../controllers/Auth.controller.ts";
import {UserDocument} from "../../interfaces/User.interface.ts";
import {ServerError} from "../../types/CustomErrors.type.ts";
import asyncHandler from "express-async-handler";

const router: Router = Router();

// POST /login
router.post(
  "/login",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const authController = new AuthController();

    try {
      const result = (await authController.logIn(req)) as UserDocument;
      if (result) {
        res.status(200).json(result);
      } else {
        throw new ServerError("Unknown error");
      }
    } catch (error) {
      next(error);
    }
  })
);

// POST /refresh_token
router.post(
  "/refresh_token",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const authController = new AuthController();

    try {
      const result = (await authController.refreshToken(req)) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /signup
router.post(
  "/signup",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const authController = new AuthController();

    try {
      const result = (await authController.signUp(req)) as UserDocument;
      res.status(201).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /activate_account
router.post(
  "/activate_account",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const authController = new AuthController();

    try {
      const result = (await authController.activateAccount(
        req
      )) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /activate_account
router.post(
  "/send_activation_email",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const authController = new AuthController();

    try {
      const result = (await authController.sendActivationEmail(
        req
      )) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /send_reset_password_email
router.post(
  "/send_reset_password_email",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const authController = new AuthController();

    try {
      const result = (await authController.sendResetPasswordEmail(
        req
      )) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /verify_reset_token
router.post(
  "/verify_reset_token",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const authController = new AuthController();

    try {
      const result = (await authController.verifyResetToken(
        req
      )) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /reset_password
router.post(
  "/reset_password",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const authController = new AuthController();

    try {
      const result = (await authController.resetPassword(req)) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

export default router;
