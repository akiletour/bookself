import {Request, Response, Router, NextFunction} from "express";
import {authenticateToken} from "../../utils/helpers.util.ts";
import {WorkController} from "../controllers/Works.controller.ts";
import {UserDocument} from "../../interfaces/User.interface.ts";
import asyncHandler from "express-async-handler";
import {WorksModelsPaginated} from "../../types/WorksModel.type.ts";

const router: Router = Router();

// GET /api/works/user/:userId
router.get(
  "/works/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const workController = new WorkController();

    try {
      const result = (await workController.get(req)) as WorksModelsPaginated;

      if (result.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// GET /api/works/user/:userId/list/:listId
router.get(
  "/works/user/:userId/list/:listId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const workController = new WorkController();

    try {
      const result = (await workController.getFromList(
        req
      )) as WorksModelsPaginated;

      if (result.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// PATCH /api/works/:workId/user/:userId
router.patch(
  "/works/:workId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const workController = new WorkController();

    try {
      const result = (await workController.updateUserWorkDate(
        req
      )) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// DELETE /api/works/:workType/:workId/user/:userId
router.delete(
  "/works/:workType/:workId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const workController = new WorkController();

    try {
      const result = (await workController.deleteUserWorkById(
        req
      )) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// DELETE /api/works/:workId/user/:userId/list/:listId
router.delete(
  "/works/:workId/user/:userId/list/:listId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const workController = new WorkController();

    try {
      const result = (await workController.deleteUserWorkFromList(
        req
      )) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

export default router;
