import {Request, Response, NextFunction, Router} from "express";
import {authenticateToken, findProvider} from "../../utils/helpers.util.ts";
import {ServerError} from "../../types/CustomErrors.type.ts";
import asyncHandler from "express-async-handler";
import {MangaDocumentsPaginated} from "../../interfaces/Manga.interface.ts";
import {MangaController} from "../controllers/Manga.controller.ts";
import {MangaDexProvider} from "../../services/MangaDexProvider.service.ts";

const router: Router = Router();

const providers = [MangaDexProvider];

// GET /api/mangas/user/:userId
router.get(
  "/mangas/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const mangaController = new MangaController();

    try {
      const result = (await mangaController.get(
        req
      )) as MangaDocumentsPaginated;

      if (result.data.length > 0) {
        res.status(200).json(result);
      } else {
        res.status(204).json(result);
      }
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/mangas/user/:userId
router.post(
  "/mangas/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported manga provider"));
    } else {
      const mangaController = new MangaController(provider!);

      try {
        const result = await mangaController.add(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// POST /api/mangas/user/:userId/list/:listId
router.post(
  "/mangas/user/:userId/list/:listId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported manga provider"));
    } else {
      const mangaController = new MangaController(provider!);

      try {
        const result = await mangaController.addToList(req);
        res.status(201).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

// POST /api/mangas/:mangaId/user/:userId
router.post(
  "/mangas/:mangaId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const mangaController = new MangaController();

    try {
      const result = await mangaController.updateUserMangaProgress(req);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// DELETE /api/mangas/:mangaId/user/:userId
router.delete(
  "/mangas/:mangaId/user/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const mangaController = new MangaController();

    try {
      const result = await mangaController.updateUserMangaProgress(req);
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// PATCH /api/mangas/:mangaId
router.patch(
  "/mangas/:mangaId",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const providerId = req.body.provider;
    const provider = findProvider(providerId, providers);

    if (!provider) {
      next(new ServerError("Unsupported manga provider"));
    } else {
      const mangaController = new MangaController(provider!);

      try {
        const result = await mangaController.update(req);
        res.status(200).json(result);
      } catch (error) {
        next(error);
      }
    }
  })
);

export default router;
