import {Request, Response, Router, NextFunction} from "express";
import multer from "multer";
import {authenticateToken} from "../../utils/helpers.util.ts";
import {
  UserActivity,
  UserDocument,
  UserDocumentsPaginated,
} from "../../interfaces/User.interface.ts";
import {UserController} from "../controllers/User.controller.ts";
import {ServerError} from "../../types/CustomErrors.type.ts";
import asyncHandler from "express-async-handler";

const router: Router = Router();

const storage = multer.diskStorage({
  destination: "images/profile/",
  filename: function (req, file, cb) {
    cb(null, `${req.params.userId}_${Date.now()}_${file.originalname}`);
  },
});

const upload = multer({
  storage,
  limits: {
    fileSize: 4000000, // 4MB
  },
});

// GET /api/users
router.get(
  "/users",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const userController = new UserController();

    try {
      const result = (await userController.get(req)) as UserDocumentsPaginated;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// GET /api/users/:userId
router.get(
  "/users/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const userController = new UserController();

    try {
      const result = (await userController.getById(req)) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// GET /api/users/:userId/activity
router.get(
  "/users/:userId/activity",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const userController = new UserController();

    try {
      const result = (await userController.getUserActivity(
        req
      )) as UserActivity;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/users
router.post(
  "/users",
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const userController = new UserController();

    try {
      const result = (await userController.add(req)) as UserDocument;
      res.status(201).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// PATCH /api/users/:userId
router.patch(
  "/users/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const userController = new UserController();

    try {
      const result = (await userController.update(req)) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// DELETE /api/users/:userId
router.delete(
  "/users/:userId",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const userController = new UserController();

    try {
      const result = (await userController.delete(req)) as boolean;
      if (result) {
        res.status(200).json(result);
      } else {
        throw new ServerError("Unknown error");
      }
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/users/:userId/picture
router.post(
  "/users/:userId/picture",
  authenticateToken,
  upload.single("file"),
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const userController = new UserController();

    try {
      const result = (await userController.uploadProfilePicture(
        req
      )) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

// POST /api/users/:userId/password
router.post(
  "/users/:userId/password",
  authenticateToken,
  asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
    const userController = new UserController();

    try {
      const result = (await userController.updatePassword(req)) as UserDocument;
      res.status(200).json(result);
    } catch (error) {
      next(error);
    }
  })
);

export default router;
