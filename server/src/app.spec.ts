import chai from "chai";
import chaiHttp from "chai-http";
import app from "./app.ts";

const {expect} = chai;
chai.use(chaiHttp);

describe("App", () => {
  describe("App routes are working", () => {
    it('should return a 200 status for "/doc" route', (done) => {
      chai
        .request(app)
        .get("/doc")
        .end((_err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
