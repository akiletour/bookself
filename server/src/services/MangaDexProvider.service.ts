import {Request} from "express";
import {WorksTypes} from "../enums/WorksTypes.enum.ts";
import {
  MangaDocument,
  MangaDocumentsPaginated,
  SearchItem,
  Volume,
} from "../interfaces/Manga.interface";
import {Provider} from "../interfaces/Provider.interface.ts";
import {
  BadRequestError,
  BaseError,
  ConflictError,
  NoContentError,
  ServerError,
  UnauthorizedError,
  UnprocessableEntityError,
} from "../types/CustomErrors.type.ts";
import {handleErrors} from "../utils/helpers.util.ts";
import {MangasProviders} from "../enums/Manga.enum.ts";
import Manga from "../api/models/Manga.model.ts";
import User from "../api/models/User.model.ts";
import axios from "axios";
import {
  MangaDexManga,
  Relationship,
} from "../interfaces/providers/Mangadex.interface.ts";
import {
  downloadAndSaveImage,
  getRating,
  getRelationshipsNames,
  getTagsNames,
  getVolumes,
  getMangaDetails,
} from "../utils/providers/mangadex.util.ts";

export class MangaDexProvider implements Provider {
  apiUrl = "https://api.mangadex.org";

  async search(req: Request): Promise<MangaDocumentsPaginated | BaseError> {
    const search = req.params.searchTerm;
    const page = req.query.page ? parseInt(req.query.page as string) : 1;

    const apiUrl = `${this.apiUrl}/manga?title=${search}&includes[]=cover_art&limit=20`;

    try {
      const response = await axios.get(apiUrl);

      if (response.data && response.data.data.length > 0) {
        const mangas: Array<MangaDocument> = [];
        response.data.data.map((manga: MangaDexManga) => {
          const coverRelationship = manga.relationships?.find(
            (relationship) => relationship.type === "cover_art"
          );

          const mangaConverted: SearchItem = {
            provider: MangasProviders.mangadex,
            providerId: manga.id,
            title: manga.attributes.title.en,
            type: WorksTypes.Manga,
            summary: manga.attributes.description?.["en"] ?? undefined,
            releaseDate: manga.attributes.year,
            cover: coverRelationship
              ? `/${manga.id}/${coverRelationship.attributes?.fileName}`
              : undefined,
          };

          mangas.push(new Manga(mangaConverted));
        });

        const pagination = {
          page,
          totalPages: Math.ceil(response.data.total / 20),
        };

        return {data: mangas, pagination};
      } else {
        throw new NoContentError("No results found for the given query");
      }
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async add(req: Request, listId?: string): Promise<MangaDocument | BaseError> {
    const {providerId, provider} = req.body;
    const userId = req.params.userId;

    if (!providerId || provider === undefined) {
      throw new BadRequestError("Missing required fields");
    }

    const authHeader = req.headers["authorization"];
    if (!authHeader) {
      throw new UnauthorizedError("Authorization failure");
    }

    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    try {
      let manga = await Manga.findOne({providerId: providerId.toString()});

      if (!manga) {
        const mangaDetailsResponse = await getMangaDetails(
          providerId.toString()
        );

        const details = mangaDetailsResponse.data;

        const [genres, themes, authors, artists, rating] = await Promise.all([
          getTagsNames("genre", details.attributes.tags),
          getTagsNames("theme", details.attributes.tags),
          getRelationshipsNames("author", details.relationships),
          getRelationshipsNames("artist", details.relationships),
          getRating(details.id),
        ]);

        let volumes: Volume[] = [];

        const volumesResult = (await getVolumes(providerId)) as Volume[];
        volumes = await Promise.all(volumesResult);

        const coverRelationship = details.relationships?.find(
          (relationship: Relationship) => relationship.type === "cover_art"
        );
        const coverImage = coverRelationship
          ? await downloadAndSaveImage(
              `${details.id}/${coverRelationship.attributes?.fileName}`,
              251
            )
          : null;

        const mangaDatas = {
          ...req.body,
          title: details.attributes.title.en,
          type: WorksTypes.Manga,
          status: details.attributes.status,
          summary: details.attributes.description?.["en"] ?? undefined,
          releaseDate: details.attributes.year,
          cover: coverImage ? `/${coverImage.replace("/", "-")}` : undefined,
          genres,
          themes,
          demographic: details.attributes.publicationDemographic,
          authors,
          artists,
          volumes,
          rating,
        };

        manga = new Manga(mangaDatas);
        await manga.save();
      }

      if (!listId) {
        const hasDuplicates = user.mangas.some(
          (mangaRef) => mangaRef._id.toString() === manga?._id.toString()
        );

        if (hasDuplicates) {
          throw new ConflictError("The specified manga has already been added");
        }

        user.mangas.push({
          _id: manga._id,
          additionDate: new Date(Date.now()),
          discoveryDate: new Date(Date.now()),
        });
      } else {
        const targetList = user.lists.find(
          (list) => list._id!.toString() === listId
        );
        if (targetList) {
          const hasDuplicates = targetList.works!.some(
            (mangaRef) => mangaRef._id.toString() === manga?._id.toString()
          );

          if (hasDuplicates) {
            throw new ConflictError(
              "The specified manga has already been added to this list"
            );
          }

          targetList.works!.push({
            model: "Manga",
            _id: manga._id,
            additionDate: new Date(Date.now()),
          });
        } else {
          throw new UnprocessableEntityError("Could not find list");
        }
      }

      await user.save();
      return manga;
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async update(req: Request): Promise<MangaDocument | BaseError> {
    const id = req.params.mangaId;

    const manga = await Manga.findById(id);

    if (manga) {
      const mangaDetailsResponse = await getMangaDetails(manga.providerId);
      const details = mangaDetailsResponse.data;

      if (details) {
        manga.status = details.attributes.status;

        let volumes: Volume[] = [];
        const volumesResult = (await getVolumes(manga.providerId)) as Volume[];
        volumes = await Promise.all(volumesResult);

        if (volumes) {
          manga.volumes = volumes;
        }

        manga.save();
        return manga;
      } else {
        throw new ServerError("Unknown error");
      }
    } else {
      throw new UnprocessableEntityError("No manga found for the given query");
    }
  }

  static supports(providerId: number): boolean {
    return providerId === MangasProviders.mangadex;
  }
}
