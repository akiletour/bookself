import {Request} from "express";
import {Provider} from "../interfaces/Provider.interface.ts";
import {
  DLC,
  GameDocument,
  GameDocumentsPaginated,
  HLTB,
  SearchItem,
} from "../interfaces/Game.interface";
import {
  BadRequestError,
  BaseError,
  ConflictError,
  NoContentError,
  UnauthorizedError,
  UnprocessableEntityError,
} from "../types/CustomErrors.type.ts";
import axios from "axios";
import Game from "../api/models/Game.model.ts";
import User from "../api/models/User.model.ts";
import {WorksTypes} from "../enums/WorksTypes.enum.ts";
import {GamesProviders} from "../enums/Game.enum.ts";
import {IGDBGame} from "../interfaces/providers/IGDB.interface.ts";
import {
  downloadAndSaveImage,
  extractTwitchAccessToken,
  getDLCs,
  getExpansions,
  getGameDetails,
  getInvolvedCompanies,
  getNames,
  getVideoIds,
} from "../utils/providers/igdb.util.ts";
import {handleErrors} from "../utils/helpers.util.ts";
import {getHowLongToBeatDatas} from "../utils/providers/hltb.util.ts";

export class IGDBProvider implements Provider {
  apiUrl = "https://api.igdb.com/v4";

  async search(req: Request): Promise<GameDocumentsPaginated | BaseError> {
    const search = req.params.searchTerm;
    const page = req.query.page ? parseInt(req.query.page as string) : 1;
    const offset = (page - 1) * 20;

    const apiUrl = `${this.apiUrl}/games`;

    const headers = {
      "Content-Type": "text/plain",
      "Client-ID": process.env.IGDB_PROVIDER_ID!,
      Authorization: `${req.headers["authorization"]}`,
    };
    const body = `search "${search}"; f category,cover.image_id,first_release_date,name,status; where version_parent = n & first_release_date != n & category = (0,3,4,8,9,10) & (status = n | status != (5,6,7,8)); limit 20; offset ${offset};`;

    try {
      const response = await axios.post(apiUrl, body, {headers});

      if (response.data && response.data.length > 0) {
        const games: Array<GameDocument> = [];

        response.data.map((game: IGDBGame) => {
          const gameConverted: SearchItem = {
            provider: GamesProviders.igdb,
            providerId: game.id,
            title: game.name,
            type: WorksTypes.Game,
            category: game.category,
            summary: game.summary,
            releaseDate: new Date(game.first_release_date * 1000),
            cover: game.cover ? "/" + game.cover.image_id + ".jpg" : undefined,
          };

          games.push(new Game(gameConverted));
        });

        const apiCountUrl = `${this.apiUrl}/games/count`;
        const countResponse = await axios.post(apiCountUrl, body, {headers});
        const pagination = {
          page,
          totalPages: Math.ceil(countResponse.data.count / 20),
        };

        return {data: games, pagination};
      } else {
        throw new NoContentError("No results found for the given query");
      }
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async add(req: Request, listId?: string): Promise<GameDocument | BaseError> {
    const {providerId, provider} = req.body;
    const userId = req.params.userId;

    if (!providerId || provider === undefined) {
      throw new BadRequestError("Missing required fields");
    }

    const authHeader = req.headers["authorization"];
    if (!authHeader) {
      throw new UnauthorizedError("Authorization failure");
    }

    const twitchAccessToken: string =
      await extractTwitchAccessToken(authHeader);

    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    try {
      let game = await Game.findOne({providerId: providerId.toString()});

      if (!game) {
        const gameDetailsResponse = await getGameDetails(
          providerId.toString(),
          twitchAccessToken
        );

        const details = gameDetailsResponse[0];
        const [genres, themes, videos, platforms, involvedCompanies] =
          await Promise.all([
            getNames(details.genres),
            getNames(details.themes),
            getVideoIds(details.videos),
            getNames(details.platforms),
            getInvolvedCompanies(details.involved_companies, twitchAccessToken),
          ]);

        let artworks;
        if (details.artworks && details.artworks.length > 0) {
          artworks = await Promise.all(
            details.artworks.map((artwork: {image_id: string}) =>
              downloadAndSaveImage(artwork.image_id, 800)
            )
          );
        }

        let dlcs: DLC[] = [];
        let expansions: DLC[] = [];

        if (details.dlcs && details.dlcs.length > 0) {
          const dlcResults = (await getDLCs(
            details.dlcs,
            twitchAccessToken
          )) as DLC[];
          dlcs = await Promise.all(dlcResults);
        }

        if (details.expansions && details.expansions.length > 0) {
          const expansionsResults = (await getExpansions(
            details.expansions,
            twitchAccessToken
          )) as DLC[];
          expansions = await Promise.all(expansionsResults);
        }

        const allDlcs = [...dlcs, ...expansions];

        const coverImage =
          details.cover && details.cover.image_id
            ? await downloadAndSaveImage(details.cover.image_id, 251)
            : null;

        const gameDatas = {
          ...req.body,
          title: details.name,
          releaseDate: new Date(details.first_release_date * 1000),
          cover: coverImage ? `/${coverImage}.jpg` : null,
          summary: details.summary,
          category: details.category,
          genres,
          themes,
          artworks,
          videos,
          dlcs: allDlcs,
          platforms,
          rating: details.rating,
          remakes: details.remakes ?? null,
          involvedCompanies,
          franchise: details.franchise ? details.franchise.name : null,
          type: WorksTypes.Game,
        };

        game = new Game(gameDatas);
        await game.save();
      }

      if (!listId) {
        const hasDuplicates = user.games.some(
          (gameRef) => gameRef._id.toString() === game?._id.toString()
        );

        if (hasDuplicates) {
          throw new ConflictError("The specified game has already been added");
        }

        user.games.push({
          _id: game._id,
          additionDate: new Date(Date.now()),
          discoveryDate: new Date(Date.now()),
        });
      } else {
        const targetList = user.lists.find(
          (list) => list._id!.toString() === listId
        );
        if (targetList) {
          const hasDuplicates = targetList.works!.some(
            (gameRef) => gameRef._id.toString() === game?._id.toString()
          );

          if (hasDuplicates) {
            throw new ConflictError(
              "The specified game has already been added to this list"
            );
          }

          targetList.works!.push({
            model: "Game",
            _id: game._id,
            additionDate: new Date(Date.now()),
          });
        } else {
          throw new UnprocessableEntityError("Could not find list");
        }
      }

      await user.save();
      return game;
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async update(req: Request): Promise<GameDocument | BaseError> {
    const id = req.params.gameId;

    const twitchAccessToken = req.headers["authorization"];

    if (!twitchAccessToken) {
      throw new UnauthorizedError("Authorization failure");
    }

    const game = await Game.findById(id);

    if (game) {
      try {
        const gameDetailsResponse = await getGameDetails(
          game.providerId.toString(),
          req.headers["authorization"]!,
          true
        );

        const details = gameDetailsResponse[0];

        let dlcs: DLC[] = [];
        let expansions: DLC[] = [];

        if (details.dlcs && details.dlcs.length > 0) {
          const dlcResults = (await getDLCs(
            details.dlcs,
            twitchAccessToken
          )) as DLC[];
          dlcs = await Promise.all(dlcResults);
        }

        if (details.expansions && details.expansions.length > 0) {
          const expansionsResults = (await getExpansions(
            details.expansions,
            twitchAccessToken
          )) as DLC[];
          expansions = await Promise.all(expansionsResults);
        }

        const allDlcs = [...dlcs, ...expansions];

        if (game.dlcs) {
          const newDlcs = allDlcs.filter((newDlc) => {
            return !game.dlcs!.some(
              (existingDlc) => existingDlc.providerId === newDlc.providerId
            );
          });

          game.dlcs = [...game.dlcs, ...newDlcs];
        } else {
          game.dlcs = allDlcs;
        }

        const hltb = (await getHowLongToBeatDatas(game.title)) as HLTB;

        if (hltb) {
          game.hltb = hltb;
        }

        game.save();

        return game;
      } catch (err: unknown) {
        throw handleErrors(err);
      }
    } else {
      throw new UnprocessableEntityError("No game found for the given query");
    }
  }

  static supports(providerId: number): boolean {
    return providerId === GamesProviders.igdb;
  }
}
