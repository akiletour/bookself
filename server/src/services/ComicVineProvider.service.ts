import {Request} from "express";
import {WorksTypes} from "../enums/WorksTypes.enum.ts";
import {
  ComicDocument,
  ComicDocumentsPaginated,
  SearchItem,
  Volume,
} from "../interfaces/Comic.interface";
import {Provider} from "../interfaces/Provider.interface.ts";
import {
  BadRequestError,
  BaseError,
  ConflictError,
  NoContentError,
  ServerError,
  UnauthorizedError,
  UnprocessableEntityError,
} from "../types/CustomErrors.type.ts";
import {handleErrors} from "../utils/helpers.util.ts";
import {ComicsProviders} from "../enums/Comic.enum.ts";
import Comic from "../api/models/Comic.model.ts";
import User from "../api/models/User.model.ts";
import axios from "axios";
import {
  downloadAndSaveImage,
  getComicDetails,
  getIssues,
} from "../utils/providers/comicvine.util.ts";
import {
  ComicVineAuthor,
  ComicVineComic,
} from "../interfaces/providers/Comicvine.interface.ts";

export class ComicVineProvider implements Provider {
  apiUrl = "https://comicvine.gamespot.com/api";

  async search(req: Request): Promise<ComicDocumentsPaginated | BaseError> {
    const search = req.params.searchTerm;
    const page = req.query.page ? parseInt(req.query.page as string) : 1;

    const apiUrl = `${this.apiUrl}/search?api_key=${process.env.COMICVINE_PROVIDER_KEY}&format=json&query=${search}&resources=volume&limit=20&page=${page}`;

    try {
      const response = await axios.get(apiUrl);

      if (response.data && response.data.results.length > 0) {
        const comics: Array<ComicDocument> = [];
        response.data.results.map((comic: ComicVineComic) => {
          const comicConverted: SearchItem = {
            provider: ComicsProviders.comicvine,
            providerId: comic.id,
            title: comic.name,
            type: WorksTypes.Comic,
            summary: comic.description,
            publisher: comic.publisher ? comic.publisher.name : undefined,
            releaseDate: parseInt(comic.start_year),
            cover: comic.image.medium_url ? comic.image.medium_url : undefined,
          };

          comics.push(new Comic(comicConverted));
        });

        const pagination = {
          page,
          totalPages: Math.ceil(response.data.number_of_total_results / 20),
        };

        return {data: comics, pagination};
      } else {
        throw new NoContentError("No results found for the given query");
      }
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async add(req: Request, listId?: string): Promise<ComicDocument | BaseError> {
    const {providerId, provider} = req.body;
    const userId = req.params.userId;

    if (!providerId || provider === undefined) {
      throw new BadRequestError("Missing required fields");
    }

    const authHeader = req.headers["authorization"];
    if (!authHeader) {
      throw new UnauthorizedError("Authorization failure");
    }

    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    try {
      let comic = await Comic.findOne({providerId: providerId.toString()});

      if (!comic) {
        // 4050- is identifier for volumes (collection) in ComicVine API
        const comicDetailsResponse = await getComicDetails(
          "4050-" + providerId.toString()
        );

        const details = comicDetailsResponse.results;

        const authors = details.people.map((author: ComicVineAuthor) => ({
          name: author.name as string,
        }));

        let volumes: Volume[] = [];

        const volumesResult = (await getIssues(details.issues)) as Volume[];
        volumes = await Promise.all(volumesResult);

        const coverImage = details.image
          ? await downloadAndSaveImage(details.image.original_url, 251)
          : null;

        const comicDatas = {
          ...req.body,
          title: details.name,
          type: WorksTypes.Comic,
          summary: details.description,
          releaseDate: details.start_year,
          publisher: details.publisher.name,
          cover: coverImage ? `/${coverImage.replace("/", "-")}` : undefined,
          authors,
          volumes,
        };

        comic = new Comic(comicDatas);
        await comic.save();
      }

      if (!listId) {
        const hasDuplicates = user.comics.some(
          (comicRef) => comicRef._id.toString() === comic?._id.toString()
        );

        if (hasDuplicates) {
          throw new ConflictError("The specified comic has already been added");
        }

        user.comics.push({
          _id: comic._id,
          additionDate: new Date(Date.now()),
          discoveryDate: new Date(Date.now()),
        });
      } else {
        const targetList = user.lists.find(
          (list) => list._id!.toString() === listId
        );
        if (targetList) {
          const hasDuplicates = targetList.works!.some(
            (comicRef) => comicRef._id.toString() === comic?._id.toString()
          );

          if (hasDuplicates) {
            throw new ConflictError(
              "The specified comic has already been added to this list"
            );
          }

          targetList.works!.push({
            model: "Comic",
            _id: comic._id,
            additionDate: new Date(Date.now()),
          });
        } else {
          throw new UnprocessableEntityError("Could not find list");
        }
      }

      await user.save();
      return comic;
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async update(req: Request): Promise<ComicDocument | BaseError> {
    const id = req.params.comicId;

    const comic = await Comic.findById(id);

    if (comic) {
      // 4050- is identifier for volumes (collection) in ComicVine API
      const comicDetailsResponse = await getComicDetails(
        "4050-" + comic.providerId
      );

      const details = comicDetailsResponse.results;

      if (details) {
        comic.summary = details.description;

        if (
          (details.issues.length > 0 && !comic.volumes) ||
          (comic.volumes && details.issues.length > comic.volumes?.length)
        ) {
          let volumes: Volume[] = [];

          const volumesResult = (await getIssues(
            details.issues,
            comic.volumes
          )) as Volume[];
          volumes = await Promise.all(volumesResult);

          if (volumes) {
            comic.volumes = volumes;
          }
        }

        comic.save();
        return comic;
      } else {
        throw new ServerError("Unknown error");
      }
    } else {
      throw new UnprocessableEntityError("No comic found for the given query");
    }
  }

  static supports(providerId: number): boolean {
    return providerId === ComicsProviders.comicvine;
  }
}
