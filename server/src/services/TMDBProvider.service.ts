import {Request} from "express";
import {
  Actor,
  Director,
  MovieDocument,
  MovieDocumentsPaginated,
  SearchItem,
} from "../interfaces/Movie.interface";
import {
  Episode,
  Season,
  SeriesDocument,
  SeriesDocumentsPaginated,
} from "../interfaces/Series.interface";
import Movie from "../api/models/Movie.model.ts";
import Series from "../api/models/Series.model.ts";
import User from "../api/models/User.model.ts";
import {Provider} from "../interfaces/Provider.interface";
import {MoviesProviders} from "../enums/Movie.enum.ts";
import {
  BadRequestError,
  BaseError,
  ConflictError,
  NoContentError,
  ServerError,
  UnprocessableEntityError,
} from "../types/CustomErrors.type.ts";
import {handleErrors} from "../utils/helpers.util.ts";
import {WorksTypes} from "../enums/WorksTypes.enum.ts";
import {
  TMDBEpisode,
  TMDBMovie,
  TMDBSeason,
  TMDBSeries,
} from "../interfaces/providers/TMDB.interface";
import axios from "axios";
import {
  downloadAndSaveImage,
  getDetails,
  getMovieCredits,
  getSeasonDetails,
  getSeriesCredits,
} from "../utils/providers/tmdb.util.ts";
import {UserDocument} from "../interfaces/User.interface.ts";

export class TMDBProvider implements Provider {
  apiUrl = "https://api.themoviedb.org/3";

  async search(
    req: Request,
    type: WorksTypes
  ): Promise<MovieDocumentsPaginated | SeriesDocumentsPaginated | BaseError> {
    const search = req.params.searchTerm;
    const page = req.query.page ? parseInt(req.query.page as string) : 1;
    const route = type === WorksTypes.Movie ? "movie" : "tv";

    const apiUrl = `${this.apiUrl}/search/${route}?api_key=${process.env.TMDB_PROVIDER_KEY}&query=${search}&page=${page}`;

    try {
      const response = await axios.get(apiUrl);

      if (response.data && response.data.results.length > 0) {
        const items: Array<MovieDocument | SeriesDocument> = [];

        response.data.results.map((item: TMDBMovie | TMDBSeries) => {
          const itemConverted: SearchItem = {
            provider: MoviesProviders.tmdb,
            providerId: item.id,
            type,
            cover: item.poster_path,
            synopsis: item.overview,
          };

          if (type === WorksTypes.Movie) {
            const movie = item as TMDBMovie;
            itemConverted.title = movie.title;
            itemConverted.releaseDate = movie.release_date;

            items.push(new Movie(itemConverted));
          } else {
            const series = item as TMDBSeries;
            itemConverted.title = series.name;
            itemConverted.releaseDate = series.first_air_date;

            items.push(new Series(itemConverted));
          }
        });

        const pagination = {
          page: response.data.page,
          totalPages: response.data.total_pages,
        };

        return {data: items, pagination};
      } else {
        throw new NoContentError("No results found for the given query");
      }
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async add(
    req: Request,
    type: WorksTypes,
    listId?: string
  ): Promise<MovieDocument | SeriesDocument | BaseError> {
    const {provider, providerId} = req.body;
    const userId = req.params.userId;

    if (!providerId || provider === undefined) {
      throw new BadRequestError("Missing required fields");
    }
    const user = await User.findById(userId);

    if (!user) {
      throw new UnprocessableEntityError("No user found for the given query");
    }

    if (type === WorksTypes.Movie) {
      return this.addMovie(req, user, listId);
    } else {
      return this.addSeries(req, user, listId);
    }
  }

  async update(req: Request): Promise<SeriesDocument | BaseError> {
    const id = req.params.seriesId;

    const series = await Series.findById(id);

    if (series) {
      const details = await getDetails(
        series.providerId.toString(),
        WorksTypes.Series
      );

      if (details) {
        series.status = details.status;

        const seasons: Season[] = await this.getSeasonsDetails(
          series.providerId.toString(),
          details.seasons
        );

        if (seasons) {
          series.numberOfEpisodes = details.number_of_episodes;
          series.numberOfSeasons = details.number_of_seasons;
          series.seasons = seasons;
        }

        series.save();
        return series;
      } else {
        throw new ServerError("Unknown error");
      }
    } else {
      throw new UnprocessableEntityError("No series found for the given query");
    }
  }

  async addMovie(
    req: Request,
    user: UserDocument,
    listId?: string
  ): Promise<MovieDocument | BaseError> {
    const {providerId} = req.body;

    try {
      let movie = await Movie.findOne({providerId: providerId.toString()});

      if (!movie) {
        const movieDetailsResponse = await getDetails(
          providerId.toString(),
          WorksTypes.Movie
        );

        const credits = await getMovieCredits(providerId.toString());

        const genres: string[] = movieDetailsResponse.genres.map(
          (genre: {id: number; name: string}) => genre.name
        );

        // Fetch cover image
        const imageName = movieDetailsResponse.poster_path;

        if (imageName) {
          await downloadAndSaveImage(imageName, "movies", 251);
        }

        const movieDatas = {
          ...req.body,
          title: movieDetailsResponse.title,
          releaseDate: movieDetailsResponse.release_date,
          cover: movieDetailsResponse.poster_path,
          synopsis: movieDetailsResponse.overview,
          genres,
          rating: movieDetailsResponse.vote_average * 10,
          duration: movieDetailsResponse.runtime,
          tagline: movieDetailsResponse.tagline,
          type: WorksTypes.Movie,
        };

        if (credits) {
          // Fetch cast images
          await Promise.all(
            credits.cast.map(async (actor: Actor) => {
              const imageName = actor.profile;
              if (imageName) {
                await downloadAndSaveImage(imageName, "actors", 80);
              }
            })
          );
          movieDatas.cast = credits.cast;

          // Fetch directors images
          await Promise.all(
            credits.directors.map(async (director: Director) => {
              const imageName = director.profile;
              if (imageName) {
                await downloadAndSaveImage(imageName, "directors", 80);
              }
            })
          );
          movieDatas.directors = credits.directors;
        }

        movie = new Movie(movieDatas);
        await movie.save();
      }

      if (!listId) {
        const hasDuplicates = user.movies.some(
          (movieRef) => movieRef._id.toString() === movie?._id.toString()
        );

        if (hasDuplicates) {
          throw new ConflictError("The specified movie has already been added");
        }

        user.movies.push({
          _id: movie._id,
          additionDate: new Date(Date.now()),
          discoveryDate: new Date(Date.now()),
        });
      } else {
        const targetList = user.lists.find(
          (list) => list._id!.toString() === listId
        );
        if (targetList) {
          const hasDuplicates = targetList.works!.some(
            (movieRef) => movieRef._id.toString() === movie?._id.toString()
          );

          if (hasDuplicates) {
            throw new ConflictError(
              "The specified movie has already been added to this list"
            );
          }

          targetList.works!.push({
            model: "Movie",
            _id: movie._id,
            additionDate: new Date(Date.now()),
          });
        } else {
          throw new UnprocessableEntityError("Could not find list");
        }
      }

      await user.save();
      return movie;
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async addSeries(
    req: Request,
    user: UserDocument,
    listId?: string
  ): Promise<SeriesDocument | BaseError> {
    const {providerId} = req.body;

    try {
      let series = await Series.findOne({providerId: providerId.toString()});

      if (!series) {
        const seriesDetailsResponse = await getDetails(
          providerId.toString(),
          WorksTypes.Series
        );

        const credits = await getSeriesCredits(providerId.toString());

        const genres: string[] = seriesDetailsResponse.genres.map(
          (genre: {id: number; name: string}) => genre.name
        );

        const creators: Director[] = await Promise.all(
          seriesDetailsResponse.created_by.map(
            async (creator: {name: string; profile_path: string}) => {
              // Fetch creators images
              const imageName = creator.profile_path;
              if (imageName) {
                await downloadAndSaveImage(imageName, "creators", 80);
              }

              return {
                name: creator.name,
                profile: creator.profile_path,
              };
            }
          )
        );

        const seasons: Season[] = await this.getSeasonsDetails(
          providerId.toString(),
          seriesDetailsResponse.seasons
        );

        // Fetch cover image
        const imageName = seriesDetailsResponse.poster_path;

        if (imageName) {
          await downloadAndSaveImage(imageName, "series", 251);
        }

        const seriesDatas = {
          ...req.body,
          title: seriesDetailsResponse.name,
          releaseDate: seriesDetailsResponse.first_air_date,
          lastAiredDate: seriesDetailsResponse.last_air_date,
          cover: seriesDetailsResponse.poster_path,
          synopsis: seriesDetailsResponse.overview,
          numberOfEpisodes: seriesDetailsResponse.number_of_episodes,
          numberOfSeasons: seriesDetailsResponse.number_of_seasons,
          rating: seriesDetailsResponse.vote_average * 10,
          status: seriesDetailsResponse.status,
          tagline: seriesDetailsResponse.tagline,
          genres,
          creators,
          seasons,
          type: WorksTypes.Series,
        };

        if (credits) {
          // Fetch cast images
          await Promise.all(
            credits.cast.map(async (actor: Actor) => {
              const imageName = actor.profile;
              if (imageName) {
                await downloadAndSaveImage(imageName, "actors", 80);
              }
            })
          );
          seriesDatas.cast = credits.cast;
        }

        series = new Series(seriesDatas);
        await series.save();
      }

      if (!listId) {
        const hasDuplicates = user.series.some(
          (seriesRef) => seriesRef._id.toString() === series?._id.toString()
        );

        if (hasDuplicates) {
          throw new ConflictError(
            "The specified series has already been added"
          );
        }

        user.series.push({
          _id: series._id,
          additionDate: new Date(Date.now()),
          discoveryDate: new Date(Date.now()),
        });
      } else {
        const targetList = user.lists.find(
          (list) => list._id!.toString() === listId
        );
        if (targetList) {
          const hasDuplicates = targetList.works!.some(
            (seriesRef) => seriesRef._id.toString() === series?._id.toString()
          );

          if (hasDuplicates) {
            throw new ConflictError(
              "The specified series has already been added to this list"
            );
          }

          targetList.works!.push({
            model: "Series",
            _id: series._id,
            additionDate: new Date(Date.now()),
          });
        } else {
          throw new UnprocessableEntityError("Could not find list");
        }
      }

      await user.save();
      return series;
    } catch (err: unknown) {
      throw handleErrors(err);
    }
  }

  async getSeasonsDetails(
    providerId: string,
    TMDBSeasons: TMDBSeason[]
  ): Promise<Season[]> {
    const seasons: Season[] = [];

    for (const rawSeason of TMDBSeasons) {
      const season = rawSeason as TMDBSeason;
      const episodes: Episode[] = [];

      const seasonDetails = await getSeasonDetails(
        providerId,
        season.season_number.toString(),
        WorksTypes.Series
      );

      const seasonObject: Season = {
        name: season.name,
        seasonNumber: season.season_number,
        releaseDate: season.air_date,
        synopsis: season.overview,
        numberOfEpisodes: season.episode_count,
        cover: season.poster_path,
        episodes,
      };

      if (seasonDetails) {
        seasonDetails.episodes.forEach((episode: TMDBEpisode) => {
          seasonObject.episodes.push({
            name: episode.name,
            number: episode.episode_number,
            releaseDate: episode.air_date,
            synopsis: episode.overview,
            duration: episode.runtime,
          });
        });
      }

      if (season.air_date && new Date(season.air_date) <= new Date()) {
        seasons.push(seasonObject);
      }
    }

    return seasons;
  }

  static supports(providerId: number): boolean {
    return providerId === MoviesProviders.tmdb;
  }
}
