import app from "./app.ts";
import mongoose from "mongoose";

const PORT: number = process.env.SERVER_PORT
  ? parseInt(process.env.SERVER_PORT)
  : 4200;

if (!process.env.NODE_ENV || process.env.NODE_ENV != "test") {
  app.listen(PORT, () => {
    app.emit("appStarted");
  });

  /**
   * Mongo DB
   */
  mongoose
    .connect(
      `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.duciefa.mongodb.net/${process.env.DB_NAME}?retryWrites=true&w=majority`
    )
    .then()
    .catch((err) => {
      throw err;
    });
}

export {app};
