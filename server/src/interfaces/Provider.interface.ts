import {Request} from "express";
import {BaseError} from "../types/CustomErrors.type";
import {WorksModel, WorksModelsPaginated} from "../types/WorksModel.type";

export interface Provider {
  apiUrl: string;
  search(
    req: Request,
    type?: string
  ): Promise<WorksModelsPaginated | BaseError>;
  add(
    req: Request,
    type?: string,
    listId?: string
  ): Promise<WorksModel | BaseError>;
  update(req: Request, type?: string): Promise<WorksModel | BaseError>;
}

export interface ProviderConstructor {
  new (): Provider;
  supports(providerId: number): boolean;
}
