import {Document} from "mongoose";
import {Work} from "../types/WorksModel.type";
import {Pagination} from "./Pagination.interface";
import {WorksTypes} from "../enums/WorksTypes.enum";
import {MangasProviders} from "../enums/Manga.enum";
import {MangaProgress} from "./User.interface";

export interface MangaDocument extends Document, Work {
  provider: MangasProviders;
  providerId: string;
  themes: string[];
  demographic: string;
  status: string;
  authors?: Author[];
  artists?: Author[];
  volumes?: Volume[];
  summary?: string;
  progress?: MangaProgress[];
}

export interface Author {
  name: string;
  profile?: string;
}

export interface Volume {
  volumeNumber: number;
  cover?: string;
}

export interface MangaDocumentsPaginated {
  data: MangaDocument[];
  pagination: Pagination;
}

export interface SearchItem {
  provider: MangasProviders;
  providerId: string;
  type: WorksTypes;
  cover?: string;
  summary?: string;
  title: string;
  releaseDate?: number;
}
