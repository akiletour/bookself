import {Document} from "mongoose";
import {Work} from "../types/WorksModel.type";
import {Pagination} from "./Pagination.interface";
import {WorksTypes} from "../enums/WorksTypes.enum";
import {MoviesProviders} from "../enums/Movie.enum";

export interface MovieDocument extends Document, Work {
  provider: MoviesProviders;
  providerId: number;
  directors?: Director[];
  cast?: Actor[];
  duration?: string;
  synopsis?: string;
  tagline?: string;
}

export interface Actor {
  name: string;
  character: string;
  profile: string;
}

export interface Director {
  name: string;
  profile: string;
}

export interface MovieDocumentsPaginated {
  data: MovieDocument[];
  pagination: Pagination;
}

export interface SearchItem {
  provider: MoviesProviders;
  providerId: number;
  type: WorksTypes;
  cover: string;
  synopsis: string;
  title?: string;
  releaseDate?: Date;
}
