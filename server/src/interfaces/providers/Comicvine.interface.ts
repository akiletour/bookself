export interface ComicVineComic {
  id: string;
  count_of_issues: number;
  image: {
    medium_url: string;
    screen_url: string;
    original_url: string;
  };
  name: string;
  publisher: {
    api_detail_url: string;
    id: number;
    name: string;
  };
  people: {
    name: string;
  };
  description: string;
  issues: Issue[];
  start_year: string;
  resource_type: string;
}

export interface Issue {
  id: number;
  name: string;
  issue_number: string;
}

export interface ComicVineAuthor {
  id: number;
  name: string;
}
