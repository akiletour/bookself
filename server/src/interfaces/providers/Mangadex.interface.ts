export interface MangaDexManga {
  id: string;
  type: string;
  attributes: Attributes;
  relationships: Relationship[];
}

interface TitleTranslation {
  en: string;
}

interface AltTitleTranslation {
  [key: string]: string;
}

interface DescriptionTranslation {
  [key: string]: string;
}

interface LinkTranslations {
  [key: string]: string;
}
export interface Relationship {
  id: string;
  type: string;
  attributes?: {
    description?: string;
    volume?: string;
    fileName?: string;
    name?: string;
    imageUrl?: string;
    locale?: string;
  };
}

export interface Tag {
  id: string;
  type: string;
  attributes: {
    group?: string;
    name: {
      en: string;
    };
  };
  relationships: Relationship[];
}

interface Attributes {
  title: TitleTranslation;
  altTitles: AltTitleTranslation[];
  description: DescriptionTranslation;
  isLocked: boolean;
  links: LinkTranslations;
  originalLanguage: string;
  lastVolume?: string;
  lastChapter?: string;
  publicationDemographic: string;
  status: string;
  year: number;
  contentRating: string;
  tags: Tag[];
  state: string;
  chapterNumbersResetOnNewVolume: boolean;
  createdAt: string;
  updatedAt: string;
  version: number;
  availableTranslatedLanguages: string[];
  latestUploadedChapter: string;
}
