import {GamesCategories, GamesStatuses} from "../../enums/Game.enum";

export interface IGDBGame {
  id: number;
  artworks: number[];
  category: GamesCategories;
  cover: {
    id: number;
    image_id: string;
  };
  dlcs: number[];
  first_release_date: number;
  franchise: number;
  genres: number[];
  involved_companies: number[];
  name: string;
  platforms: number[];
  rating: number;
  remakes: number[];
  status: GamesStatuses;
  storyline: string;
  summary: string;
  themes: number[];
  videos: number[];
}
