import {Document} from "mongoose";
import {Work} from "../types/WorksModel.type";
import {
  GamesCategories,
  GamesProviders,
  GamesStatuses,
} from "../enums/Game.enum";
import {Pagination} from "./Pagination.interface";
import {WorksTypes} from "../enums/WorksTypes.enum";

export interface GameDocument extends Document, Work {
  provider: GamesProviders;
  providerId: number;
  artworks?: string[];
  involvedCompanies?: Company[];
  category?: GamesCategories;
  dlcs?: DLC[];
  userDlcs?: {
    providerId: number;
  }[];
  franchise?: string;
  platforms: string[];
  remakes?: string[];
  summary?: string;
  status: GamesStatuses;
  themes: string[];
  videos?: string[];
  hltb: HLTB;
}

export interface DLC {
  providerId: number;
  name: string;
  cover?: string;
  releaseDate?: Date;
}

export interface Company {
  name: string;
  role: string;
  logo?: string;
}

export interface HLTB {
  main?: number;
  mainExtra?: number;
  completionist?: number;
}

export interface GameDocumentsPaginated {
  data: GameDocument[];
  pagination: Pagination;
}

export interface SearchItem {
  provider: GamesProviders;
  providerId: number;
  type: WorksTypes;
  cover?: string;
  category: GamesCategories;
  summary: string;
  title?: string;
  releaseDate?: Date;
}
