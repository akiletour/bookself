import {Document} from "mongoose";
import {Work} from "../types/WorksModel.type";
import {Actor, Director} from "./Movie.interface";
import {Pagination} from "./Pagination.interface";
import {WorksTypes} from "../enums/WorksTypes.enum";
import {MoviesProviders} from "../enums/Movie.enum";
import {SeriesProgress} from "./User.interface";

export interface SeriesDocument extends Document, Work {
  provider: MoviesProviders;
  providerId: number;
  lastAiredDate: Date;
  creators?: Director[];
  cast?: Actor[];
  numberOfEpisodes: number;
  numberOfSeasons: number;
  seasons?: Season[];
  synopsis?: string;
  status: string;
  tagline: string;
  progress?: SeriesProgress;
}

export interface Season {
  name: string;
  seasonNumber: number;
  releaseDate: Date;
  synopsis: string;
  numberOfEpisodes: number;
  cover: string;
  episodes: Episode[];
}

export interface Episode {
  name: string;
  number: number;
  releaseDate?: Date;
  synopsis?: string;
  duration?: number;
}

export interface SeriesDocumentsPaginated {
  data: SeriesDocument[];
  pagination: Pagination;
}

export interface SearchItem {
  provider: MoviesProviders;
  providerId: number;
  type: WorksTypes;
  cover: string;
  synopsis: string;
  title?: string;
  releaseDate?: Date;
}
