import {Document} from "mongoose";
import {Work} from "../types/WorksModel.type";
import {Pagination} from "./Pagination.interface";
import {WorksTypes} from "../enums/WorksTypes.enum";
import {ComicsProviders} from "../enums/Comic.enum";
import {ComicProgress} from "./User.interface";

export interface ComicDocument extends Document, Work {
  provider: ComicsProviders;
  providerId: string;
  publisher?: string;
  authors?: Author[];
  volumes?: Volume[];
  summary?: string;
  progress?: ComicProgress[];
}

export interface Author {
  name: string;
  profile?: string;
}

export interface Volume {
  volumeNumber: number;
  volumeName: string;
  cover?: string;
}

export interface ComicDocumentsPaginated {
  data: ComicDocument[];
  pagination: Pagination;
}

export interface SearchItem {
  provider: ComicsProviders;
  providerId: string;
  type: WorksTypes;
  cover?: string;
  summary?: string;
  title: string;
  publisher?: string;
  releaseDate?: number;
}
