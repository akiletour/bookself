import mongoose, {Document, Model, ObjectId} from "mongoose";
import {MovieDocument} from "./Movie.interface";
import {SeriesDocument} from "./Series.interface";
import {GameDocument} from "./Game.interface";
import {Pagination} from "./Pagination.interface";
import {ComicDocument} from "./Comic.interface";
import {MangaDocument} from "./Manga.interface";

export interface UserDocument extends Document {
  email: string;
  firstname?: string;
  lastname?: string;
  password: string;
  username: string;
  picture?: string;
  movies: UserMovie[];
  series: UserSeries[];
  games: UserGame[];
  mangas: UserManga[];
  comics: UserComic[];
  // books: UserBook[];
  lists: List[];
  tokens?: {
    accessToken: string;
    refreshToken: string;
    twitchAccessToken: string;
  };
  activated: boolean;
  createdAt: Date;
  updatedAt: Date;
  theme: string;
  activationToken?: string;
  activationTokenExpiration?: Date;
  resetToken?: string;
  resetTokenExpiration?: Date;
}

export interface UserLogged {
  email: string;
  firstname?: string;
  lastname?: string;
  password: string;
  username: string;
  picture?: string;
  movies: UserMovie[];
  series: UserSeries[];
  games: UserGame[];
  mangas: UserManga[];
  comics: UserComic[];
  // books: UserBook[];
  lists: List[];
  tokens?: {
    accessToken: string;
    refreshToken: string;
    twitchAccessToken: string;
  };
  createdAt: Date;
  updatedAt: Date;
  resetToken?: string;
  resetTokenExpiration?: Date;
}

export interface List {
  _id?: ObjectId;
  name: string;
  description?: string;
  works?: {
    model: string;
    _id: string;
    additionDate: Date;
    cover?: string;
  }[];
  createdAt?: Date;
  updatedAt?: Date;
}

export interface UserMovie {
  _id: {type: mongoose.Schema.Types.ObjectId; ref: MovieDocument};
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
}

export interface UserSeries {
  _id: {type: mongoose.Schema.Types.ObjectId; ref: SeriesDocument};
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
  progress?: SeriesProgress;
}

export interface SeriesProgress {
  season: number;
  episode: number;
  completed: boolean;
}

export interface UserManga {
  _id: {type: mongoose.Schema.Types.ObjectId; ref: MangaDocument};
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
  progress?: MangaProgress[];
}

export interface MangaProgress {
  volume: number;
}

export interface UserComic {
  _id: {type: mongoose.Schema.Types.ObjectId; ref: ComicDocument};
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
  progress?: ComicProgress[];
}

export interface ComicProgress {
  volume: number;
}

export interface UserGame {
  _id: {type: mongoose.Schema.Types.ObjectId; ref: GameDocument};
  additionDate: Date;
  discoveryDate: Date;
  rating?: number;
  dlcs?: {
    providerId: number;
  }[];
}

export interface UserDocumentsPaginated {
  data: UserDocument[];
  pagination: Pagination;
}

export interface UserActivity {
  movies?: number;
  series?: number;
  games?: number;
  // animation?: number,
  mangas?: number;
  comics?: number;
  // books?: number,
}

export interface ActivityItem {
  _id: mongoose.Schema.Types.ObjectId;
  additionDate: Date;
  discoveryDate: Date;
}

export interface UserLoginPayload {
  email: string;
  username: string;
}

export interface UserModel extends Model<UserDocument> {
  findOneByEmail(email: string): Promise<UserDocument | null>;
}
