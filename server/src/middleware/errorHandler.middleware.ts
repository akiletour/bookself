import {Request, Response, NextFunction} from "express";
import {BaseError} from "../types/CustomErrors.type.ts";

export function errorHandler(
  error: Error,
  _req: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  _next: NextFunction
) {
  if (error instanceof BaseError) {
    return res.status(error.status).json({error: error.message});
  }
}
