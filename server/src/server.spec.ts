const PORT: number =
  process.env.NODE_ENV === "test" ? +process.env.TEST_PORT! : 5000;

import chai from "chai";
import chaiHttp from "chai-http";
import http from "http";
import {AddressInfo} from "net";
import {app} from "./server.ts";

const {expect} = chai;
chai.use(chaiHttp);

describe("Check server has started", () => {
  let server: http.Server;

  beforeEach(function (done) {
    server = app.listen(PORT);
    done();
  });

  afterEach(function (done) {
    server.close();
    done();
  });

  it("should exist", function () {
    expect(app).to.exist;
  });

  it("should be listening on a specified port", function () {
    expect((server.address() as AddressInfo).port).to.equal(PORT);
  });

  it("should listen without errors", (done) => {
    chai
      .request(app)
      .get("/doc")
      .end((_err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });
});
