import axios from "axios";
import * as utils from "../helpers.util.ts";
import {IGDBProvider} from "../../services/IGDBProvider.service.ts";
import fs from "fs";

export async function extractTwitchAccessToken(authHeader: string) {
  const tokens = authHeader.split(",").map((token: string) => token.trim());
  return tokens.length >= 2 && tokens[1].startsWith("Bearer ") ? tokens[1] : "";
}

export async function getGameDetails(
  id: string,
  auth: string,
  dlcsOnly: boolean = false
) {
  const apiUrl = `${new IGDBProvider().apiUrl}/games`;

  const headers = {
    "Content-Type": "text/plain",
    "Client-ID": process.env.IGDB_PROVIDER_ID!,
    Authorization: `${auth}`,
  };

  const body = !dlcsOnly
    ? `f artworks.image_id,category,cover.image_id,dlcs.name,expansions.name,first_release_date,release_dates,franchise.name,genres.name,involved_companies.company.name,involved_companies.developer,name,parent_game.name,platforms.name,rating,remakes,status,storyline,summary,themes.name,videos.video_id; where id = (${id});`
    : `f dlcs.name,expansions.name; where id = (${id});`;

  try {
    const response = await axios.post(apiUrl, body, {headers});
    return response?.data;
  } catch (err) {
    throw utils.handleErrors(err);
  }
}

export async function getGameCompanyLogo(id: string, auth: string) {
  const apiUrl = `${new IGDBProvider().apiUrl}/companies`;

  const headers = {
    "Content-Type": "text/plain",
    "Client-ID": process.env.IGDB_PROVIDER_ID!,
    Authorization: `${auth}`,
  };

  const body = `f logo.image_id; where id = (${id});`;

  try {
    const response = await axios.post(apiUrl, body, {headers});
    return response?.data;
  } catch (err) {
    throw utils.handleErrors(err);
  }
}

export async function getNames(items: [{name: string}]) {
  if (items) {
    return items.map((item) => item.name);
  }
  return;
}

export async function getVideoIds(videos: [{video_id: number}]) {
  if (videos) {
    return videos.map((video) => video.video_id);
  }
  return;
}

export async function getInvolvedCompanies(
  companies: {company: {id: number; name: string}; developer: string}[],
  twitchAccessToken: string
) {
  try {
    // Remove duplicates
    const uniqueCompanies = Array.from(
      new Map(
        companies.map((company) => [company.company.id, company])
      ).values()
    );
    const combinedCompanyIds = uniqueCompanies
      .map((company) => company.company.id)
      .join(",");

    const companyDetailsResponses = await getGameCompanyLogo(
      combinedCompanyIds,
      twitchAccessToken
    );

    return Promise.all(
      uniqueCompanies.map(async (involved_company, index) => {
        const companyDetails = companyDetailsResponses[index];

        if (companyDetails.logo && companyDetails.logo.image_id) {
          const imageName = companyDetails.logo.image_id;
          await downloadAndSaveImage(imageName, 80);
        }

        return {
          name: involved_company.company.name,
          role: involved_company.developer ? "developer" : "other",
          logo: `/${companyDetails.logo?.image_id}.jpg`,
        };
      })
    );
  } catch (err) {
    throw utils.handleErrors(err);
  }
}

export async function getDLCs(
  dlcIds: [{id: number}],
  twitchAccessToken: string
) {
  try {
    const combinedDLCsIds = dlcIds.map((dlc) => dlc.id).join(",");

    const dlcDetailsResponse = await getGameDetails(
      combinedDLCsIds,
      twitchAccessToken
    );

    return Promise.all(
      dlcDetailsResponse
        .filter(
          (dlcDetails: {
            first_release_date: number;
            id: number;
            name: string;
            cover: {image_id: string};
          }) => dlcDetails && dlcDetails.first_release_date
        )
        .map(
          async (dlcDetails: {
            first_release_date: number;
            id: number;
            name: string;
            cover: {image_id: string};
          }) => {
            if (dlcDetails && dlcDetails.first_release_date) {
              const imageName = dlcDetails.cover?.image_id ?? undefined;
              if (imageName) {
                await downloadAndSaveImage(imageName, 80);
              }

              return {
                providerId: dlcDetails.id,
                name: dlcDetails.name,
                cover: imageName ? `/${imageName}.jpg` : null,
                releaseDate: new Date(dlcDetails.first_release_date * 1000),
              };
            }
          }
        )
    );
  } catch (err) {
    throw utils.handleErrors(err);
  }
}

export async function getExpansions(
  expansionIds: [{id: number}],
  twitchAccessToken: string
) {
  try {
    const combinedExpansionIds = expansionIds
      .map((expansion) => expansion.id)
      .join(",");

    const expansionDetailsResponse = await getGameDetails(
      combinedExpansionIds,
      twitchAccessToken
    );

    return Promise.all(
      expansionDetailsResponse
        .filter(
          (expansionDetails: {
            first_release_date: number;
            id: number;
            name: string;
            cover: {image_id: string};
          }) => expansionDetails && expansionDetails.first_release_date
        )
        .map(
          async (expansionDetails: {
            first_release_date: number;
            id: number;
            name: string;
            cover: {image_id: string};
          }) => {
            if (expansionDetails && expansionDetails.first_release_date) {
              const imageName = expansionDetails.cover?.image_id ?? undefined;
              if (imageName) {
                await downloadAndSaveImage(imageName, 80);
              }

              return {
                providerId: expansionDetails.id,
                name: expansionDetails.name,
                cover: imageName ? `/${imageName}.jpg` : null,
                releaseDate: new Date(
                  expansionDetails.first_release_date * 1000
                ),
              };
            }
          }
        )
    );
  } catch (err) {
    throw utils.handleErrors(err);
  }
}

export async function downloadAndSaveImage(imageId: string, maxWidth?: number) {
  const imageUrl = `https://images.igdb.com/igdb/image/upload/t_1080p/${imageId}.jpg`;
  const destinationPath = `${utils.__dirname}/../images/games/${imageId}.jpg`;

  if (!fs.existsSync(`${utils.__dirname}/../images/games`)) {
    fs.mkdirSync(`${utils.__dirname}/../images/games`, {recursive: true});
  }

  if (maxWidth !== undefined) {
    await utils.saveImage(imageUrl, destinationPath, maxWidth);
  } else {
    await utils.saveImage(imageUrl, destinationPath);
  }

  const cleanedFilename = imageId?.replace(/%/g, "_");
  return cleanedFilename;
}
