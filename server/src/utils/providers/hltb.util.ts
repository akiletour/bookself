import {HowLongToBeatService, HowLongToBeatEntry} from "howlongtobeat";
import {HLTB} from "../../interfaces/Game.interface.ts";
import * as utils from "../helpers.util.ts";

export async function getHowLongToBeatDatas(
  title: string
): Promise<HLTB | void> {
  const hltbService = new HowLongToBeatService();
  try {
    const hltb: HowLongToBeatEntry[] = await hltbService.search(title);
    if (hltb?.length > 0 && hltb[0].name === title) {
      const game = hltb[0];

      return {
        main: game.gameplayMain ?? null,
        mainExtra: game.gameplayMainExtra ?? null,
        completionist: game.gameplayCompletionist ?? null,
      };
    }
    return;
  } catch (err) {
    throw utils.handleErrors(err);
  }
}
