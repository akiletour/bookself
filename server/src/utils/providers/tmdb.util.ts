import axios from "axios";
import * as utils from "../helpers.util.ts";
import {WorksTypes} from "../../enums/WorksTypes.enum.ts";
import {TMDBProvider} from "../../services/TMDBProvider.service.ts";
import fs from "fs";

export async function getDetails(id: string, type: WorksTypes) {
  const route = type === WorksTypes.Movie ? "movie" : "tv";
  const apiUrl = `${new TMDBProvider().apiUrl}/${route}/${id}?api_key=${
    process.env.TMDB_PROVIDER_KEY
  }`;

  try {
    const response = await axios.get(apiUrl);

    if (response.data) {
      return response.data;
    }
  } catch (err) {
    return false;
  }
}

export async function getSeasonDetails(
  seriesId: string,
  seasonNumber: string,
  type: WorksTypes
) {
  const route = type === WorksTypes.Movie ? "movie" : "tv";
  const apiUrl = `${
    new TMDBProvider().apiUrl
  }/${route}/${seriesId}/season/${seasonNumber}?api_key=${
    process.env.TMDB_PROVIDER_KEY
  }`;

  try {
    const response = await axios.get(apiUrl);

    if (response.data) {
      return response.data;
    }
  } catch (err) {
    return false;
  }
}

export async function getMovieCredits(id: string) {
  const apiUrl = `${new TMDBProvider().apiUrl}/movie/${id}/credits?api_key=${
    process.env.TMDB_PROVIDER_KEY
  }`;

  try {
    const response = await axios.get(apiUrl);

    if (response.data && response.data.cast) {
      const cast = response.data.cast
        .slice(0, 3)
        .map(
          ({
            name,
            character,
            profile_path,
          }: {
            name: string;
            character: string;
            profile_path: string;
          }) => ({name, character, profile: profile_path})
        );

      const directors = response.data.crew
        .filter((member: {job: string}) => member.job === "Director")
        .map(({name, profile_path}: {name: string; profile_path: string}) => ({
          name,
          profile: profile_path,
        }));

      return {cast, directors};
    }
  } catch (err) {
    return false;
  }
}

export async function getSeriesCredits(id: string) {
  const apiUrl = `${new TMDBProvider().apiUrl}/tv/${id}/credits?api_key=${
    process.env.TMDB_PROVIDER_KEY
  }`;

  try {
    const response = await axios.get(apiUrl);

    if (response.data && response.data.cast) {
      const cast = response.data.cast
        .slice(0, 3)
        .map(
          ({
            name,
            character,
            profile_path,
          }: {
            name: string;
            character: string;
            profile_path: string;
          }) => ({name, character, profile: profile_path})
        );

      return {cast};
    }
  } catch (err) {
    return false;
  }
}

export async function downloadAndSaveImage(
  imageId: string,
  path: string,
  maxWidth?: number
) {
  const imageUrl = `https://image.tmdb.org/t/p/original/${imageId}`;
  const destinationPath = `${utils.__dirname}/../images/${path}/${imageId}`;

  if (!fs.existsSync(`${utils.__dirname}/../images/${path}`)) {
    fs.mkdirSync(`${utils.__dirname}/../images/${path}`, {recursive: true});
  }

  if (maxWidth !== undefined) {
    await utils.saveImage(imageUrl, destinationPath, maxWidth);
  } else {
    await utils.saveImage(imageUrl, destinationPath);
  }

  const cleanedFilename = imageId?.replace(/%/g, "_");
  return cleanedFilename;
}
