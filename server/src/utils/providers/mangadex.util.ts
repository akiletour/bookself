import axios from "axios";
import * as utils from "../helpers.util.ts";
import {MangaDexProvider} from "../../services/MangaDexProvider.service.ts";
import {
  Relationship,
  Tag,
} from "../../interfaces/providers/Mangadex.interface.ts";
import {Author, Volume} from "../../interfaces/Manga.interface.ts";
import {BaseError} from "../../types/CustomErrors.type.ts";
import fs from "fs";

export async function getMangaDetails(id: string) {
  const apiUrl = `${
    new MangaDexProvider().apiUrl
  }/manga/${id}?includes[]=author&includes[]=artist&includes[]=cover_art&includes[]=manga`;

  try {
    const response = await axios.get(apiUrl);
    return response?.data;
  } catch (err) {
    throw utils.handleErrors(err);
  }
}

export async function getTagsNames(
  type: string = "genre",
  items: Tag[]
): Promise<string[] | undefined> {
  if (items) {
    return items
      .filter((item) => item.attributes?.group === type)
      .map((item) => item.attributes?.name?.en);
  }
  return;
}

export async function getRelationshipsNames(
  type: string = "author",
  items: Relationship[]
): Promise<Author[] | undefined> {
  if (items) {
    return items
      .filter((item) => item.type === type)
      .map((item) => ({
        name: item.attributes?.name as string,
        profile: item.attributes?.imageUrl as string,
      }));
  }
  return;
}

export async function getRating(id: string) {
  const apiUrl = `${new MangaDexProvider().apiUrl}/statistics/manga/${id}`;

  try {
    const response = await axios.get(apiUrl);
    const {rating} = response.data.statistics[id];
    // We want ratings out of 100
    return rating.bayesian * 10;
  } catch (err) {
    throw utils.handleErrors(err);
  }
}

export async function getVolumes(id: string): Promise<Volume[] | BaseError> {
  const apiUrl = new MangaDexProvider().apiUrl;
  const apiCoversUrl = `${apiUrl}/cover?manga[]=${id}&limit=99`;

  try {
    let mangaCovers: {
      attributes: {
        volume: string;
        fileName: string;
        locale: string;
        version: number;
      };
    }[] = [];
    let offset = 0;
    let totalCovers = 0;

    do {
      const coversResponse = await axios.get(
        `${apiCoversUrl}&offset=${offset}`
      );

      const coversData = coversResponse.data.data;
      mangaCovers = mangaCovers.concat(coversData);

      offset += coversData.length;
      totalCovers = coversResponse.data.total;
    } while (offset < totalCovers);

    const volumes = await Promise.all(
      mangaCovers
        .filter((mangaCover) => mangaCover.attributes.locale === "ja")
        .filter((mangaCover) => {
          const volumeNumber = mangaCover.attributes.volume;
          return volumeNumber && /^\d+$/.test(volumeNumber);
        })
        .map(async (mangaCover) => {
          const volumeNumber = mangaCover.attributes.volume;
          const cover = mangaCover.attributes.fileName;
          const coverImage = cover
            ? await downloadAndSaveImage(`${id}/${cover}`, 251)
            : null;

          return {
            volumeNumber: typeof volumeNumber === "string" ? +volumeNumber : 0,
            cover: `/${coverImage?.replace("/", "-")}`,
          };
        })
    );

    const uniqueVolumes = new Set();
    const uniqueVolumesArray = volumes.filter((volume) => {
      if (!uniqueVolumes.has(volume.volumeNumber)) {
        uniqueVolumes.add(volume.volumeNumber);
        return true;
      }
      return false;
    });

    return uniqueVolumesArray;
  } catch (err) {
    console.error("Error:", err);
    throw utils.handleErrors(err);
  }
}

export async function downloadAndSaveImage(imageId: string, maxWidth?: number) {
  const imageUrl = `https://uploads.mangadex.org/covers/${imageId}.512.jpg`;
  const destinationPath = `${
    utils.__dirname
  }/../images/mangas/${imageId.replace("/", "-")}`;

  if (!fs.existsSync(`${utils.__dirname}/../images/mangas`)) {
    fs.mkdirSync(`${utils.__dirname}/../images/mangas`, {recursive: true});
  }

  if (maxWidth !== undefined) {
    await utils.saveImage(imageUrl, destinationPath, maxWidth);
  } else {
    await utils.saveImage(imageUrl, destinationPath);
  }

  const cleanedFilename = imageId?.replace(/%/g, "_");
  return cleanedFilename;
}
