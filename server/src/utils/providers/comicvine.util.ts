import axios from "axios";
import * as utils from "../helpers.util.ts";
import {ComicVineProvider} from "../../services/ComicVineProvider.service.ts";
import {Volume} from "../../interfaces/Comic.interface.ts";
import {BaseError} from "../../types/CustomErrors.type.ts";
import fs from "fs";
import {Issue} from "../../interfaces/providers/Comicvine.interface.ts";

export async function getComicDetails(id: string) {
  const apiUrl = `${new ComicVineProvider().apiUrl}/volume/${id}?api_key=${
    process.env.COMICVINE_PROVIDER_KEY
  }&format=json`;

  try {
    const response = await axios.get(apiUrl);
    return response?.data;
  } catch (err) {
    throw utils.handleErrors(err);
  }
}

export async function getIssues(
  issues: Issue[],
  volumes?: Volume[]
): Promise<Volume[] | BaseError> {
  const apiUrl = new ComicVineProvider().apiUrl;

  try {
    const uniqueIssues = issues.filter((issue) => {
      // Filter for new issues only
      return (
        !volumes ||
        volumes.every(
          (volume) => volume.volumeNumber !== parseInt(issue.issue_number)
        )
      );
    });

    const newVolumes = await Promise.all(
      uniqueIssues.map(async (issue) => {
        // 4000- is identifier for issues in ComicVine API
        const apiCoversUrl = `${apiUrl}/issue/4000-${issue.id}?api_key=${process.env.COMICVINE_PROVIDER_KEY}&format=json`;

        try {
          const coversResponse = await axios.get(apiCoversUrl);

          const coverImage = coversResponse.data.results.image
            ? await downloadAndSaveImage(
                coversResponse.data.results.image.original_url,
                251
              )
            : null;

          return {
            volumeName: issue.name,
            volumeNumber: parseInt(issue.issue_number),
            cover: coverImage ? `/${coverImage.replace("/", "-")}` : undefined,
          };
        } catch (err) {
          console.error("Error:", err);
          throw utils.handleErrors(err);
        }
      })
    );

    const allVolumes = volumes ? [...volumes, ...newVolumes] : newVolumes;

    return allVolumes;
  } catch (err) {
    console.error("Error:", err);
    return utils.handleErrors(err);
  }
}

export async function downloadAndSaveImage(
  imageUrl: string,
  maxWidth?: number
) {
  const matchResult = imageUrl.match(/\/([^/]+\.jpg)/);
  const filename = matchResult ? matchResult[1] : null;

  const destinationPath = `${utils.__dirname}/../images/comics/${filename}`;

  if (!fs.existsSync(`${utils.__dirname}/../images/comics`)) {
    fs.mkdirSync(`${utils.__dirname}/../images/comics`, {recursive: true});
  }

  if (maxWidth !== undefined) {
    await utils.saveImage(imageUrl, destinationPath, maxWidth);
  } else {
    await utils.saveImage(imageUrl, destinationPath);
  }

  const cleanedFilename = filename?.replace(/%/g, "_");
  return cleanedFilename;
}
