import Express, {NextFunction} from "express";
import path from "path";
import {fileURLToPath} from "url";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import axios from "axios";
import fs from "fs";
import sharp from "sharp";
import {
  BaseError,
  ServerError,
  TooManyRequestsError,
  UnauthorizedError,
} from "../types/CustomErrors.type.ts";
import {
  Provider,
  ProviderConstructor,
} from "../interfaces/Provider.interface.ts";

export const __filename: string = fileURLToPath(import.meta.url);
export const __dirname: string = path.join(path.dirname(__filename), "../");

export const trimRequestBody = (
  req: Express.Request,
  _res: Express.Response,
  next: NextFunction
) => {
  for (const key in req.body) {
    if (typeof req.body[key] === "string") {
      req.body[key] = req.body[key].trim();
    }
  }
  next();
};

export const comparePassword = (password: string, hashedPassword: string) => {
  return bcrypt.compare(password, hashedPassword);
};

export const authenticateToken = (
  req: Express.Request,
  res: Express.Response,
  next: NextFunction
) => {
  const authHeader = req.headers["authorization"];

  if (!authHeader) {
    return res.status(401).json({message: "Unauthorized"});
  }

  const tokens = authHeader.split(",").map((token) => token.trim());

  let accessToken: string | null = null;

  tokens.forEach((token) => {
    if (token.startsWith("Bearer ")) {
      if (!accessToken) {
        accessToken = token.slice(7);
      }
    }
  });

  if (accessToken == null)
    return res.status(401).json({message: "Unauthorized"});

  jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET!, (err) => {
    if (err) {
      return res.status(401).json({message: "Unauthorized"});
    }
    next();
  });
};

export async function saveImage(
  imageUrl: string,
  destinationPath: string,
  maxWidth?: number
) {
  if (process.env.NODE_ENV !== "test") {
    const response = await axios.get(imageUrl, {
      responseType: "arraybuffer",
    });

    const cleanedFilename = destinationPath.replace(/%/g, "_");

    if (maxWidth !== undefined) {
      const resizedBuffer = await sharp(response.data)
        .resize({width: maxWidth, withoutEnlargement: true})
        .toBuffer();

      fs.writeFileSync(cleanedFilename, resizedBuffer);

      const sourcePath = cleanedFilename.replace(
        /(\.[\w\d_-]+)$/i,
        "-source$1"
      );

      fs.writeFileSync(sourcePath, response.data);
    } else {
      fs.writeFileSync(cleanedFilename, response.data);
    }
  }
}

export function findProvider(
  providerId: number,
  providers: ProviderConstructor[]
): Provider | undefined {
  let provider: Provider | undefined;

  for (const Provider of providers) {
    if (Provider.supports(providerId)) {
      provider = new Provider();
      break;
    }
  }

  return provider;
}

export function handleErrors(err: unknown): BaseError {
  if (axios.isAxiosError(err)) {
    if (err.response!.status === 401) {
      return new UnauthorizedError("Authorization failure");
    } else if (err.response!.status === 429) {
      return new TooManyRequestsError("Too many requests");
    }
    return new ServerError("Error during axios call");
  } else if (err instanceof BaseError) {
    return err;
  } else {
    return new ServerError("An unknown error occured");
  }
}
