import * as helpers from "./helpers.util.ts";
import {expect} from "chai";
import sinon from "sinon";
import httpMocks from "node-mocks-http";

const helpersConstsArray = [helpers.__dirname, helpers.__filename];

describe("Helpers", () => {
  describe("Helpers variables", () => {
    helpersConstsArray.forEach((helper) => {
      it(`Check that helper variable is a path`, () => {
        expect(helper).to.be.a("string");
      });
    });
  });

  describe("trimRequestBody", () => {
    it(`Check that body vars are trimed`, () => {
      const req = httpMocks.createRequest({
        body: {
          username: "test ",
        },
      });

      const res = httpMocks.createResponse();
      const next = sinon.spy();

      helpers.trimRequestBody(req, res, next);

      expect(req.body.username).to.equal("test");
    });
  });
});
