// commonSetup.ts
import {Application} from "express";
import {Server} from "http";
import mongoose from "mongoose";

export async function setup(app: Application, PORT: number): Promise<Server> {
  const server: Server = app.listen(PORT);

  await mongoose.connect(
    `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.duciefa.mongodb.net/${process.env.DB_TEST_NAME}?retryWrites=true&w=majority`
  );

  return server;
}

export async function teardown(
  server: Server,
  collection: string[]
): Promise<void> {
  for (const collectionName of collection) {
    await mongoose.connection.db.dropCollection(collectionName);
  }
  await mongoose.connection.close();
  server.close();
}
