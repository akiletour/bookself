export class BaseError extends Error {
  status: number;

  constructor(message: string, status: number) {
    super(message);
    this.status = status;
  }
}

export class NoContentError extends BaseError {
  constructor(message: string) {
    super(message, 204);
  }
}

export class BadRequestError extends BaseError {
  constructor(message: string) {
    super(message, 400);
  }
}

export class UnauthorizedError extends BaseError {
  constructor(message: string) {
    super(message, 401);
  }
}

export class ForbiddenError extends BaseError {
  constructor(message: string) {
    super(message, 403);
  }
}

export class ConflictError extends BaseError {
  constructor(message: string) {
    super(message, 409);
  }
}

export class ActivatedError extends BaseError {
  constructor(message: string) {
    super(message, 410);
  }
}

export class UnprocessableEntityError extends BaseError {
  constructor(message: string) {
    super(message, 422);
  }
}

export class TooManyRequestsError extends BaseError {
  constructor(message: string) {
    super(message, 429);
  }
}

export class InvalidTokenError extends BaseError {
  constructor(message: string) {
    super(message, 498);
  }
}

export class ServerError extends BaseError {
  constructor(message: string) {
    super(message, 500);
  }
}
