import {MovieDocument} from "../interfaces/Movie.interface";
import {SeriesDocument} from "../interfaces/Series.interface";
import {GameDocument} from "../interfaces/Game.interface";
import {WorksTypes} from "../enums/WorksTypes.enum";
import {Pagination} from "../interfaces/Pagination.interface";
import {MangaDocument} from "../interfaces/Manga.interface";
import {ComicDocument} from "../interfaces/Comic.interface";

export type Work = {
  title: string;
  releaseDate: Date;
  additionDate?: Date;
  discoveryDate?: Date;
  cover: string;
  genres?: string[];
  lists?: {
    name: string;
    id?: string;
  }[];
  type: WorksTypes;
  rating: number;
  userRating: number;
};

export type WorksModel =
  | MovieDocument
  | SeriesDocument
  | GameDocument
  | MangaDocument
  | ComicDocument;

export interface WorksModelsPaginated {
  data: WorksModel[];
  pagination: Pagination;
}
